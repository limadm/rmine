#include <Commons/Storage.h>
#include <Graphs/NodeNetTests.h>
#include <Graphs/NodePlot.h>
#include <Graphs/EdgePlot.h>
#include <SuperGraph/GTreeNode.h>

using namespace std;
using namespace rmine::disk;

bool operator== (TNodePlot &a, TNodePlot &b)
{
	return a.GetLabel() == b.GetLabel();
}

template<typename TNet>
bool eq (const TNet& n1, const TNet& n2)
{
	assert(n1.GetNodes() == n2.GetNodes());
	assert(n1.GetEdges() == n2.GetEdges());
	for (TNet::TNodeI it1 = n1.BegNI(), it2 = n2.BegNI(); it1 != n1.EndNI(); it1++, it2++) {
		assert(it1() == it2());
	}
	for (TNet::TEdgeI it1 = n1.BegEI(), it2 = n2.BegEI(); it1 != n1.EndEI(); it1++, it2++) {
		assert(it1() == it2());
	}
	return true;
}

void StorageTest()
{
	// continuation page test
	{
		string s1 = "[ This string has length 0x45, sufficient to overflow storage page. ]";
		{
			OutputStorage file ("test/data/1.dat",64);
			file << s1;
			file << s1;
		}
		{
			InputStorage file ("test/data/1.dat");
			string s2;
			file >> s2;
			assert(s1 == s2);
			file >> s2;
			assert(s1 == s2);
		}
	}
	// recursive child pages test
	{
		string s1 = "[String]";
		{
			OutputStorage file ("test/data/2.dat",64);
			file << s1;
			file.SaveCs();
			{
				stPageID id;
				file.NewPage(id);
				file << s1;
				file.SaveCs();
				file.EndPage();
				file.SaveCs();
			}
			file << s1;
			file.SaveCs();
		}
		{
			stPageID id;
			string s2;
			InputStorage file ("test/data/2.dat");
			file >> s2;
			assert(s1 == s2);
			file.LoadCs();
			{
				file >> id;
				file.NewPage(id);
				file >> s2;
				assert(s1 == s2);
				file.LoadCs();
				file.EndPage();
				file.LoadCs();
			}
			file >> s2;
			assert(s1 == s2);
			file.LoadCs();
		}
	}
	// super recursive/continuation page test
	{
		string s1 = "[ Uma string de 0x46 caracteres para ultrapassar o limite da p�gina. ]";
		{
			stPageID id;
			OutputStorage file ("test/data/3.dat",64);
			file << s1;
			file.SaveCs();
			{
				file.NewPage(id);
				file << s1;
				file.SaveCs();
				{
					file.NewPage(id);
					file << s1;
					file.SaveCs();
					file.EndPage();
					file.SaveCs();
				}
				file.EndPage();
				file.SaveCs();
			}
			file << s1;
		}
		{
			string s2;
			InputStorage file ("test/data/3.dat");
			stPageID id;
			file >> s2;
			assert(s1 == s2);
			file.LoadCs();
			{
				file >> id;
				file.NewPage(id);
				file >> s2;
				assert(s1 == s2);
				file.LoadCs();
				{
					file >> id;
					file.NewPage(id);
					file >> s2;
					assert(s1 == s2);
					file.LoadCs();
					file.EndPage();
					file.LoadCs();
				}
				file.EndPage();
				file.LoadCs();
			}
			file >> s2;
			assert(s1 == s2);
		}
	}
	// vector
	{
		TVec<TInt> v1;
		for (int i = 0xCAFE00; i < 0xCAFE08; ++i) {
			v1.Add(i);
		}
		{
			OutputStorage file ("test/data/4.dat",64);
			v1.Save(file);
			file.SaveCs();

			v1.Save(file);
			file.SaveCs();
		}
		{
			InputStorage file ("test/data/4.dat");
			TVec<TInt> v2 (file);
			assert(v1 == v2);
			file.LoadCs();

			v2.Load(file);
			assert(v1 == v2);
			file.LoadCs();
		}
	}
	// booleans
	{
		bool t = true, f = false;
		{
			OutputStorage file ("test/data/5.dat",64);
			file << t;
			file.SaveCs();

			file << f;
			file.SaveCs();
		}
		{
			InputStorage file ("test/data/5.dat");
			bool b;
			file >> b;
			assert(b == t);
			file.LoadCs();

			file >> b;
			assert(b == f);
			file.LoadCs();
		}
	}
	// NodeNetTests
	{
		typedef TNodeNetTests<TStr,TStr> TNet;

		string s1 = "(^_^)";
		TNet n1;
		for (int i = 1; i <= 4; ++i) {
			n1.AddNode(i, TStr(("(node "+ToString(i)+")").c_str()));
		}
		for (int i = 1; i <= 4; ++i) {
			for (int j = 1; j <= 4; ++j) {
				int eid = n1.AddEdge(i,j);
				n1.GetEI(eid)() = TStr(("(edge "+ToString(i)+" "+ToString(j)+")").c_str());
			}
		}
		{
			OutputStorage file ("test/data/6.dat",64);
			n1.Save(file);
			file.SaveCs();

			file << s1;
			file.SaveCs();

			{
				stPageID id;
				file.NewPage(id);

				n1.Save(file);
				file.SaveCs();

				file << s1;
				file.SaveCs();

				file.EndPage();
				file.SaveCs();
			}

			n1.Save(file);
			file.SaveCs();

			file << s1;
			file.SaveCs();
		}
		{
			InputStorage file ("test/data/6.dat");

			TNet n2(file);
			assert(eq(n1,n2));
			file.LoadCs();

			TStr s2 (file);
			assert(TStr(s1.c_str()) == s2);
			file.LoadCs();

			{
				stPageID id;
				file >> id;
				file.NewPage(id);

				n2 = TNet(file);
				assert(eq(n1,n2));
				file.LoadCs();

				file >> s2;
				assert(TStr(s1.c_str()) == s2);
				file.LoadCs();

				file.EndPage();
				file.LoadCs();
			}

			//n2.Load(file);
			n2 = TNet(file);
			assert(eq(n1,n2));
			file.LoadCs();

			file >> s2;
			assert(s1 == string(s2.CStr()));
			file.LoadCs();
		}
	}
	// NodeNetTests< NodePlot, EdgePlot >
	{
		typedef TNodeNetTests<TNodePlot,TEdgePlot> TNet;

		string s1 = "(^_^)";
		TNet n1;
		for (int i = 1; i <= 4; ++i) {
			int nid = n1.AddNode(i, TNodePlot());
			n1.GetNI(nid)().SetLabel("(node "+ToString(i)+")");
		}
		for (int i = 1; i <= 4; ++i) {
			for (int j = 1; j <= 4; ++j) {
				int eid = n1.AddEdge(i,j);
				n1.GetEI(eid)().SetWeight(i+j);
			}
		}
		{
			OutputStorage file ("test/data/7.dat",64);
			n1.Save(file);
			file.SaveCs();

			file << s1;
			file.SaveCs();

			{
				stPageID id;
				file.NewPage(id);

				n1.Save(file);
				file.SaveCs();

				file << s1;
				file.SaveCs();

				file.EndPage();
				file.SaveCs();
			}

			n1.Save(file);
			file.SaveCs();

			file << s1;
			file.SaveCs();
		}
		{
			InputStorage file ("test/data/7.dat");

			TNet n2(file);
			assert(eq(n1,n2));
			file.LoadCs();

			TStr s2 (file);
			assert(TStr(s1.c_str()) == s2);
			file.LoadCs();

			{
				stPageID id;
				file >> id;
				file.NewPage(id);

				n2 = TNet(file);
				assert(eq(n1,n2));
				file.LoadCs();

				file >> s2;
				assert(TStr(s1.c_str()) == s2);
				file.LoadCs();

				file.EndPage();
				file.LoadCs();
			}

			n2 = TNet(file);
			assert(eq(n1,n2));
			file.LoadCs();

			file >> s2;
			assert(s1 == string(s2.CStr()));
			file.LoadCs();
		}
	}
	// TGTreeSuperEdge
	{
		typedef TGTreeSuperEdge TEdge;
		TGTreeSuperNodeEntry sn1 ("(snode 1)",NULL),
		                     sn2 ("(snode 2)",NULL);
		string s1 = "(^_^)";
		TEdge e1 (&sn1, &sn2);
		{
			OutputStorage file ("test/data/8.dat",64);
			e1.Save(file);
			file.SaveCs();

			file << s1;
			file.SaveCs();

			{
				stPageID id;
				file.NewPage(id);

				e1.Save(file);
				file.SaveCs();

				file << s1;
				file.SaveCs();

				file.EndPage();
				file.SaveCs();
			}

			e1.Save(file);
			file.SaveCs();

			file << s1;
			file.SaveCs();
		}
		{
			InputStorage file ("test/data/8.dat");

			TEdge e2 (&sn1, &sn2, file);
			assert(eq(e1,e2));
			file.LoadCs();

			TStr s2 (file);
			assert(TStr(s1.c_str()) == s2);
			file.LoadCs();

			{
				stPageID id;
				file >> id;
				file.NewPage(id);

				e2 = TEdge(&sn1, &sn2, file);
				assert(eq(e1,e2));
				file.LoadCs();

				file >> s2;
				assert(TStr(s1.c_str()) == s2);
				file.LoadCs();

				file.EndPage();
				file.LoadCs();
			}

			e2 = TEdge(&sn1, &sn2, file);
			assert(eq(e1,e2));
			file.LoadCs();

			file >> s2;
			assert(s1 == string(s2.CStr()));
			file.LoadCs();
		}
	}
	// out of order loading
	{
		string s1 = "[ This string has length 0x45, sufficient to overflow storage page. ]";
		{
			OutputStorage file ("test/data/9.dat",64);
			stPageID id;
			file << s1;
			file.SaveCs();

			file.NewPage(id);
			{
				file << s1;
				file.SaveCs();
			}
			file.EndPage();
			file.SaveCs();

			file.NewPage(id);
			{
				file << s1;
				file.SaveCs();
			}
			file.EndPage();
			file.SaveCs();

			file << s1;
			file.SaveCs();
		}
		{
			stPageID id1,id2;
			string s2;
			InputStorage file ("test/data/9.dat");
			file >> s2;
			assert(s1 == s2);
			file.LoadCs();

			file >> id1;
			file.LoadCs();

			file >> id2;
			file.LoadCs();

			file.NewPage(id2);
			{
				file >> s2;
				assert(s1 == s2);
				file.LoadCs();
			}
			file.EndPage();

			file.NewPage(id1);
			{
				file >> s2;
				assert(s1 == s2);
				file.LoadCs();
			}
			file.EndPage();

			file >> s2;
			assert(s1 == s2);
			file.LoadCs();
		}
	}
}
