all: debug

debug:
	python make.py gcc debug

release:
	python make.py gcc release

clean:
	python make.py clean

.PHONY: clean
