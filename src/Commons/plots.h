/////////////////////////////////////////////////
// Graph Plots
namespace TGPlot {

void PlotAll(const PNGraph& Graph, const TStr& FNmPref, const TStr& DescStr,
  const bool& PlotDD=true, const bool& PlotCDD=true, const bool& PlotHop=true, 
  const bool& PlotWcc=true, const bool& PlotScc=true, const bool& PlotSVal=true, 
  const bool& PlotSVec=true, const int& SizeX=1000, const int& SizeY=800);
template <class PGraph> 
void PlotAll(const PGraph& Graph, const TStr& FNmPref, const TStr& DescStr,
  const bool& PlotDD=true, const bool& PlotCDD=true, const bool& PlotHop=true, 
  const bool& PlotWcc=true, const bool& PlotScc=true,const int& SizeX=1000, const int& SizeY=800);
template <class PGraph> void PlotDegDist(const PGraph& Graph, const TStr& FNmPref, const TStr& DescStr,
  const bool& PlotInDeg=true, const bool& PlotOutDeg=true, const bool& PowerFit=false, const bool& SavePng=true,
  const bool& SmoothIn=false, const bool& SmoothOut=false, const int& SizeX=1000, const int& SizeY=800);
template <class PGraph> void PlotCumDegDist(const PGraph& Graph, const TStr& FNmPref, const TStr& DescStr,
  const bool& PlotInDeg=true, const bool& PlotOutDeg=true, const bool& PowerFit=false, 
  const bool& SavePng=true, const int& SizeX=1000, const int& SizeY=800);
template <class PGraph> void PlotHops(const PGraph& Graph, const bool& IsDir, const TStr& FNmPref, const TStr& DescStr,
                                      const bool& SavePng=true, const int& SizeX=1000, const int& SizeY=800);
template <class PGraph> void PlotWccDist(const PGraph& Graph, const TStr& FNmPref, const TStr& DescStr,
                                         const bool& SavePng=true, const int& SizeX=1000, const int& SizeY=800);
template <class PGraph> void PlotSccDist(const PGraph& Graph, const TStr& FNmPref, const TStr& DescStr,
                                         const bool& SavePng=true, const int& SizeX=1000, const int& SizeY=800);
void PlotSngVals(const PNGraph& Graph, const int& SngVals, const TStr& FNmPref, const TStr& DescStr,
                 const bool& SavePng=true, const int& SizeX=1000, const int& SizeY=800);
void PlotSngVec(const PNGraph& Graph, const TStr& FNmPref, const TStr& DescStr,
                const bool& SavePng=true, const int& SizeX=1000, const int& SizeY=800);

/////////////////////////////////////////////////
// Plots
template <class PGraph>
void PlotAll(const PGraph& Graph, const TStr& FNmPref, const TStr& DescStr,
 const bool& PlotDD, const bool& PlotCDD, const bool& PlotHop, const bool& PlotWcc,
 const bool& PlotScc, const int& SizeX, const int& SizeY) {
  if (PlotDD) { 
    printf("Plot Degree distibution...\n");
    PlotDegDist(Graph, FNmPref, DescStr, true, true, true, true, false, false);
    PlotDegDist(Graph, FNmPref, DescStr, true, true, true, true, true, true); // smoothed
  }
  if (PlotCDD) {
    printf("Plot Cummulative Degree distibution...\n");
    PlotCumDegDist(Graph, FNmPref, DescStr); }
  if (PlotHop) {
    printf("Plot Hops...\n");
    PlotHops(Graph, false, FNmPref, DescStr); }
  if (PlotWcc) {
    printf("Plot CC distribution...\n");
    PlotWccDist(Graph, FNmPref, DescStr); }
  if (PlotScc) {
    printf("Plot SCC distibution...\n");
    PlotSccDist(Graph, FNmPref, DescStr); }
}

template <class PGraph>
void PlotDegDist(const PGraph& Graph, const TStr& FNmPref, const TStr& DescStr,
 const bool& PlotInDeg, const bool& PlotOutDeg, const bool& PowerFit, const bool& SavePng,
 const bool& SmoothIn, const bool& SmoothOut, const int& SizeX, const int& SizeY) {
  TFltPrV InDegToCntV, OutDegToCntV; // number of nodes with degree D
  int plotId;
  if (PlotInDeg) {
    TSnap::GetInDegCnt(Graph, InDegToCntV); }
  if (PlotOutDeg) {
    TSnap::GetOutDegCnt(Graph, OutDegToCntV); }
  const TStr Pref = (SmoothIn || SmoothOut) ? "dds." : "dd.";
  TGnuPlot GnuPlot(Pref+FNmPref+".tab", Pref+FNmPref+".plt",
	TStr::Fmt("%s. Degree. G(%d, %d)", DescStr.CStr(), Graph->GetNodes(), Graph->GetEdges()), true);
  GnuPlot.SetXYLabel("Degree", "Count (number of nodes with degree D)");
  GnuPlot.SetScale(gpsLog10XY);
  if (PlotInDeg) {
    if (! SmoothIn) 
      plotId = GnuPlot.AddPlot(InDegToCntV, gpwPoints, "IN Degree");
    else {
      TFltPrV DegV, SmoothV;
      for (int i = 0; i < InDegToCntV.Len(); i++) {
        DegV.Add(InDegToCntV[i]);
	    }
      TGnuPlot::MakeExpBins(DegV, SmoothV, 1.5, 1);
      plotId = GnuPlot.AddPlot(SmoothV, gpwLinesPoints, "smooth IN Degree");
    }
    if (PowerFit) GnuPlot.AddPwrFit(plotId, gpwLines);
  }
  if (PlotOutDeg) {
    if (! SmoothOut) 
      plotId = GnuPlot.AddPlot(OutDegToCntV, gpwPoints, "OUT Degree");
    else {
      TFltPrV DegV, SmoothV;
      for (int i = 0; i < OutDegToCntV.Len(); i++) {
        DegV.Add(OutDegToCntV[i]);
      }
      TGnuPlot::MakeExpBins(DegV, SmoothV, 1.5, 1);
      plotId = GnuPlot.AddPlot(SmoothV, gpwLinesPoints, "smooth OUT Degree");
    }
    if (PowerFit) GnuPlot.AddPwrFit(plotId, gpwLines);
  }
  GnuPlot.AddCmd("set key right top Right");
  if (! SavePng) GnuPlot.Plot();
  else { GnuPlot.Pause(false);  GnuPlot.SavePng(Pref+FNmPref+".png", SizeX, SizeY); }
}

template <class PGraph>
void PlotCumDegDist(const PGraph& Graph, const TStr& FNmPref, const TStr& DescStr,
 const bool& PlotInDeg, const bool& PlotOutDeg, const bool& PowerFit, const bool& SavePng,
 const int& SizeX, const int& SizeY) {
  TIntPrV DegToCntV;
  TIntKdV InDegToCumCntV, OutDegToCumCntV; // number of nodes with degree at least D

  if (PlotInDeg) {
    TSnap::GetInDegCnt(Graph, DegToCntV);
    InDegToCumCntV.Gen(DegToCntV.Len());
    TInt key, dat;
    DegToCntV.Last().GetVal(key, dat);
    InDegToCumCntV.Last().Key = key; // highest degree node
    InDegToCumCntV.Last().Dat = dat; // -------- " --------
    for (int d = DegToCntV.Len()-2; d >= 0; d--) {
      TInt key, dat;
      DegToCntV[d].GetVal(key, dat);
      InDegToCumCntV[d].Key = key; // degree
      InDegToCumCntV[d].Dat = InDegToCumCntV[d+1].Dat + dat; // count
    }
  }
  if (PlotOutDeg) {
    TSnap::GetOutDegCnt(Graph, DegToCntV);
    OutDegToCumCntV.Gen(DegToCntV.Len());
    TInt key, dat;
    DegToCntV.Last().GetVal(key, dat);
    OutDegToCumCntV.Last().Key = key; // highest degree node
    OutDegToCumCntV.Last().Dat = dat; // -------- " --------
    for (int d = DegToCntV.Len()-2; d >= 0; d--) {
      DegToCntV[d].GetVal(key, dat);
      OutDegToCumCntV[d].Key = key; // degree
      OutDegToCumCntV[d].Dat = OutDegToCumCntV[d+1].Dat + dat; // count
    }
  }
  TGnuPlot GnuPlot("cDD."+FNmPref+".tab", "cDD."+FNmPref+".plt",
    TStr::Fmt("%s. Commulative Degree. G(%d, %d)", DescStr.CStr(), Graph->GetNodes(), Graph->GetEdges()), true);
  GnuPlot.SetXYLabel("Degree", "Count (number of nodes with degree D or more)");
  GnuPlot.SetScale(gpsLog10XY);
  if (PlotInDeg) {
    const int plotId1 = GnuPlot.AddPlot(InDegToCumCntV, gpwPoints, "IN Degree");
    if (PowerFit) GnuPlot.AddPwrFit(plotId1, gpwLines); }
  if (PlotOutDeg) {
    const int plotId2 = GnuPlot.AddPlot(OutDegToCumCntV, gpwPoints, "OUT Degree");
    if (PowerFit) GnuPlot.AddPwrFit(plotId2, gpwLines);
  }
  GnuPlot.AddCmd("set key right top Right");
  if (! SavePng) GnuPlot.Plot();
  else { GnuPlot.Pause(false);  GnuPlot.SavePng("cDD."+FNmPref+".png", SizeX, SizeY); }
}

template <class PGraph>
void PlotHops(const PGraph& Graph, const bool& IsDir, const TStr& FNmPref, 
              const TStr& DescStr, const bool& SavePng, const int& SizeX, const int& SizeY) {
  TIntFltKdV DistNbhsV;
  TGraphAnf<PGraph>(Graph).GetGraphAnf(DistNbhsV, -1, IsDir);
  TGnuPlot GnuPlot("hop."+FNmPref+".tab", "hop."+FNmPref+".plt",
    TStr::Fmt("%s. Hop plot. G(%d, %d)", DescStr.CStr(), Graph->GetNodes(), Graph->GetEdges()), true);
  GnuPlot.SetXYLabel("Distance", "Number of pairs of nodes");
  GnuPlot.SetScale(gpsLog10Y);
  GnuPlot.AddPlot(DistNbhsV, gpwLinesPoints, "");
  if (! SavePng) GnuPlot.Plot();
  else { GnuPlot.Pause(false);  GnuPlot.SavePng("hop."+FNmPref+".png", SizeX, SizeY); }
}

template <class PGraph>
void PlotWccDist(const PGraph& Graph, const TStr& FNmPref, const TStr& DescStr,
                 const bool& SavePng, const int& SizeX, const int& SizeY) {
  TIntPrV WccSzCnt;
  TSnap::GetWccSzCnt(Graph, WccSzCnt);
  TGnuPlot GnuPlot("wcc."+FNmPref+".tab", "wcc."+FNmPref+".plt",
    TStr::Fmt("%s. WCC Distribution. G(%d, %d)", DescStr.CStr(), Graph->GetNodes(), Graph->GetEdges()), true);
  GnuPlot.SetXYLabel("Size of weakly connected component", "Number of components");
  GnuPlot.SetScale(gpsLog10XY);
  GnuPlot.AddPlot(WccSzCnt, gpwLinesPoints, "");
  if (! SavePng) GnuPlot.Plot();
  else { GnuPlot.Pause(false);  GnuPlot.SavePng("wcc."+FNmPref+".png", SizeX, SizeY); }
}

template <class PGraph>
void PlotSccDist(const PGraph& Graph, const TStr& FNmPref, const TStr& DescStr, 
                 const bool& SavePng, const int& SizeX, const int& SizeY) {
  TIntPrV SccSzCnt;
  TSnap::GetSccSzCnt(Graph, SccSzCnt);
  TGnuPlot GnuPlot("scc."+FNmPref+".tab", "scc."+FNmPref+".plt",
    TStr::Fmt("%s. SCC Distribution. G(%d, %d)", DescStr.CStr(), Graph->GetNodes(), Graph->GetEdges()), true);
  GnuPlot.SetXYLabel("Size of strongy connected component", "Number of components");
  GnuPlot.SetScale(gpsLog10XY);
  GnuPlot.AddPlot(SccSzCnt, gpwLinesPoints, "");
  if (! SavePng) GnuPlot.Plot();
  else { GnuPlot.Pause(false);  GnuPlot.SavePng("scc."+FNmPref+".png", SizeX, SizeY); }
}

}; // namespace TPlot
