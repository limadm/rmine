#include <wx/wx.h>

namespace rmine
{
	namespace wx
	{
		void Initialize();
		wxCommandEvent & GetEvent(wxEventType type);
		extern wxPen * blackPen;
		extern wxPen * greyPen;
		extern wxPen * mediumGreyPen;
		extern wxPen * lightGreyPen;
		extern wxPen * greenPen;

	}
}

#define wxCommandEvent(wxEVT_NULL) rmine::wx::GetEvent(wxEVT_NULL)

#undef wxBLACK_PEN
#undef wxGREY_PEN
#undef wxMEDIUM_GREY_PEN
#undef wxLIGHT_GREY_PEN
#undef wxGREEN_PEN

#define wxBLACK_PEN       rmine::wx::blackPen
#define wxGREY_PEN        rmine::wx::greyPen
#define wxMEDIUM_GREY_PEN rmine::wx::mediumGreyPen
#define wxLIGHT_GREY_PEN  rmine::wx::lightGreyPen
#define wxGREEN_PEN    	  rmine::wx::greenPen
