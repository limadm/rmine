#pragma once
// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

// Interface pattern-faking macros
#define DeclareInterface(name) class name
#define EndInterface };
#define implements public

// hash_map and hash_set namespace aliasing to std::
#include <hash_map>
#include <hash_set>
// Microsoft VC 2005 moved hash_map and hash_set from std:: to stdext::
#ifdef _MSC_VER
	namespace std {
		using stdext::hash_map;
		using stdext::hash_set;
	}
// GNU GCC put hash_map and hash_set in std::tr1:: as unordered_map and unordered_set
#else
	#include <tr1/unordered_map>
	#include <tr1/unordered_set>
	#define hash_map tr1::unordered_map
	#define hash_set tr1::unordered_set
#endif

#include <iostream>

#include <snap-core/Snap.h>
// max and min macros declared in Snap conflict with std:: ones
#undef max
#undef min

#include "plots.h"

#include <wx/wx.h>
#ifdef __WXGTK__
	#define wxMDIParentFrame wxFrame
	#define wxMDIChildFrame  wxFrame
#else
	#define MDIFRAMES
#endif
#include "wxUtils.h"

#include <DebugWindow.h>