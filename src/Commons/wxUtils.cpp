#include <wx/wx.h>

namespace font {
	wxFont sNodes (14, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD,   false, "Arial", wxFONTENCODING_ISO8859_1);
	wxFont sEdges (10, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL, false, "Arial", wxFONTENCODING_ISO8859_1);
}

namespace rmine
{
	namespace wx
	{
		wxCommandEvent * evt = NULL;
		wxPen * blackPen = NULL;
		wxPen * greyPen = NULL;
		wxPen * mediumGreyPen = NULL;
		wxPen * lightGreyPen = NULL;
		wxPen * greenPen = NULL;

		void Initialize()
		{
			blackPen = new wxPen(*wxBLACK_PEN);
			greyPen = new wxPen(*wxGREY_PEN);
			mediumGreyPen = new wxPen(*wxMEDIUM_GREY_PEN);
			lightGreyPen = new wxPen(*wxLIGHT_GREY_PEN);
			greenPen = new wxPen(*wxGREEN_PEN);
		}

		wxCommandEvent& GetEvent(wxEventType type)
		{
			if (evt) {
				delete evt;
			}
			evt = new wxCommandEvent(type);
			return *evt;
		}
	}
}
