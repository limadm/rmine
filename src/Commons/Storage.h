#pragma once

#ifndef _RMINE_STORAGE_
#define _RMINE_STORAGE_

#include <list>
#include <stack>
#include <vector>
#include <wx/defs.h>
#include <snap-core/Snap.h>
#include <arboretum/stDiskPageManager.h>

namespace rmine {
namespace disk {

	/**
	Don't use this class directly. Use InputStorage and OutputStorage instead.
	*/
	class StorageBase {

#pragma pack(1)
		struct Header {
			stPageID next;
			stSize   end;
		};
#pragma pack()

		struct PagePtr {
			stPageID pageId;
			stSize   offset;

			PagePtr(stPageID pageId, stSize offset)
			: pageId(pageId),
			  offset(offset) {}
		};

		stPageManager * manager;
		stPage * page;
		stSize offset;
		std::stack<PagePtr> stack;

		Header& header();

		void loadPage(stPageID id);

	protected:
		StorageBase(const char * file, bool newFile, uint pageSize);
		virtual ~StorageBase();

		void write(const void* buffer, stSize length);
		void read(void* buffer, stSize length);
		char peek();
		void flush();
		
		stPageID fork();
		void seek(stPageID);
		void back();
	};

	class InputStorage : public TSIn, public StorageBase {
	private:
		std::stack<int> sums;
	public:
		InputStorage(const char * file);
		~InputStorage();

		bool Eof()       { throw std::exception("unsupported"); }
		int  Len() const { throw std::exception("unsupported"); }
		void Reset()     { throw std::exception("unsupported"); }

		char GetCh();
		char PeekCh();
		int GetBf(const void* buffer, const TSize& length);
		
		template<typename T>
		InputStorage& operator >> (T &o) {
			Load(o);
			return *this;
		}
		InputStorage& operator >> (TStr &o);
		InputStorage& operator >> (std::string &o);

		void NewPage(const stPageID &);
		void EndPage();
	};

	class OutputStorage : public TSOut, public StorageBase {
	private:
		std::stack<int> sums;
	public:
		OutputStorage(const char * file, uint pageSize=8172);
		~OutputStorage();
	
		int PutCh(const char& c);
		int PutBf(const void* buffer, const TSize& length);
		void Flush();
		
		template<typename T>
		OutputStorage& operator << (const T &o) {
			Save(o);
			return *this;
		}
		OutputStorage& operator << (const TStr &o);
		OutputStorage& operator << (const std::string &o);
		
		void NewPage(stPageID &);
		void EndPage();
	};
	
}}

#endif