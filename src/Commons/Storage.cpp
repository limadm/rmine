#include "Storage.h"
#include <Commons/Auxiliar.h>

namespace rmine
{
namespace disk
{

StorageBase::Header& StorageBase::header()
{
	return *(Header*)page->GetData();
}

void StorageBase::loadPage(stPageID id)
{
	assert(id != 0);
	page = manager->GetPage(id);
	offset = sizeof(Header);
}

StorageBase::StorageBase(const char * file, bool newFile, uint pageSize)
	:
	manager(NULL),
	page(NULL),
	offset(0)
{
	if (newFile) {
		manager = new stDiskPageManager(file, pageSize);
	} else {
		if (!FileExists(file)) {
			throw std::invalid_argument("file not exists");
		}
		manager = new stDiskPageManager(file);
	}
	const char version_string[] = "GraphTree.v01";
	const size_t version_length = strlen(version_string);
	stPage* headerPage = manager->GetHeaderPage();
	stPageID id;
	if (newFile) {
		page   = manager->GetNewPage();
		offset = sizeof(Header);
		id     = page->GetPageID();
		page->Clear();
		headerPage->Clear();
		headerPage->Write((stByte*)version_string, version_length, 0);
		headerPage->Write((stByte*)&id, sizeof(stPageID), version_length);
		manager->WriteHeaderPage(headerPage);
	} else {
		id = *(stPageID*)(headerPage->GetData()+version_length);
		loadPage(id);
	}
	delete headerPage;
}

StorageBase::~StorageBase()
{
	manager->ReleasePage(page);
	delete manager;
}

void StorageBase::write(const void* buffer, stSize length)
{
	stSize remaining = length;
	while (remaining > 0) {
		stSize available = page->GetPageSize() - offset;
		stByte* src = ((stByte*)buffer)+(length-remaining);
		if (available < remaining) {
			page->Write(src, available, offset);
			remaining -= available;

			// continuation page
			header().end = page->GetPageSize();
			stPageID prevID = page->GetPageID();
			manager->WritePage(page);
			manager->ReleasePage(page);

			stPage*  newpage = manager->GetNewPage();
			stPageID nextID  = newpage->GetPageID();
			newpage->Clear();
			manager->WritePage(newpage);
			manager->ReleasePage(newpage);

			loadPage(prevID);
			header().next = nextID;
			manager->WritePage(page);
			manager->ReleasePage(page);

			loadPage(nextID);
		} else {
			page->Write(src, remaining, offset);
			offset += remaining;
			header().end = offset;
			return;
		}
	}
}

void StorageBase::read(void* buffer, stSize length)
{
	stSize remaining = length;
	while (remaining > 0) {
		stSize available = header().end - offset;
		stByte* dest = ((stByte*)buffer)+(length-remaining);
		if (available < remaining) {
			memcpy(dest, page->GetData()+offset, available);
			remaining -= available;
			manager->ReleasePage(page);
			loadPage(header().next);
		} else {
			memcpy(dest, page->GetData()+offset, remaining);
			offset += remaining;
			return;
		}
	}
}

char StorageBase::peek()
{
	return *(char*)(page->GetData()+offset);
}

void StorageBase::flush()
{
	manager->WritePage(page);
}

stPageID StorageBase::fork()
{
	stPageID _id     = page->GetPageID();
	stSize   _offset = offset;
	manager->WritePage(page);
	manager->ReleasePage(page);

	stPage*  newpage = manager->GetNewPage();
	stPageID newID   = newpage->GetPageID();
	newpage->Clear();
	manager->WritePage(newpage);
	manager->ReleasePage(newpage);

	loadPage(_id);
	offset = _offset;
	return newID;
}

void StorageBase::back()
{
	manager->WritePage(page);
	manager->ReleasePage(page);
	loadPage(stack.top().pageId);
	offset = stack.top().offset;
	stack.pop();
}

void StorageBase::seek(stPageID id)
{
	stack.push((PagePtr(page->GetPageID(),offset)));
	manager->WritePage(page);
	manager->ReleasePage(page);
	loadPage(id);
}

InputStorage::InputStorage(const char * file)
	: TSBase(file),
	  StorageBase(file, false, 0),
	  sums()
{
	assert(Cs.Get() == 0);
}
InputStorage::~InputStorage() {}

char InputStorage::GetCh()
{
	char c;
	read(&c, 1);
	return c;
}

char InputStorage::PeekCh()
{
	return peek();
}

int InputStorage::GetBf(const void* buffer, const TSize& length)
{
	read((void*)buffer, length);
	return TCs::GetCsFromBf((char*)buffer, length).Get();
}

InputStorage& InputStorage::operator >> (TStr& o)
{
	o = TStr(*this);
	return *this;
}

InputStorage& InputStorage::operator >> (std::string& o)
{
	o = TStr(*this).CStr();
	return *this;
}

void InputStorage::NewPage(const stPageID &id)
{
	sums.push(Cs.Get());
	Cs = 0;
	seek(id);
}

void InputStorage::EndPage()
{
	Cs = sums.top();
	sums.pop();
	back();
}

OutputStorage::OutputStorage(const char * file, uint pageSize)
	:
	TSBase(file),
	StorageBase(file, true, pageSize),
	sums()
{
	assert(Cs.Get() == 0);
}

OutputStorage::~OutputStorage() {
	Flush();
}

int OutputStorage::PutCh(const char& c)
{
	write(&c, 1);
	return c;
}

int OutputStorage::PutBf(const void* buffer, const TSize& length)
{
	write(buffer, length);
	return TCs::GetCsFromBf((char*)buffer, length).Get();
}

void OutputStorage::Flush()
{
	flush();
}

OutputStorage& OutputStorage::operator << (const TStr& o)
{
	o.Save(*this);
	return *this;
}

OutputStorage& OutputStorage::operator << (const std::string& o)
{
	TStr(o.c_str()).Save(*this);
	return *this;
}

void OutputStorage::NewPage(stPageID &id)
{
	id = fork();
	Save(id);

	sums.push(Cs.Get());
	Cs = 0;
	seek(id);
}

void OutputStorage::EndPage()
{
	Cs = sums.top();
	sums.pop();
	back();
}

}
}