#pragma once
#ifndef _Auxiliar_
#define _Auxiliar_

#include "stdafx.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <typeinfo>
#include <stdexcept>
#include <list>
#include <set>
#include <hash_set>
#include <hash_map>
#include <functional>

#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif
#include <wx/dcbuffer.h>
#include <wx/graphics.h>
#include <wx/dcgraph.h>

#ifdef _WINDOWS_
#define KMETIS_EXE "tools\\kmetis.exe"
#define PMETIS_EXE "tools\\pmetis.exe"
#else
#define KMETIS_EXE "kmetis"
#define PMETIS_EXE "pmetis"
#endif

#define PI 3.141592654
#define _2PI 6.283185308
#define ABS(a) ((a >= 0) ? a : -a)
#define ROUND(a) ((int)(a+0.5))

#define IMAGE_HORIZONTAL_RESOLUTION 400
#define IMAGE_VERTICAL_RESOLUTION 400

typedef unsigned char byte;	// 0 - 255

// Log cpu usage to the "cputime.log" file, at ctor/dtor
class CpuTimeLog
{
private:
	std::size_t time;
public:
	CpuTimeLog(const char* label);
	~CpuTimeLog();
};

class TRectangle
{
public:
	float dXLeft;
	float fYBotton;
	float dXRight;
	float fYTop;
};
/*------------------------------------------------------------*/
/*Auxiliary class for unresolved edges lists*/
class TEdgeTemp
{
private:
	int iSource;
	int iDestination;
	int iWeight;
public:
	bool bResolved;
public:
	TEdgeTemp(int iASource, int iADestination, int iAWeight = 0);
	bool IsIt(int iASource, int iADestination);
	int GetSource() {
		return iSource;
	};
	int GetDestination() {
		return iDestination;
	};
	int GetWeight() {
		return iWeight;
	}
};
TEdgeTemp* FindEdgeEntry(std::hash_map< std::string, TEdgeTemp*> *lListOfEdges, int iId1, int iId2);
void DeleteResolvedEdges(std::hash_map< std::string, TEdgeTemp*> *lListOfEdges);
/*------------------------------------------------------------*/
class TNodesAuxiliar
{
private:
#ifndef PERFORMANCE
	std::string sLabel;
#endif
	void* gtsneSuperNode;
public:
#ifndef PERFORMANCE
	TNodesAuxiliar(std::string sALabel, void* gtsneASuperNode) {
		sLabel = sALabel;
		gtsneSuperNode = gtsneASuperNode;
	}
#else
	TNodesAuxiliar(void* gtsneASuperNode) {
		gtsneSuperNode = gtsneASuperNode;
	}
#endif
	TNodesAuxiliar& operator = (TNodesAuxiliar& NodesAuxiliar) {
#ifndef PERFORMANCE
		sLabel = NodesAuxiliar.GetLabel();
#endif
		gtsneSuperNode = NodesAuxiliar.GetSuperNodeEntry();
	}
#ifndef PERFORMANCE
	std::string GetLabel() {
		return sLabel;
	}
#endif
	void* GetSuperNodeEntry() {
		return gtsneSuperNode;
	}
};
/*------------------------------------------------------------*/
template<class _Ty>
struct sortByStringField : public std::greater<_Ty> {
	bool operator()(const _Ty& _Left, const _Ty& _Right) const {
		// apply operator> to operands
		return (std::string(_Left.first) < std::string(_Right.first));
	}
};

class TShapeType
{
public:
	typedef enum {
	    saSquare, saCircle, saDiamond
	} TShapeAcronysm;

	static TShapeAcronysm GetShapeAcronym(std::string sAShape) {
		if(sAShape.compare("s") == 0) {
			return saSquare;
		} else if(sAShape.compare("c") == 0) {
			return saCircle;
		} else if(sAShape.compare("d") == 0) {
			return saDiamond;
		}
		return saSquare;
	}
	static std::string GetShapeString(TShapeAcronysm caAShape) {
		if(caAShape == saSquare) {
			return "s";
		} else if(caAShape == saCircle) {
			return "c";
		} else if(caAShape == saDiamond) {
			return "d";
		}
		return "s";
	}
	static void GetShapeNames(wxArrayString& wxsShapeTypes) {
		wxsShapeTypes.Add("Square");
		wxsShapeTypes.Add("Circle");
		wxsShapeTypes.Add("Diamond");
	};
	TStr GetTStrVersion(TShapeAcronysm caAShape) {
		if(caAShape == saSquare) {
			return TStr("s");
		} else if(caAShape == saCircle) {
			return TStr("c");
		} else if(caAShape == saDiamond) {
			return TStr("d");
		}
		return TStr("s");
	}
};

class BadConversion : public std::runtime_error
{
public:
	BadConversion(const std::string& s)
		: std::runtime_error(s)
	{ }
};


template<typename T>
class TUtil
{
	/* Usage example:
	int iMin = TUtil<double>::GetMin(3, 5);
	int iMax = TUtil<double>::GetMax(3, 5);
	*/
public:
	static T GetMin(T tOneNumber, T tAnotherNumber) {
		if(tOneNumber < tAnotherNumber) {
			return tOneNumber;
		}
		return tAnotherNumber;
	}
	static T GetMax(T tOneNumber, T tAnotherNumber) {
		if(tOneNumber > tAnotherNumber) {
			return tOneNumber;
		}
		return tAnotherNumber;
	}

};

template<typename T>
class TConverter
{
	/* Usage example:
		std::string sAString = TConverter<float>::ToString(1.2345f);
		std::string sAString = TConverter<int>::ToString(3242);
		double dADouble = TConverter<double>::FromString("1.2345");
	*/

private:
	static void Convert(const std::string& s, T& x, bool failIfLeftoverChars = true) {
		std::istringstream i(s);
		char c;
		if (!(i >> x) || (failIfLeftoverChars && i.get(c))) {
			throw BadConversion(s);
		}
	}
public:
	static std::string ToString(const T& x) {
		std::ostringstream o;
		if (!(o << x)) {
			throw BadConversion(std::string("stringify(") + typeid(x).name() + ")");
		}
		return o.str();
	}

	static T FromString(const std::string& s, bool failIfLeftoverChars = true) {
		T x;
		Convert(s, x, failIfLeftoverChars);
		return x;
	}
};

template<typename T>
std::string ToString(const T& o)
{
	return TConverter<T>::ToString(o);
}

template<typename T>
T FromString(const std::string& s)
{
	return TConverter<T>::FromString(s);
}

time_t GetTime(std::string sADateString);
void LaunchApp(const char *program, char *cmdline, const char *path, bool bWait);
bool FileExists(std::string sAFileName);
void Replace(std::string& sAString, const char from, const char to);

/*Get List of files in a directory*/
int GetFilesFromDir(std::string sABaseDir, std::list<std::string> *lListOfFiles);
/*Checks for directory existence*/
bool DirectoryExists(std::string sADirFullPath);
/*Removes a directory and empties it first if it is not empty*/
bool EmptyAndRemoveDirectory(std::string sADirFullPath);
/*Given a full file path, return only the file name*/
std::string ExtractFileName(std::string sAFullFileName);

template<typename _TypeName>
class TFileHash_set: public std::hash_set<_TypeName>
{
public:
	bool Save(std::string sAFileNameAndPath);
	bool Load(std::string sAFileNameAndPath);
};
template<typename _TypeName>
bool TFileHash_set<_TypeName>::Save(std::string sAFileNameAndPath)
{
	FILE *fHashSetFile;
	int iDataCounting;
	if ((fHashSetFile = fopen(sAFileNameAndPath.c_str(), "w+t")) == NULL) {
		return false;
	}

	iDataCounting = 0;
	typename std::hash_set<_TypeName>::iterator iHashIterator;
	for(iHashIterator = this->begin(); iHashIterator !=  this->end(); iHashIterator++) {
		fprintf(fHashSetFile, "%s", TConverter<_TypeName>::ToString(*iHashIterator).c_str());
		fprintf(fHashSetFile, "\n");
	}
	fclose(fHashSetFile);
	if(iDataCounting > 0) {
		return true;
	} else {
		return false;
	}
};
template<typename _TypeName>
bool TFileHash_set<_TypeName>::Load(std::string sAFileNameAndPath)
{
	int iDataCounting = 0;
	std::string sTemp;
	std::ifstream fHashSetFile;
	this->clear();
	fHashSetFile.open(sAFileNameAndPath.c_str());
	if(!fHashSetFile) {
		return false;
	}
	while ( !fHashSetFile.eof() ) {
		fHashSetFile >> sTemp;
		if(sTemp.size() > 0) {
			this->insert(TConverter<_TypeName>::FromString(sTemp));
			iDataCounting++;
		}
	}
	fHashSetFile.close();
	if(iDataCounting > 0) {
		return true;
	} else {
		return false;
	}
};
template <>
bool TFileHash_set<int>::Load(std::string sAFileNameAndPath);
template <>
bool TFileHash_set<int>::Save(std::string sAFileNameAndPath);

#endif
