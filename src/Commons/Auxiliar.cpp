#include "Auxiliar.h"

#include <stack>
#include <ctime>

#include <wx/dir.h>
#include <wx/filename.h>

/*------------------------------------------------------------*/
TEdgeTemp::TEdgeTemp(int iASource, int iADestination, int iAWeight)
{
	iSource = iASource;
	iDestination = iADestination;
	bResolved = false;
	iWeight = iAWeight;
};
bool TEdgeTemp::IsIt(int iASource, int iADestination)
{
	if((this->iSource == iASource) &&
	        (this->iDestination == iADestination)) {
		return true;
	}
	return false;
};
TEdgeTemp* FindEdgeEntry(std::hash_map< std::string, TEdgeTemp*> *lListOfEdges, int iId1, int iId2)
{
	std::hash_map< std::string, TEdgeTemp*>::const_iterator EI;
	for (EI = lListOfEdges->begin(); EI != lListOfEdges->end(); EI++) {
		if(((*EI).second->GetSource() == iId1) && ((*EI).second->GetDestination() == iId2)) {
			return (*EI).second;
		}
	}
	return NULL;
}

void DeleteResolvedEdges(std::hash_map< std::string, TEdgeTemp*> *lListOfEdges)
{
	std::hash_map< std::string, TEdgeTemp*>::iterator EI, iTemp;
	for (EI = lListOfEdges->begin(); EI != lListOfEdges->end();) {
		if((*EI).second->bResolved) {
			iTemp = EI;
			EI++;
			delete (*iTemp).second;
			lListOfEdges->erase(iTemp);
		} else {
			EI++;
		}
	}
}
/*------------------------------------------------------------*/
time_t GetTime(std::string sADateString)
{
	struct tm *tmDate;
	time_t rawtime;
	char *cDateString = (char *)sADateString.c_str();
	char *cDateParser;

	time (&rawtime);
	/*Create an instance of the strucuture you need*/
	tmDate = localtime (&rawtime);
	/*Reset time*/
	tmDate->tm_hour = 0;
	tmDate->tm_min = 0;
	tmDate->tm_sec = 0;
	/*Parse date*/
	cDateParser = strtok((char *)cDateString,"-\r\n\t ");
	tmDate->tm_year = TConverter<int>::FromString(cDateParser) - 1900;
	cDateParser = strtok(NULL,"-\r\n\t ");
	tmDate->tm_mon = TConverter<int>::FromString(cDateParser) - 1;
	cDateParser = strtok(NULL,"-\r\n\t ");
	tmDate->tm_mday = TConverter<int>::FromString(cDateParser);
	return mktime(tmDate);
}

void LaunchApp(const char *program, char *cmdline, const char *path, bool bWait)
{
	if(!FileExists(program)) {
		std::string sMessage = "Could not find ";
		sMessage += program;
		sMessage += ", support application required by RMine. Press Ok to quit application.";
		wxMessageDialog(NULL, _T(sMessage.c_str()),
		                _T("Missing support application"), wxOK).ShowModal();
		exit(0);
	}
	wxString command(program);
	if (cmdline) {
		command << " " << cmdline;
	}
	wxExecute(command, (bWait ? wxEXEC_SYNC : wxEXEC_ASYNC));
}

bool FileExists(std::string sAFileName)
{
	return wxFileName::FileExists(sAFileName);
}

/*Get List of files in a directory*/
int GetFilesFromDir(std::string sABaseDir, std::list<std::string> *lListOfFiles)
{
	wxDir dir(wxString(sABaseDir.c_str()));
	if (!dir.IsOpened()) {
		return -1;
	}
	wxString file;
	if (dir.GetFirst(&file)) {
		lListOfFiles->push_back(std::string(file.c_str()));
	}
	while (dir.GetNext(&file)) {
		lListOfFiles->push_back(std::string(file.c_str()));
	}
	return 0;
};

/*To verify the existence of directory we try to write whitin it*/
bool DirectoryExists(std::string sADirFullPath)
{
	return wxFileName::DirExists(sADirFullPath);
}

bool EmptyAndRemoveDirectory(std::string sADirFullPath)
{
	/*Verify if directory exists*/
	if(!DirectoryExists(sADirFullPath)) {
		return true;
	}
	std::string sFileName;
	/*Get all files*/
	std::list<std::string> lListOfFiles;
	GetFilesFromDir(sADirFullPath, &lListOfFiles);
	std::list<std::string>::iterator iLIterator;
	/*Remove all files*/
	for(iLIterator = lListOfFiles.begin(); iLIterator != lListOfFiles.end(); iLIterator++) {
		sFileName = sADirFullPath + "/" +(*iLIterator);
		remove(sFileName.c_str());
	}
	/*Remove directory*/
	if(RemoveDirectory(sADirFullPath.c_str())) {
		return true;
	}
	return false;
};

void Replace(std::string& sAString, const char from, const char to)
{
	for (uint i = 0; i < sAString.size(); ++i) {
		if (sAString[i] == from) {
			sAString[i] = to;
		}
	}
};
std::string ExtractFileName(std::string sAFullFileName)
{
	wxFileName wxfTemp(sAFullFileName.c_str());
	return std::string(wxfTemp.GetFullName());
}

/*------------------------------------------------------------*/

template<>
bool TFileHash_set<int>::Save(std::string sAFileNameAndPath)
{
	TFOut file (sAFileNameAndPath.c_str());
	int count = 0;
	std::hash_set<int>::iterator it;
	for (it = this->begin(); it != this->end(); it++) {
		TInt(*it).Save(file);
		count++;
	}
	return (count > 0);
};

template<>
bool TFileHash_set<int>::Load(std::string sAFileNameAndPath)
{
	this->clear();
	TFIn file (sAFileNameAndPath.c_str());
	int count = 0;
	while (!file.Eof()) {
		TInt value (file);
		this->insert((int)value);
		count++;
	}
	return (count > 0);
};

/*------------------------------------------------------------*/

namespace rmine
{
namespace cputime
{
using namespace std;

stack<bool> subcalls;
int indent = 0;

FILE* GetLogFile()
{
	static FILE *logfile = NULL;
	if (logfile == NULL) {
		logfile = fopen("cputime.log","a");
		// print timestamp
		time_t t;
		t = time(&t);
		tm * l = localtime(&t);
		fprintf(logfile,
			"\n###################################################"
			"\n#\n# %04d/%02d/%02d %02d:%02d:%02d new session started\n#",
		        l->tm_year+1900,
		        l->tm_mon+1,
		        l->tm_mday,
		        l->tm_hour,
		        l->tm_min,
		        l->tm_sec);
	}
	return logfile;
}

size_t GetTime()
{
#ifdef _WINDOWS_
	LARGE_INTEGER ticks, freq;
	QueryPerformanceCounter(&ticks);
	QueryPerformanceFrequency(&freq);
	return (ticks.QuadPart * 1000000) / freq.QuadPart;
#else
	rusage u;
	getrusage(RUSAGE_SELF, &u);
	return 1000000*u.ru_utime.tv_sec + u.ru_utime.tv_usec
	       + 1000000*u.ru_stime.tv_sec + u.ru_stime.tv_usec;
#endif
}

void fprintTime(FILE *file, size_t time)
{
	uint us = time,
	     s  = us / 1000000,
	     m  = s  / 60,
	     h  = m  / 60;
	m  %= 60;
	s  %= 60;
	us %= 1000000;
	fprintf(file, "\t%02d:%02d:%02d.%06d", h, m, s, us);
}

size_t Start(const char *label)
{
	FILE *logfile = GetLogFile();
	fputc('\n', logfile);
	for (int i=indent; i>0; --i) {
		fputc('\t', logfile);
	}
	fputs(label, logfile);
	if (!subcalls.empty()) {
		subcalls.top() = true;
	}
	subcalls.push(false);
	indent++;
	return GetTime();
}

void End(size_t time)
{
	size_t elapsed = GetTime() - time;
	indent--;
	FILE *logfile = GetLogFile();
	if (subcalls.top()) {
		fputc('\n', logfile);
		for (int i=indent; i>0; --i) {
			fputc('\t', logfile);
		}
	} else {
		fputc(' ', logfile);
	}
	fprintTime(logfile, elapsed);
	subcalls.pop();
	if (indent == 0) {
		fflush(logfile);
	}
}
}
}

CpuTimeLog::CpuTimeLog(const char* label)
	: time(rmine::cputime::Start(label))
{}

CpuTimeLog::~CpuTimeLog()
{
	rmine::cputime::End(time);
}