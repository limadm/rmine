#pragma once
#include "Commons/stdafx.h"
#include <wx/wxprec.h>
#include <wx/wx.h>

// Define a new application type, each program should derive a class from wxApp
class RMine : public wxApp
{
public:
   // this one is called on application startup and is a good place for the app
   // initialization (doing it here and not in the ctor allows to have an error
   // return: if OnInit() returns false, the application terminates)
   bool OnInit();
   int  OnRun();
};

DECLARE_APP(RMine)
