#include "NodePlot.h"

void TNodePlot::SetColor(std::string sAColorRGBCode){
   /*Deprecated - Backward compatibility*/
   if(sAColorRGBCode == "r") sAColorRGBCode = "255_000_000";
   else if(sAColorRGBCode == "g") sAColorRGBCode = "000_255_000";
   else if(sAColorRGBCode == "b") sAColorRGBCode = "000_000_255";

   std::string sComponent;
   /*Color string - xxx_xxx_xxx - 3 decimal digits for each RGB color component*/
   sComponent = sAColorRGBCode.substr(0, 3);
   this->bRed = (unsigned char)TConverter<int>::FromString(sComponent);
   sComponent = sAColorRGBCode.substr(4, 3);
   this->bGreen = (unsigned char)TConverter<int>::FromString(sComponent);
   sComponent = sAColorRGBCode.substr(8, 3);
   this->bBlue = (unsigned char)TConverter<int>::FromString(sComponent);
}

std::string TNodePlot::GetColor(){
   std::string sComponent;       std::string sFinalColor;
   /*Color string - xxx_xxx_xxx - 3 decimal digits for each RGB color component*/
   sComponent = TConverter<int>::ToString((int)this->bRed);
   if(sComponent.size() == 1) sComponent = "00" + sComponent;
   else if(sComponent.size() == 2) sComponent = "0" + sComponent;
   sFinalColor += sComponent + "_";
   sComponent = TConverter<int>::ToString((int)this->bGreen);
   if(sComponent.size() == 1) sComponent = "00" + sComponent;
   else if(sComponent.size() == 2) sComponent = "0" + sComponent;
   sFinalColor += sComponent + "_";
   sComponent = TConverter<int>::ToString((int)this->bBlue);
   if(sComponent.size() == 1) sComponent = "00" + sComponent;
   else if(sComponent.size() == 2) sComponent = "0" + sComponent;
   sFinalColor += sComponent;
   return sFinalColor;
}

void TNodePlot::SetLabel(std::string sALabel){
   sLabel = sALabel.c_str();
};

void TNodePlot::AddSonToInfluenceSet(int iANodeIndex){
   /*Class Set<int> guarantees no duplicates in sSonNodes*/
   this->sSonNodes.insert(iANodeIndex);
};

TNodePlot& TNodePlot::operator = (const TNodePlot& NodePlot) { 
   if( this != &NodePlot) {
      sLabel = NodePlot.sLabel;
      X = NodePlot.X;  Y = NodePlot.Y;
      Size = NodePlot.Size;
      bRed = NodePlot.bRed;         bGreen = NodePlot.bGreen;
      bBlue = NodePlot.bBlue;
      saShapeType = NodePlot.saShapeType;

      dPageRank = NodePlot.dPageRank;

      bVisible = NodePlot.bVisible;
      bInfected = NodePlot.bInfected;         
      bImmunized = NodePlot.bImmunized;

      sSonNodes = NodePlot.sSonNodes;
      iInfluenceLevel = NodePlot.iInfluenceLevel;
      iMETISPartitionNumber = NodePlot.iMETISPartitionNumber;
   }
   return *this;
}

void TNodePlot::ScaleSize(double dAScaleValue){   
   double dSize = dAScaleValue*0.2;
   if(dSize < 0.01)
      dSize = 0.01;
   this->Size = dSize;
}

void TNodePlot::InfectNode(){
   if(bImmunized)
      return;
   bInfected = true;
};

bool TNodePlot::IsPointOver(double _X, double _Y){
   TRectangle NodeBoundingBox = GetBoundingBox();
   return (
           (  (  NodeBoundingBox.dXLeft < _X) && (_X < NodeBoundingBox.dXRight) ) &&
           (  (NodeBoundingBox.fYBotton < _Y) && (_Y < NodeBoundingBox.fYTop  ) )
          );
}

void TNodePlot::Save(TSOut& SOut) const{
   X.Save(SOut);
   Y.Save(SOut);
   Size.Save(SOut);
   sLabel.Save(SOut);
   TStr sShapeType(TShapeType::GetShapeString(saShapeType).c_str());
   sShapeType.Save(SOut);
   iDeviceX.Save(SOut);
   iDeviceY.Save(SOut);
}
TRectangle TNodePlot::GetBoundingBox(){
   TRectangle NodeBoundingBox;
   TFlt SizeTemp = Size.Val/2.0;

   NodeBoundingBox.dXLeft = X.Val - SizeTemp;        
   NodeBoundingBox.fYBotton = Y.Val - SizeTemp;
   NodeBoundingBox.dXRight = X.Val + SizeTemp;
   NodeBoundingBox.fYTop = Y.Val + SizeTemp;

   return NodeBoundingBox;
}

void TNodePlot::SetRed(byte bARedValue){
   bRed = bARedValue;
}
void TNodePlot::SetGreen(byte bAGreeValue){
   bGreen = bAGreeValue;
}
void TNodePlot::SetBlue(byte bABlueValue){
   bBlue = bABlueValue;
}
byte TNodePlot::GetRed(){
   return bRed;
}
byte TNodePlot::GetGreen(){
   return bGreen;
}
byte TNodePlot::GetBlue(){
   return bBlue;
}



PXmlTok TNodePlot::GetXmlTok() const {
  PXmlTok NodeTok = TXmlTok::New("NodePlot");
  NodeTok->AddArg(TStr("X"), double(X));
  NodeTok->AddArg(TStr("Y"), double(Y));
  NodeTok->AddArg(TStr("Size"), double(Size));
  return NodeTok;
}