#pragma once
#ifndef _EdgePlot_
#define _EdgePlot_

#include <Commons/stdafx.h>

class TEdgePlot {
private:
#ifndef PERFORMANCE
   TInt iWeight;
#endif
public:
#ifndef PERFORMANCE
   TEdgePlot() : iWeight(1){ }
   TEdgePlot(TSIn& SIn) : iWeight(SIn){ };
#else
   TEdgePlot(){ }
   TEdgePlot(TSIn& SIn){ };
#endif
   TEdgePlot& operator = (const TEdgePlot& Edge){
#ifndef PERFORMANCE
      if (this != &Edge) {
         iWeight = Edge.iWeight;
      }
#endif
      return *this;
   }
   void Save(TSOut& SOut) const{
#ifndef PERFORMANCE
      iWeight.Save(SOut);
#endif
   }
   bool operator == (const TEdgePlot& Edge){
#ifndef PERFORMANCE
      if(iWeight == Edge.iWeight)
         return true;
      return false;
#endif
      return true;
   };
   void SetWeight(TInt iAWeight){
#ifndef PERFORMANCE
      iWeight = iAWeight;
#endif
   }
   TInt GetWeight() {
#ifndef PERFORMANCE
      return iWeight; 
#else
      return 1;
#endif
   }
};
#endif