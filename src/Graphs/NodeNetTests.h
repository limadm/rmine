#pragma once
#ifndef _NodeNetTests_
#define _NodeNetTests_

#include <Commons/stdafx.h>

#include <string>
#include <limits>
#include <iostream>
#include <fstream>
#include <queue>
#include <vector>
#include <hash_map>
#include <hash_set>

#include <wx/filename.h>

#include <Commons/Auxiliar.h>
#include <Commons/Storage.h>
#include <Dialogs/DialogWarning.h>

typedef enum {
    nnmNormal, nnmVirusSimulation, nnmForestFireSampling, nnmInfluence
} TNodeNetMode;

template <class _TNodeData, class _TEdgeData>
class TNodeNetTests : public TNodeEdgeNet <_TNodeData, _TEdgeData> {
public:
    typedef TNodeEdgeNet<_TNodeData, _TEdgeData> NodeEdgeNet;
    typedef typename NodeEdgeNet::TNodeI TNodeI;
    typedef typename NodeEdgeNet::TEdgeI TEdgeI;
public:
    /*Temporary auxiliars*/
    std::vector<int> vTotalSonsForLevel;
    std::set<int> sFirstLevelNodes;
    bool bInfluenceDraw;
    bool bLayoutFlag;
    bool bTimeStampedDraw;
    TNodeNetMode nnmMode;
    int iMaxInfluenceLevel;
public:
    TNodeNetTests();
    TNodeNetTests(TSIn &SIn);
    ~TNodeNetTests();
    void Save(TSOut& SOut);

    int GetMaxInfluenceLevel() {
        return iMaxInfluenceLevel;
    }

    std::set<int>& GetFirstLevelNodes() {
        return sFirstLevelNodes;
    }
    void GetAllSourceNodes(std::hash_set<int>& hsHashOfDestinationNodes);
    void SetAllNodesVisibility(bool bVisible);
    void SetAllNodesSize(double dASize);
    void ScaleAllNodesSize(double dAScale);

    double GetAllNodesSize() {
        return dAllNodesSize;
    }
    void Reset(bool bResetVisibility = false);
    int GetRandomNodeId();
    int GetNumberOfGrandSons(TNodeI niASubjectNode);
    void NormalizeAndSetPositions();
    void DifferentiateSetOfNodes(const TIntV& NodeIdsToDifferentiate);
    int GetNumberOfNodesInLevel(int iALevel);

    int GetLowestTimePoint() {
        return tLowestTime;
    }

    int GetHighestTimePoint() {
        return tHighestTime;
    }

    std::string GetLoadedEdgesFileName() {
        return sEdgeFileName;
    }
    void CountSonForLevel(unsigned int iALevel);
    bool GetEmphasizedNode(TNodeI& niANode);
    void SetEmphasizedNode(TNodeI& niANode);
    void CopyLabelDataToHashMap(std::hash_map<int, TNodesAuxiliar>& hmAHashMap, void* vASuperNodeEntry);
    void FillNodeNetWithLabelDataFromHashMap(std::hash_map<int, TNodesAuxiliar>& hmAHashMap);
    bool GetFirstKnownNeighbor(TNodeI& niANodeI, int& iANodeId);
    friend class TPt< TNodeNetTests<_TNodeData, _TEdgeData> >;
private:
    double dAllNodesSize;
    time_t tLowestTime;
    time_t tHighestTime;
    /*File names*/
    std::string sEdgeFileName;
    std::string sNodesFileName;
    std::string sLayoutFileName;
    /*Temporary auxiliars*/
    TNodeI niEmphasizedNode;
    bool bIsThereEmphasizedNode;
public:
    // io operations
    void WriteHanghangEdgeFile(std::string sAEdgeFileName = "");
    bool WriteEdgeFile(std::string sAEdgeFileName = "");
    bool WriteNodesFile();
    bool WriteLayoutFile();

    bool ReadEdgesFile(std::string sAEdgesFileName);
    bool ReadNodesFile(std::string sANodeFileName);
    bool ReadLayoutFile(std::string sALayoutFileName);

    bool LoadGraphTextFile(std::string sAPath);
    bool ReadDBLPLikeFile(std::string sAPath);

    void MountFileNames(std::string sAPath);
};

template <class _TNodeData, class _TEdgeData>
TNodeNetTests<_TNodeData, _TEdgeData>::TNodeNetTests() : TNodeEdgeNet <_TNodeData, _TEdgeData>(), dAllNodesSize(0.02) {
    bLayoutFlag = false;
    bInfluenceDraw = false;
    bTimeStampedDraw = false;
    nnmMode = nnmNormal;
    iMaxInfluenceLevel = -1;
    tLowestTime = (std::numeric_limits<int>::max)();
    tHighestTime = 0;
    niEmphasizedNode = this->EndNI();
    bIsThereEmphasizedNode = false;
}

template <class _TNodeData, class _TEdgeData>
TNodeNetTests<_TNodeData, _TEdgeData>::TNodeNetTests(TSIn &SIn)
: TNodeEdgeNet<_TNodeData, _TEdgeData>(SIn) {
    TStr strEdgeFileName(SIn);
    sEdgeFileName = strEdgeFileName.CStr();
    TStr strNodesFileName(SIn);
    sNodesFileName = strNodesFileName.CStr();
    TStr strLayoutFileName(SIn);
    sLayoutFileName = strLayoutFileName.CStr();
    bLayoutFlag = false;
    bInfluenceDraw = false;
    bTimeStampedDraw = false;
    nnmMode = nnmNormal;
    iMaxInfluenceLevel = -1;
    tLowestTime = (std::numeric_limits<int>::max)();
    tHighestTime = 0;
    niEmphasizedNode = this->EndNI();
    bIsThereEmphasizedNode = false;
}

template <class _TNodeData, class _TEdgeData>
TNodeNetTests<_TNodeData, _TEdgeData>::~TNodeNetTests() {
}

template <class _TNodeData, class _TEdgeData>
void TNodeNetTests<_TNodeData, _TEdgeData>::Save(TSOut& SOut) {
    TNodeEdgeNet<_TNodeData, _TEdgeData>::Save(SOut);
    TStr strEdgeFileName(sEdgeFileName.c_str());
    strEdgeFileName.Save(SOut);
    TStr strNodesFileName(sNodesFileName.c_str());
    sNodesFileName = ExtractFileName(sNodesFileName);
    strNodesFileName.Save(SOut);
    TStr strLayoutFileName(sLayoutFileName.c_str());
    strLayoutFileName.Save(SOut);
}

template <class _TNodeData, class _TEdgeData>
void TNodeNetTests<_TNodeData, _TEdgeData>::DifferentiateSetOfNodes(const TIntV& NodeIdsToDifferentiate) {
    for (int i = 0; i < NodeIdsToDifferentiate.Len(); i++) {
        this->GetNI(NodeIdsToDifferentiate[i])().SetVisible(true);
        this->GetNI(NodeIdsToDifferentiate[i])().SetInfected(true);
    }
}

template <class _TNodeData, class _TEdgeData>
void TNodeNetTests<_TNodeData, _TEdgeData>::SetAllNodesVisibility(bool bVisible) {
    for (TNodeI NI = this->BegNI(); NI < this->EndNI(); NI++) {
        NI().SetVisible(bVisible);
    }
}

template <class _TNodeData, class _TEdgeData>
void TNodeNetTests<_TNodeData, _TEdgeData>::SetAllNodesSize(double dASize) {
    dAllNodesSize = dASize;
    for (TNodeI NI = this->BegNI(); NI < this->EndNI(); NI++) {
        NI().SetSize(dASize);
    }
}

template <class _TNodeData, class _TEdgeData>
void TNodeNetTests<_TNodeData, _TEdgeData>::ScaleAllNodesSize(double dAScale) {
    for (TNodeI NI = this->BegNI(); NI < this->EndNI(); NI++) {
        NI().ScaleSize(dAScale);
    }
}

template <class _TNodeData, class _TEdgeData>
int TNodeNetTests<_TNodeData, _TEdgeData>::GetNumberOfNodesInLevel(int iALevel) {
    std::vector<int>::const_iterator vLevelCounter = vTotalSonsForLevel.begin();
    return (*(vLevelCounter + iALevel));
}

template <class _TNodeData, class _TEdgeData>
void TNodeNetTests<_TNodeData, _TEdgeData>::Reset(bool bResetVisibility) {
    bInfluenceDraw = false;
    bTimeStampedDraw = false;
    for (TNodeI NI = this->BegNI(); NI < this->EndNI(); NI++) {
        if (bResetVisibility)
            NI().SetVisible(true);
        NI().SetImmunized(false);
        NI().SetInfected(false);
        NI().SetColor("000_255_000");
        NI().SetShape("s");
        NI().SetSize(0.02);
    }
}

class TNodeIdDegree {
public:

    TNodeIdDegree(int iANodeId, int iADegree) {
        iNodeId = iANodeId;
        iDegree = iADegree;
    }
    int iNodeId;
    int iDegree;

    bool operator<(const TNodeIdDegree& nidANodeIdDegree) const {
        return this->iDegree < nidANodeIdDegree.iDegree;
    }
};

template <class _TNodeData, class _TEdgeData>
int TNodeNetTests<_TNodeData, _TEdgeData>::GetRandomNodeId() {
    return max(0, (int) ((double) rand() / (double) RAND_MAX * this->GetNodes()));
}

template <class _TNodeData, class _TEdgeData>
bool TNodeNetTests<_TNodeData, _TEdgeData>::GetEmphasizedNode(TNodeI& niANode) {
    niANode = niEmphasizedNode;
    return bIsThereEmphasizedNode;
}

template <class _TNodeData, class _TEdgeData>
void TNodeNetTests<_TNodeData, _TEdgeData>::SetEmphasizedNode(TNodeI& niANode) {
    niEmphasizedNode = niANode;
    if (niANode == this->EndNI())
        bIsThereEmphasizedNode = false;
    else
        bIsThereEmphasizedNode = true;
}

/*------------------------------------------------------------------------------------------*/
template <class _TNodeData, class _TEdgeData>
void TNodeNetTests<_TNodeData, _TEdgeData>::CountSonForLevel(unsigned int iALevel) {
    std::vector<int>::iterator vLevelCounter;
    if (this->vTotalSonsForLevel.size() < iALevel) {
        this->vTotalSonsForLevel.push_back((int) 0);
    }
    vLevelCounter = this->vTotalSonsForLevel.begin();
    (*(vLevelCounter + iALevel))++;
}

/*------------------------------------------------------------------------------------------*/
template <class _TNodeData, class _TEdgeData>
int TNodeNetTests<_TNodeData, _TEdgeData>::GetNumberOfGrandSons(TNodeI niASubjectNode) {
    std::set<int>::const_iterator sIterator;
    int iTotalGrandSons = 0;
    for (sIterator = niASubjectNode().GetSonNodes().begin(); sIterator != niASubjectNode().GetSonNodes().end(); ++sIterator) {
        iTotalGrandSons += this->GetNI(*sIterator)().GetSonNodes().size();
    }
    return iTotalGrandSons;
}

/*------------------------------------------------------------------------------------------*/
template <class _TNodeData, class _TEdgeData>
void TNodeNetTests<_TNodeData, _TEdgeData>::NormalizeAndSetPositions() {
    if (this->GetNodes() <= 1) /*Only one node, keep its position*/
        return;
    double dMaxX;
    double dMaxY;
    double dMinX;
    double dMinY;
    double dXDifference;
    double dYDifference;
    dMaxX = this->BegNI()().GetX();
    dMaxY = this->BegNI()().GetY();
    dMinX = this->BegNI()().GetX();
    dMinY = this->BegNI()().GetY();
    /*Search for extreme values*/
    for (TNodeI NI = this->BegNI(); NI < this->EndNI(); NI++) {
        if (NI().GetX() > dMaxX)
            dMaxX = NI().GetX();
        if (NI().GetX() < dMinX)
            dMinX = NI().GetX();

        if (NI().GetY() > dMaxY)
            dMaxY = NI().GetY();
        if (NI().GetY() < dMinY)
            dMinY = NI().GetY();
    }
    dXDifference = dMaxX - dMinX;
    dYDifference = dMaxY - dMinY;
    /*If the layout information is corrupted (all nodes in the
      in the same place, this condition will not be satisfied*/
    if ((dXDifference > 0) && (dYDifference > 0)) {
        /*Normalization and update*/
        for (TNodeI NI = this->BegNI(); NI < this->EndNI(); NI++) {
            NI().SetX((NI().GetX() - dMinX) / dXDifference);
            NI().SetY((NI().GetY() - dMinY) / dYDifference);
            /*Small eventual roundings*/
            if (NI().GetX() < 0.0) NI().SetX(0.0);
            if (NI().GetX() > 1.0) NI().SetX(1.0);
            if (NI().GetY() < 0.0) NI().SetY(0.0);
            if (NI().GetY() > 1.0) NI().SetY(1.0);
        }
    }
}

/*------------------------------------------------------------------------------------------*/
template <class _TNodeData, class _TEdgeData>
bool TNodeNetTests<_TNodeData, _TEdgeData>::LoadGraphTextFile(std::string sAPath) {
    MountFileNames(sAPath);

    /*Check for DBLP edges file*/
    if ((sAPath.find("_ef", 0) != -1) || (sAPath.find("_lf", 0) != -1) || (sAPath.find("_nf", 0) != -1)) {
        if (!ReadEdgesFile(sEdgeFileName))
            return false; /*Cannot continue without this file*/
    } else {
        if (!ReadDBLPLikeFile(sEdgeFileName))
            return false; /*Cannot continue without this file*/
    }
    ReadNodesFile(sNodesFileName);
    ReadLayoutFile(sLayoutFileName);
    return true;
}

template <class _TNodeData, class _TEdgeData>
void TNodeNetTests<_TNodeData, _TEdgeData>::MountFileNames(std::string sAPath) {
    std::string sDirectory;
    std::string sFileNameRoot;
    std::string sFileName;

    /*sAPath contains file name (and path) - files must all be at the same directory and have the same name root*/
    wxFileName wxfnFileName(sAPath.c_str());
    sDirectory = wxfnFileName.GetPath() + wxFileName::GetPathSeparator();
    sFileName = wxfnFileName.GetName(); /*Automatically excludes extension*/
    if ((sAPath.find("_ef", 0) != -1) || (sAPath.find("_lf", 0) != -1) || (sAPath.find("_nf", 0) != -1)) {
        /*sAPath contains edge file like name (ends with _ef.txt)*/
        sFileNameRoot = sDirectory + sFileName.substr(0, sFileName.size() - 3);
        sEdgeFileName = sFileNameRoot + "_ef.txt";
    } else {
        /*sAPath contains dblp like file name (ends with .txt)*/
        sFileNameRoot = sDirectory + sFileName.substr(0, sFileName.size());
        sEdgeFileName += sFileNameRoot + ".txt";
    }

    sNodesFileName += sFileNameRoot + "_nf.txt";
    sLayoutFileName += sFileNameRoot + "_lf.txt";
}

/*------------------------------------------------------------------------------------------*/

/*Edges file*/
template <class _TNodeData, class _TEdgeData>
bool TNodeNetTests<_TNodeData, _TEdgeData>::ReadDBLPLikeFile(std::string sAPath) {
    /*Sample line to be read: "23232  1992-07-22  1  2  3" 1->2, 1->3, 2->3 (two first fields ignored) */
    std::ifstream indata;
    int iPaperId;
    std::string sDate;
    char cAuthorIds[500];
    char *cNextAuthor = NULL;
    int iAuthorId;
    tLowestTime = (std::numeric_limits<int>::max)();
    tHighestTime = 0;
    std::vector<int> vListOfAuthors;

    indata.open(sAPath.c_str());
    if (!indata) {
        TDialogWarning dwWarning("Error: authors file " + sAPath + " could not be opened. Try a different file or path.", "Error: invalid data source.");
        return false;
    }

    while (!indata.eof()) {
        indata >> iPaperId; /*paper id  - ignored*/
        indata >> sDate; /*date		- ignored*/

        indata.getline(cAuthorIds, 500); /*Get all author ids*/
        cNextAuthor = strtok(cAuthorIds, " 	\n\r"); /*Tokenizer, get next author id*/
        vListOfAuthors.clear();

        while (cNextAuthor != NULL) {
            iAuthorId = TConverter<int>::FromString(cNextAuthor);
            vListOfAuthors.push_back(iAuthorId);
            if (!this->IsNode(iAuthorId)) {
                this->AddNode(iAuthorId);
            }
            cNextAuthor = strtok(NULL, " 	\n\r");
        }

        for (unsigned int iCounter = 0; iCounter < vListOfAuthors.size(); iCounter++) {
            for (unsigned int iCounter2 = iCounter + 1; iCounter2 < vListOfAuthors.size(); iCounter2++) {
                if (!this->IsEdge(vListOfAuthors[iCounter], vListOfAuthors[iCounter2]))
                    this->AddEdge(vListOfAuthors[iCounter], vListOfAuthors[iCounter2]);
            }
        }
    }
    /*Set labels the same as the ids*/
    /*	for (TNodeI& NI = this->BegNI(); NI < this->EndNI(); NI++) {
            this->GetNI(NI.GetId())().SetLabel(TConverter<int>::ToString(NI.GetId()));
        }
     */
    indata.close();
    return true;
}

template <class _TNodeData, class _TEdgeData>
bool TNodeNetTests<_TNodeData, _TEdgeData>::ReadEdgesFile(std::string sAEdgesFileName) {
    /*Sample line to be read: "1 3 2" 1->3 weight 2*/
    int iDataCounting = 0;
    std::string sTemp;
    int iWeight;
    int iFrom;
    int iTo;
    int iInsertionIndex;
    int iNewWeight, iEdgeIndex;
    std::string sLine;
    char *cNextDataItem;
    std::ifstream ifsDataFile;
    char cLineTrash[1000];

    ifsDataFile.open(sAEdgesFileName.c_str());
    if (!ifsDataFile) {
        TDialogWarning dwWarning("Error: edges file " + sAEdgesFileName + " could not be opened. Try a different file or path.", "Error: invalid data source");
        return false;
    }
    try {
        /*Edge file*/
        while (!ifsDataFile.eof() && ifsDataFile >> sTemp) {
            if (sTemp.find("#", 0) != -1) {
                ifsDataFile.getline(cLineTrash, 100); /*Read and discard full line*/

                if (sTemp.find("Uncon", 0) != -1) { /*Single node detected*/
                    ifsDataFile >> sTemp;
                    /*Single node*/
                    iFrom = TConverter<int>::FromString(sTemp); /*Convert initialy read string to int*/
                    /*Insert into graph structure*/
                    if (!this->IsNode(iFrom)) {
                        iInsertionIndex = this->AddNode(iFrom);
                        this->GetNI(iInsertionIndex)().SetLabel(TConverter<int>::ToString(iFrom));
                    }
                    iDataCounting++;
                }
                continue; /*Found scape caracter, jump to next line*/
            }

            /*Have values now, no scape caracter was found*/
            iFrom = TConverter<int>::FromString(sTemp); /*Convert initialy read string to int*/
            /*Insert into graph structure*/
            if (!this->IsNode(iFrom)) {
                iInsertionIndex = this->AddNode(iFrom);
                this->GetNI(iInsertionIndex)().SetLabel(TConverter<int>::ToString(iFrom));
            }

            /*Check for second and third data items*/
            ifsDataFile >> iTo;
            ifsDataFile >> iWeight;

            /*Insert into graph structure*/
            if (!this->IsNode(iTo)) {
                iInsertionIndex = this->AddNode(iTo);
                this->GetNI(iInsertionIndex)().SetLabel(TConverter<int>::ToString(iTo));
            }

            /*If it is not an edge, add it with the weight that was read*/
            if (!this->IsEdge(iFrom, iTo, iEdgeIndex)) {
                this->GetEI(this->AddEdge(iFrom, iTo))().SetWeight(TInt(iWeight));
            }				/*Otherwise, add the weight that was read to the existent weight*/
            else {
                iNewWeight = this->GetEI(iEdgeIndex)().GetWeight();
                iNewWeight += iWeight;
                this->GetEI(iEdgeIndex)().SetWeight(TInt(iNewWeight));
            }
            iDataCounting++;
        }
    } catch (std::exception& ex) {
        ifsDataFile.close();
        std::cerr << "exception caught: " << ex.what() << std::endl;
        TDialogWarning dwWarning("Error: wrong file format: " + sAEdgesFileName + " . Try other data format. Total read " + TConverter<int>::ToString(iDataCounting), "Error: invalid data source");
        return false;
    }
    ifsDataFile.close();
    if (iDataCounting > 0)
        return true;
    else {
        TDialogWarning dwWarning("Error: edges " + sAEdgesFileName + " file does not contain valid data (or it is empty). Try other data format.", "Error: invalid data source");
        return false;
    }
}

template <class _TNodeData, class _TEdgeData>
void TNodeNetTests<_TNodeData, _TEdgeData>::WriteHanghangEdgeFile(std::string sAEdgeFileName) {
    /*sAEdgeFileName must be a full name*/
    FILE *fHanghangEdgeFile;
    std::string sDirectory;
    std::string sFileNameRoot;
    std::string sFileName;
    int iNeighbors, iNextNeighbor, iEdgeIndex;
    /*Create edge file*/
    if ((fHanghangEdgeFile = fopen(sAEdgeFileName.c_str(), "w+t")) == NULL) {
        return;
    }

    fprintf(fHanghangEdgeFile, "%d ", this->GetNodes());
    fprintf(fHanghangEdgeFile, "\n");

    for (TNodeI& NI = this->BegNI(); NI < this->EndNI(); NI++) {
        iNeighbors = NI.GetOutDeg();
        if (iNeighbors > 0) {
            fprintf(fHanghangEdgeFile, "%d", NI.GetId());
            fprintf(fHanghangEdgeFile, "%s", ":");
            fprintf(fHanghangEdgeFile, "%d", NI.GetOutDeg());
            fprintf(fHanghangEdgeFile, "\n");

            for (int iNeighCounter = 0; iNeighCounter < iNeighbors; iNeighCounter++) {
                iNextNeighbor = NI.GetOutNId(iNeighCounter);
                this->IsEdge(NI.GetId(), iNextNeighbor, iEdgeIndex);
                if (iEdgeIndex != -1) { /*If edge is ok*/
                    if (iNeighCounter > 0)
                        fprintf(fHanghangEdgeFile, "%s", " ");
                    fprintf(fHanghangEdgeFile, "%d", iNextNeighbor);
                    fprintf(fHanghangEdgeFile, "%s", ":");
                    fprintf(fHanghangEdgeFile, "%d", this->GetEI(iEdgeIndex)().GetWeight());
                }
            }
        }
        if (iNeighbors > 0)
            fprintf(fHanghangEdgeFile, "\n");
    }
    fclose(fHanghangEdgeFile);
}

/*Edge files*/
template <class _TNodeData, class _TEdgeData>
bool TNodeNetTests<_TNodeData, _TEdgeData>::WriteEdgeFile(std::string sAEdgeFileName) {
    FILE *fEdgeFile;
    int iDataCounting;
    std::string sDirectory;
    std::string sFileNameRoot;
    std::string sFileName;
    /*Define file name*/
    if (sAEdgeFileName.compare("") != 0) {
        MountFileNames(sAEdgeFileName);
    } else {
        if (sEdgeFileName.find("_ef", 0) == -1) {
            wxFileName wxfnFileName(sEdgeFileName.c_str());
            sDirectory = wxfnFileName.GetPath() + wxFileName::GetPathSeparator();
            sFileName = wxfnFileName.GetName(); /*Get file name without extension*/
            sFileName += "_ef.txt"; /*Add "edge file" extension*/
            sEdgeFileName = sDirectory + sFileName; /*Form final name*/
        }
    }
    /*Create edge file*/
    if ((fEdgeFile = fopen(sEdgeFileName.c_str(), "w+t")) == NULL) {
        TDialogWarning dwWarning("Cannot create edge file " + sEdgeFileName + ". Please verify path and permissions.");
        return false;
    }

    //fprintf(fEdgeFile,"%s","#lines started by # are ignored\n");
    //fprintf(fEdgeFile,"%s","#from to weight\n");
    iDataCounting = 0;

    for (TEdgeI EI = this->BegEI(); EI != this->EndEI(); EI++) {
        fprintf(fEdgeFile, "%d ", this->GetNI(EI.GetSrcNId()).GetId());
        fprintf(fEdgeFile, "%d ", this->GetNI(EI.GetDstNId()).GetId());
        fprintf(fEdgeFile, "%d", EI().GetWeight());
        fprintf(fEdgeFile, "\n");
        iDataCounting++;
    }

    for (TNodeI NI = this->BegNI(); NI < this->EndNI(); NI++) {
        if (NI.GetDeg() == 0) {
            //fprintf(fEdgeFile,"%s","#Unconnected nodes go like this");
            fprintf(fEdgeFile, "\n");
            fprintf(fEdgeFile, "%d", NI.GetId());
            fprintf(fEdgeFile, "\n");
            iDataCounting++;
        }
    }

    fclose(fEdgeFile);
    if (iDataCounting > 0)
        return true;
    else {
        //TDialogWarning dwWarning("Error: nothing written to file " +sEdgeFileName + ".");
        return false;
    }
}
/*------------------------------------------------------------------------------------------*/

/*Node files*/
template <class _TNodeData, class _TEdgeData>
bool TNodeNetTests<_TNodeData, _TEdgeData>::ReadNodesFile(std::string sANodeFileName) {
    int iNodeId;
    int iDataCounting = 0;
    std::string sNodeLabel;
    std::string sShapeId;
    std::string sColorId;
    std::ifstream ifsDataFile;
    char cLineTrash[100];
    std::string sTemp;
    ifsDataFile.open(sANodeFileName.c_str());
    if (!ifsDataFile) {
        return false;
    }

    /*Node file*/
    while (!ifsDataFile.eof() && ifsDataFile >> sTemp) {
        /*First read as string to search for scape caracter*/
        if (sTemp.find("#", 0) != -1) {
            ifsDataFile.getline(cLineTrash, 100); /*Read and discard full line*/
            continue; /*Found scape caracter, jump to next line*/
        }
        /*Have values now, no scape caracter was found*/
        iNodeId = TConverter<int>::FromString(sTemp); /*Convert initialy read string to int*/
        ifsDataFile >> sNodeLabel;
        ifsDataFile >> sShapeId;
        ifsDataFile >> sColorId;

        if (ifsDataFile) {
            /*Nodes might be unconnected and therefore were not loaded in ReadEdgesFile*/
            /*Here for back compatibility, unconnected nodes go on the edges file as well, see WriteEdgeFile()*/
            if (!this->IsNode(iNodeId))
                this->AddNode(iNodeId);
            if (sNodeLabel.compare("_") == 0) {
                /*If no labels, set labels the same as the ids*/
                this->GetNI(iNodeId)().SetLabel(TConverter<int>::ToString(this->GetNI(iNodeId).GetId()));
            } else {
				Replace(sNodeLabel, '_', ' ');
                this->GetNI(iNodeId)().SetLabel(sNodeLabel);
                this->GetNI(iNodeId)().GetX();
            }
            this->GetNI(iNodeId)().SetShape(sShapeId);
            this->GetNI(iNodeId)().SetColor(sColorId);

            iDataCounting++;
        }
    }
    ifsDataFile.close();
    if (iDataCounting > 0)
        return true;
    else {
        TDialogWarning dwWarning("Warning: node file does not contain valid information. Please verify data format.");
        return false;
    }
}

template <class _TNodeData, class _TEdgeData>
bool TNodeNetTests<_TNodeData, _TEdgeData>::WriteNodesFile() {
    FILE *fNodesFile;
    int iDataCounting = 0;

    /*Create file*/
    if ((fNodesFile = fopen(sNodesFileName.c_str(), "w+t")) == NULL) {
        TDialogWarning dwWarning("Cannot create nodes file " + sNodesFileName + ". Please verify path and permissions.");
        return false;
    }

    fprintf(fNodesFile, "%s", "#lines started by # are ignored\n");
    fprintf(fNodesFile, "%s", "#id label shape color\n");
    for (TNodeI NI = this->BegNI(); NI < this->EndNI(); NI++) {
        fprintf(fNodesFile, "%d ", NI.GetId());
        if (NI().GetLabel().compare("") == 0) {
            fprintf(fNodesFile, "%s ", "_");
        } else
            fprintf(fNodesFile, "%s ", NI().GetLabel().c_str());
        fprintf(fNodesFile, "%s ", NI().GetShape().c_str());
        fprintf(fNodesFile, "%s", NI().GetColor().c_str());
        fprintf(fNodesFile, "\n");
        iDataCounting++;
    }
    fclose(fNodesFile);

    if (iDataCounting > 0)
        return true;
    else {
        //TDialogWarning dwWarning("Error: nothing written to file " +sNodesFileName + ".");
        return false;
    }
}
/*------------------------------------------------------------------------------------------*/

/*Layout files*/
template <class _TNodeData, class _TEdgeData>
bool TNodeNetTests<_TNodeData, _TEdgeData>::ReadLayoutFile(std::string sALayoutFileName) {
    if (bLayoutFlag)
        return false;
    bool bFileHasNewFormat = false; /*Files with new format have header started by # and pagerank information*/
    int iDataCounting = 0;
    char cLineTrash[100];
    std::string sTemp;
    int iNodeId;
    double dX;
    double dY;
    double dSize;
    double dPageRank;
    std::ifstream ifsDataFile;

    ifsDataFile.open(sALayoutFileName.c_str());
    if (!ifsDataFile) {
        return false;
    }

    ifsDataFile.imbue(std::locale::classic());

    /*Layout file*/
    while (!ifsDataFile.eof()) {
        /*First read as string to search for scape caracter*/
        ifsDataFile >> sTemp;
        if (sTemp.empty()) break;
        if (sTemp[0] == '#') {
            bFileHasNewFormat = true; /*Found header*/
            ifsDataFile.getline(cLineTrash, 100); /*Read and discard full line*/
            continue; /*Found scape caracter, jump to next line*/
        }

        /*Have values now, no scape caracter was found*/
        iNodeId = TConverter<int>::FromString(sTemp); /*Convert initialy read string to int*/
        ifsDataFile >> dX;
        ifsDataFile >> dY;
        ifsDataFile >> dSize;

        /*Files in old format can be read and will receive the default page rank*/
        if (bFileHasNewFormat)
            ifsDataFile >> dPageRank;
        else
            dPageRank = 0.15;

        if (this->IsNode(iNodeId)) {
            this->GetNI(iNodeId)().SetXY(dX, dY);
            this->GetNI(iNodeId)().SetSize(dSize);
            this->GetNI(iNodeId)().SetPageRank(dPageRank);
        }
        iDataCounting++;
        sTemp.clear();
    }
    ifsDataFile.close();
    if (iDataCounting > 0) {
        bLayoutFlag = true;
        return true;
    } else {
        TDialogWarning dwWarning("Layout file does not contain valid data.", "Attention");
        return false;
    }
}

template <class _TNodeData, class _TEdgeData>
bool TNodeNetTests<_TNodeData, _TEdgeData>::WriteLayoutFile() {
    /*If no layout was applied, either read from layout or calculated by
      layout algorithm do not save layout file*/
    if (!bLayoutFlag)
        return false;

    int iDataCounting = 0;
    this->NormalizeAndSetPositions();
    /*Create files*/
    std::ofstream layoutFile(sLayoutFileName.c_str());
    if (!layoutFile) {
        TDialogWarning dwWarning("Cannot create layout file " + sLayoutFileName + ". Please verify path and permissions.", "Attention");
        return false;
    }
    layoutFile << "#lines started by # are ignored\n"
              << "#id x y size pagerank\n";

    // default precision in _lf.txt, but could be up to std::numeric_limits::digits10
    layoutFile.precision(6);
    layoutFile.setf(std::ios::fixed, std::ios::floatfield);

    layoutFile.imbue(std::locale::classic());

    for (TNodeI NI = this->BegNI(); NI < this->EndNI(); NI++) {
        layoutFile << NI.GetId() << ' '
                   << NI().GetX() << ' '
                   << NI().GetY() << ' '
                   << NI().GetSize() << ' '
                   << NI().GetPageRank()
                   << '\n';
        iDataCounting++;
    }
    layoutFile.close();

    if (iDataCounting > 0)
        return true;
    else {
        //TDialogWarning dwWarning("Error: nothing written to file " +sLayoutFileName + ".");
        return false;
    }
}

template <class _TNodeData, class _TEdgeData>
void TNodeNetTests<_TNodeData, _TEdgeData>::GetAllSourceNodes(std::hash_set<int>& hsHashOfDestinationNodes) {
    hsHashOfDestinationNodes.clear();
    for (TEdgeI EI = this->BegEI(); EI != this->EndEI(); EI++) {
        /*std:set won't insert duplicates*/
        hsHashOfDestinationNodes.insert(EI.GetSrcNId());
    }
}

template <class _TNodeData, class _TEdgeData>
void TNodeNetTests<_TNodeData, _TEdgeData>::CopyLabelDataToHashMap(std::hash_map<int, TNodesAuxiliar>& hmAHashMap, void* vASuperNodeEntry) {
    for (TNodeI NI = this->BegNI(); NI < this->EndNI(); NI++) {
#ifndef PERFORMANCE
        hmAHashMap.insert(std::pair<int, TNodesAuxiliar > (NI.GetId(), TNodesAuxiliar(NI().GetLabel(), vASuperNodeEntry)));
#else
        hmAHashMap.insert(std::pair<int, TNodesAuxiliar > (NI.GetId(), TNodesAuxiliar(vASuperNodeEntry)));
#endif
    }
}

template <class _TNodeData, class _TEdgeData>
void TNodeNetTests<_TNodeData, _TEdgeData>::FillNodeNetWithLabelDataFromHashMap(std::hash_map<int, TNodesAuxiliar>& hmAHashMap) {
    for (TNodeI NI = this->BegNI(); NI < this->EndNI(); NI++) {
		std::hash_map<int,TNodesAuxiliar>::iterator it = hmAHashMap.find(NI.GetId());
		if (it != hmAHashMap.end()) {
			NI().SetLabel(it->second.GetLabel());
		}
    }
}

template <class _TNodeData, class _TEdgeData>
bool TNodeNetTests<_TNodeData, _TEdgeData>::GetFirstKnownNeighbor(TNodeI& niANodeI, int& iANodeId) {
    int iNeighbors = niANodeI.GetOutDeg();
    for (int i = 0; i < iNeighbors; i++) {
        iANodeId = niANodeI.GetOutNId(i);
        return true;
    }
    iNeighbors = niANodeI.GetInDeg();
    for (int i = 0; i < iNeighbors; i++) {
        iANodeId = niANodeI.GetInNId(i);
        return true;
    }
    return false;
}

/*------------------------------------------------------------------------------------------*/
#endif
