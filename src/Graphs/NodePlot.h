#pragma once
#ifndef _NodePlot_
#define _NodePlot_

#include <set>
#include <vector>

#include <Commons/Auxiliar.h>

class TNodePlot {
private:
   /*---Descriptive---We save and restore these ones*/
   TFlt X, Y;      /*Drawing normalized coordinates*/
   TFlt Size;       TStr sLabel;
   TShapeType::TShapeAcronysm saShapeType;
   /*Auxiliars*/
   TInt iDeviceX, iDeviceY; /*Device int coordinates (optimization)*/
   int iMETISPartitionNumber;
       /*PageRank data*/
   TFlt dPageRank;
   byte bRed;   byte bGreen;   byte bBlue;
   /*Other auxiliars*/
   bool bVisible;
   /*Virus simulation*/
   bool bInfected;             bool bImmunized;
   int iInfluenceLevel;
   /*For influence calculus*/
   std::set<int> sSonNodes; 
public:
   TNodePlot() : X(0), Y(0), Size(0.01), sLabel("_"), saShapeType(TShapeType::saSquare), iDeviceX(-1),iDeviceY(-1), bInfected(false), bImmunized(false), bVisible(true), iInfluenceLevel(0), dPageRank(0.15), bRed(0), bGreen(255), bBlue(0), iMETISPartitionNumber(-1) {
      sSonNodes.clear();
   };                           
   TNodePlot(TSIn& SIn) :  bInfected(false), bImmunized(false), bVisible(true), iInfluenceLevel(0), dPageRank(0.15), bRed(0), bGreen(255), bBlue(0), iMETISPartitionNumber(-1){
      X = TFlt(SIn);  Y = TFlt(SIn);  Size = TFlt(SIn);   sLabel = TStr(SIn);
      TStr sShapeType(SIn);
      saShapeType = TShapeType::GetShapeAcronym(std::string(sShapeType.CStr()));
      iDeviceX = TInt(SIn); iDeviceY = TInt(SIn);

   };
   std::set<int>& GetSonNodes(){return sSonNodes;};
   /*For influence calculus*/
   int GetInfluenceLevel(){return iInfluenceLevel;}
   void SetInfluenceLevel(int iAInfluenceLevel){iInfluenceLevel = iAInfluenceLevel;};
   bool GetVisible(){return bVisible;}
   void SetVisible(bool bAVisible){bVisible = bAVisible;};
   bool GetInfected(){return bInfected;}
   void SetInfected(bool bAInfected){bInfected = bAInfected;};
   bool GetImmunized(){return bImmunized;}
   void SetImmunized(bool bAImmunized){bImmunized = bAImmunized;};
   int GetNumberOfSons(){ return sSonNodes.size(); };
   double GetDistanceFrom(TNodePlot &npANodePlot);
   void SetRed(byte bARedValue);
   void SetGreen(byte bAGreeValue);
   void SetBlue(byte bABlueValue);
   byte GetRed();
   byte GetGreen();
   byte GetBlue();

   void ClearNodeSons(){this->sSonNodes.clear();};
   void AddSonToInfluenceSet(int iANodeIndex);
   void Save(TSOut& SOut) const;
   TNodePlot& operator = (const TNodePlot& NodePlot);
   void InfectNode();
   PXmlTok GetXmlTok() const;
   TRectangle GetBoundingBox();
   bool IsPointOver(double _X, double _Y);
   void ScaleSize(double dAScaleValue);
   /*Layout data access*/
   void SetXY(double _X, double _Y){
      X.Val = _X;
      Y.Val = _Y; };
   void SetX(double _X){
      X.Val = _X; };
   void SetY(double _Y){
      Y.Val = _Y; };
   void SetSize(const double _Size){
      if(_Size < 0.01)
         this->Size = 0.01;
      else if(_Size > 0.2)
         this->Size = 0.2;
      else
         this->Size = _Size;
   };
   double GetSize(){ return Size.Val; };
   double GetX(){ return X.Val; };
   double GetY(){ return Y.Val; };
   double GetDeviceX(){ return iDeviceX; };
   double GetDeviceY(){ return iDeviceY; };
   void SetDeviceX(int iADeviceX){ iDeviceX = iADeviceX; };
   void SetDeviceY(int iADeviceY){ iDeviceY = iADeviceY; };
   void SetShape(std::string sAShape){
      saShapeType = TShapeType::GetShapeAcronym(sAShape);
   };
   void SetShape(TShapeType::TShapeAcronysm saAShapeType){ saShapeType = saAShapeType; };
   void SetColor(std::string sAColorRGBCode);
   std::string GetColor();

   std::string GetShape(){ return TShapeType::GetShapeString(saShapeType); };
   TShapeType::TShapeAcronysm GetShapeType(){ return saShapeType; };
   std::string GetLabel(){ return std::string(sLabel.CStr()); };
   void SetLabel(std::string sALabel);
   void SetMETISPartitionNumber(int iAMETISPartitionNumber){ iMETISPartitionNumber = iAMETISPartitionNumber; };
   int GetMETISPartitionNumber(){ return iMETISPartitionNumber; };   
   /*PageRank data access*/
   double GetPageRank(){return dPageRank;};
   void SetPageRank(double dAPageRank){dPageRank = dAPageRank;};
};

#endif
