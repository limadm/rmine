// CEPS.h: interface for the CEPS class.
//
//////////////////////////////////////////////////////////////////////
#ifndef _CEPS_
#define _CEPS_

#include <Graphs/NodePlot.h>
#include <Graphs/EdgePlot.h>
#include <Graphs/NodeNetTests.h>

#ifndef BOOL
#define BOOL int
#endif

struct node{
int iThisNodeIndex;
int length;//how many adjacent neighbor
int * index;
float * weight;
};

struct graph{
	int nodenum;
	node * nodelist;
};
struct CepsNode{
int idx;
float * indscore;
int qn;
float comscore;
CepsNode * next;
};
class CEPS{
public:
	CEPS();
	virtual ~CEPS();

	//the variable that should be setup at the very begining;
	int n;//total number of the nodes
	float ** ScoreMatrix;//n x n score matrix
	std::string * AuthorList;//author name list;
	
	/*****************Operation related with graph*/
	graph AdjGraph;//adjacent matrix;
	//if two node is adjacent
	BOOL isAdj(int idx1, int idx2);
	//Return the weight of AdjGraph(i,j)
	float AdjPos(int i, int j);
	/*********************************************/

	/********Operation related with CepsNodes************/
	int CepsNodeLength(CepsNode * CNList);
	BOOL InsertCepsNode(CepsNode * &CNList, CepsNode * CN);
	int GetIndexInHangHangGraph(int iNodeIndex, int iNNodes);
	//
	BOOL CepsNodeExist(CepsNode * CNList, CepsNode * CN);
	//return the point of i th node in CandList;
	CepsNode * CepsNodePos(CepsNode * CNList, int i);
	//return the point of (i-1) th node in CandList;
	CepsNode * CepsNodePre(CepsNode * CNList, int i);
	//Exchange the i^th and j^th nodes in CandList;
	BOOL CepsNodeExchange(CepsNode * CNList, int i,int j);
	//sort CandList according to qidx individual score;
	BOOL SortCepsNode(CepsNode * CNList, int pos, int flag);

	BOOL MergeCepsNode(CepsNode * CNList1, CepsNode * CNList2);
	BOOL PrintCepsNode(CepsNode * CNList);
	//
	CepsNode * CepsNodeNew(int i);
	
	BOOL CepsNodeDelete(CepsNode * &CNList);
	/*********************************************/

	/*******Pre-Load***********************************/
	//Setup n, ScoreMatrix, AuthorList, AdjGraph;
	BOOL Pre_Ceps(std::string sFile, std::string aFile, std::string gFile);
	BOOL ImportFromJureStructure(TPt< TNodeNetTests<TNodePlot, TEdgePlot> > pANodeNetData);
	//Destroy ScoreMatrix, AuthorList, AdjGraph;
	BOOL Post_Ceps();
	/*********************************************/

	//setup at query time
	//int qnum;//query number
	int b;//budget
	int k;//soft-and coefficient
	int qn;//query number
	int CandSetSize;//candidate set size;
	float CandSetThre;
	CepsNode * CandList;//Candidate List for find a path;
	BOOL QueryExist;//Indicate if the query list exist
	int MaxPathLen;//maximum path length
	float **IndScore;//individual score list
	float * ComScore;//combined score list;
	float * SortComScore;//Sorted combined score list;
	int * SortComIdx;//Index of sorted combined score list;
	std::string * QueryList;//query list
	int * QueryInx;//query index;
	CepsNode * CepsIdx;//Output Ceps index;
	CepsNode * PathIdx;//Path Index;
	//int CepsSize;//


	//see if the author is in the author list; return -1 if not exist; else the au position
	int IsAuthor(std::string au);
	//Setup QueryList from the input;
	BOOL SetQueryList(std::string * InputList, int len);
	//QueryList to QueryInx; also setup QueryExist
	//BOOL SetQueryIndex();
	//Setup individual score from querylist;
	BOOL SetIndScore();
	//Combine individual socre
	float CombineScoreOne(float * iScore, int k);
	BOOL CombineScore();
	//setup CandSetThre
	BOOL SetupThre();
	//combine all above modules together: given input query list, setup combine score list and candsetth;
	BOOL SetupComScore(std::string * InputList, int len);


	//see if idx is in the list of ConList
	int IsInList(int * ConList, int len, int val);
	//sort the list, flag==1, maximum first; else, minimun first;
	//and return an index list, indicating which elmement shall be in which place in the ordered list
	int * SortList(float * slist, int len, int flag);
	//Merge two index list (add idx2 into idx1)
	BOOL MergeList(int * idx1, int len1, int * idx2, int len2);


	//Pick up the next most promising node
	//CurSG: is the current partially built subgraph
	int Pick_Up_One(CepsNode * CepsIdx);
	//Generate Candidate List
	//Returning a sorted index for path finding, (Combinded Score>=CandSetThre, and IndScore between that startnode and endnode)
	BOOL Gene_Cand_List(int startnode, int endnode);
	//Find a path according to CandList
	//flag....
	BOOL Find_Next_Path(int flag); 
	BOOL Find_Next_Steiner();

	BOOL BuildCeps();//
	BOOL QueryCeps(std::string * InputList, int len, int budget, int k_soft);//
	BOOL OutputCeps(std::string pre_outfile);

};

#endif
