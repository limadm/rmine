// CEPS.cpp: implementation of the CEPS class.
//
//////////////////////////////////////////////////////////////////////

#include <Commons/stdafx.h>
#include "CEPS.h"

/*#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif*/

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CEPS::CEPS()
{
	CandSetSize = 400;
	QueryExist = FALSE;
	MaxPathLen = 10;
	ScoreMatrix = NULL;
	AuthorList = NULL;
	IndScore = NULL;
	ComScore = NULL;
	SortComScore = NULL;
	SortComIdx = NULL;
	CandList = NULL;
	QueryList = NULL;
	QueryInx = NULL;
	CepsIdx = NULL;
	PathIdx = NULL;

}

CEPS::~CEPS()
{

}

int CEPS::IsAuthor(std::string au)
{
	if(!AuthorList)
		return -1;
	
	for(int i=0;i<n;i++)
	{
		if((*(AuthorList+i)).compare(au) == 0)
			return i;
	}
	return -1;
}
BOOL CEPS::SetQueryList(std::string * InputList, int len)
{
	if(!InputList)
	{
		qn = 0;
		QueryExist = FALSE;
		return FALSE;
	}
	qn = len;
	
	int pos;
	if(QueryInx)
		delete []QueryInx;
	QueryInx = new int[qn];
	for(int i=0;i<qn;i++)
	{
		pos = IsAuthor(*(InputList+i));
		if(pos==-1)
		{
			delete []QueryInx;
			QueryExist = FALSE;
			return FALSE;
		}
		else
			QueryInx[i] = pos;

	}
	QueryExist = TRUE;
	return TRUE;
}
BOOL CEPS::SetIndScore()
{
	//new it in Pre_Ceps//delete it in Post_Ceps
	IndScore = new float * [n];
	for(int i=0;i<n;i++)
	{
		IndScore[i] = new float[qn];
		for(int j=0;j<qn;j++)
			IndScore[i][j] = ScoreMatrix[i][QueryInx[j]];
	}
	return TRUE;
}
int CEPS::GetIndexInHangHangGraph(int iNodeIndex, int iNNodes){
	for(int i=0; i < iNNodes; i++){
		if(AdjGraph.nodelist[i].iThisNodeIndex == iNodeIndex){
			return i;
		}
	}
	return -1;
}
BOOL CEPS::ImportFromJureStructure(TPt< TNodeNetTests<TNodePlot, TEdgePlot> > pANodeNetData){
	int iNextNeighbor,iNeighCounter,iNeighbors;
	int iEdgeIndex, iGraphIndex, iNeighborIndex;
	TNodeNetTests<TNodePlot, TEdgePlot>::TNodeI NI = pANodeNetData->BegNI();
	AdjGraph.nodenum = 13;//pANodeNetData->GetNodes();
	AdjGraph.nodelist = new node [pANodeNetData->GetNodes()];

	if(AuthorList){
		delete []AuthorList;
		AuthorList = NULL;
	}
	n = AdjGraph.nodenum;
	AuthorList = new std::string[n];
	iGraphIndex = 0;
	for(NI = pANodeNetData->BegNI(); NI < pANodeNetData->EndNI(); NI++){
		AdjGraph.nodelist[iGraphIndex].iThisNodeIndex = NI.GetId();
		AdjGraph.nodelist[iGraphIndex].length = NI.GetOutDeg();
		AdjGraph.nodelist[iGraphIndex].weight = new float [NI.GetOutDeg()];
		AdjGraph.nodelist[iGraphIndex].index = new int [NI.GetOutDeg()];
		AuthorList[iGraphIndex] = NI().GetLabel();
		iGraphIndex++;
	}

	for(iGraphIndex=0; iGraphIndex < pANodeNetData->GetNodes(); iGraphIndex++){
		NI = pANodeNetData->GetNI(AdjGraph.nodelist[iGraphIndex].iThisNodeIndex);
		iNeighbors = NI.GetOutDeg();	 iNeighborIndex = 0;
		for(iNeighCounter = 0; iNeighCounter < iNeighbors; iNeighCounter++){
			iNextNeighbor = NI.GetOutNId(iNeighCounter);
			AdjGraph.nodelist[iGraphIndex].index[iNeighborIndex] = GetIndexInHangHangGraph(iNextNeighbor,pANodeNetData->GetNodes());
			pANodeNetData->IsEdge(NI.GetId(),iNextNeighbor,iEdgeIndex);			
			AdjGraph.nodelist[iGraphIndex].weight[iNeighborIndex] = pANodeNetData->GetEI(iEdgeIndex)().GetWeight();
			iNeighborIndex++;
		}
	}

	float weight;
	FILE * fp1= fopen("C:\\Documents and Settings\\junio\\Desktop\\ToJunio\\data\\toy\\sFile", "r");
	if(!fp1)
	{
		fclose(fp1);
		return FALSE;
	}
	//Setup Score matrix
	if(ScoreMatrix)
	{
		for(int i=0;i < n;i++)
			delete []ScoreMatrix[i];
		delete []ScoreMatrix;
	}
	ScoreMatrix = new float *[n];
	for(int i = 0;i < n;i++)
		ScoreMatrix[i] = new float [n];

	fscanf(fp1, "%d\n", &n);

	for(int i = 0; i < n; i++)
	{
		for(int j = 0;j < n; j++)
		{
			fscanf(fp1,"%f ", &weight);
			ScoreMatrix[i][j] = weight;
		}
	}
	fclose(fp1);

	return true;
}
float CEPS::CombineScoreOne(float * iScore, int k)
{
	float Re = 1.0f;
	if(k==qn)//And Query
	{
		for(int i=0;i<qn;i++)
			Re = Re * iScore[i];
	}
	//K_SoftAnd Has not yet implemented//
	return Re;
}

BOOL CEPS::CombineScore()
{
	//new it in Pre_Ceps//delete it in Post_Ceps
	ComScore = new float [n];
	for(int i=0;i<n;i++)
	{
		ComScore[i] = CombineScoreOne(IndScore[i],k);
	}
	return TRUE;
}
BOOL CEPS::SetupThre()
{
	if(SortComScore)
		delete []SortComScore;
 
	SortComScore = new float [n];
	for(int i=0;i<n;i++)
		SortComScore[i] = ComScore[i];
	if(!SortComIdx)
		delete []SortComIdx;
	SortComIdx = SortList(SortComScore,n,1);
	CandSetThre = SortComScore[CandSetSize];

	return TRUE;
}
BOOL CEPS::SetupComScore(std::string * InputList, int len)
{
	if(!SetQueryList(InputList,len))
		return FALSE;
	if(QueryExist)
	{
		SetIndScore();
		CombineScore();
		/***************************************/
		/*for(int i=0;i<n;i++)
		{
			for(int j=0;j<qn;j++)
			{
				printf("%f ",IndScore[i][j]);
			}
			printf("%f\n", ComScore[i]);
		}*/
		/*************************************/

		SetupThre();

		/*for(int i=0;i<n;i++)
		{			
			printf("%d: %f\n", SortComIdx[i], SortComScore[i]);
		}*/

	}

	return TRUE;
}
int CEPS::IsInList(int * ConList, int len, int val)
{
	if(!ConList)
		return -1;
	
	for(int i=0;i<len;i++)
	{
		if(ConList[i]==val)
			return i;
	}
	return -1;
}

int * CEPS::SortList(float * slist, int len, int flag)
{
	if(!slist)
		return NULL;
	
	int * idx = new int [len];

	for(int i=0;i<len;i++)
	{
		idx[i] = i;
	}
	float tmp_val;
	int tmp_idx;
	for(int i=0;i<len-1;i++)
	{
		for(int j=i+1;j<len;j++)
			if(flag==1)//large first
			{
				if(slist[j]>slist[i])
				{
					tmp_val = slist[i];
					slist[i] = slist[j];
					slist[j] = tmp_val;

					tmp_idx = idx[i];
					idx[i] = idx[j];
					idx[j] = tmp_idx;

				}
			}
			else
			{
				if(slist[j]<slist[i])
				{
					tmp_val = slist[i];
					slist[i] = slist[j];
					slist[j] = tmp_val;

					tmp_idx = idx[i];
					idx[i] = idx[j];
					idx[j] = tmp_idx;
				}
			}
	}
	return idx;
}

int CEPS::MergeList(int * idx1, int len1, int * idx2, int len2)
{
	if(!idx1||!idx2)
		return 0;
	int len = 0;
	int tmp;
	int * tmp_idx = new int[len2];
	for(int i=0;i<len2;i++)
	{
		tmp = IsInList(idx1,len1,idx2[i]);
		if(tmp==-1)
		{
			len = len + 1;
			tmp_idx[i] = idx2[i];
		}
	}

	int * idx0 = new int[len1+len];
	for(int i=0;i<len1;i++)
		idx0[i] = idx1[i];
	for(int i=len1;i<len1+len;i++)
		idx0[i] = tmp_idx[i];
	delete []tmp_idx;
	delete []idx1;

	idx1 = idx0;
	return len+len1;
}

//Setup n, ScoreMatrix, AuthorList, AdjGraph;
BOOL CEPS::Pre_Ceps(std::string sFile, std::string aFile, std::string gFile)
{
	
	FILE * fp1= fopen(sFile.c_str(), "r");
	if(!fp1)
	{
		fclose(fp1);
		return FALSE;
	}
	FILE * fp2= fopen(aFile.c_str(), "r");
	if(!fp2)
	{
		fclose(fp2);
		return FALSE;
	}
	FILE * fp3= fopen(gFile.c_str(), "r");
	if(!fp3)
	{
		fclose(fp3);
		return FALSE;
	}

	char au[200];
	int i,j;
	int idx,num;
	float weight;

	//Setup Author list
	fscanf(fp2, "%d\n", &n);
	if(AuthorList){
		delete []AuthorList;
		AuthorList = NULL;
	}
	AuthorList = new std::string[n];
	//for(int i=0;i<n;i++)
	//	AuthorList[i] = "tong";
	for(int i=0;i<n;i++)
	{
		fscanf(fp2, "%s", au);
		//sprintf(AuthorList[i],"%s",au);
		AuthorList[i] = au;
		
	}
	fclose(fp2);
	

	//Setup Graph 
	fscanf(fp3, "%d\n", &n);
	AdjGraph.nodenum = n;
	AdjGraph.nodelist = new node [n];
	for(int i=0;i<n;i++)
	{
		fscanf(fp3, "%d:%d\n", &idx, &num);
		AdjGraph.nodelist[i].length = num;		
		AdjGraph.nodelist[i].weight = new float [num];
		AdjGraph.nodelist[i].index = new int [num];
		for(j=0;j<num;j++)
		{
			fscanf(fp3, "%d:%f ", &idx, &weight);
			AdjGraph.nodelist[i].index[j] = idx-1;
			AdjGraph.nodelist[i].weight[j] = weight;
		}
		fscanf(fp3,"\n");
	}
	fclose(fp3);

	//Setup Score matrix
	if(ScoreMatrix)
	{
		for(int i=0;i<n;i++)
			delete []ScoreMatrix[i];
		delete []ScoreMatrix;
	}
	ScoreMatrix = new float *[n];
	for(int i=0;i<n;i++)
		ScoreMatrix[i] = new float [n];

	fscanf(fp1, "%d\n", &n);

	for(int i=0;i<n;i++)
	{
		for(j=0;j<n;j++)
		{
			fscanf(fp1,"%f ", &weight);
			ScoreMatrix[i][j] = weight;
		}
	}
	fclose(fp1);
	return TRUE;
}
//Destroy ScoreMatrix, AuthorList, AdjGraph;
BOOL CEPS::Post_Ceps()
{
	//delete AuthorList
	delete []AuthorList;
	for(int i=0;i<n;i++)
	{
		delete []AdjGraph.nodelist[i].index;
		delete []AdjGraph.nodelist[i].weight;
	}
	delete [] AdjGraph.nodelist;
	//delete AdjGraph


	//delete Scorematrix;
	if(!ScoreMatrix)
	{
		for(int i=0;i<n;i++)
			delete []ScoreMatrix[i];
		delete []ScoreMatrix;
	}

	return TRUE;
}
int CEPS::Pick_Up_One(CepsNode * CepsIdx)
{
	int CurSGSize = CepsNodeLength(CepsIdx);
	int * CurSG = new int[CurSGSize];
	CepsNode * p = CepsIdx;
	for(int i=0;i<CurSGSize;i++)
	{
		CurSG[i] = p->idx;
		p = p->next;
	}
	for(int i=0;i<CandSetSize;i++)
	{
		if(IsInList(CurSG,CurSGSize,SortComIdx[i])==-1)
		{
			delete []CurSG;
			return SortComIdx[i];
		}
	}
	delete []CurSG;
	return -1;
}
BOOL CEPS::Gene_Cand_List(int startnode, int endnode)
{
	int pos;
	if(CandList)
		CepsNodeDelete(CandList);

	pos = IsInList(QueryInx,qn,startnode);
	if(pos==-1)
		return FALSE;

	CepsNode * p1 = CepsNodeNew(startnode);
	if(p1)
		InsertCepsNode(CandList, p1);
	p1 = CepsNodeNew(endnode);
	if(p1)
		InsertCepsNode(CandList, p1);


	float mn_is,mx_is;
	if(IndScore[startnode][pos]>IndScore[endnode][pos])
	{
		mn_is = IndScore[endnode][pos];
		mx_is = IndScore[startnode][pos];
	}
	else
	{
		mx_is = IndScore[endnode][pos];
		mn_is = IndScore[startnode][pos];
	}
	
	int idx;
	for(int i=0;i<CandSetSize&&i<n;i++)
	{
		idx = SortComIdx[i];
		if(idx!=startnode&&idx!=endnode
			&&IndScore[idx][pos]>=mn_is
			&&IndScore[idx][pos]<=mx_is)
		{
			p1 = CepsNodeNew(idx);
			if(p1)
				InsertCepsNode(CandList, p1);
		}
	}
	//PrintCepsNode(CandList);
	SortCepsNode(CandList, pos,1);
	//PrintCepsNode(CandList);

	//Make sure the endnode is in the END in the case of tie
	int len = CepsNodeLength(CandList);
	p1 = CepsNodePos(CandList,len-1);
	if(p1->idx!=endnode)
	{
		int j;
		CepsNode * p2 = CandList;
		for(int i=0;i<len;i++)
		{
			if(p2->idx==endnode)
			{
				j=i;
				break;
			}
			else
			{
				p2 = p2->next;
			}
		}		
		CepsNodeExchange(CandList,j,len-1);
	}

	return TRUE;
}
BOOL CEPS::Find_Next_Path(int flag)
{

	int len = CepsNodeLength(CandList);
	int i,j,k;
	int kp,st,edd,pt;
	float score;
	int ** Pre_Pos = new int * [MaxPathLen];
	float ** DP_Table = new float * [MaxPathLen];
	for(int i=0;i<MaxPathLen;i++)
	{
		Pre_Pos[i] = new int [len];
		DP_Table[i] = new float [len];
	}
	for(int i=0;i<MaxPathLen;i++)
	{
		for(j=0;j<len;j++)
		{
			Pre_Pos[i][j] = -1;
			DP_Table[i][j]= 0.0f;
		}
	}

	CepsNode * p = CandList, *p1;
	DP_Table[0][0] = CandList->comscore;
	Pre_Pos[0][0] = 0;

	//Setup DP table
	for(i=1;i<len;i++)//for every candidate node
	{
		p = p->next;
		if(CepsNodeExist(CepsIdx,p))
			st = 1;		
		else
			st = 2;
		
		if(i>=MaxPathLen-1)
			edd = MaxPathLen;
		else
			edd = i+1;
		for(k=st;k<=edd;k++)//every possible length of path
		{
			if(st==1)
				kp = k;
			else
				kp = k-1;
			score = 0.0f;
			pt = -1;
			for(j=0;j<i;j++)//everything node before i
			{
				p1 = CepsNodePos(CandList,j);
				if(isAdj(p->idx,p1->idx)&&
					DP_Table[kp-1][j]>score)//is adjacent
				{
					score = DP_Table[kp-1][j];
					pt = j;
				}
			}
			if(score>0)
			{
				DP_Table[k-1][i] = score+p->comscore;
				Pre_Pos[k-1][i] = pt;
			}
		}
	}

	//find max_score/length
	score = 0.0f;
	pt = -1;
	int pl = 0;
	if(flag==0)
		st = 2;
	else
	{
		st = 1;
		for(k=1;k<MaxPathLen;k++)
		{
			if(DP_Table[k][len-1]>0)
				st=2;
		}		
		
	}
	for(k=st;k<=MaxPathLen;k++)
	{
		if(DP_Table[k-1][len-1]/k>score)
		{
			score = DP_Table[k-1][len-1]/k;
			pl = k;
		}
	}
	if(pl==0)
		return FALSE;

	//find path
	if(PathIdx)
	{
		CepsNodeDelete(PathIdx);
		PathIdx = NULL;
	}
	k = pl;
	j = len-1;
	i=0;
	while(i<len)
	{
		i++;
		p = CepsNodePos(CandList,j);
		p = CepsNodeNew(p->idx);
		InsertCepsNode(PathIdx,p);

		if(p->idx==CandList->idx)//find the starting point
			i = len;

		j = Pre_Pos[k-1][j];
		if(!CepsNodeExist(CepsIdx,p))
			k = k-1;
	}

	//PrintCepsNode(PathIdx);


	for(int i=0;i<MaxPathLen;i++)
	{
		delete []Pre_Pos[i];
		delete []DP_Table[i];
	}
	delete []Pre_Pos;
	delete []DP_Table;
	return TRUE;

}
BOOL CEPS::Find_Next_Steiner()
{
	int endnode = Pick_Up_One(CepsIdx);
	if(endnode==-1)
		return FALSE;
	PrintCepsNode(CepsIdx);
	for(int i=0;i<qn;i++)
	{
		Gene_Cand_List(QueryInx[i],endnode);
		/***********************************************/
		/*CepsNode *p = CandList;
		printf("%d\n",p->idx);
		while(p->next)
		{
			p = p->next;
			printf("%d\n",p->idx);

		}*/
		  /***********************************************/
		//PrintCepsNode(CandList);
		Find_Next_Path(i);

		//PrintCepsNode(CepsIdx);
		//PrintCepsNode(PathIdx);
		
		MergeCepsNode(CepsIdx,PathIdx);

		//PrintCepsNode(CepsIdx);
		
		CepsNodeDelete(PathIdx);
	}
	//PrintCepsNode(CepsIdx);
	return TRUE;
}
BOOL CEPS::BuildCeps()
{
	int i;
	CepsNode * p;
	if(CepsIdx)
	{
		CepsNodeDelete(CepsIdx);
		CepsIdx = NULL;
	}
	//Include query node
	for(int i=0;i<qn;i++)
	{
		p = CepsNodeNew(QueryInx[i]);
		InsertCepsNode(CepsIdx,p);
	}
	i = 0;
	while(CepsNodeLength(CepsIdx)<b&&i<=b)
	{
		Find_Next_Steiner();
		i++;
	}

	return TRUE;

}

BOOL CEPS::QueryCeps(std::string * InputList, int len, int budget, int k_soft)
{
	b = budget;
	k = k_soft;


	if(!SetupComScore(InputList,len))
		return FALSE;


	BOOL Re = BuildCeps();

	/******output something*******/
	/******delete something*******/
	return Re;


}

BOOL CEPS::isAdj(int idx1, int idx2)
{
	if(idx1<0||idx1>=n)
		return FALSE;
	if(idx2<0||idx2>=n)
		return FALSE;
	for(int i=0;i < AdjGraph.nodelist[idx1].length; i++)
	{
		if(AdjGraph.nodelist[idx1].index[i]==idx2)
			return TRUE;
	}
	return FALSE;
}

float CEPS::AdjPos(int i, int j)
{
	if(i<0||i>=n)
		return -1.0f;
	if(j<0||j>=n)
		return -1.0f;
	for(int k=0;k<AdjGraph.nodelist[i].length;k++)
	{
		if(AdjGraph.nodelist[i].index[k]==j)
			return AdjGraph.nodelist[i].weight[k];
	}
	return 0.0f;

}

int CEPS::CepsNodeLength(CepsNode * CNList)
{
	if(!CNList)
		return 0;
	CepsNode * cn = CNList;
	int len = 1;
	while(cn->next)
	{
		len = len + 1;
		cn = cn->next;
	}
	return len;
}

BOOL CEPS::InsertCepsNode(CepsNode * &CNList, CepsNode * CN)
{
	int len = CepsNodeLength(CNList);
	if(len==0)
	{
		CNList = CN;
		return TRUE;
	}
	CepsNode * p_cn = CNList;
	while(p_cn->next)
	{		
		p_cn = p_cn->next;
	}
	p_cn->next = CN;
	return TRUE;
}

CepsNode * CEPS::CepsNodePos(CepsNode * CNList, int i)
{
	int len = CepsNodeLength(CNList);
	if(i<0||i>len)
		return NULL;
	CepsNode * p1 = CNList;
	for(int k=0;k<i;k++)
		p1 = p1->next;
	return p1;
}
CepsNode * CEPS::CepsNodePre(CepsNode * CNList,int i)
{
	return CepsNodePos(CNList, i-1);
}
BOOL CEPS::CepsNodeExchange(CepsNode * CNList,int i,int j)
{
	CepsNode * p1 = CepsNodePos(CNList, i);
	if(!p1)
		return FALSE;
	CepsNode * p2 = CepsNodePos(CNList, j);
	if(!p2)
		return FALSE;
	if(i==j)
		return TRUE;

	//exchange index;
	int tmp;
	tmp = p1->idx;
	p1->idx = p2->idx;
	p2->idx = tmp;

	//exchange individual score;
	float t2;
	for(int k=0;k<p1->qn;k++)
	{
		t2 = p1->indscore[k];
		p1->indscore[k] = p2->indscore[k];
		p2->indscore[k] = t2;
	}
	//exchange combine score;
	t2 = p1->comscore;
	p1->comscore = p2->comscore;
	p2->comscore = t2;

	return TRUE;
}

BOOL CEPS::SortCepsNode(CepsNode * CNList, int pos, int flag)
{
	//int pos = IsInList(QueryInx,qn,qidx);
	if(pos<0||pos>qn)
		return FALSE;
	int len = CepsNodeLength(CNList);
	CepsNode  *p1, *p2;
	for(int i=0;i<len-1;i++)
	{
		p1 = CepsNodePos(CNList, i);

		for(int j=i+1;j<len;j++)
		{
			p2 = CepsNodePos(CNList, j);
			if(flag==1)//largest first
			{
				if(p2->indscore[pos]>p1->indscore[pos])
					CepsNodeExchange(CNList, i,j);
			}//small first
			else
			{
				if(p2->indscore[pos]<p1->indscore[pos])
					CepsNodeExchange(CNList, i,j);
			}
		}
	}
	return TRUE;

}
CepsNode * CEPS::CepsNodeNew(int i)
{
	if(i<0||i>n)
		return NULL;
	CepsNode * p1 = new CepsNode;//
	p1->indscore = new float [qn];
	p1->next = NULL;
	p1->idx = i;
	p1->qn = qn;
	for(int j=0;j<qn;j++)
	{
		p1->indscore[j] = IndScore[i][j];
	}
	p1->comscore = ComScore[i];
	p1->next = NULL;
	return p1;
}
BOOL CEPS::CepsNodeExist(CepsNode * CNList, CepsNode * CN)
{
	int len = CepsNodeLength(CNList);
	if(len<=0||!CN)
		return FALSE;
	CepsNode * p = CNList;
	for(int i=0;i<len;i++)
	{
		if(p->idx==CN->idx)
			return TRUE;
		p = p->next;
	}
	return FALSE;		
}
BOOL CEPS::MergeCepsNode(CepsNode * CNList1, CepsNode * CNList2)
{
	int len1 = CepsNodeLength(CNList1);
	if(len1<=0)
		return FALSE;
	int len2 = CepsNodeLength(CNList2);
	if(len2<=0)
		return FALSE;

	CepsNode *p = CNList2;
	CepsNode *p1;
	for(int i=0;i<len2;i++)
	{
		if(!CepsNodeExist(CNList1,p))
		{
			p1 = CepsNodeNew(p->idx);
			InsertCepsNode(CNList1,p1);
		}
		p = p->next;
	}
	return TRUE;
}
BOOL CEPS::CepsNodeDelete(CepsNode * &CNList)
{
	if(!CNList)
		return FALSE;
	int len = CepsNodeLength(CNList);
	CepsNode * p = CNList, *p1;
	while(p->next)
	{
		p1 = p->next;
		//delete p
		delete []p->indscore;
		delete p;
		p = p1;
	}
	delete []p->indscore;
	delete p;
	CNList  = NULL;
	return TRUE;

}
BOOL CEPS::PrintCepsNode(CepsNode * CNList)
{
	if(!CNList)
	{
		printf("empty CepsNode List\n");
		return FALSE;
	}
	int i;
	CepsNode *p = CNList;
	printf("\n\n");
	printf("%d ",p->idx);
	for(int i=0;i<p->qn;i++)
		printf("%f ", p->indscore[i]);
	printf("%f\n", p->comscore);
	while(p->next)
	{
		p = p->next;
		printf("%d ",p->idx);
		for(int i=0;i<p->qn;i++)
			printf("%f ", p->indscore[i]);
		printf("%f\n", p->comscore);
	}
	printf("\n\n");
	return TRUE;
}
BOOL CEPS::OutputCeps(std::string pre_outfile)
{
	if(!CepsIdx)
		return FALSE;

	int len = CepsNodeLength(CepsIdx);
	CepsNode * p;
	//output node file
	std::string NodeFile;
	NodeFile += pre_outfile;
	NodeFile += "_nf.txt";
	FILE * fp = fopen(NodeFile.c_str(), "w");
	if(!fp)
		return FALSE;
	p = CepsIdx;
	for(int i=0;i<len;i++)
	{
		fprintf(fp,"%d %s ", p->idx+1, AuthorList[p->idx].c_str());
		if(IsInList(QueryInx,qn,p->idx)!=-1)
			fprintf(fp,"d 255_000_000\n");
		else
			fprintf(fp,"c 000_000_255\n");
		p = p->next;
	}
	fclose(fp);

	//output edge file
	std::string EdgeFile;
	EdgeFile += pre_outfile;
	EdgeFile += "_ef.txt";
	fp = fopen(EdgeFile.c_str(), "w");
	if(!fp)
		return FALSE;
	CepsNode * p1;
	float weight;
	for(int i=0;i<len;i++)
	{
		p = CepsNodePos(CepsIdx,i);
		for(int j=0;j<len;j++)
		{
			p1 = CepsNodePos(CepsIdx,j);
			weight = AdjPos(p->idx,p1->idx);
			if(weight>0)
				fprintf(fp,"%d %d %d\n",p->idx+1,p1->idx+1,int(weight));
		}
	}	
	fclose(fp);
	return TRUE;
}
