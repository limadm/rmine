#ifdef NOTIGNORE

#include "FrameShowImageNonChild.h"

#include <wx/image.h>

BEGIN_EVENT_TABLE(TFrameShowImageNonChild, wxFrame)
EVT_PAINT(TFrameShowImageNonChild::OnPaint)
EVT_MOVE(TFrameShowImageNonChild::OnMove)
EVT_SIZE(TFrameShowImageNonChild::OnSize)
EVT_SHOW(TFrameShowImageNonChild::OnShow)
EVT_CLOSE(TFrameShowImageNonChild::OnClose)
END_EVENT_TABLE()


TFrameShowImageNonChild::TFrameShowImageNonChild(wxMDIParentFrame* parent, const wxString& title,
                  int iLeft, int iTop, int iWidth, int iHeight, wxWindow *wxwARootWindow)
:wxFrame(parent, wxID_ANY, title, wxPoint(iLeft, iTop),wxSize(iWidth, iHeight)){
   wxwRootWindow = wxwARootWindow;
   fPercentage = 0.95;
   wxmMenuBar = new wxMenuBar();
   this->SetMenuBar(wxmMenuBar);
}

void TFrameShowImageNonChild::Refresh(bool eraseBackground, const wxRect* rect){
   wxFrame::Refresh(false);
}

void TFrameShowImageNonChild::ShowImage(wxString aFilePathString){
#if wxUSE_LIBPNG
   wxImage::AddHandler( new wxPNGHandler );
#endif
   frameImage = new wxImage();
   dc = NULL;
   filePathString = new wxString(aFilePathString);
   this->Show(true);
}

void TFrameShowImageNonChild::CloseWindow(){
   this->Show(false);
   this->Destroy();
}

void TFrameShowImageNonChild::OnClose(wxCloseEvent& event){
   if(wxwRootWindow != NULL){
      wxwRootWindow->Refresh();
      wxwRootWindow->SetFocus();
   }
}
void TFrameShowImageNonChild::OnClickClose(wxCommandEvent& event){
   this->Show(false);
   event.Skip();
}

void TFrameShowImageNonChild::OnShow(wxShowEvent& event){
   frameImage->LoadFile(*filePathString, wxBITMAP_TYPE_PNG);
   if(wxwRootWindow != NULL){
      wxwRootWindow->Refresh();
   }
   this->Refresh();
   event.Skip();
} 
void TFrameShowImageNonChild::OnMove(wxMoveEvent& event){
   if(wxwRootWindow != NULL){
      wxwRootWindow->Refresh();
   }
   this->Refresh();
   event.Skip();
}
void TFrameShowImageNonChild::OnSize(wxSizeEvent& event){
   if(wxwRootWindow != NULL){
      wxwRootWindow->Refresh();
   }
   this->Refresh();
   event.Skip();
}
void TFrameShowImageNonChild::OnPaint(wxPaintEvent& event){
   wxBufferedPaintDC dc( this );
   dc.Clear();
   dc.DrawBitmap(wxBitmap((frameImage->Scale((this->GetSize()).GetWidth(),
                                              (this->GetSize()).GetHeight()*fPercentage))),
                                               0,0, TRUE);
   event.Skip();
}
#endif