#include "FrameShowImage.h"

#include <wx/image.h>

BEGIN_EVENT_TABLE(TFrameShowImage, wxMDIChildFrame)
EVT_PAINT(TFrameShowImage::OnPaint)
EVT_MOVE(TFrameShowImage::OnMove)
EVT_SIZE(TFrameShowImage::OnSize)
EVT_SHOW(TFrameShowImage::OnShow)
EVT_CLOSE(TFrameShowImage::OnClose)
EVT_ACTIVATE(TFrameShowImage::OnActivate)
EVT_KILL_FOCUS(TFrameShowImage::OnLoseFocus)
END_EVENT_TABLE()


TFrameShowImage::TFrameShowImage(wxMDIParentFrame* parent, const wxString& title,
                  int iLeft, int iTop, int iWidth, int iHeight, wxWindow *wxwARootWindow)
:wxMDIChildFrame(parent, wxID_ANY, title, wxPoint(iLeft, iTop),wxSize(iWidth, iHeight)){
   fufcfFunctor = NULL;
   wxwRootWindow = wxwARootWindow;
   fPercentage = 0.95;
   wxmMenuBar = new wxMenuBar();
   this->SetMenuBar(wxmMenuBar);
}

void TFrameShowImage::Refresh(bool eraseBackground, const wxRect* rect){
   wxMDIChildFrame::Refresh(false);
}

void TFrameShowImage::ShowImage(wxString aFilePathString){
#if wxUSE_LIBPNG
   wxImage::AddHandler( new wxPNGHandler );
#endif
   frameImage = new wxImage();
   dc = NULL;
   filePathString = new wxString(aFilePathString);
   this->Show(true);
}

void TFrameShowImage::OnClose(wxCloseEvent& event){
   if(wxwRootWindow != NULL){
      wxwRootWindow->Refresh();
      wxwRootWindow->SetFocus();
      if(fufcfFunctor != NULL)
         fufcfFunctor->ExteriorProcessing(this);
   }
   this->Destroy();
   event.Skip();
}
void TFrameShowImage::OnClickClose(wxCommandEvent& event){
   this->Show(false);
   event.Skip();
}

void TFrameShowImage::OnShow(wxShowEvent& event){
   frameImage->LoadFile(*filePathString, wxBITMAP_TYPE_PNG);
   if(wxwRootWindow != NULL){
      wxwRootWindow->Refresh();
   }
   event.Skip();
} 
void TFrameShowImage::OnMove(wxMoveEvent& event){
   if(wxwRootWindow != NULL){
      wxwRootWindow->Refresh();
   }
   this->Refresh();
   event.Skip();
}
void TFrameShowImage::OnSize(wxSizeEvent& event){
   if(wxwRootWindow != NULL){
      wxwRootWindow->Refresh();
   }
   this->Refresh();
   event.Skip();
}
void TFrameShowImage::OnPaint(wxPaintEvent& event){
   wxBufferedPaintDC dc( this );
   dc.Clear();
   dc.DrawBitmap(wxBitmap((frameImage->Scale((this->GetSize()).GetWidth(),
                                             (this->GetSize()).GetHeight()*fPercentage))),
                                              0,0, TRUE);
   event.Skip();
}
void TFrameShowImage::OnActivate(wxActivateEvent& event){
   this->Refresh();    event.Skip(); 
}
void TFrameShowImage::OnLoseFocus(wxFocusEvent& event){
   if(wxwRootWindow != NULL){
      wxwRootWindow->Refresh();
   }
   event.Skip();
}