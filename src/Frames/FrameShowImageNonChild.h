#pragma once
#ifndef _FrameShowImageNonChild_
#define _FrameShowImageNonChild_

#include <Commons/Auxiliar.h>

class TFrameShowImageNonChild : public wxFrame{
public:
   TFrameShowImageNonChild(wxMDIParentFrame* parent,
                  const wxString& title,
                  int iLeft, int iTop,
                  int iWidth, int iHeight,
                  wxWindow *wxwARootWindow = NULL);
   void ShowImage(wxString aFilePathString);
   void CloseWindow();
private:
   wxWindow *wxwRootWindow;
   wxString *filePathString;
   wxImage *frameImage;
   wxMenuBar* wxmMenuBar;
   wxMenu *wxmFilePlotMenu;
   wxBufferedPaintDC *dc;
   double fPercentage;
   void OnShow(wxShowEvent& event);
   void OnPaint(wxPaintEvent& event);
   void OnMove(wxMoveEvent& event);
   void OnSize(wxSizeEvent& event);
   void OnClose(wxCloseEvent& event);
   void OnClickClose(wxCommandEvent& event);
   virtual void Refresh(bool eraseBackground = true, const wxRect* rect = NULL);   
   DECLARE_EVENT_TABLE()
};

#endif