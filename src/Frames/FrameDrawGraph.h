#pragma once
#ifndef _FrameDrawGraph_
#define _FrameDrawGraph_

#include <Commons/stdafx.h>

#include <vector>

#include <wx/cmndata.h>
#include <wx/button.h>

#include <Commons/Auxiliar.h>
#include <Dialogs/DialogListBoxOptions.h>
#include <Dialogs/DialogChooseColor.h>
#include <Dialogs/DialogSlider.h>
#include <Dialogs/DialogSelectNodes.h>
#include <Dialogs/DialogSelectValue.h>
#include <Drawings/DrawWxWidget.h>
#include <Frames/FrameDrawGraphEventsEnumeration.h>
#include <Layouts/LayoutInterface.h>
#include <Processings/ProcessingForestFire.h>
#include <Processings/ProcessingVirusSimulation.h>
#include <Processings/ProcessingPageRank.h>
#include <Processings/ProcessingMETISPartitioning.h>
#include <Processings/ProcessingConnectionSubGraph.h>
#include <Processings/ProcessingRandomGraphGenerator.h>

#ifdef _ONRESEARCH
#include <Layouts/LayoutHierarchical.h>
#include <Partitioning/Partition.h>
#include <Processings/ProcessingInfluentialHierarchy.h>
#endif

/*********************Call back functors********************/
/*We declare them here so that we can restrict their 
  implementation code to the end of this file*/
template <class _TNodeNet>
class TFunctorGraphUpdaterForSelectValue;

template <class _TNodeNet>
class TFunctorGraphUpdaterForSelectNodes;

template <class _TNodeNet>
class TFunctorHierarchyUpdaterForSlider;

template <class _TNodeNet>
class TFunctorCloseSlider;

template <class _TNodeNet>
class TFunctorSizeUpdaterForSlider;

template <class _TNodeNet>
class TFunctorScaleSizeUpdaterForSlider;

template <class _TNodeNet>
class TFunctorNodeSizeUpdaterForSlider;

template <class _TNodeNet>
class TFunctorShapeUpdaterForListBox;

template <class _TNodeNet>
class TFunctorColorUpdater;

template <class _TNodeNet>
class TFunctorDeleteChildFrame;

template <class _TNodeNet>
class TFunctorTimePointUpdaterForSlider;

template <class _TNodeNet>
class TFunctorUpdaterForRootWindow;

/***********************************************************/

template <typename T>
class PopupTimer : public wxTimer {
public:
   PopupTimer(T * frame) : f(frame) {}
   void Notify() {
      f->HidePopup();
   }
private:
   T * f;
};

/***********************************************************/

template <class _TNodeNet>
class TFrameDrawGraph : public wxMDIChildFrame{
public:
   TDrawInterface< _TNodeNet > *diDrawer;
   /*Comes from TFrameDrawSuperGraph*/
   TFunctorUpdaterForRootWindow<_TNodeNet>* furwDelSuperEdgeGraphWindow;  
public:
   TFrameDrawGraph(wxMDIParentFrame *parent, const wxPoint& wxpAPosition,const wxSize& wxsAsize,
                   TPt<_TNodeNet> pANodeNetData, int iStyle = wxMAXIMIZE, wxWindow *wxwARootWindow = NULL);
   ~TFrameDrawGraph();
   void SetConfirmModificationsDialog(bool bOption);
   void SetGraphData(TPt<_TNodeNet> pANodeNetData);
   void ApplyLayout(TLayoutInterface<_TNodeNet> &gliALayoutImplementation);
   void SetDrawer(TDrawInterface<_TNodeNet>* mdwADrawer);
   void SetSupressDrawing();
   void SetShowLabel(bool bAOption);
   bool Show(bool show = true);
   void ShowPopup();
   void HidePopup();

protected:
   TPt<_TNodeNet> pNodeNetData;
   std::vector<TFrameShowImage *> vListOfChildPlots;
   /*The window which called the creation of this one*/
   wxWindow *wxwRootWindow;

   bool bTranslatingGraph;
   wxCoord wxcOldWidth;               wxCoord wxcOldHeight;
   int iIntialTranlationX;            int iIntialTranlationY;
   int iMousePosX;                    int iMousePosY;
   bool bShowLabels;                  
   bool bConfirmSaveModifications;
      /*Goes to TFrameShowImage*/
   TFunctorDeleteChildFrame<_TNodeNet>* fdcfFunctorDeleteChildFrame;
   TFunctorGraphUpdaterForSelectNodes<_TNodeNet>* fsuTemp;
   /*It is possible to avoid graph drawing, and have the graph used only for processing*/
   bool bSupressDrawing;
   typename _TNodeNet::TNodeI niPickedNode;         bool bWasNodeIdentified;
   typename _TNodeNet::TNodeI niPickedNodeTarget;   bool bWasTargetIdentified;

   wxBitmap  *bDrawBitmap;
   /*Controls*/
   wxMenuBar *menuBar;
   wxMenu *wxmFilePlotMenu;                  wxMenu *wxmActions;
   wxMenu *wxmGraphPlotMenu;                 wxMenu *wxmEditGraph;   
   wxMenu *wxmEditNodes;
   wxPanel *wxpButtonsPanel;                 wxFlexGridSizer *wxfgsButtonsSizer;
   wxBitmapButton *wxbbNode, *wxbbGraph;

   TDialogSlider *fsHierarchySlider;
   TDialogSlider *fsSizingSlider;            TDialogListBoxOptions *dlbListBox;
   TDialogSlider *fsTimeSlider;              TDialogSelectNodes *dsnDialogSelectNodes;
   TDialogSelectValue *dsvDialogSelectValue;
   TDialogChooseColor *dccDialogChooseColor; wxColourData cdColourData;
   
   PopupTimer< TFrameDrawGraph<_TNodeNet> > popupTimer;

protected:
   void Refresh();
   void InitializeControls();
   void FinalizeControls();
   void ApplyHierarchicalLayout();
   void UpdateNodeMenu(bool bAOption);
   void OnClickDegreeDist(wxCommandEvent& event);
   void OnClickCummulativeDegreeDist(wxCommandEvent& event);
   void OnClickHops(wxCommandEvent& event);
   void OnClickWeakComponents(wxCommandEvent& event);
   void OnClickStrongComponents(wxCommandEvent& event);
   void OnClickSingValues(wxCommandEvent& event);
   void OnClickCloseWindow(wxCommandEvent& event){
      event.Skip();
      this->Close();
   };
   void OnClickApplyHierarchicalLayout(wxCommandEvent& event);
   void OnClickShowInfluenceSlider(wxCommandEvent& event);
   void OnClickCalculatePageRank(wxCommandEvent& event);
   void OnClickSubGraphCalculus(wxCommandEvent& event);
   void OnClickGenretaRandomGraph(wxCommandEvent& event);
   void SubGraphCalculusExecution(std::list<std::string> &lAListOfLabels);
   void OnClickReset(wxCommandEvent& event);
   void OnClickForestFire(wxCommandEvent& event);
   void OnClickMETISPartitioning(wxCommandEvent& event);
   void OnClickSimulateViruses(wxCommandEvent& event);
   void OnClickEnableNodesEdition(wxCommandEvent& event);
   void OnClickEnableGraphEdition(wxCommandEvent& event);
   virtual void EnableNodesEdition(wxCommandEvent& event = wxCommandEvent(wxEVT_NULL));
   virtual void EnableGraphEdition(wxCommandEvent& event = wxCommandEvent(wxEVT_NULL));
   void OnClickApplyMETISPartitioningFile(wxCommandEvent& event);
   void OnClickShowGraphData(wxCommandEvent& event);   
   void OnClickResizeNodes(wxCommandEvent& event);
   void OnClickScaleNodes(wxCommandEvent& event);
   void OnClickSetNodeShape(wxCommandEvent& event);
   void OnClickSetNodeColor(wxCommandEvent& event);
   void OnClickSetNodeLabel(wxCommandEvent& event);
   void OnClickSetNodeSize(wxCommandEvent& event);
   virtual void OnPaint(wxPaintEvent& event);
   void OnMouseEvent(wxMouseEvent& event);
   void OnSetFocus(wxFocusEvent& event){ event.Skip(); this->Refresh(); }
   void OnActivate(wxActivateEvent& event){ event.Skip(); this->Refresh(); }
   void OnKillFocus(wxFocusEvent& event);
   void OnMove(wxMoveEvent& event);
   void OnContextMenu(wxContextMenuEvent& event);
   void OnClickHierarchicalPartitioning(wxCommandEvent& event);
   void OnShow(wxShowEvent& event);
   void OnClose(wxCloseEvent& event);
   void OnReSize(wxSizeEvent& event);
   void OnClickNodeButton(wxCommandEvent& event);
   void OnClickGraphButton(wxCommandEvent& event);
   void OnEraseBackGround(wxEraseEvent& event);
   
   DECLARE_EVENT_TABLE()
   
   /****************************************************************************************/
   friend class TFunctorGraphUpdaterForSelectValue<_TNodeNet>;
   friend class TFunctorGraphUpdaterForSelectNodes<_TNodeNet>;
   friend class TFunctorHierarchyUpdaterForSlider<_TNodeNet>;
   friend class TFunctorCloseSlider<_TNodeNet>;
   friend class TFunctorSizeUpdaterForSlider<_TNodeNet>;
   friend class TFunctorScaleSizeUpdaterForSlider<_TNodeNet>;
   friend class TFunctorNodeSizeUpdaterForSlider<_TNodeNet>;
   friend class TFunctorShapeUpdaterForListBox<_TNodeNet>;
   friend class TFunctorColorUpdater<_TNodeNet>;
   friend class TFunctorDeleteChildFrame<_TNodeNet>;
   friend class TFunctorTimePointUpdaterForSlider<_TNodeNet>;
   friend class TFunctorUpdaterForRootWindow<_TNodeNet>;
};

template <class _TNodeNet>
TFrameDrawGraph<_TNodeNet>::TFrameDrawGraph(wxMDIParentFrame *parent, const wxPoint& wxpAPosition,const wxSize& wxsAsize,
                                            TPt<_TNodeNet> pANodeNetData, int iStyle = wxMAXIMIZE, wxWindow *wxwARootWindow = NULL)
:  wxMDIChildFrame(parent,wxID_ANY, "", wxpAPosition,wxsAsize,iStyle),
   popupTimer(PopupTimer< TFrameDrawGraph<_TNodeNet> >(this))
{
   iIntialTranlationX = 0;         iIntialTranlationY = 0;
   pNodeNetData = pANodeNetData;   
   bWasNodeIdentified = false;     bWasTargetIdentified = false;
   bTranslatingGraph = false;      diDrawer = NULL;    
   iMousePosX = 0;                 iMousePosY = 0;
   bShowLabels = false;            bDrawBitmap = (wxBitmap *) NULL; 
   bSupressDrawing = false;        bConfirmSaveModifications = true;
   fdcfFunctorDeleteChildFrame = NULL;
   furwDelSuperEdgeGraphWindow = NULL;
   fsuTemp = NULL;
   fsHierarchySlider = NULL;
   fsTimeSlider = NULL;          fsSizingSlider = NULL;
   dsnDialogSelectNodes = NULL;  dsvDialogSelectValue = NULL;
   dlbListBox = NULL;            dccDialogChooseColor = NULL;
   wxwRootWindow = wxwARootWindow;
   wxpButtonsPanel = NULL;       wxfgsButtonsSizer = NULL;
   wxbbNode = NULL;              wxbbGraph = NULL;
   SetBackgroundStyle(wxBG_STYLE_CUSTOM);
   /*This one cannot go in InitializeControls (don't know why)*/
   dccDialogChooseColor = new TDialogChooseColor(this);
}

template <class _TNodeNet>
TFrameDrawGraph<_TNodeNet>::~TFrameDrawGraph(){
   this->FinalizeControls();
}
template <class _TNodeNet>
bool TFrameDrawGraph<_TNodeNet>::Show(bool show = true){
   bool bShowStatus;
   if(show){
      bShowStatus = wxMDIChildFrame::Show(show);
      if(bShowStatus && show)
         this->InitializeControls();
   }else{  /*Show(false)*/
      bShowStatus = wxMDIChildFrame::Show(show);
   }
   return bShowStatus;
}
template <class _TNodeNet>
void TFrameDrawGraph<_TNodeNet>::SetConfirmModificationsDialog(bool bOption){
   bConfirmSaveModifications = bOption;
}

template <class _TNodeNet>
void TFrameDrawGraph<_TNodeNet>::SetGraphData(TPt<_TNodeNet> pANodeNetData){
   pNodeNetData = pANodeNetData;
   iMousePosX = 0;                 iMousePosY = 0;
}
template <class _TNodeNet>
void TFrameDrawGraph<_TNodeNet>::ApplyLayout(TLayoutInterface<_TNodeNet> &gliALayoutImplementation){
   if(diDrawer == NULL)
      return;
   diDrawer->ApplyLayout(gliALayoutImplementation);
}
template <class _TNodeNet>
void TFrameDrawGraph<_TNodeNet>::SetDrawer(TDrawInterface<_TNodeNet>* mdwADrawer){
   diDrawer = mdwADrawer;
   if(diDrawer != NULL)
      diDrawer->SetFrame(this);
};
template <class _TNodeNet>
void TFrameDrawGraph<_TNodeNet>::SetSupressDrawing(){
   bSupressDrawing = true;
}
template <class _TNodeNet>
void TFrameDrawGraph<_TNodeNet>::SetShowLabel(bool bAOption){
   diDrawer->bShowAllLabels = bAOption;
   wxmEditGraph->Check(DrawGraph_ShowAllLabels, bAOption);
}

template <class _TNodeNet>
void TFrameDrawGraph<_TNodeNet>::Refresh(){
   if((diDrawer == NULL)||(pNodeNetData.GetRefs() <= 0)||bSupressDrawing)
      return;
   wxMDIChildFrame::Refresh();
}

template <class _TNodeNet>
void TFrameDrawGraph<_TNodeNet>::OnPaint(wxPaintEvent& event){
   event.Skip();
   if(diDrawer == NULL)
      return;
   
   wxAutoBufferedPaintDC wxbfpDeviceContext(this);
   wxbfpDeviceContext.SetBackground(*wxWHITE_BRUSH);
   wxbfpDeviceContext.Clear();
   
   wxFont font(9, wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL);
   wxbfpDeviceContext.SetFont(font);

   diDrawer->DrawGraph(&wxbfpDeviceContext);
};

template <class _TNodeNet>
void TFrameDrawGraph<_TNodeNet>::OnKillFocus(wxFocusEvent& event){ 
   event.Skip();   
   this->Refresh();
   if(wxwRootWindow != NULL)
      wxwRootWindow->Refresh();
}
template <class _TNodeNet>
void TFrameDrawGraph<_TNodeNet>::OnMove(wxMoveEvent& event){
   event.Skip();
   this->Refresh();
   if(wxwRootWindow != NULL)
      wxwRootWindow->Refresh();
}

template <class _TNodeNet>
void TFrameDrawGraph<_TNodeNet>::OnContextMenu(wxContextMenuEvent& event){
   event.Skip();
   if(diDrawer == NULL)
      return;

   if(wxmEditGraph->IsChecked(DrawGraph_EnableGraphEdition)){
      PopupMenu(wxmEditGraph);
   }else if(wxmEditNodes->IsChecked(DrawGraph_EnableNodesEdition)){
      PopupMenu(wxmEditNodes);
   }
}

template <class _TNodeNet>
void TFrameDrawGraph<_TNodeNet>::OnMouseEvent(wxMouseEvent& event){
   //event.Skip();
   if(diDrawer == NULL)
      return;
   if (event.LeftDown()){
      /*Graph edition*/
      if(wxmEditGraph->IsChecked(DrawGraph_EnableGraphEdition)){
         if(wxmEditGraph->IsChecked(DrawGraph_Translate)){
            if(!bTranslatingGraph){
               bTranslatingGraph = true;
               iIntialTranlationX = event.GetPosition().x;
               iIntialTranlationY = event.GetPosition().y;
            }
         }else if(wxmEditGraph->IsChecked(DrawGraph_AddEdge)||wxmEditGraph->IsChecked(DrawGraph_DeleteEdge)){
            if(!bWasNodeIdentified){
               bWasNodeIdentified = diDrawer->IdentifyNode(event.GetPosition().x,event.GetPosition().y, niPickedNode);
            }else{
               bWasTargetIdentified = diDrawer->IdentifyNode(event.GetPosition().x,event.GetPosition().y, niPickedNodeTarget);
            }
         }
      }if(wxmEditNodes->IsChecked(DrawGraph_EnableNodesEdition)){
         if(wxmEditNodes->IsChecked(DrawGraph_PickNode)  || wxmEditNodes->IsChecked(DrawGraph_DeleteNode)){
            if(!bWasNodeIdentified)
               bWasNodeIdentified = diDrawer->IdentifyNode(event.GetPosition().x,event.GetPosition().y, niPickedNode);
         }
      }
      if(wxmActions->IsChecked(DrawGraph_SubGraphCalculus)){
         bWasNodeIdentified = diDrawer->IdentifyNode(event.GetPosition().x,event.GetPosition().y, niPickedNode);
         if(bWasNodeIdentified)
            dsnDialogSelectNodes->AddSelectedItemForExhibition(niPickedNode().GetLabel());
      }
      if(bWasNodeIdentified)
         pNodeNetData->SetEmphasizedNode(niPickedNode);
      else {
         typename _TNodeNet::TNodeI end = pNodeNetData->EndNI();
         pNodeNetData->SetEmphasizedNode(end);
      }
   }else if (event.LeftUp()){
      /*Graph edition*/
      if(wxmEditGraph->IsChecked(DrawGraph_EnableGraphEdition)){
         if(wxmEditGraph->IsChecked(DrawGraph_Scale_Up)){
            diDrawer->SetRelativeScale((double)1.1,(double)1.1,
               event.GetPosition().x,event.GetPosition().y);
         }else if(wxmEditGraph->IsChecked(DrawGraph_Scale_Down)){
            diDrawer->SetRelativeScale((double)0.9,(double)0.9,
               event.GetPosition().x,event.GetPosition().y);
         }else if(wxmEditGraph->IsChecked(DrawGraph_Translate)){
            if(bTranslatingGraph){
               bTranslatingGraph = false;
               diDrawer->SetTranslation(event.GetPosition().x - iIntialTranlationX,
                  event.GetPosition().y - iIntialTranlationY);
            }
         }else if(wxmEditGraph->IsChecked(DrawGraph_AddEdge)||wxmEditGraph->IsChecked(DrawGraph_DeleteEdge)){
            if(bWasNodeIdentified && bWasTargetIdentified){
               if(niPickedNode.GetId() != niPickedNodeTarget.GetId()){    /*Different?*/
                  if(wxmEditGraph->IsChecked(DrawGraph_AddEdge)){           /*Add edge*/
                     /*Not an edge yet?*/
                     if(!pNodeNetData->IsEdge(niPickedNode.GetId(),niPickedNodeTarget.GetId())){
                        pNodeNetData->AddEdge(niPickedNode.GetId(),niPickedNodeTarget.GetId());
					 }
                  }else if(wxmEditGraph->IsChecked(DrawGraph_DeleteEdge)){  /*Delete edge*/
                     /*Is it really an edge?*/
                     /*1-2*/
                     if(pNodeNetData->IsEdge(niPickedNode.GetId(),niPickedNodeTarget.GetId()))
                        pNodeNetData->DelEdge(niPickedNode.GetId(),niPickedNodeTarget.GetId(),false);
                     /*2-1*/
                     else if(pNodeNetData->IsEdge(niPickedNodeTarget.GetId(),niPickedNode.GetId()))
                        pNodeNetData->DelEdge(niPickedNodeTarget.GetId(),niPickedNode.GetId(),false);
                  }                  
                  bWasNodeIdentified = false;
                  bWasTargetIdentified = false;
                  typename _TNodeNet::TNodeI end = pNodeNetData->EndNI();
                  pNodeNetData->SetEmphasizedNode(end);
               }
            }
         }
         /*Nodes edition*/
      }else if(wxmEditNodes->IsChecked(DrawGraph_EnableNodesEdition)){
         if(wxmEditNodes->IsChecked(DrawGraph_AddNode)){
            typename _TNodeNet::TNodeI niTemp = pNodeNetData->GetNI(pNodeNetData->AddNode());
            diDrawer->SetNodePosition(niTemp,event.GetPosition().x, event.GetPosition().y);
            niTemp().SetSize(0.03);
         }else if(wxmEditNodes->IsChecked(DrawGraph_PickNode)){
            if(bWasNodeIdentified)
                 diDrawer->SetNodePosition(niPickedNode,event.GetPosition().x,event.GetPosition().y);
         }else if(wxmEditNodes->IsChecked(DrawGraph_DeleteNode)){
            if(bWasNodeIdentified){
               HidePopup();
               pNodeNetData->DelNode(niPickedNode.GetId());
               typename _TNodeNet::TNodeI end = pNodeNetData->EndNI();
               pNodeNetData->SetEmphasizedNode(end);
            }
         }
         bWasNodeIdentified = false;
      }
      this->Refresh();

   }else if(event.Dragging()){
      /*Graph edition*/
      if(wxmEditGraph->IsChecked(DrawGraph_EnableGraphEdition)){
         if(wxmEditGraph->IsChecked(DrawGraph_Translate)){
            if(bTranslatingGraph){
               diDrawer->SetTranslation(event.GetPosition().x - iIntialTranlationX,
                  event.GetPosition().y - iIntialTranlationY);
               iIntialTranlationX = event.GetPosition().x;
               iIntialTranlationY = event.GetPosition().y;
            }
         }
         /*Nodes edition*/
      }else if(wxmEditNodes->IsChecked(DrawGraph_EnableNodesEdition)){
         if(wxmEditNodes->IsChecked(DrawGraph_PickNode)){
            if(bWasNodeIdentified)
               diDrawer->SetNodePosition(niPickedNode,event.GetPosition().x,event.GetPosition().y);
         }
      }
      this->Refresh();
   }else if (event.Moving()){
      bWasNodeIdentified = diDrawer->IdentifyNode(event.GetPosition().x, event.GetPosition().y, niPickedNode);
      if (bWasNodeIdentified) {
         ShowPopup();
         popupTimer.Start(1600, wxTIMER_ONE_SHOT);
      }
   }
   
   if(diDrawer != NULL){
      typename _TNodeNet::TNodeI niTemp;
      /*GetEmphasizedNode returns bool on "Is there emph. node?"*/
      UpdateNodeMenu(pNodeNetData->GetEmphasizedNode(niTemp));
   }
};

template <class _TNodeNet>
void TFrameDrawGraph<_TNodeNet>::UpdateNodeMenu(bool bAOption){
   wxmEditNodes->Enable(DrawGraph_SetNodeShape, bAOption);
   wxmEditNodes->Enable(DrawGraph_SetNodeColor, bAOption);
   wxmEditNodes->Enable(DrawGraph_SetNodeLabel, bAOption);
   wxmEditNodes->Enable(DrawGraph_SetNodeSize, bAOption);
}

template <class _TNodeNet>
void TFrameDrawGraph<_TNodeNet>::OnClickDegreeDist(wxCommandEvent& event){
   event.Skip();
   TFrameShowImage *sifImageFrame = (TFrameShowImage *)diDrawer->DegreeDist((wxMDIParentFrame *)this->GetParent(),this);
   sifImageFrame->fufcfFunctor = this->fdcfFunctorDeleteChildFrame;
   sifImageFrame->fufcfFunctor->SetInterfaceToBeUpdated(this);
   vListOfChildPlots.push_back(sifImageFrame);
};
template <class _TNodeNet>
void TFrameDrawGraph<_TNodeNet>::OnClickCummulativeDegreeDist(wxCommandEvent& event){
   event.Skip();   
   TFrameShowImage *sifImageFrame = (TFrameShowImage *)diDrawer->CummulativeDegreeDist((wxMDIParentFrame *)this->GetParent(),this);
   sifImageFrame->fufcfFunctor = this->fdcfFunctorDeleteChildFrame;
   sifImageFrame->fufcfFunctor->SetInterfaceToBeUpdated(this);
   vListOfChildPlots.push_back(sifImageFrame);
};
template <class _TNodeNet>
void TFrameDrawGraph<_TNodeNet>::OnClickHops(wxCommandEvent& event){
   event.Skip();
   TFrameShowImage *sifImageFrame = (TFrameShowImage *)diDrawer->Hops((wxMDIParentFrame *)this->GetParent(),this);
   sifImageFrame->fufcfFunctor = this->fdcfFunctorDeleteChildFrame;
   sifImageFrame->fufcfFunctor->SetInterfaceToBeUpdated(this);
   vListOfChildPlots.push_back(sifImageFrame);
};
template <class _TNodeNet>
void TFrameDrawGraph<_TNodeNet>::OnClickSingValues(wxCommandEvent& event){
   event.Skip();
   /*TFrameShowImage *sifImageFrame = (TFrameShowImage *)diDrawer->SingValues((wxMDIParentFrame *)this->GetParent(),this);
   sifImageFrame->fufcfFunctor = this->fdcfFunctorDeleteChildFrame;
   sifImageFrame->fufcfFunctor->SetInterfaceToBeUpdated(this);
   vListOfChildPlots.push_back(sifImageFrame);   
   */
};

template <class _TNodeNet>
void TFrameDrawGraph<_TNodeNet>::OnClickWeakComponents(wxCommandEvent& event){
   event.Skip();
   TFrameShowImage *sifImageFrame = (TFrameShowImage *)diDrawer->WeakComponents((wxMDIParentFrame *)this->GetParent(),this);
   sifImageFrame->fufcfFunctor = this->fdcfFunctorDeleteChildFrame;
   sifImageFrame->fufcfFunctor->SetInterfaceToBeUpdated(this);
   vListOfChildPlots.push_back(sifImageFrame);
};

template <class _TNodeNet>
void TFrameDrawGraph<_TNodeNet>::OnClickStrongComponents(wxCommandEvent& event){
   event.Skip();
   TFrameShowImage *sifImageFrame = (TFrameShowImage *)diDrawer->StrongComponents((wxMDIParentFrame *)this->GetParent(),this);
   sifImageFrame->fufcfFunctor = this->fdcfFunctorDeleteChildFrame;
   sifImageFrame->fufcfFunctor->SetInterfaceToBeUpdated(this);
   vListOfChildPlots.push_back(sifImageFrame);
};

template <class _TNodeNet>
void TFrameDrawGraph<_TNodeNet>::OnClickSimulateViruses(wxCommandEvent& event){
   event.Skip();
   TProcessingVirusSimulation<_TNodeNet> pvsVirusSimulation(pNodeNetData);
   if(pvsVirusSimulation.SetParametersVisually()){
      for(int i = 0; i< pvsVirusSimulation.GetTimeSteps(); i++){
         pvsVirusSimulation.StepForwardVirusSimulation();
         wxMilliSleep(400);
         this->Refresh();
      }
   }
}

template <class _TNodeNet>
void TFrameDrawGraph<_TNodeNet>::OnClickForestFire(wxCommandEvent& event){
   event.Skip();
   TProcessingForestFire<TPt <_TNodeNet> > ffTest;
   TPt <_TNodeNet>& pNodeNetTemp = diDrawer->GetPointerToData();
   ffTest.SetGraph(pNodeNetTemp);
   if(ffTest.SetParametersVisually()){
      pNodeNetTemp->SetAllNodesVisibility(false);
      for(int i = 0; i < ffTest.GetTimeSteps(); i++){
         ffTest.StepForwardForestFire();
         pNodeNetTemp->DifferentiateSetOfNodes(ffTest.GetBurnedNodes());
         wxMilliSleep(ffTest.GetFrameToFrameInterval());
         this->Refresh();
      }
      ffTest.PlotFire("Teste", false, true);
   }
   this->Refresh();
}

template <class _TNodeNet>
void TFrameDrawGraph<_TNodeNet>::OnClickMETISPartitioning(wxCommandEvent& event){
   event.Skip();
   TProcessingMETISPartitioning<_TNodeNet> pprProcessingMETISPartitioning(pNodeNetData);
   pprProcessingMETISPartitioning.CallMetisPartitioning();
   this->Refresh();
}

template <class _TNodeNet>
void TFrameDrawGraph<_TNodeNet>::OnClickSetNodeShape(wxCommandEvent& event){
   event.Skip();
   wxArrayString wxsShapeTypes;
   TShapeType::GetShapeNames(wxsShapeTypes);
   TFunctorShapeUpdaterForListBox<_TNodeNet> *fsuflbTemp = new TFunctorShapeUpdaterForListBox<_TNodeNet>();
   dlbListBox->fufsUpdater = fsuflbTemp;
   dlbListBox->fufsUpdater->SetInterfaceToBeUpdated(this);
   dlbListBox->SetOptions(wxsShapeTypes);
   dlbListBox->ShowModal();
   dlbListBox->fufsUpdater = NULL;
   delete fsuflbTemp;
}

template <class _TNodeNet>
void TFrameDrawGraph<_TNodeNet>::OnClickSetNodeLabel(wxCommandEvent& event){
   event.Skip();
   if(diDrawer == NULL)
      return;
   typename _TNodeNet::TNodeI niTemp;
   if(!pNodeNetData->GetEmphasizedNode(niTemp)){
      TDialogWarning dwWarning("Warning: first pick a node using \"Pick Node\" option on \"Edit Graph\" menu.");
      return;
   }
   wxTextEntryDialog dialog(this,_T("Type the new label:"),_T("New label for node"),_T(""),wxOK | wxCANCEL);
   dialog.SetValue((diDrawer->GetLabelFromEmphasizedNode()).c_str());
   if (dialog.ShowModal() == wxID_OK){
      diDrawer->SetLabelForEmphasizedNode(dialog.GetValue().c_str());
      this->Refresh();
   }
}
template <class _TNodeNet>
void TFrameDrawGraph<_TNodeNet>::OnClickSetNodeSize(wxCommandEvent& event){
   event.Skip();
   if(diDrawer == NULL)
      return;
   fsSizingSlider->SetLimits(0, 50);
   fsSizingSlider->SetSliderPosition(diDrawer->GetSizeForEmphasizedNode()*100);
   TFunctorNodeSizeUpdaterForSlider<_TNodeNet>* fnsTemp = new TFunctorNodeSizeUpdaterForSlider<_TNodeNet>();
   fsSizingSlider->fufsUpdater = fnsTemp;
   fsSizingSlider->fufsUpdater->SetInterfaceToBeUpdated(this);
   fsSizingSlider->ShowModal();
   fsSizingSlider->fufsUpdater = NULL;
   delete fnsTemp;   
}

template <class _TNodeNet>
void TFrameDrawGraph<_TNodeNet>::OnClickSetNodeColor(wxCommandEvent& event){
   event.Skip();
   TFunctorColorUpdater<_TNodeNet>* fcuTemp = new TFunctorColorUpdater<_TNodeNet>();
   dccDialogChooseColor->fufsUpdater = fcuTemp;
   dccDialogChooseColor->fufsUpdater->SetInterfaceToBeUpdated(this);
   int iStatus = dccDialogChooseColor->ShowModal();
   if ((iStatus == wxID_OK) ||
       (iStatus == wxID_CANCEL)
       ){
      this->Refresh();
      this->SetFocus();
   }
   dccDialogChooseColor->fufsUpdater = NULL;
   delete fcuTemp;
}
template <class _TNodeNet>
void TFrameDrawGraph<_TNodeNet>::OnClickGenretaRandomGraph(wxCommandEvent& event){
   event.Skip();
   TPt<_TNodeNet> pnnGeneratedGraph(new _TNodeNet());
   TProcessingRandomGraphGenerator< TPt<_TNodeNet> > prggGenerator;   
   prggGenerator.GenerateRandomGraph(pnnGeneratedGraph);
}

template <class _TNodeNet>
void TFrameDrawGraph<_TNodeNet>::OnClickSubGraphCalculus(wxCommandEvent& event){
   event.Skip();
   dsnDialogSelectNodes->fufsUpdater = fsuTemp;
   dsnDialogSelectNodes->fufsUpdater->SetInterfaceToBeUpdated(this);
   dsnDialogSelectNodes->Show(true);
}

template <class _TNodeNet>
void TFrameDrawGraph<_TNodeNet>::SubGraphCalculusExecution(std::list<std::string> &lAListOfLabels){
   if(lAListOfLabels.size() == 0)
      return;
   TFunctorGraphUpdaterForSelectValue<_TNodeNet>* fsgusvTemp = new TFunctorGraphUpdaterForSelectValue<_TNodeNet>();
   dsvDialogSelectValue->fufsUpdater = fsgusvTemp;
   dsvDialogSelectValue->fufsUpdater->SetInterfaceToBeUpdated(this);
   dsvDialogSelectValue->SetValuesRange(1,pNodeNetData->GetNodes());
   dsvDialogSelectValue->ShowModal();
   delete fsgusvTemp;
   dsvDialogSelectValue->fufsUpdater = NULL;
   int iChosenNumberOfItemsForSubGraph = dsvDialogSelectValue->GetValue();

   TProcessingConnectionSubGraph<_TNodeNet> pcsgProcessingConnectionSubGraph(pNodeNetData);
   if(pcsgProcessingConnectionSubGraph.QueryCeps(lAListOfLabels,iChosenNumberOfItemsForSubGraph)){
      wxFileDialog *wxfdChooseSaveFile = new wxFileDialog(this,"Choose/Create a file to save subgraph","","","*.*",wxSAVE);
      wxfdChooseSaveFile->SetFilename("Type_a_file_name_without_extension");
      wxfdChooseSaveFile->ShowModal();
      pcsgProcessingConnectionSubGraph.OutputCeps(wxfdChooseSaveFile->GetPath().c_str());
      this->Refresh();
   }else{
      TDialogWarning dwWarning("Warning: subgraph not created.");
   }
}

template <class _TNodeNet>
void TFrameDrawGraph<_TNodeNet>::OnClickReset(wxCommandEvent& event){
   event.Skip();
   pNodeNetData->Reset(true);
   this->Refresh();
}

template <class _TNodeNet>
void TFrameDrawGraph<_TNodeNet>::OnClickCalculatePageRank(wxCommandEvent& event){
   event.Skip();
   TDialogWarning dwWarning("Warning: the page rank will perform 20 iterations. If it does not converge, try runing this function again. The processing may take a while to finish.");
   TProcessingPageRank<_TNodeNet> pprProcessingPageRank(pNodeNetData);
   pprProcessingPageRank.ComputePageRank();
   TDialogWarning dwDoneWarning("Done!");
}

template <class _TNodeNet>
void TFrameDrawGraph<_TNodeNet>::OnClickShowInfluenceSlider(wxCommandEvent& event){
event.Skip();
#ifdef _ONRESEARCH
   if(diDrawer == NULL)
      return;
   if(!pNodeNetData->bInfluenceDraw){
      /*First, page rank*/
      TProcessingPageRank<_TNodeNet> pprProcessingPageRank(pNodeNetData);
      pprProcessingPageRank.ComputePageRank();
      /*Hierarchical Influence and Exhibition level*/      
      TProcessingInfluentialHierarchy<_TNodeNet> pihProcessingInfluentialHierarchy(pNodeNetData);
      pihProcessingInfluentialHierarchy.ComputeNodeSetInfluence();
      diDrawer->SetExibitionLevel(pNodeNetData->GetMaxInfluenceLevel());      
   }
   fsHierarchySlider->SetLimits(0, 100);
   fsHierarchySlider->SetSliderPosition(0);
   fsHierarchySlider->ShowModal();
   pNodeNetData->Reset(false);
   this->Refresh();
#endif
}

template <class _TNodeNet>
void TFrameDrawGraph<_TNodeNet>::OnClickResizeNodes(wxCommandEvent& event){
   event.Skip();
   fsSizingSlider->SetLimits(0, 50);
   fsSizingSlider->SetSliderPosition(pNodeNetData->GetAllNodesSize()*100);
   TFunctorSizeUpdaterForSlider<_TNodeNet>* fsuTemp = new TFunctorSizeUpdaterForSlider<_TNodeNet>();
   fsSizingSlider->fufsUpdater = fsuTemp;
   fsSizingSlider->fufsUpdater->SetInterfaceToBeUpdated(this);
   fsSizingSlider->ShowModal();
   fsSizingSlider->fufsUpdater = NULL;
   delete fsuTemp;
}

template <class _TNodeNet>
void TFrameDrawGraph<_TNodeNet>::OnClickScaleNodes(wxCommandEvent& event){
   event.Skip();
   fsSizingSlider->SetLimits(0, 100, 1);
   fsSizingSlider->SetSliderPosition(1);
   TFunctorScaleSizeUpdaterForSlider<_TNodeNet>* fsuTemp = new TFunctorScaleSizeUpdaterForSlider<_TNodeNet>();
   fsSizingSlider->fufsUpdater = fsuTemp;
   fsSizingSlider->fufsUpdater->SetInterfaceToBeUpdated(this);
   fsSizingSlider->ShowModal();
   fsSizingSlider->fufsUpdater = NULL;
   delete fsuTemp;
}

template <class _TNodeNet>
void TFrameDrawGraph<_TNodeNet>::OnClickApplyHierarchicalLayout(wxCommandEvent& event){
   event.Skip();
   ApplyHierarchicalLayout();
}

template <class _TNodeNet>
void TFrameDrawGraph<_TNodeNet>::ApplyHierarchicalLayout(){
#ifdef _ONRESEARCH
   TLayoutHierarchical<_TNodeNet> hlTemp;
   hlTemp.ApplyLayout(pNodeNetData);
   diDrawer->UpadteGraphDeviceCoordinates();
   this->Refresh();
#endif
}

template <class _TNodeNet>
void TFrameDrawGraph<_TNodeNet>::OnClickNodeButton(wxCommandEvent& event){
   event.Skip();
   EnableNodesEdition();
}

template <class _TNodeNet>
void TFrameDrawGraph<_TNodeNet>::OnClickGraphButton(wxCommandEvent& event){
   event.Skip();
   EnableGraphEdition();
}

template <class _TNodeNet>
void TFrameDrawGraph<_TNodeNet>::OnEraseBackGround(wxEraseEvent& event){
   /*This empty event is necessary for avoiding the flickering effect
     Do not remove it nor put event.skip() here.*/
}

template <class _TNodeNet>
void TFrameDrawGraph<_TNodeNet>::OnClickEnableNodesEdition(wxCommandEvent& event){
   event.Skip();
   EnableNodesEdition(event);
}
template <class _TNodeNet>
void TFrameDrawGraph<_TNodeNet>::OnClickEnableGraphEdition(wxCommandEvent& event){
   event.Skip();
   EnableGraphEdition(event);
}
template <class _TNodeNet>
void TFrameDrawGraph<_TNodeNet>::EnableNodesEdition(wxCommandEvent& event){
   /*Enable/disable complimentary functionalities*/
   wxmEditNodes->Check(DrawGraph_EnableNodesEdition, true);
   wxmEditGraph->Check(DrawGraph_EnableGraphEdition, false);
   /*Reset emphasized node to prevent unpredicted behavior*/
   typename _TNodeNet::TNodeI end = pNodeNetData->EndNI();
   pNodeNetData->SetEmphasizedNode(end);

   wxbbNode->Disable();
   wxbbGraph->Enable();

   if(event.GetEventType() != wxEVT_NULL){
      /*CheckItem special case treatment to work for menu and popmenu*/
      if(event.GetId() == DrawGraph_ShowLabels){
         bShowLabels = !bShowLabels;
         wxmEditNodes->Check(DrawGraph_ShowLabels, bShowLabels);
      }else{ /*Allways set radio items (all others)*/
         wxmEditNodes->Check(event.GetId(), true);
      }
   }
   menuBar->Refresh();
}
template <class _TNodeNet>
void TFrameDrawGraph<_TNodeNet>::EnableGraphEdition(wxCommandEvent& event){
   if(diDrawer == NULL)
      return;
   /*Enable/disable complimentary functionalities*/
   wxmEditGraph->Check(DrawGraph_EnableGraphEdition, true);
   wxmEditNodes->Check(DrawGraph_EnableNodesEdition, false);
   /*Reset auxiliar data to avoid unpredicted behavior*/
   typename _TNodeNet::TNodeI end = pNodeNetData->EndNI();
   pNodeNetData->SetEmphasizedNode(end);
   bWasNodeIdentified = false;     bWasTargetIdentified = false;
   
   wxbbNode->Enable();
   wxbbGraph->Disable();

   if(event.GetEventType() != wxEVT_NULL){
      /*CheckItem special case treatment to work for menu and popmenu*/
      if(event.GetId() == DrawGraph_ShowAllLabels){
         diDrawer->bShowAllLabels = !diDrawer->bShowAllLabels;
         wxmEditGraph->Check(DrawGraph_ShowAllLabels, diDrawer->bShowAllLabels);
         this->Refresh();
      }else if(event.GetId() == DrawGraph_ShowEdgeWeights){
         diDrawer->bShowWeights = !diDrawer->bShowWeights;
         wxmEditGraph->Check(DrawGraph_ShowEdgeWeights, diDrawer->bShowWeights);
         this->Refresh();   
      }else /*Allway set radio items (else)*/
         wxmEditGraph->Check(event.GetId(), true);
   }
   menuBar->Refresh();
}

template <class _TNodeNet>
void TFrameDrawGraph<_TNodeNet>::OnClickApplyMETISPartitioningFile(wxCommandEvent& event){
   event.Skip();
#ifdef _ONRESEARCH
   TProcessingMETISPartitioning<_TNodeNet> pprProcessingMETISPartitioning(pNodeNetData);
   pprProcessingMETISPartitioning.ApplyMETISPartitionFile();
   this->Refresh();
#endif
}

template <class _TNodeNet>
void TFrameDrawGraph<_TNodeNet>::OnClickShowGraphData(wxCommandEvent& event){
   event.Skip();
   TDialogWarning dwDoneWarning("Graph data: " + TConverter<int>::ToString(pNodeNetData->GetNodes()) + " nodes and " + TConverter<int>::ToString(pNodeNetData->GetEdges()) + " edges");
}

template <class _TNodeNet>
void TFrameDrawGraph<_TNodeNet>::ShowPopup() {
   if (diDrawer != NULL) {
      /*Draw the edges and their weights and the node data*/
      diDrawer->SetShowNeighborHoodDetails(true,
                                           wxmEditNodes->IsChecked(DrawGraph_ShowLabels),
                                           niPickedNode);
      Refresh();  /*Draw nodes only*/
   }
}

template <class _TNodeNet>
void TFrameDrawGraph<_TNodeNet>::HidePopup() {
   if (diDrawer != NULL) {
      /*If nodes edition is off, get out*/
      diDrawer->SetShowNeighborHoodDetails(false,
                                           false,
                                           niPickedNode);
      Refresh(); /*Draw nodes only*/
   }
}

template <class _TNodeNet>
void TFrameDrawGraph<_TNodeNet>::InitializeControls(){
   fdcfFunctorDeleteChildFrame = new TFunctorDeleteChildFrame<_TNodeNet>();
   fsuTemp = new TFunctorGraphUpdaterForSelectNodes<_TNodeNet>();
   /*Sliders and other controls initialization*/
   wxSize sSizeOfThis = this->GetSize(); 

   fsHierarchySlider = new TDialogSlider(this, "Set hierarchy shift", wxPoint(sSizeOfThis.GetWidth()*0.65,sSizeOfThis.GetHeight()*0.1));
   fsHierarchySlider->fufsUpdater = new TFunctorHierarchyUpdaterForSlider<_TNodeNet>();
   fsHierarchySlider->fufsUpdater->SetInterfaceToBeUpdated(this);
   fsHierarchySlider->fufsOnCloseUpdater = new TFunctorCloseSlider<_TNodeNet>();
   fsHierarchySlider->fufsOnCloseUpdater->SetInterfaceToBeUpdated(this);

   fsTimeSlider = new TDialogSlider(this, "Set time point", wxPoint(sSizeOfThis.GetWidth()*0.1,sSizeOfThis.GetHeight()*0.1));
   fsTimeSlider->fufsUpdater = new TFunctorTimePointUpdaterForSlider<_TNodeNet>();
   fsTimeSlider->fufsUpdater->SetInterfaceToBeUpdated(this);

   fsSizingSlider = new TDialogSlider(this, "Set nodes size");
   dlbListBox = new TDialogListBoxOptions(this, "Choose one option");

   dsnDialogSelectNodes = new TDialogSelectNodes(this, "Select nodes");
   dsvDialogSelectValue = new TDialogSelectValue(this, "Select max number of nodes for subgraph");

   /*Menus*/
   menuBar = new wxMenuBar();
   wxmFilePlotMenu = new wxMenu();
   wxmFilePlotMenu->Append(DrawGraph_CloseWindow, _T("&Close window"), _T("Close Window"));

   wxmGraphPlotMenu = new wxMenu();
   wxmGraphPlotMenu->Append(DrawGraph_DegreeDist, _T("&Degree Distribution"), _T("Plot degree distribution"));
   wxmGraphPlotMenu->Append(DrawGraph_CummDegreeDist, _T("&Cummulative Degree Distribution"), _T("Plot cummulative degree distribution"));
   wxmGraphPlotMenu->Append(DrawGraph_Hops, _T("&Hops"), _T("Plot hops"));
   wxmGraphPlotMenu->Append(DrawGraph_WeakComponents, _T("&Weak Components"), _T("Plot weak components"));
   wxmGraphPlotMenu->Append(DrawGraph_StrongComponents, _T("&Strong Components"), _T("Plot strong components"));
   //         wxmGraphPlotMenu->Append(DrawGraph_SingValues, _T("&Singular values"), _T("Plot singular values"));   

   wxmActions = new wxMenu();
#ifdef _ONRESEARCH
   wxmActions->Append(DrawGraph_InfluenceSlider, _T("&Influence layout\tAlt-h"), _T("Influence layout"));
   wxmActions->Append(DrawGraph_HierarchicalPartitioning, _T("&Hierarchical Partitioning"), _T("Hierarchical Partitioning"));
#endif
   wxmActions->Append(DrawGraph_SimulateViruses, _T("&Simulate viruses\tAlt-v"), _T("Simulate viruses"));
   wxmActions->Append(DrawGraph_ForestFire, _T("&Forest fire\tAlt-f"), _T("Forest fire"));
   wxmActions->Append(DrawGraph_METIS_Partitioning, _T("&METIS partitioning\tAlt-n"), _T("Calculate page rank"));
   wxmActions->Append(DrawGraph_CalculatePageRank, _T("&Calculate page rank\tAlt-p"), _T("Calculate page rank"));
   //         wxmActions->Append(DrawGraph_HierarchicalLayout, _T("&Hierarchical layout\tAlt-h"), _T("Hierarchical layout"));
   wxmActions->Append(DrawGraph_SubGraphCalculus, _T("&SubGraph calculus"), _T("SubGraph calculus"));
   wxmActions->Append(DrawGraph_GenerateRandomGraph, _T("&Generate random graph"), _T("Generate random graph"));
   wxmActions->Append(DrawGraph_Reset, _T("&Reset graph visualization\tAlt-r"), _T("Reset graph visualization"));

   wxmEditNodes = new wxMenu();
   wxmEditNodes->AppendCheckItem(DrawGraph_EnableNodesEdition, _T("&Enable nodes edition"), _T("Enable nodes edition"));
   wxmEditNodes->Check(DrawGraph_EnableNodesEdition,true);
   wxmEditNodes->AppendSeparator();
   wxmEditNodes->AppendRadioItem(DrawGraph_PickNode, _T("&Pick node\tShift-p"), _T("Pick node"));
   wxmEditNodes->Check(DrawGraph_PickNode, true);
   wxmEditNodes->AppendRadioItem(DrawGraph_AddNode, _T("&Add node\tShift-a"), _T("Add node"));
   wxmEditNodes->AppendRadioItem(DrawGraph_DeleteNode, _T("&Delete node\tShift-d"), _T("Delete node"));
#ifdef _ONRESEARCH
   //wxmEditNodes->AppendRadioItem(DrawGraph_InfectNode, _T("&Infect node\tShift-i"), _T("Infect node"));
#endif
   wxmEditNodes->AppendSeparator();
   wxmEditNodes->Append(DrawGraph_SetNodeShape, _T("&Set node shape\tShift-f"), _T("Set node shape"));
   wxmEditNodes->Append(DrawGraph_SetNodeColor, _T("&Set node color\tShift-c"), _T("Set node color"));
   wxmEditNodes->Append(DrawGraph_SetNodeLabel, _T("&Set node label\tShift-l"), _T("Set node label"));
   wxmEditNodes->Append(DrawGraph_SetNodeSize, _T("&Set node size\tShift-s"), _T("Set node size"));
   wxmEditNodes->AppendCheckItem(DrawGraph_ShowLabels, _T("&Show labels\tShift-e"), _T("Show labels"));

   wxmEditGraph = new wxMenu();
   wxmEditGraph->AppendCheckItem(DrawGraph_EnableGraphEdition, _T("&Enable graph edition"), _T("Enable graph edition"));
   wxmEditGraph->Check(DrawGraph_EnableGraphEdition,false);
   wxmEditGraph->AppendSeparator();
   wxmEditGraph->AppendRadioItem(DrawGraph_Translate, _T("&Translate graph\tCtrl-t"), _T("Translate graph"));
   wxmEditGraph->Check(DrawGraph_Translate, true);
   wxmEditGraph->AppendRadioItem(DrawGraph_Scale_Down, _T("&Scale down graph\tCtrl-o"), _T("Scale down graph"));
   wxmEditGraph->AppendRadioItem(DrawGraph_Scale_Up, _T("&Scale up graph\tCtrl-i"), _T("Scale up graph"));
   wxmEditGraph->AppendRadioItem(DrawGraph_AddEdge, _T("&Add edge\tCtrl-e"), _T("Add edge"));
   wxmEditGraph->AppendRadioItem(DrawGraph_DeleteEdge, _T("&Delete edge\tCtrl-g"), _T("Delete edge"));
   wxmEditGraph->AppendSeparator();
   wxmEditGraph->Append(DrawGraph_ScaleAllNodesSize, _T("&Scale nodes\tCtrl-s"), _T("Scale nodes"));
   wxmEditGraph->Append(DrawGraph_ResizeNodes, _T("&Resize nodes\tCtrl-r"), _T("Resize nodes"));
   wxmEditGraph->AppendCheckItem(DrawGraph_ShowAllLabels, _T("&Show all labels\tCtrl-a"), _T("Show all labels"));
   wxmEditGraph->AppendCheckItem(DrawGraph_ShowEdgeWeights, _T("&Show edge weights\tCtrl-w"), _T("Show edge weights"));
   wxmEditGraph->Append(DrawGraph_ApplyMETISPartitioningFile, _T("&Apply METIS partitioning file\tCtrl-n"), _T("Apply METIS partitioning file"));
   wxmEditGraph->Append(DrawGraph_ShowGraphData, _T("&Show graph data\tCtrl-d"), _T("Show graph data"));

   menuBar->Append(wxmFilePlotMenu, _T("&File"));
   menuBar->Append(wxmGraphPlotMenu, _T("&Plot"));
   menuBar->Append(wxmActions, _T("&Actions"));
   menuBar->Append(wxmEditNodes, _T("&Nodes"));
   menuBar->Append(wxmEditGraph, _T("&Graph"));
   
   this->SetMenuBar(menuBar);
   this->SetTitle("RMine System");
   
   wxpButtonsPanel = new wxPanel(this,wxID_ANY,wxDefaultPosition,wxDefaultSize,wxNO_BORDER);
   wxpButtonsPanel->SetOwnBackgroundColour(*wxWHITE);
   wxfgsButtonsSizer = new wxFlexGridSizer(1, 3, 10, 10);
   wxpButtonsPanel->SetSizer(wxfgsButtonsSizer);

   wxbbNode = new wxBitmapButton(
	   wxpButtonsPanel,
	   DrawGraph_NodeButton,
	   wxBitmap("assets/Node.bmp",wxBITMAP_TYPE_BMP),
	   wxDefaultPosition,
	   wxDefaultSize,
	   wxNO_BORDER);
   wxbbNode->SetBitmapDisabled(wxBitmap("assets/NodeActive.bmp",wxBITMAP_TYPE_BMP));
   wxbbNode->Disable();

   wxbbGraph = new wxBitmapButton(
	   wxpButtonsPanel,
	   DrawGraph_GraphButton,
	   wxBitmap("assets/Graph.bmp",wxBITMAP_TYPE_BMP),
	   wxDefaultPosition,
	   wxDefaultSize,
	   wxNO_BORDER);
   wxbbGraph->SetBitmapDisabled(wxBitmap("assets/GraphActive.bmp",wxBITMAP_TYPE_BMP));
   
   wxfgsButtonsSizer->Add(wxbbNode,wxALIGN_CENTER);
   wxfgsButtonsSizer->Add(wxbbGraph,wxALIGN_CENTER);

   wxpButtonsPanel->SetAutoLayout(true);
   
   this->SetAutoLayout(true);
   this->Layout();
   wxpButtonsPanel->SetSize(wxSize(wxbbNode->GetSize().GetWidth()+wxbbGraph->GetSize().GetWidth() + 20,
                                   wxbbNode->GetSize().GetHeight()));
   wxpButtonsPanel->Move(this->GetClientSize().GetWidth()-wxpButtonsPanel->GetSize().GetWidth()-10,
                         this->GetClientSize().GetHeight()-wxpButtonsPanel->GetSize().GetHeight()-10);

}

template <class _TNodeNet>
void TFrameDrawGraph<_TNodeNet>::FinalizeControls(){
   if(fdcfFunctorDeleteChildFrame !=NULL) delete fdcfFunctorDeleteChildFrame;
   if(fsuTemp != NULL) delete fsuTemp;   
   if(fsHierarchySlider != NULL)          delete fsHierarchySlider;
   if(fsTimeSlider != NULL)               delete fsTimeSlider;
   if(fsSizingSlider != NULL)             delete fsSizingSlider;
   if(dsvDialogSelectValue != NULL)       delete dsvDialogSelectValue;
   if(dsnDialogSelectNodes != NULL)       delete dsnDialogSelectNodes;
   if(dlbListBox != NULL)                 delete dlbListBox;
   if(dccDialogChooseColor != NULL)       delete dccDialogChooseColor;
   /*Menus - parent wxWindow is responsible for menus deletion*/
}

template <class _TNodeNet>
void TFrameDrawGraph<_TNodeNet>::OnShow(wxShowEvent& event){
   event.Skip();
   if(diDrawer == NULL)
      return;
   wxcOldWidth = this->GetClientSize().GetWidth()*0.90;
   wxcOldHeight = this->GetClientSize().GetHeight()*0.90;
   diDrawer->SetWidthAndHeight(wxcOldWidth, wxcOldHeight);
   /*Scale down after initial presentation because coordinates
     were calculated using the boundaries of the window. It causes
     the boundaries of the graph to be hidden.*/
   diDrawer->SetRelativeScale((double)0.9,(double)0.9,
                              wxcOldWidth/2,wxcOldHeight/2);
#ifndef MDIFRAMES
   GetParent()->Hide();
#endif
}

template <class _TNodeNet>
void TFrameDrawGraph<_TNodeNet>::OnClose(wxCloseEvent& event){
   event.Skip();
   /*Interrupt popup thread*/
   HidePopup();
   /*Confirm to save modifications - only for draw graph mode*/
   if(bConfirmSaveModifications){
      wxMessageDialog dialog(NULL, _T("Do you want to automatically save your edition and layout information?"),
         _T("Confirmation:"), wxNO_DEFAULT|wxYES_NO);

      switch ( dialog.ShowModal() )
      {
      case wxID_YES:
         pNodeNetData->WriteEdgeFile();
         pNodeNetData->WriteNodesFile();
         pNodeNetData->WriteLayoutFile();
         #ifdef _DEBUG
           /*Outputs edge file in Hanghang's CEPs format*/
           //pNodeNetData->WriteHanghangEdgeFile("Etc/HanghangEdgeFile.txt");
         #endif
         break;
      case wxID_NO:
         break;
      }
   }
   /*Close ploting windows*/
   std::vector<TFrameShowImage *>::iterator vIterator;
   for(vIterator = vListOfChildPlots.begin(); vIterator < vListOfChildPlots.end(); ++vIterator) {
      (*vIterator)->Show(false);
   }
#ifndef MDIFRAMES
   GetParent()->Show();
   GetParent()->Raise();
#endif
   /*Call exterior deletion procedure*/
   if(furwDelSuperEdgeGraphWindow != NULL)
      this->furwDelSuperEdgeGraphWindow->ExteriorProcessing(this);   
   this->Restore();
   this->Destroy();
   /*Refresh root window*/
   if(wxwRootWindow != NULL)
      wxwRootWindow->Refresh();
}

template <class _TNodeNet>
void TFrameDrawGraph<_TNodeNet>::OnReSize(wxSizeEvent& event){
   event.Skip();
   if(diDrawer == NULL)
      return;
   wxMDIChildFrame::OnSize(event);

   diDrawer->SetAbsoluteScale(((double)this->GetSize().GetWidth())/(double)wxcOldWidth,
      ((double)this->GetSize().GetHeight())/(double)wxcOldHeight);
   wxcOldWidth = this->GetSize().GetWidth();
   wxcOldHeight = this->GetSize().GetHeight();
   if(wxpButtonsPanel != NULL){
      wxpButtonsPanel->Move(this->GetClientSize().GetWidth()-wxpButtonsPanel->GetSize().GetWidth()-10,
                            this->GetClientSize().GetHeight()-wxpButtonsPanel->GetSize().GetHeight()-10);
   }
   this->Refresh();
}
template <class _TNodeNet>
void TFrameDrawGraph<_TNodeNet>::OnClickHierarchicalPartitioning(wxCommandEvent& event){
   event.Skip();
#ifdef _ONRESEARCH
   TPartition<_TNodeNet> pPartition(pNodeNetData);
   pPartition.PartitionData();
#endif
   this->Refresh();
}

/********wxWidgets stuff, ignore if you do not intend to deal with interface events********/
BEGIN_EVENT_TABLE_TEMPLATE1(TFrameDrawGraph, wxMDIChildFrame, T)
   
   /*All mouse events to the same function*/
   EVT_LEFT_DOWN ( TFrameDrawGraph<T>::OnMouseEvent )
   EVT_LEFT_UP   ( TFrameDrawGraph<T>::OnMouseEvent )
   EVT_MOTION    ( TFrameDrawGraph<T>::OnMouseEvent )
   
   /*Window events*/
   EVT_PAINT      ( TFrameDrawGraph<T>::OnPaint     )
   EVT_SIZE       ( TFrameDrawGraph<T>::OnReSize    )
   EVT_SHOW       ( TFrameDrawGraph<T>::OnShow      )
   EVT_MOVE       ( TFrameDrawGraph<T>::OnMove      )
   EVT_CLOSE      ( TFrameDrawGraph<T>::OnClose     )
   EVT_SET_FOCUS  ( TFrameDrawGraph<T>::OnSetFocus  )
   EVT_KILL_FOCUS ( TFrameDrawGraph<T>::OnKillFocus )
   EVT_ACTIVATE   ( TFrameDrawGraph<T>::OnActivate  )
   
   /*Menu events*/
   EVT_MENU ( DrawGraph_DegreeDist,       TFrameDrawGraph<T>::OnClickDegreeDist            )
   EVT_MENU ( DrawGraph_CummDegreeDist,   TFrameDrawGraph<T>::OnClickCummulativeDegreeDist )
   EVT_MENU ( DrawGraph_Hops,             TFrameDrawGraph<T>::OnClickHops                  )
   EVT_MENU ( DrawGraph_SingValues,       TFrameDrawGraph<T>::OnClickSingValues            )
   EVT_MENU ( DrawGraph_WeakComponents,   TFrameDrawGraph<T>::OnClickWeakComponents        )
   EVT_MENU ( DrawGraph_StrongComponents, TFrameDrawGraph<T>::OnClickStrongComponents      )
   
   EVT_MENU ( DrawGraph_CloseWindow,      TFrameDrawGraph<T>::OnClickCloseWindow )
   
   EVT_MENU ( DrawGraph_SimulateViruses,      TFrameDrawGraph<T>::OnClickSimulateViruses          )
   EVT_MENU ( DrawGraph_HierarchicalLayout,   TFrameDrawGraph<T>::OnClickApplyHierarchicalLayout  )
   EVT_MENU ( DrawGraph_InfluenceSlider,      TFrameDrawGraph<T>::OnClickShowInfluenceSlider      )
   EVT_MENU ( DrawGraph_ForestFire,           TFrameDrawGraph<T>::OnClickForestFire               )
   EVT_MENU ( DrawGraph_METIS_Partitioning,   TFrameDrawGraph<T>::OnClickMETISPartitioning        )
   EVT_MENU ( DrawGraph_CalculatePageRank,    TFrameDrawGraph<T>::OnClickCalculatePageRank        )
   EVT_MENU ( DrawGraph_SubGraphCalculus,     TFrameDrawGraph<T>::OnClickSubGraphCalculus         )
   EVT_MENU ( DrawGraph_GenerateRandomGraph,  TFrameDrawGraph<T>::OnClickGenretaRandomGraph       )
   EVT_MENU ( DrawGraph_Reset,                TFrameDrawGraph<T>::OnClickReset                    )
   
   EVT_MENU ( DrawGraph_EnableGraphEdition,         TFrameDrawGraph<T>::OnClickEnableGraphEdition         )
   EVT_MENU ( DrawGraph_Scale_Up,                   TFrameDrawGraph<T>::OnClickEnableGraphEdition         )
   EVT_MENU ( DrawGraph_Scale_Down,                 TFrameDrawGraph<T>::OnClickEnableGraphEdition         )
   EVT_MENU ( DrawGraph_Translate,                  TFrameDrawGraph<T>::OnClickEnableGraphEdition         )
   EVT_MENU ( DrawGraph_AddEdge,                    TFrameDrawGraph<T>::OnClickEnableGraphEdition         )
   EVT_MENU ( DrawGraph_DeleteEdge,                 TFrameDrawGraph<T>::OnClickEnableGraphEdition         )      
   EVT_MENU ( DrawGraph_ResizeNodes,                TFrameDrawGraph<T>::OnClickResizeNodes                )
   EVT_MENU ( DrawGraph_ScaleAllNodesSize,          TFrameDrawGraph<T>::OnClickScaleNodes                 )
   EVT_MENU ( DrawGraph_ShowAllLabels,              TFrameDrawGraph<T>::OnClickEnableGraphEdition         )
   EVT_MENU ( DrawGraph_ShowEdgeWeights,            TFrameDrawGraph<T>::OnClickEnableGraphEdition         )
   EVT_MENU ( DrawGraph_ApplyMETISPartitioningFile, TFrameDrawGraph<T>::OnClickApplyMETISPartitioningFile )
   EVT_MENU ( DrawGraph_ShowGraphData,              TFrameDrawGraph<T>::OnClickShowGraphData              )
   
   EVT_MENU ( DrawGraph_EnableNodesEdition,       TFrameDrawGraph<T>::OnClickEnableNodesEdition       )
   EVT_MENU ( DrawGraph_PickNode,                 TFrameDrawGraph<T>::OnClickEnableNodesEdition       )
   EVT_MENU ( DrawGraph_AddNode,                  TFrameDrawGraph<T>::OnClickEnableNodesEdition       )
   EVT_MENU ( DrawGraph_DeleteNode,               TFrameDrawGraph<T>::OnClickEnableNodesEdition       )
   EVT_MENU ( DrawGraph_InfectNode,               TFrameDrawGraph<T>::OnClickEnableNodesEdition       )
   EVT_MENU ( DrawGraph_SetNodeShape,             TFrameDrawGraph<T>::OnClickSetNodeShape             )
   EVT_MENU ( DrawGraph_SetNodeColor,             TFrameDrawGraph<T>::OnClickSetNodeColor             )
   EVT_MENU ( DrawGraph_SetNodeLabel,             TFrameDrawGraph<T>::OnClickSetNodeLabel             )
   EVT_MENU ( DrawGraph_SetNodeSize,              TFrameDrawGraph<T>::OnClickSetNodeSize              )
   EVT_MENU ( DrawGraph_ShowLabels,               TFrameDrawGraph<T>::OnClickEnableNodesEdition       )
   EVT_MENU ( DrawGraph_HierarchicalPartitioning, TFrameDrawGraph<T>::OnClickHierarchicalPartitioning )
   
   /*Popup menu*/
   EVT_CONTEXT_MENU ( TFrameDrawGraph<T>::OnContextMenu )
   
   /*Button events*/
   EVT_BUTTON ( DrawGraph_NodeButton,  TFrameDrawGraph<T>::OnClickNodeButton  )
   EVT_BUTTON ( DrawGraph_GraphButton, TFrameDrawGraph<T>::OnClickGraphButton )
   
   /*New events go here*/
   EVT_ERASE_BACKGROUND ( TFrameDrawGraph<T>::OnEraseBackGround )

END_EVENT_TABLE()

/*********************Call back functors********************/
/*Small class that teaches the TDialogSelectValue dialog how to update the TFrameDrawGraph*/
template <class _TNodeNet>
class TFunctorGraphUpdaterForSelectValue : public TDialogSelectValue::TFunctorUpdaterForSelectValue{
   void RefreshInterface(){
      TFunctorUpdaterForSelectValue::RefreshInterface();
      TFrameDrawGraph<_TNodeNet> *fdgTemp = ((TFrameDrawGraph<_TNodeNet> *)interfaceToBeUpdated);
      fdgTemp->Refresh();
   }
};
/*********************Call back functors********************/
/*Small class that teaches the select nodes dialog how to update the TFrameDrawGraph*/
template <class _TNodeNet>
class TFunctorGraphUpdaterForSelectNodes : public TDialogSelectNodes::TFunctorUpdaterForSelectNodesDialog{
   void RefreshInterface(){
      TFunctorUpdaterForSelectNodesDialog::RefreshInterface();
      TFrameDrawGraph<_TNodeNet> *fdgTemp = ((TFrameDrawGraph<_TNodeNet> *)interfaceToBeUpdated);
      fdgTemp->Refresh();
   }
   void Execute(){
      std::list<std::string> lListOfSelectedNodes;
      // TFunctorUpdaterForSelectNodes::Execute();
      TFrameDrawGraph<_TNodeNet> *fdgTemp = ((TFrameDrawGraph<_TNodeNet> *)interfaceToBeUpdated);
      fdgTemp->wxmActions->Check(DrawGraph_SubGraphCalculus,false);
      fdgTemp->dsnDialogSelectNodes->fufsUpdater = NULL;
      fdgTemp->dsnDialogSelectNodes->GetListOfSelectedNodes(lListOfSelectedNodes);
      fdgTemp->SubGraphCalculusExecution(lListOfSelectedNodes);
   }
   void CloseSequence(){
      // TFunctorUpdaterForSelectNodes::CloseSequence();
      TFrameDrawGraph<_TNodeNet> *fdgTemp = ((TFrameDrawGraph<_TNodeNet> *)interfaceToBeUpdated);
      fdgTemp->dsnDialogSelectNodes->fufsUpdater = NULL;
      fdgTemp->wxmActions->Check(DrawGraph_SubGraphCalculus,false);
      fdgTemp->Refresh();
   }
};
      
/*Small class that teaches the slider how to update the TFrameDrawGraph*/
template <class _TNodeNet>
class TFunctorHierarchyUpdaterForSlider : public TDialogSlider::TFunctorUpdaterForSlider{
   void UpdateSlider(){
#ifdef _ONRESEARCH
      TDialogSlider::TFunctorUpdaterForSlider::UpdateSlider();
      TFrameDrawGraph<_TNodeNet> *fdgTemp = ((TFrameDrawGraph<_TNodeNet> *)interfaceToBeUpdated);
      TProcessingInfluentialHierarchy<_TNodeNet> pihProcessingInfluentialHierarchy(fdgTemp->pNodeNetData);
      pihProcessingInfluentialHierarchy.SetHierarchyMobilityRatio(fdgTemp->fsHierarchySlider->GetValue()/100.0);
      fdgTemp->ApplyHierarchicalLayout();
      fdgTemp->Refresh();
#endif
   }
   void RefreshInterface(){
      TFunctorUpdaterForSlider::RefreshInterface();
      TFrameDrawGraph<_TNodeNet> *fdgTemp = ((TFrameDrawGraph<_TNodeNet> *)interfaceToBeUpdated);
      fdgTemp->Refresh();
   }
};

/*Small class that teaches the slider how to update the TFrameDrawGraph
when it is closed*/
template <class _TNodeNet>
class TFunctorCloseSlider : public TDialogSlider::TFunctorUpdaterForSlider{
   void UpdateSlider(){
      TDialogSlider::TFunctorUpdaterForSlider::UpdateSlider();
      TFrameDrawGraph<_TNodeNet> *fdgTemp = ((TFrameDrawGraph<_TNodeNet> *)interfaceToBeUpdated);         
      fdgTemp->pNodeNetData->Reset(false);
      fdgTemp->Refresh();
   }
   void RefreshInterface(){
      TFunctorUpdaterForSlider::RefreshInterface();
      TFrameDrawGraph<_TNodeNet> *fdgTemp = ((TFrameDrawGraph<_TNodeNet> *)interfaceToBeUpdated);
      fdgTemp->Refresh();
   }
};

/*Small class that teaches the node resizing slider how to update the TFrameDrawGraph*/
template <class _TNodeNet>
class TFunctorSizeUpdaterForSlider : public TDialogSlider::TFunctorUpdaterForSlider{
   void UpdateSlider(){
      TDialogSlider::TFunctorUpdaterForSlider::UpdateSlider();
      TFrameDrawGraph<_TNodeNet> *fdgTemp = ((TFrameDrawGraph<_TNodeNet> *)interfaceToBeUpdated);
      fdgTemp->pNodeNetData->SetAllNodesSize(fdgTemp->fsSizingSlider->GetValue()/100.0);
      fdgTemp->Refresh();
   }
   void RefreshInterface(){
      TFunctorUpdaterForSlider::RefreshInterface();
      TFrameDrawGraph<_TNodeNet> *fdgTemp = ((TFrameDrawGraph<_TNodeNet> *)interfaceToBeUpdated);
      fdgTemp->Refresh();
   }
};
/*Small class that teaches the node scale size slider how to update the TFrameDrawGraph*/
template <class _TNodeNet>
class TFunctorScaleSizeUpdaterForSlider : public TDialogSlider::TFunctorUpdaterForSlider{
   void UpdateSlider(){
      TDialogSlider::TFunctorUpdaterForSlider::UpdateSlider();
      TFrameDrawGraph<_TNodeNet> *fdgTemp = ((TFrameDrawGraph<_TNodeNet> *)interfaceToBeUpdated);
      fdgTemp->pNodeNetData->ScaleAllNodesSize(fdgTemp->fsSizingSlider->GetValue()/100.0);
      fdgTemp->Refresh();
   }
   void RefreshInterface(){
      TFunctorUpdaterForSlider::RefreshInterface();
      TFrameDrawGraph<_TNodeNet> *fdgTemp = ((TFrameDrawGraph<_TNodeNet> *)interfaceToBeUpdated);
      fdgTemp->Refresh();
   }
};

/*Small class that teaches the node resizing slider how to update the one single node*/
template <class _TNodeNet>
class TFunctorNodeSizeUpdaterForSlider : public TDialogSlider::TFunctorUpdaterForSlider{
   void UpdateSlider(){
      TDialogSlider::TFunctorUpdaterForSlider::UpdateSlider();
      TFrameDrawGraph<_TNodeNet> *fdgTemp = ((TFrameDrawGraph<_TNodeNet> *)interfaceToBeUpdated);
      fdgTemp->diDrawer->SetSizeForEmphasizedNode(fdgTemp->fsSizingSlider->GetValue()/100.0);
      fdgTemp->Refresh();
   }
      void RefreshInterface(){
      TFunctorUpdaterForSlider::RefreshInterface();
      TFrameDrawGraph<_TNodeNet> *fdgTemp = ((TFrameDrawGraph<_TNodeNet> *)interfaceToBeUpdated);
      fdgTemp->Refresh();
   }
};
/*Small class that teaches the node list of shapes dialog how to update the TFrameDrawGraph*/
template <class _TNodeNet>
class TFunctorShapeUpdaterForListBox : public TFunctorUpdaterForListBox{
   void UpdateListBox(){
      TFunctorUpdaterForListBox::UpdateListBox();
      TFrameDrawGraph<_TNodeNet> *fdgTemp = ((TFrameDrawGraph<_TNodeNet> *)interfaceToBeUpdated);
      fdgTemp->diDrawer->SetShapeForEmphasizedNode((TShapeType::TShapeAcronysm)fdgTemp->dlbListBox->GetSelectedItemIndex());
      fdgTemp->Refresh();
   }
};
/*Small class that teaches the node list of color dialog how to update the TFrameDrawGraph*/
template <class _TNodeNet>
class TFunctorColorUpdater : public TFunctorUpdaterDialogChooseColor{
   void UpdateDialogChooseColor(){
      TFunctorUpdaterDialogChooseColor::UpdateDialogChooseColor();
      TFrameDrawGraph<_TNodeNet> *fdgTemp = ((TFrameDrawGraph<_TNodeNet> *)interfaceToBeUpdated);
      fdgTemp->diDrawer->SetColorForEmphasizedNode(fdgTemp->dccDialogChooseColor->GetColourData().GetColour());
      fdgTemp->Refresh();
   }
   void RefreshInterface(){
      TFunctorUpdaterDialogChooseColor::RefreshInterface();
      TFrameDrawGraph<_TNodeNet> *fdgTemp = ((TFrameDrawGraph<_TNodeNet> *)interfaceToBeUpdated);
      fdgTemp->Refresh();
   }
};
/*Small class that teaches the TFrameShowImage to make TFrameDrawGraph aware of its deletion*/
template <class _TNodeNet>
class TFunctorDeleteChildFrame : public TFrameShowImage::TFunctorUpdaterForChildFrame{
   void ExteriorProcessing(TFrameShowImage *fsiAFrame){
      TFrameDrawGraph<_TNodeNet> *fdgTemp = ((TFrameDrawGraph<_TNodeNet> *)interfaceToBeUpdated);
      std::vector<TFrameShowImage *>::iterator vIterator;
      for(vIterator = fdgTemp->vListOfChildPlots.begin(); vIterator < fdgTemp->vListOfChildPlots.end(); ++vIterator) {
         if((*vIterator) == fsiAFrame){
            fdgTemp->vListOfChildPlots.erase(vIterator);
            break;
         }
      }
   };
};
/*Small class that teaches the time point slider how to update the graph exhibition*/
template <class _TNodeNet>
class TFunctorTimePointUpdaterForSlider : public TDialogSlider::TFunctorUpdaterForSlider{
   void UpdateSlider(){
      TDialogSlider::TFunctorUpdaterForSlider::UpdateSlider();
      TFrameDrawGraph<_TNodeNet> *fdgTemp = ((TFrameDrawGraph<_TNodeNet> *)interfaceToBeUpdated);         
      fdgTemp->diDrawer->SetLowTimeLimit(fdgTemp->fsTimeSlider->GetValue()*1000000);
      fdgTemp->Refresh();
   }
   void RefreshInterface(){
      TFunctorUpdaterForSlider::RefreshInterface();
      TFrameDrawGraph<_TNodeNet> *fdgTemp = ((TFrameDrawGraph<_TNodeNet> *)interfaceToBeUpdated);
      fdgTemp->Refresh();
   }
};
/*Interface to receive information on how to delete SuperEdge
subgraph windows*/
template <class _TNodeNet>
class TFunctorUpdaterForRootWindow{
protected:
   void *interfaceToBeUpdated;
public:
   TFunctorUpdaterForRootWindow():interfaceToBeUpdated(NULL){};         
   virtual void SetInterfaceToBeUpdated(void *aInterfaceToBeUpdated){
      interfaceToBeUpdated = aInterfaceToBeUpdated;
   };
   virtual void ExteriorProcessing(TFrameDrawGraph<_TNodeNet> *fsiAFrame) = 0;
};
/*************************************************************************************/

#endif
