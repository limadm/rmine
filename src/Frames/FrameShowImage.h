#pragma once
#ifndef _FrameShowImage_
#define _FrameShowImage_

#include <Commons/Auxiliar.h>

class TFrameShowImage : public wxMDIChildFrame{
public:

   class TFunctorUpdaterForChildFrame{
      protected:
         void *interfaceToBeUpdated;
      public:
         TFunctorUpdaterForChildFrame():interfaceToBeUpdated(NULL){};         
         virtual void SetInterfaceToBeUpdated(void *aInterfaceToBeUpdated){
            interfaceToBeUpdated = aInterfaceToBeUpdated;
         };
         virtual void ExteriorProcessing(TFrameShowImage *fsiAFrame) = 0;
   };
   TFrameShowImage(wxMDIParentFrame* parent,
                  const wxString& title,
                  int iLeft, int iTop,
                  int iWidth, int iHeight,
                  wxWindow *wxwARootWindow = NULL);
   void ShowImage(wxString aFilePathString);
   TFunctorUpdaterForChildFrame *fufcfFunctor;
private:
   wxWindow *wxwRootWindow;
   wxString *filePathString;
   wxImage *frameImage;
   wxMenuBar* wxmMenuBar;
   wxMenu *wxmFilePlotMenu;
   wxBufferedPaintDC *dc;
   double fPercentage;
   void OnShow(wxShowEvent& event);
   void OnPaint(wxPaintEvent& event);
   void OnMove(wxMoveEvent& event);
   void OnSize(wxSizeEvent& event);
   void OnClose(wxCloseEvent& event);
   void OnClickClose(wxCommandEvent& event);
   void OnActivate(wxActivateEvent& event);
   void OnLoseFocus(wxFocusEvent& event);
   virtual void Refresh(bool eraseBackground = true, const wxRect* rect = NULL);
   void CloseWindow();
   DECLARE_EVENT_TABLE()
};

#endif