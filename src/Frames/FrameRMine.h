#pragma once
#ifndef _FrameRMine_
#define _FrameRMine_

#include <time.h>
#include <string>
#include <fstream>
#include <iostream>

#include <Commons/Auxiliar.h>
#include <Dialogs/DialogParamsInput.h>
#include <Drawings/DrawWxWidget.h>
#include <Frames/FrameShowImage.h>
#include <Frames/FrameDrawGraph.h>
#include <Graphs/EdgePlot.h>
#include <Graphs/NodePlot.h>
#include <Graphs/NodeNetTests.h>
#include <Layouts/LayoutForceDirected.h>

#ifdef _ONRESEARCH
#include <SuperGraph/FrameDrawSuperGraph.h>
#endif

class TFrameRMine : public wxMDIParentFrame{
public:
   TFrameRMine(const wxString& title);
   ~TFrameRMine();
   bool ShowChooseDialog(std::string& sTheChosenFile, bool bFileOrDir, const char* ext);
private:
#ifndef PERFORMANCE
   TFrameDrawGraph< TNodeNetTests<TNodePlot, TEdgePlot> > *dgfFrameDrawGraph;
   TDrawWxWidget< TNodeNetTests<TNodePlot, TEdgePlot> > *dwwGraphDrawer;
#else
   TFrameDrawGraph< TNodeNetTests<TNodePerformance, TEdgePlot> > *dgfFrameDrawGraph;
   TDrawWxWidget< TNodeNetTests<TNodePerformance, TEdgePlot> > *dwwGraphDrawer;
#endif
   void OnQuit(wxCommandEvent& event);
   void OnAbout(wxCommandEvent& event);
   void OnClickLoadFile(wxCommandEvent& event);
   void OnClickLoadAndDrawFile(wxCommandEvent& event);
   void OnClickBuildAndDrawSuperGraphDir(wxCommandEvent& event);
   void OnClickLoadAndDrawPreBuildSuperGraphDir(wxCommandEvent& event);
   void OnClickGenerateRandomGraph(wxCommandEvent& event);
   void OnClose(wxCloseEvent& event);
#ifdef _ONRESEARCH
#ifndef PERFORMANCE
   TFrameDrawSuperGraph< TNodeNetTests<TNodePlot, TEdgePlot> > *fdsgFrameDrawSuperGraph;
#else
   TFrameDrawSuperGraph< TNodeNetTests<TNodePerformance, TEdgePlot> > *fdsgFrameDrawSuperGraph;
#endif
#endif

   DECLARE_EVENT_TABLE()
};
#endif