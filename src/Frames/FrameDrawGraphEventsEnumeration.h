#pragma once
#ifndef _FrameDrawGraphEventsEnumeration_
#define _FrameDrawGraphEventsEnumeration_

enum{
   DrawGraph_CloseWindow = wxID_HIGHEST + 101,

   DrawGraph_DegreeDist,
   DrawGraph_CummDegreeDist,
   DrawGraph_Hops,
   DrawGraph_WeakComponents,
   DrawGraph_StrongComponents,
   DrawGraph_SingValues,

   DrawGraph_SimulateViruses,
   DrawGraph_HierarchicalLayout,
   DrawGraph_CalculatePageRank,
   DrawGraph_InfluenceSlider,
   DrawGraph_ForestFire,
   DrawGraph_METIS_Partitioning,
   DrawGraph_SubGraphCalculus,
   DrawGraph_GenerateRandomGraph,
   DrawGraph_Reset,
   DrawGraph_HierarchicalPartitioning,

   DrawGraph_EnableGraphEdition,
   DrawGraph_Scale_Up,
   DrawGraph_Scale_Down,
   DrawGraph_Translate,
   DrawGraph_AddEdge,
   DrawGraph_DeleteEdge,
   DrawGraph_ResizeNodes,
   DrawGraph_ShowAllLabels,
   DrawGraph_ShowEdgeWeights,
   DrawGraph_ApplyMETISPartitioningFile,
   DrawGraph_ScaleAllNodesSize,
   DrawGraph_ShowGraphData,

   DrawGraph_EnableNodesEdition,
   DrawGraph_PickNode,
   DrawGraph_AddNode,
   DrawGraph_DeleteNode,
   DrawGraph_InfectNode,
   DrawGraph_SetNodeShape,
   DrawGraph_SetNodeColor,
   DrawGraph_SetNodeLabel,
   DrawGraph_SetNodeSize,
   DrawGraph_ShowLabels,

   DrawGraph_NodeButton,
   DrawGraph_GraphButton
};

#endif