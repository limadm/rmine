#include "FrameRMine.h"

#include <vector>
#include <iostream>
#include <limits>

#include <RMine.h>
#include <Graphs/NodeNetTests.h>

// the application icon (under Windows and OS/2 it is in resources and even
// though we could still include the XPM here it would be unused)
#if !defined(__WXMSW__) && !defined(__WXPM__)
#include <wxSampleIcon.xpm>
#endif

enum{
   RMine_LoadAndDrawGraphFile = wxID_HIGHEST + 1,
   RMine_LoadGraphFile,
   RMine_BuildAndDrawSuperGraphDir,
   RMine_LoadAndDrawPreBuiltSuperGraph,
   RMine_GenerateRandomGraph,
   RMine_About = wxID_ABOUT,
   RMine_Quit = wxID_EXIT
};

BEGIN_EVENT_TABLE(TFrameRMine, wxMDIParentFrame)
 EVT_MENU(RMine_LoadAndDrawGraphFile, TFrameRMine::OnClickLoadAndDrawFile)
 EVT_MENU(RMine_LoadGraphFile, TFrameRMine::OnClickLoadFile) 
 EVT_MENU(RMine_BuildAndDrawSuperGraphDir, TFrameRMine::OnClickBuildAndDrawSuperGraphDir)
 EVT_MENU(RMine_LoadAndDrawPreBuiltSuperGraph, TFrameRMine::OnClickLoadAndDrawPreBuildSuperGraphDir)
 EVT_MENU(RMine_GenerateRandomGraph, TFrameRMine::OnClickGenerateRandomGraph)

 EVT_MENU(RMine_Quit, TFrameRMine::OnQuit)
 EVT_MENU(RMine_About, TFrameRMine::OnAbout)
 EVT_CLOSE(TFrameRMine::OnClose)
END_EVENT_TABLE()


TFrameRMine::TFrameRMine(const wxString& title)
:wxMDIParentFrame((wxFrame *) NULL, -1,
                  title, wxDefaultPosition,
				  wxSize(960,720), wxDEFAULT_FRAME_STYLE | wxMAXIMIZE)
{
   dgfFrameDrawGraph = NULL;
   dwwGraphDrawer = NULL;
#ifdef _ONRESEARCH
   fdsgFrameDrawSuperGraph = NULL;
#endif

   wxMenu *fileMenu = new wxMenu;
   fileMenu->Append(RMine_LoadGraphFile, _T("&Load graph file..."), _T("Load graph file"));
   fileMenu->Append(RMine_LoadAndDrawGraphFile, _T("Load and &draw graph file..."), _T("Load and draw graph file"));
   #ifdef _ONRESEARCH
   fileMenu->Append(RMine_BuildAndDrawSuperGraphDir, _T("&Build and draw super graph dir..."), _T("Build and draw super graph dir..."));
   fileMenu->Append(RMine_LoadAndDrawPreBuiltSuperGraph, _T("Load and draw &pre built SuperGraph"), _T("Load and draw super graph file..."));
   #endif
   fileMenu->Append(RMine_GenerateRandomGraph, _T("Create &random graph..."), _T("Generate and draw a random graph..."));
   fileMenu->Append(RMine_Quit, _T("E&xit"), _T("Quit this program"));

   wxMenu *helpMenu = new wxMenu;
   helpMenu->Append(RMine_About, _T("&About...\tF1"), _T("Show about dialog"));

   wxMenuBar *menuBar = new wxMenuBar();
   menuBar->Append(fileMenu, _T("&File"));
   menuBar->Append(helpMenu, _T("&Help"));

   SetMenuBar(menuBar);

   CreateStatusBar(2);
   SetStatusText(_T("Welcome to RMine!"));
   this->SetIcon(wxICON(sample));
   this->Maximize();
}
TFrameRMine::~TFrameRMine(){
//   if(dwwGraphDrawer != NULL)
//      delete dwwGraphDrawer;
   /*dgfFrameDrawGraph and fdsgFrameDrawSuperGraph are MDI children 
     of this, so, this is responsible for their deletion*/
}
/*-------------------------------------------------------------*/
void TFrameRMine::OnClickBuildAndDrawSuperGraphDir(wxCommandEvent& event){
   event.Skip();
#ifdef _ONRESEARCH
   std::string sDirName;
   if(!ShowChooseDialog(sDirName, false, NULL))
      return;
   #ifdef _DEBUG
      //sDirName = "Etc/Release/Agma_Caetano_Christos_Anastassia200";
      //sDirName = "temp/Teste";
   #endif
#ifndef PERFORMANCE
   fdsgFrameDrawSuperGraph = new TFrameDrawSuperGraph< TNodeNetTests<TNodePlot, TEdgePlot> >(this,wxPoint(0,0),this->GetClientSize(), sDirName);
#else
   fdsgFrameDrawSuperGraph = new TFrameDrawSuperGraph< TNodeNetTests<TNodePerformance, TEdgePlot> >(this,wxPoint(0,0),this->GetClientSize(), sDirName);
#endif
   fdsgFrameDrawSuperGraph->BuildGTree();
   if(fdsgFrameDrawSuperGraph->SucceessfullyLoaded()){
      fdsgFrameDrawSuperGraph->Show(true);
   }else{
      fdsgFrameDrawSuperGraph->SetConfirmModificationsDialog(false);
      fdsgFrameDrawSuperGraph->Close();
      this->DestroyChildren();
   }
#endif
}
void TFrameRMine::OnClickLoadAndDrawPreBuildSuperGraphDir(wxCommandEvent& event){
   event.Skip();
#ifdef _ONRESEARCH
   std::string sDirName;
   if(!ShowChooseDialog(sDirName, true, "*.gtree"))
      return;
   #ifdef _DEBUG
      //sDirName = "Etc/Release/Agma_Caetano_Christos_Anastassia200";
      //sDirName = "temp/Teste";
   #endif
#ifndef PERFORMANCE
   fdsgFrameDrawSuperGraph = new TFrameDrawSuperGraph< TNodeNetTests<TNodePlot, TEdgePlot> >(this,wxPoint(0,0),this->GetClientSize(), sDirName);
#else
   fdsgFrameDrawSuperGraph = new TFrameDrawSuperGraph< TNodeNetTests<TNodePerformance, TEdgePlot> >(this,wxPoint(0,0),this->GetClientSize(), sDirName);
#endif
   fdsgFrameDrawSuperGraph->LoadBuildGTree();
   if(fdsgFrameDrawSuperGraph->SucceessfullyLoaded()){
#ifndef PERFORMANCE
      fdsgFrameDrawSuperGraph->Show(true);
#endif
   }else{
      fdsgFrameDrawSuperGraph->SetConfirmModificationsDialog(false);
      fdsgFrameDrawSuperGraph->Close();
      this->DestroyChildren();
   }

#endif
}

void TFrameRMine::OnClickLoadAndDrawFile(wxCommandEvent& event){
   event.Skip();   
   std::string sFileName;
   if(!ShowChooseDialog(sFileName, true, "*_ef.*"))
      return;
   
#ifndef PERFORMANCE
   TPt< TNodeNetTests<TNodePlot, TEdgePlot> > pANodeNetData(new TNodeNetTests<TNodePlot, TEdgePlot>());
   if(!pANodeNetData->LoadGraphTextFile(sFileName))
      return;
   
   dgfFrameDrawGraph = new TFrameDrawGraph< TNodeNetTests<TNodePlot, TEdgePlot> >(this,this->GetPosition(),this->GetSize(),pANodeNetData);
   dwwGraphDrawer = new TDrawWxWidget< TNodeNetTests<TNodePlot, TEdgePlot> >(pANodeNetData);
   TLayoutForceDirected< TNodeNetTests<TNodePlot, TEdgePlot> > layout;
#else
   TPt< TNodeNetTests<TNodePerformance, TEdgePlot> > pANodeNetData(new TNodeNetTests<TNodePerformance, TEdgePlot>());
   if(!pANodeNetData->LoadGraphTextFile(sFileName))
      return;
   
   dgfFrameDrawGraph = new TFrameDrawGraph< TNodeNetTests<TNodePerformance, TEdgePlot> >(this,wxPoint(0,0),this->GetClientSize(),pANodeNetData);   
   dwwGraphDrawer = new TDrawWxWidget< TNodeNetTests<TNodePerformance, TEdgePlot> >(pANodeNetData);
   TLayoutForceDirected< TNodeNetTests<TNodePerformance, TEdgePlot> > layout;
#endif
   
   dgfFrameDrawGraph->SetDrawer(dwwGraphDrawer);
   dgfFrameDrawGraph->ApplyLayout(layout);
   dgfFrameDrawGraph->Show(true);
}

void TFrameRMine::OnClickLoadFile(wxCommandEvent& event){
   event.Skip();
   std::string sFileName;
   if(!ShowChooseDialog(sFileName, true, "*_ef.*"))
      return;

#ifndef PERFORMANCE
   TPt< TNodeNetTests<TNodePlot, TEdgePlot> > pANodeNetData(new TNodeNetTests<TNodePlot, TEdgePlot>());
   dgfFrameDrawGraph = new TFrameDrawGraph< TNodeNetTests<TNodePlot, TEdgePlot> >(this,wxPoint(0,0),this->GetClientSize(),pANodeNetData);
#else
   TPt< TNodeNetTests<TNodePerformance, TEdgePlot> > pANodeNetData(new TNodeNetTests<TNodePerformance, TEdgePlot>());
   dgfFrameDrawGraph = new TFrameDrawGraph< TNodeNetTests<TNodePerformance, TEdgePlot> >(this,wxPoint(0,0),this->GetClientSize(),pANodeNetData);
#endif
   if(!pANodeNetData->LoadGraphTextFile(sFileName))
      return;   
   /*Just load, do not draw*/
   dgfFrameDrawGraph->SetConfirmModificationsDialog(false);
   dgfFrameDrawGraph->SetSupressDrawing();
   dgfFrameDrawGraph->Show(true);  /*Show for menu options*/
}

void TFrameRMine::OnClickGenerateRandomGraph(wxCommandEvent& event) {
   event.Skip();
   TPt< TNodeNetTests<TNodePlot, TEdgePlot> > graph(new TNodeNetTests<TNodePlot, TEdgePlot>());
   TProcessingRandomGraphGenerator< TPt< TNodeNetTests<TNodePlot, TEdgePlot> > > randomGenerator;
   randomGenerator.GenerateRandomGraph(graph);
   
   if (graph->GetNodes() > 0) {
	   dgfFrameDrawGraph = new TFrameDrawGraph< TNodeNetTests<TNodePlot, TEdgePlot> >(this, wxPoint(0,0), this->GetClientSize(), graph);
	   dwwGraphDrawer = new TDrawWxWidget< TNodeNetTests<TNodePlot, TEdgePlot> >(graph);
	   TLayoutForceDirected< TNodeNetTests<TNodePlot, TEdgePlot> > layout;
   
	   dgfFrameDrawGraph->SetDrawer(dwwGraphDrawer);
	   dgfFrameDrawGraph->ApplyLayout(layout);
	   dgfFrameDrawGraph->Show(true);
   }
}

/*-------------------------------------------------------------*/
bool TFrameRMine::ShowChooseDialog(std::string& sTheChosenFile, bool bFileOrDir, const char* ext){
   /*bFileOrDir ==  true -> works for file picking
     bFileOrDir == false -> works for directory picking*/
   std::string sDialogInput;             wxDialog *wxdADialogForChoice = NULL;
   if(bFileOrDir)
      wxdADialogForChoice = new wxFileDialog(this, "Choose file",".", "", ext, wxOPEN);
   else {
      char *cTemp = new char[200];
      GetCurrentDirectory(200, cTemp);
      wxdADialogForChoice = new wxDirDialog(this, "Choose directory", cTemp);
   }

   if ( wxdADialogForChoice->ShowModal() == wxID_OK ){
      if(bFileOrDir)
         sDialogInput = ((wxFileDialog*)wxdADialogForChoice)->GetPath();
      else
         sDialogInput = ((wxDirDialog*)wxdADialogForChoice)->GetPath();

      wxdADialogForChoice->Destroy();
      sTheChosenFile = sDialogInput;
      return true;
   }else{
      return false;
   }
}

void TFrameRMine::OnQuit(wxCommandEvent& event){
   Close(true);
   event.Skip();
}

void TFrameRMine::OnClose(wxCloseEvent& event){
   event.Skip();
   wxGetApp().ExitMainLoop();
}

void TFrameRMine::OnAbout(wxCommandEvent& event){
   event.Skip();
   wxString msg;
   msg.Printf( _T("RMine software (2013.12)\n")
      _T("    by [danielm, junio, agma]@icmc.usp.br\n")
      _T("Forked from GMine software,\n")
	  _T("    by [junio, htong, christos, agma, jure]@cs.cmu.edu\n\n")
	  _T("This is free software under the GBDI-ICMC-USP Software License v1.0"));

   wxMessageBox(msg, _T("About RMine"), wxOK | wxICON_INFORMATION, this);
}
