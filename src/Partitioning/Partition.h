#pragma once
#ifndef _Partition_
#define _Partition_

#include <Commons/stdafx.h>

#include <stdio.h>

#include <wx/filename.h>
#include <wx/numdlg.h>

#include <Commons/Auxiliar.h>
#include <Dialogs/DialogWarning.h>

template <class _TNodeNetData>
class TPartition {
private:
   int iNumberOfHierarchyLevels;
   TPt<_TNodeNetData> pNodeNetData;
   std::string sMETISEdgeFile;               std::string sTempPartitioningFile;
   const char* cMETISApplication;            std::string sPartitionDirectory;
   bool WriteEdgeFileForMETIS(_TNodeNetData* nnANodeNet, std::string sAFileName);
   void ApplyMETIS(std::string sNextTempEdgeFile, int iNumberOfPartitions);

   void InterpretMETISPartitionFile(_TNodeNetData* nnNextNodeNet, _TNodeNetData* nnNodeNetxExternalNodes, int iPartition, _TNodeNetData* nnFormerNodeNet, std::string sAMETISPartitionsFile);
   int PartitionDataRecursive(_TNodeNetData* nnANodeNetData, int iHierarchyLevel, std::string sId, int iNumberOfPartitions);
   bool WriteNodeFiles(_TNodeNetData* nnNextNodeNet, _TNodeNetData* nnNodeNetxExternalNodes, int iPartition, std::string sAMETISPartitionsFile);
   void CancelSinglePartition(int iPartition, std::string sAPartitionId);
   std::string GetPartitionIdRootFromMETISPartitionsFileName(std::string sAMETISPartitionsFile);
   std::string GetPartitionsNameRoot();
public:
   TPartition(TPt<_TNodeNetData> pANodeNetData, std::string sAPartitionDirectory = "")
   :  pNodeNetData(pANodeNetData),
      cMETISApplication(PMETIS_EXE) //Path to METIS
   {            
      /*Determine directory for partitioning*/
      if(sAPartitionDirectory.compare("") == 0){
         sAPartitionDirectory = pNodeNetData->GetLoadedEdgesFileName();
         std::string sTokenToSearch = "_ef";
         int iSizeOfDirectoryName = sAPartitionDirectory.rfind(sTokenToSearch);
         std::string sDirectoryName = sAPartitionDirectory.substr(0, iSizeOfDirectoryName);         
         EmptyAndRemoveDirectory(sDirectoryName);
         CreateDirectory(sDirectoryName.c_str(), NULL);
         sPartitionDirectory = sDirectoryName;
      }
      std::string sDirectory;       std::string sFileNameRoot;
      std::string sFileName;        iNumberOfHierarchyLevels = 0;
      sMETISEdgeFile = pNodeNetData->GetLoadedEdgesFileName();
      /*Create file - form the name*/
      wxFileName wxfnFileName(sMETISEdgeFile.c_str());
      sDirectory = sPartitionDirectory + wxFileName::GetPathSeparator();
      sFileName = wxfnFileName.GetName();       /*Get file name without extension*/
      sFileName += "_EDGES_METIS.txt";          /*Add file identifier*/
      sMETISEdgeFile = sDirectory + sFileName;  /*Form final name*/
      sTempPartitioningFile = sDirectory + "temp.metis.partitioning.";
   }
   void PartitionData();
};

template <class _TNodeNetData>
void TPartition<_TNodeNetData>::ApplyMETIS(std::string sNextTempEdgeFile, int iNumberOfPartitions){
   char* cCommandLine = new char[200];
   std::string sCommandLine = " \"" + sNextTempEdgeFile + "\" " + TConverter<int>::ToString(iNumberOfPartitions);
   strcpy(cCommandLine, sCommandLine.c_str());
   LaunchApp(cMETISApplication, cCommandLine, sPartitionDirectory.c_str(), true);
   delete cCommandLine;
}

/*Edge files*/
template <class _TNodeNetData>
bool TPartition<_TNodeNetData>::WriteEdgeFileForMETIS(_TNodeNetData* nnANodeNet, std::string sAFileName){
   FILE *fEdgeFile;
   int iNextNeighbor;            int iNeighbors;
   int iCounter;                 int iEdgesCounter;
   THash<TInt, TInt> hCorrespondences;

   /*Open file for writing*/
   if ((fEdgeFile = fopen(sAFileName.c_str(), "w+t")) == NULL){
      TDialogWarning dwWarning("Cannot create edge file " + sAFileName + ". Please verify path and permissions.");
      return false;
   }
   /*Temporary, because we don't know the correct number of edges yet*/
   fprintf(fEdgeFile, "%d ", nnANodeNet->GetNodes());
   fprintf(fEdgeFile, "%d",  nnANodeNet->GetEdges());
   fprintf(fEdgeFile, "\n");

   /*Create correspondence between actual node ids and METIS ids*/
   iCounter = 0;
   /*METIS starts on node 1, _TNodeNetData starts on node 0 we add 1*/
   for(typename _TNodeNetData::TNodeI NI = nnANodeNet->BegNI(); NI < nnANodeNet->EndNI(); NI++){
      hCorrespondences.AddDat(TInt(NI.GetId()), TInt(iCounter+1));
      iCounter++;
   }

   iEdgesCounter = 0;
   for(typename _TNodeNetData::TNodeI NI = nnANodeNet->BegNI(); NI < nnANodeNet->EndNI(); NI++){
      iNeighbors = NI.GetOutDeg();
      for(int i = 0; i < iNeighbors; i++){
         iNextNeighbor = NI.GetOutNId(i);         
         fprintf(fEdgeFile,"%d ", hCorrespondences.GetDat(iNextNeighbor));
         iEdgesCounter++;
      }
      /*Note: if two nodes are crosslinked (id1->id2 and id2->id1), METIS file 
        will have duplicated information. This has not caused any problems.*/
      iNeighbors = NI.GetInDeg();
      for(int i = 0; i < iNeighbors; i++){
         iNextNeighbor = NI.GetInNId(i);
         fprintf(fEdgeFile,"%d ", hCorrespondences.GetDat(iNextNeighbor));
         iEdgesCounter++;
      }
      
      fprintf(fEdgeFile, "\n");
   }
   /*Now that we know the correct number of edges, we rewrite it*/
   fseek(fEdgeFile, 0, SEEK_SET);
   fprintf(fEdgeFile, "%d ", nnANodeNet->GetNodes());
   fprintf(fEdgeFile, "%d", iEdgesCounter/2);
   fprintf(fEdgeFile, "\n");
   /*Close*/
   fclose(fEdgeFile);
   return true;
};

template <class _TNodeNetData>
std::string TPartition<_TNodeNetData>::GetPartitionIdRootFromMETISPartitionsFileName(std::string sAMETISPartitionsFile){
   int iTemp = sAMETISPartitionsFile.find("partitioning.") + 13;
   std::string sTokenToSearch = ".txt.part";
   int iTemp2 = sAMETISPartitionsFile.rfind(sTokenToSearch);
   return sAMETISPartitionsFile.substr(iTemp, iTemp2 - iTemp);   
}

template <class _TNodeNetData>
std::string TPartition<_TNodeNetData>::GetPartitionsNameRoot(){
   wxFileName wxfnFileName(pNodeNetData->GetLoadedEdgesFileName().c_str());
   std::string sMainFileName = sPartitionDirectory;
   sMainFileName += wxFileName::GetPathSeparator() + wxfnFileName.GetName();
   std::string sTokenToSearch = "_ef";
   int iSizeOfFileNameRoot = sMainFileName.rfind(sTokenToSearch);
   return sMainFileName.substr(0, iSizeOfFileNameRoot); 
}

template <class _TNodeNetData>
bool TPartition<_TNodeNetData>::WriteNodeFiles(_TNodeNetData* nnNextNodeNet, _TNodeNetData* nnNodeNetxExternalNodes, int iPartition, std::string sAMETISPartitionsFile){
   bool bResult;
   std::string sPartitionId = GetPartitionIdRootFromMETISPartitionsFileName(sAMETISPartitionsFile);
   sPartitionId += "_" + TConverter<int>::ToString(iPartition);  
   /* files must all be at the same directory and have the same name root*/
   std::string sSubGraphFilesRoot = GetPartitionsNameRoot();   

   std::string sInternalEdgesFileName = sSubGraphFilesRoot + ".IN.part." + sPartitionId + "_ef.txt";
   std::string sExternalEdgesFileName = sSubGraphFilesRoot + ".OUT.part." + sPartitionId + "_ef.txt";
   /*In some cases, METIS will not generate the desired number of partitions.
   For example, when asking for 5 partitions from a 3 nodes graph.
   In these situations, nnNextNodeNet will be empty and WriteEdgeFile will fail.
   We delete the correspondent files.*/
   if(!nnNodeNetxExternalNodes->WriteEdgeFile(sExternalEdgesFileName))
      remove(sExternalEdgesFileName.c_str()); /*OUT.part file*/

   if(!nnNextNodeNet->WriteEdgeFile(sInternalEdgesFileName)){
      remove(sInternalEdgesFileName.c_str());  /*IN.part file*/
      bResult = false;
   }else
      nnNextNodeNet->WriteNodesFile();
      nnNextNodeNet->WriteLayoutFile();
      bResult = true;
   return bResult;
}

template <class _TNodeNetData>
void TPartition<_TNodeNetData>::CancelSinglePartition(int iPartition, std::string sAPartitionId){
   std::string sPartitionId = sAPartitionId;
   std::string sSubGraphFilesRoot = GetPartitionsNameRoot();
   /*The partition files will receive their supernode parent name*/
   std::string sInternalEdgesFileName_new = sSubGraphFilesRoot + ".IN.part." + sPartitionId + "_ef.txt";
   std::string sExternalEdgesFileName_new = sSubGraphFilesRoot + ".OUT.part." + sPartitionId + "_ef.txt";
   /*The single partition files have these names*/
   sPartitionId += "_" + TConverter<int>::ToString(0);  
   std::string sInternalEdgesFileName_old = sSubGraphFilesRoot + ".IN.part." + sPartitionId + "_ef.txt";
   std::string sExternalEdgesFileName_old = sSubGraphFilesRoot + ".OUT.part." + sPartitionId + "_ef.txt";
   /*We simply change the file names*/
   rename(sInternalEdgesFileName_old.c_str(), sInternalEdgesFileName_new.c_str());
   if(FileExists(sExternalEdgesFileName_old))
      rename(sExternalEdgesFileName_old.c_str(), sExternalEdgesFileName_new.c_str());
}
/*------------------------------------------------------------------------------------------*/
template <class _TNodeNetData>
void TPartition<_TNodeNetData>::InterpretMETISPartitionFile(_TNodeNetData* nnNextNodeNet, _TNodeNetData* nnNodeNetxExternalNodes, int iPartition,
                                                            _TNodeNetData* nnFormerNodeNet, std::string sAMETISPartitionsFile){
   std::ifstream ifsDataFile;    int iPartitionNumber;
   int iNeighbors = 0;           int iNextNeighbor = -1;
   int iSuperWeight;             int iOriginalSuperEdgeId;
   int iNewSuperEdgeId;
   typename _TNodeNetData::TNodeI niTemp = nnFormerNodeNet->BegNI();

   /*File opening*/
   ifsDataFile.open(sAMETISPartitionsFile.c_str());

   /*Fill structure nnNextNodeNet only with the nodes in partition iPartition*/
   nnNextNodeNet->bLayoutFlag = nnFormerNodeNet->bLayoutFlag;
   /*First the nodes*/
   niTemp = nnFormerNodeNet->BegNI();
   while ( !ifsDataFile.eof() ) {
      /*The ith line (a single number) tells to which partition the ith node belongs*/
      ifsDataFile >> iPartitionNumber;
      /*Node in partition iPartition, copy it*/
      if(iPartitionNumber == iPartition){
         /*Add by copy*/
         nnNextNodeNet->AddNode(niTemp.GetId(),niTemp());
      }
      niTemp++;
      /*Quit on last node*/
      if(niTemp == nnFormerNodeNet->EndNI()){
         break;
      }
   }

   /*Now the edges*/
   /*Traverse all nodes in nnNextNodeNet*/
   for(typename _TNodeNetData::TNodeI NI = nnNextNodeNet->BegNI(); NI < nnNextNodeNet->EndNI(); NI++){
      /*Get the version of this node from the original graph partition*/
      niTemp = pNodeNetData->GetNI(NI.GetId());
      /*Check all edges - for internal edges, we simply write the initial connectivity
        For external edges, we write complimentary .OUT. files with double edges 
        like (src, dst) and (dst, src)*/
      iNeighbors = niTemp.GetOutDeg();
      for(int i = 0; i < iNeighbors; i++){
         iNextNeighbor = niTemp.GetOutNId(i);
         /*Internal edge*/
         if(nnNextNodeNet->IsNode(iNextNeighbor)){
            /*Did I have this edge in the original graph?*/
            if(pNodeNetData->IsEdge(NI.GetId(), iNextNeighbor, iOriginalSuperEdgeId)){
               iSuperWeight = pNodeNetData->GetEI(iOriginalSuperEdgeId)().GetWeight();
               iNewSuperEdgeId = nnNextNodeNet->AddEdge(NI.GetId(), iNextNeighbor);
               nnNextNodeNet->GetEI(iNewSuperEdgeId)().SetWeight(TInt(iSuperWeight));
            }
         /*External edge*/
         }else{
            if(!nnNodeNetxExternalNodes->IsNode(NI.GetId()))
               nnNodeNetxExternalNodes->AddNode(NI.GetId());
            if(!nnNodeNetxExternalNodes->IsNode(iNextNeighbor))
               nnNodeNetxExternalNodes->AddNode(iNextNeighbor);
            /*Did I have this edge in the original graph?*/
            if(pNodeNetData->IsEdge(NI.GetId(), iNextNeighbor, iOriginalSuperEdgeId)){
               iSuperWeight = pNodeNetData->GetEI(iOriginalSuperEdgeId)().GetWeight();
               iNewSuperEdgeId = nnNodeNetxExternalNodes->AddEdge(NI.GetId(), iNextNeighbor);
               nnNodeNetxExternalNodes->GetEI(iNewSuperEdgeId)().SetWeight(TInt(iSuperWeight));
            }
         }
      }
      iNeighbors = niTemp.GetInDeg();
      for(int i = 0; i < iNeighbors; i++){
         iNextNeighbor = niTemp.GetInNId(i);
         /*Internal edge*/
         if(nnNextNodeNet->IsNode(iNextNeighbor)){
            /*Did I have this edge in the original graph?*/
            if(pNodeNetData->IsEdge(NI.GetId(), iNextNeighbor))
               nnNextNodeNet->AddEdge(NI.GetId(), iNextNeighbor);
         /*External edge*/
         }else{
            if(!nnNodeNetxExternalNodes->IsNode(NI.GetId()))
               nnNodeNetxExternalNodes->AddNode(NI.GetId());
            if(!nnNodeNetxExternalNodes->IsNode(iNextNeighbor))
               nnNodeNetxExternalNodes->AddNode(iNextNeighbor);
            nnNodeNetxExternalNodes->AddEdge(NI.GetId(), iNextNeighbor);
         }
      }
   }
   ifsDataFile.close();
}
template <class _TNodeNetData>
void TPartition<_TNodeNetData>::PartitionData(){
   long lLevels = wxGetNumberFromUser( _T("Number of levels"),
                                    _T("Tell me the number of levels for partitioning:"), 
                                    _T("Number of levels:"), 3, 0, 100, NULL );
   this->iNumberOfHierarchyLevels = (int)lLevels;
   long lPartitions = wxGetNumberFromUser( _T("Number of partitions per level"),
                                           _T("Tell me the number of partitions per level:"), 
                                           _T("Number of partitions per level:"), 4, 0, 100, NULL );
   /*The algorithm starts at 0, so n Levels go from 0 to n-1*/
    this->iNumberOfHierarchyLevels =  this->iNumberOfHierarchyLevels - 1;
   PartitionDataRecursive(&(*pNodeNetData), 0, "_0", (int)lPartitions);
};
template <class _TNodeNetData>
int TPartition<_TNodeNetData>::PartitionDataRecursive(_TNodeNetData* nnANodeNetData, int iHierarchyLevel, std::string sId, int iNumberOfPartitions){
   nnANodeNetData->bLayoutFlag;
   std::string sNextTempEdgeFile = sTempPartitioningFile + sId + ".txt";
   std::string sNextTempPartitioningFile = sNextTempEdgeFile + ".part." + TConverter<int>::ToString(iNumberOfPartitions);
   /*We keep track of partitionings that generate just 1 partition, when this 
     leaf is cancealed and its parent is turned into a leaf*/
   int iTotalPartitions = 0;     int iGeneratedPartitions = 0;

   TConverter<int>::ToString(iNumberOfPartitions);
   std::string sNextIdRoot = sId;          std::string sNextId;
   _TNodeNetData* nnNextNodeNet = NULL;
   _TNodeNetData* nnNodeNetxExternalNodes = NULL;

   /*Write a version of the graph in memory in METIS format*/
   WriteEdgeFileForMETIS(nnANodeNetData, sNextTempEdgeFile);
   /*Apply METIS to generate iNumberOfPartitions*/
   ApplyMETIS(sNextTempEdgeFile, iNumberOfPartitions);
   /*Go through each partition*/
   for(int iPartition = 0; iPartition < iNumberOfPartitions; iPartition++){
      sNextId = sNextIdRoot + "_" + TConverter<int>::ToString(iPartition);      
      nnNextNodeNet = new _TNodeNetData();              /*Next subgraph to be partitioned*/
      nnNodeNetxExternalNodes = new _TNodeNetData();
      /*Fills nnNextNodeNet with the iPartition-th from nnANodeNetData
        we will call another recursion cycle using nnNextNodeNet (and so on)*/
      InterpretMETISPartitionFile(nnNextNodeNet, nnNodeNetxExternalNodes, iPartition, nnANodeNetData, sNextTempPartitioningFile);
      /*If not at last hierarchy level, call another recursion*/
      if((iHierarchyLevel+1) < iNumberOfHierarchyLevels){
         if(nnNextNodeNet->GetNodes() > iNumberOfPartitions){
            iGeneratedPartitions = PartitionDataRecursive(nnNextNodeNet, iHierarchyLevel+1, sNextId, iNumberOfPartitions);
            iTotalPartitions += iGeneratedPartitions;
         }else{  /*Partition not big enough becomes a leaf*/
            if(WriteNodeFiles(nnNextNodeNet,nnNodeNetxExternalNodes, iPartition, sNextTempPartitioningFile))
               iTotalPartitions++;
         }
      }else{   /*Else, we're at the last level -> write edges (IN and OUT) files*/
         if(WriteNodeFiles(nnNextNodeNet,nnNodeNetxExternalNodes, iPartition, sNextTempPartitioningFile))
            iTotalPartitions++;
      }
      delete nnNextNodeNet;
      delete nnNodeNetxExternalNodes;
   }
   remove(sNextTempEdgeFile.c_str());
   remove(sNextTempPartitioningFile.c_str());
   /*If a partitioning created just one single partition, we eliminate this subtree*/
   if(iTotalPartitions == 1)
      CancelSinglePartition(0, sId);  
   return iTotalPartitions;
};
/*------------------------------------------------------------------------------------------*/
#endif
