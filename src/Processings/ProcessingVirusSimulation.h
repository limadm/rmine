#pragma once
#ifndef _ProcessingVirusSimulation_
#define _ProcessingVirusSimulation_

#include <sstream>

#include <Commons/stdafx.h>
#include <Commons/Auxiliar.h>
#include <Dialogs/DialogParamsInput.h>

template <class _TNodeNetData>
class TProcessingVirusSimulation {
private:
   int iTimeSteps;    int iSimulationTimeCounter;
   TPt<_TNodeNetData> pNodeNetData;
public:
   TProcessingVirusSimulation(TPt<_TNodeNetData> pANodeNetData) :
      pNodeNetData(pANodeNetData), iSimulationTimeCounter(0), fInfectProbability(0.2),
      dHealingProbabilityMean(10), dHealingProbabilityVariance(0.45), iTimeSteps(-1)
      {
         srand((unsigned)time(NULL));                           
      }
      ~TProcessingVirusSimulation(){};

   double fInfectProbability;
   double dHealingProbabilityMean;
   double dHealingProbabilityVariance;
   double GetGaussianProbability(double dAMean, double dAVariance, double dAX);
   bool VerifyProbability(double fAProbability);
   void ExecuteVirusSimulation(int iTimeSteps);
   void StepForwardVirusSimulation();
   void ExecuteNodeSimulation(typename _TNodeNetData::TNodeI& niASubjectNode, int iIthTimeStep);
   bool SetParametersVisually();
   int GetTimeSteps(){ return iTimeSteps; };
};

template <class _TNodeNetData>
bool TProcessingVirusSimulation<_TNodeNetData>::SetParametersVisually(){
   TDialogParamsInput fpiDialogParamsInput(NULL, "Enter virus simulation parameters");
   fpiDialogParamsInput.SetParam1Name("Number of time units");
   fpiDialogParamsInput.SetParam2Name("Virus infection probability");
   fpiDialogParamsInput.SetParam3Name("Healing mean factor");
   fpiDialogParamsInput.SetParam4Name("Healing variance factor");
   fpiDialogParamsInput.SetInitialValues("30","0.8","15","5");
   fpiDialogParamsInput.MakeDialog();
   if(fpiDialogParamsInput.ShowModal() == wxID_OK){
      this->iTimeSteps = atoi(fpiDialogParamsInput.GetParam1());
      this->fInfectProbability = (double)atof(fpiDialogParamsInput.GetParam2());
      this->dHealingProbabilityMean = (double)atof(fpiDialogParamsInput.GetParam3());
      this->dHealingProbabilityVariance = (double)atof(fpiDialogParamsInput.GetParam4());
      return true;
   }else{
      return false;
   }
}

template <class _TNodeNetData>
bool TProcessingVirusSimulation<_TNodeNetData>::VerifyProbability(double fAProbability){
   return (((double)rand()/(double)RAND_MAX) < fAProbability);
}

template <class _TNodeNetData>
void TProcessingVirusSimulation<_TNodeNetData>::ExecuteVirusSimulation(int iTimeSteps){
   iSimulationTimeCounter = 0;
   for(int i = 0; i < iTimeSteps; i++){
      for(typename _TNodeNetData::TNodeI& NI = pNodeNetData->BegNI(); NI < pNodeNetData->EndNI(); NI++) {         
         ExecuteNodeSimulation(NI, iSimulationTimeCounter + i);
      }
   }
}

template <class _TNodeNetData>
void TProcessingVirusSimulation<_TNodeNetData>::StepForwardVirusSimulation(){
   iSimulationTimeCounter++;
   for(typename _TNodeNetData::TNodeI NI = pNodeNetData->BegNI(); NI < pNodeNetData->EndNI(); NI++) {         
      ExecuteNodeSimulation(NI, iSimulationTimeCounter);
   }
}

template <class _TNodeNetData>
double TProcessingVirusSimulation<_TNodeNetData>::GetGaussianProbability(double dAMean, double dAVariance, double dAX){
   double dFirstPart = 1.0/(dAVariance*sqrt(2.0*PI));
   double dExpPower = -pow(dAX - dAMean,2)/(2*pow(dAVariance,2));
   return dFirstPart*exp(dExpPower);
}


template <class _TNodeNetData>
void TProcessingVirusSimulation<_TNodeNetData>::ExecuteNodeSimulation(typename _TNodeNetData::TNodeI& niASubjectNode, int iIthTimeStep){
   if(niASubjectNode().GetImmunized())
      return;

   double dImmunityProbability = GetGaussianProbability(dHealingProbabilityMean,
                                                        dHealingProbabilityVariance,
                                                        (double)iIthTimeStep);

   if(VerifyProbability(dImmunityProbability)){
      niASubjectNode().SetInfected(false);
      niASubjectNode().SetImmunized(true);
   }else{
      int iNextNeighbor;
      int iNeighbors = niASubjectNode.GetOutDeg();
      for(int i = 0; i < iNeighbors; i++){
         if(VerifyProbability((double)fInfectProbability)){
            niASubjectNode().SetInfected(true);
            iNextNeighbor = niASubjectNode.GetOutNId(i);
            (pNodeNetData->GetNI(iNextNeighbor))().InfectNode();
         }
      }
   }
}

#endif
