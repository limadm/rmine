#pragma once
#ifndef _ProcessingRandomGraphGenerator_
#define _ProcessingRandomGraphGenerator_

#include <algorithm>

#include <Commons/stdafx.h>
#include <Dialogs/DialogParamsInput.h>

template <class _PGraph>
class TProcessingRandomGraphGenerator {
private:
  _PGraph Graph;
public:
  TProcessingRandomGraphGenerator(){
  }
  ~TProcessingRandomGraphGenerator(){ };
  void GenerateRandomGraph(_PGraph pAGraphPointer);
private:
  void Process(_PGraph pAGraphPointer, const int& Nodes, const int& Edges, const bool& IsDir = true, TRnd& Rnd = TInt::Rnd);
  int CalculateMaximumNumberOfEdges(int iGivenNumberOfNodes);
};
template <class _PGraph>
void TProcessingRandomGraphGenerator<_PGraph>::GenerateRandomGraph(_PGraph pAGraphPointer){
   TDialogParamsInput fpiDialogParamsInput(NULL, "Enter number of nodes and number of edges");
   fpiDialogParamsInput.SetParam1Name("Number of nodes");
   fpiDialogParamsInput.SetParam2Name("Number of edges");
   fpiDialogParamsInput.SetInitialValues("0","0","0","0");
   fpiDialogParamsInput.MakeDialog();
   if(fpiDialogParamsInput.ShowModal() == wxID_OK){
      int iNumberOfNodes = TConverter<int>::FromString(std::string(fpiDialogParamsInput.GetParam1().c_str()));
      int iNumberOfEdges = TConverter<int>::FromString(std::string(fpiDialogParamsInput.GetParam2().c_str()));
      int iMaximumNumberOfEdges = CalculateMaximumNumberOfEdges(iNumberOfNodes);
      iNumberOfEdges = std::min<int>(iNumberOfEdges,iMaximumNumberOfEdges);
      this->Process(pAGraphPointer,iNumberOfNodes,iNumberOfEdges);
   }
}
template <class _PGraph>
void TProcessingRandomGraphGenerator<_PGraph>::Process(_PGraph pAGraphPointer, const int& Nodes, const int& Edges, const bool& IsDir, TRnd& Rnd){
   pAGraphPointer->Reserve(Nodes, Edges);
   for (int node = 0; node < Nodes; node++) {
      IAssert(pAGraphPointer->AddNode(node) == node); 
   }
   for (int edge = 0; edge < Edges; edge++) {
      int SrcNId = Rnd.GetUniDevInt(Nodes);
      int DstNId = Rnd.GetUniDevInt(Nodes);
      while (SrcNId == DstNId ||
             pAGraphPointer->IsEdge(SrcNId, DstNId)||
             pAGraphPointer->IsEdge(DstNId, SrcNId)) { 
      SrcNId = Rnd.GetUniDevInt(Nodes);
      DstNId = Rnd.GetUniDevInt(Nodes); 
      }
      pAGraphPointer->AddEdge(SrcNId, DstNId);
      if (! IsDir) pAGraphPointer->AddEdge(DstNId, SrcNId);
   }
   pAGraphPointer->Defrag();

   wxFileDialog *wxfdChooseSaveFile = new wxFileDialog(NULL,"Choose/Create a file to save subgraph","","","*.*",wxSAVE);
   wxfdChooseSaveFile->SetFilename("Type_a_file_name_without_extension");
   wxfdChooseSaveFile->ShowModal();
   pAGraphPointer->WriteEdgeFile(std::string(wxfdChooseSaveFile->GetPath().c_str()) + "_ef.txt");
}
template <class _PGraph>
int TProcessingRandomGraphGenerator<_PGraph>::CalculateMaximumNumberOfEdges(int iGivenNumberOfNodes){
   int iSum = 0;
   for(int iCounter = iGivenNumberOfNodes; iCounter > 0; iCounter--){
      iSum += iCounter;
   }
   return iSum;
}
#endif