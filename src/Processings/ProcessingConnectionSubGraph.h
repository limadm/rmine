#pragma once
#ifndef _ProcessingConnectionSugGraph_
#define _ProcessingConnectionSugGraph_

/*Code by Hanghang Tong (htong@cs.cmu.edu*/

#include <math.h>

#include <Commons/stdafx.h>
#include <Commons/Auxiliar.h>
#include <Graphs/NodeNetTests.h>
#include <Graphs/NodePlot.h>
#include <Graphs/EdgePlot.h>

typedef int BOOL;

struct node{
   int iThisNodeIndex;
   int length;//how many adjacent neighbor
   int * index;
   float * weight;
};

struct graph{
	int nodenum;
	node * nodelist;
};

struct CepsNode{
   int idx;
   float * indscore;
   int qn;
   float comscore;
   CepsNode * next;
};

class RandomWalkWithRestart  
{
public:
	RandomWalkWithRestart();
	virtual ~RandomWalkWithRestart();
	
	float c;
	//query index
	int q;
	int ite_max;
	float tolorance;
	int length;

	float * r2;
	float * r1;

	//
	BOOL SetupRWR(int len, double my_c, int my_tol, int my_itemax);
	//delete r1,r2;
	BOOL DestroyRWR();

	float VecDiff(float *v1, float * v2);
	//v1=v2;
	BOOL VecCopy(float * &v1, float *v2);
	//v1 = gph * v2;
	BOOL VecMu(graph * gph, float * &v1, float * v2,float* Deg);

	BOOL InitRWR();
	BOOL IterateOne(graph * gph, float * Deg);
	BOOL RWR(graph * gph, float * Deg, int query);
};


template <class _TNodeNetData>
class TProcessingConnectionSubGraph {
private:
   TPt<_TNodeNetData> pNodeNetData;
	//the variable that should be setup at the very begining;
	int n;//total number of the nodes
	float ** ScoreMatrix;//n x n score matrix
	float * Deg;//degree vector;
   std::string * AuthorList;// author name list;
	RandomWalkWithRestart my_rwr;
	//setup at query time
	//int qnum;//query number
	int b;//budget
	int k;//soft-and coefficient
	int qn;//query number
	int CandSetSize;//candidate set size;
	float CandSetThre;
	CepsNode * CandList;//Candidate List for find a path;
	BOOL QueryExist;//Indicate if the query list exist
	int MaxPathLen;//maximum path length
	float **IndScore;//individual score list
	float * ComScore;//combined score list;
	float * SortComScore;//Sorted combined score list;
	int * SortComIdx;//Index of sorted combined score list;
	std::string * QueryList;//query list
	int * QueryInx;//query index;
	CepsNode * CepsIdx;//Output Ceps index;
	CepsNode * PathIdx;//Path Index;
	graph AdjGraph;//adjacent matrix;
private:
   float AdjPos(int i, int j);
	//if two node is adjacent
	BOOL isAdj(int idx1, int idx2);
	//Return the weight of AdjGraph(i,j)
	float GetEdgeWeight(int i, int j);
	/*********************************************/

	/********Operation related with CepsNodes************/
	int CepsNodeLength(CepsNode * CNList);
	BOOL InsertCepsNode(CepsNode * &CNList, CepsNode * CN);
	//
	BOOL CepsNodeExist(CepsNode * CNList, CepsNode * CN);
	//return the point of i th node in CandList;
	CepsNode * CepsNodePos(CepsNode * CNList, int i);
	//return the point of (i-1) th node in CandList;
	CepsNode * CepsNodePre(CepsNode * CNList, int i);
	//Exchange the i^th and j^th nodes in CandList;
	BOOL CepsNodeExchange(CepsNode * CNList, int i,int j);
	//sort CandList according to qidx individual score;
	BOOL SortCepsNode(CepsNode * CNList, int pos, int flag);

	BOOL MergeCepsNode(CepsNode * CNList1, CepsNode * CNList2);
	BOOL PrintCepsNode(CepsNode * CNList);
	//
	CepsNode * CepsNodeNew(int i);
	
	BOOL CepsNodeDelete(CepsNode * &CNList);
	/*********************************************/

	/*******Pre-Load***********************************/
	//Setup n, ScoreMatrix, AuthorList, AdjGraph;
	BOOL Pre_Ceps(std::string sFile, std::string aFile, std::string gFile);
	//Destroy ScoreMatrix, AuthorList, AdjGraph;
	BOOL Post_Ceps();
	/*********************************************/

	//see if the author is in the author list; return -1 if not exist; else the au position
	int IsAuthor(std::string au);
	//Setup QueryList from the input;
	BOOL SetQueryList(std::string * InputList, int len);
	//QueryList to QueryInx; also setup QueryExist
	//BOOL SetQueryIndex();
	//Setup individual score from querylist;
	BOOL SetIndScore();
	//Combine individual socre
	float CombineScoreOne(float * iScore, int k);
	BOOL CombineScore();
	//setup CandSetThre
	BOOL SetupThre();
	//combine all above modules together: given input query list, setup combine score list and candsetth;
	BOOL SetupComScore(std::string * InputList, int len);

	//see if idx is in the list of ConList
	int IsInList(int * ConList, int len, int val);
	//sort the list, flag==1, maximum first; else, minimun first;
	//and return an index list, indicating which elmement shall be in which place in the ordered list
	int * SortList(float * slist, int len, int flag);
	//Merge two index list (add idx2 into idx1)
	BOOL MergeList(int * idx1, int len1, int * idx2, int len2);


	//Pick up the next most promising node
	//CurSG: is the current partially built subgraph
	int Pick_Up_One(CepsNode * CepsIdx);
	//Generate Candidate List
	//Returning a sorted index for path finding, (Combinded Score>=CandSetThre, and IndScore between that startnode and endnode)
	BOOL Gene_Cand_List(int startnode, int endnode);
	//Find a path according to CandList
	//flag....
	BOOL Find_Next_Path(int flag); 
	BOOL Find_Next_Steiner();

	BOOL BuildCeps();//
	BOOL QueryCeps(std::string * InputList, int len, int budget, int k_soft);
   int GetIndexInHangHangGraph(int iNodeIndex, int iNNodes);
   int GetIndexInNodeNetGraph(int iNodeIndex);
public:
   BOOL QueryCeps(std::list<std::string> &lListOfNodeLabels, int budget);
	BOOL OutputCeps(std::string pre_outfile);
public:
   /*This constructor permits CEPS to work with a given TPt<_TNodeNetData> information, which is replicated*/
   TProcessingConnectionSubGraph(TPt<_TNodeNetData> pANodeNetData):pNodeNetData(pANodeNetData){
	   CandSetSize = 400;          QueryExist = FALSE;
	   MaxPathLen = 10;            ScoreMatrix = NULL;
	   AuthorList = NULL;          IndScore = NULL;
	   ComScore = NULL;            SortComScore = NULL;
	   SortComIdx = NULL;          CandList = NULL;
	   QueryList = NULL;           QueryInx = NULL;
	   CepsIdx = NULL;             PathIdx = NULL;
      Deg = NULL;

	   //Setup n, ScoreMatrix, AuthorList, AdjGraph;
      int iNextNeighbor,iNeighCounter,iNeighbors;
      int iEdgeIndex, iGraphIndex, iNeighborIndex, iWeight;
      int iIndexInHangHangGraph;
#ifndef PERFORMANCE
      TNodeNetTests<TNodePlot, TEdgePlot>::TNodeI NI = pANodeNetData->BegNI();
#else
      TNodeNetTests<TNodePerformance, TEdgePlot>::TNodeI NI = pANodeNetData->BegNI();
#endif
	   AdjGraph.nodenum = pANodeNetData->GetNodes();
	   AdjGraph.nodelist = new node[AdjGraph.nodenum];
      n = AdjGraph.nodenum;

   	Deg = new float[AdjGraph.nodenum];
	   for(int i=0;i<n;i++)
		   Deg[i] = 0.0f;

	   if(AuthorList){
		   delete []AuthorList;
		   AuthorList = NULL;
	   }
	   AuthorList = new std::string[n];

      iGraphIndex = 0;
      /*Translates nodes information to Hanghang's structure*/
      for(NI = pANodeNetData->BegNI(); NI < pANodeNetData->EndNI(); NI++){
         /*Store original id*/
         AdjGraph.nodelist[iGraphIndex].iThisNodeIndex = NI.GetId();
         /*Store degree of out edges*/
		   AdjGraph.nodelist[iGraphIndex].length = NI.GetOutDeg();
         if(NI.GetOutDeg() > 0){  /*if it has out edges*/
		      AdjGraph.nodelist[iGraphIndex].weight = new float[NI.GetOutDeg()];  /*Initialize vector of out edges*/
		      AdjGraph.nodelist[iGraphIndex].index = new int[NI.GetOutDeg()];     /*Initialize vector of out nodes*/
         }else{
		      AdjGraph.nodelist[iGraphIndex].weight = NULL;
		      AdjGraph.nodelist[iGraphIndex].index = NULL;
         }
         /*Store label*/
         AuthorList[iGraphIndex] = NI().GetLabel();
         /*Increase new index*/
         iGraphIndex++;
      }

      /*Translates out edges and out nodes information to Hanghang's structure*/
	   for(iGraphIndex=0; iGraphIndex < pANodeNetData->GetNodes(); iGraphIndex++){
         /*Get original node instance in pANodeNetData*/
         NI = pANodeNetData->GetNI(AdjGraph.nodelist[iGraphIndex].iThisNodeIndex);
         /*Out edges*/
         iNeighbors = NI.GetOutDeg();    iNeighborIndex = 0;
         /*Scans all out edges following Hanghang's index*/
         for(iNeighCounter = 0; iNeighCounter < iNeighbors; iNeighCounter++){
            /*Next out edge*/
            iNextNeighbor = NI.GetOutNId(iNeighCounter);
            /*Index in Hanghang structure*/
            iIndexInHangHangGraph = GetIndexInHangHangGraph(iNextNeighbor,pANodeNetData->GetNodes());
            AdjGraph.nodelist[iGraphIndex].index[iNeighborIndex] = iIndexInHangHangGraph;  /*Storing*/
            /*Retrieve original edge*/
            pANodeNetData->IsEdge(AdjGraph.nodelist[iGraphIndex].iThisNodeIndex,iNextNeighbor,iEdgeIndex);
            if(iEdgeIndex != -1){  /*If edge is ok*/
               /*Get edge's weight*/
               iWeight = pANodeNetData->GetEI(iEdgeIndex)().GetWeight();
               /*Store it for the corresponding out node*/
               AdjGraph.nodelist[iGraphIndex].weight[iNeighborIndex] = iWeight;
               /*Acumulate weight for weight total summarization*/
			      Deg[iIndexInHangHangGraph] += iWeight;
            }
            /*Next out node*/
            iNeighborIndex++;
         }
      }            
	   my_rwr.SetupRWR(n, 0.9, 1e-9, 50);
      int iweight,iindex,totaledges,total;
      total = 0;
      std::string sAuthor;
	   for(int i=0; i < n; i++){
         iNeighCounter=AdjGraph.nodelist[i].iThisNodeIndex;
         sAuthor = AuthorList[i];
		   totaledges=AdjGraph.nodelist[i].length;
         total += totaledges;
         for(int j=0; j < totaledges; j++){
		      iweight=AdjGraph.nodelist[i].weight[j];
		      iindex=AdjGraph.nodelist[i].index[j];
         }

      }
   }
   ~TProcessingConnectionSubGraph(){
	   delete []AuthorList;
	   for(int i=0;i<n;i++)
	   {
         if(AdjGraph.nodelist[i].index != NULL)
		      delete []AdjGraph.nodelist[i].index;
         if(AdjGraph.nodelist[i].weight != NULL)
		      delete []AdjGraph.nodelist[i].weight;
	   }
	   delete [] AdjGraph.nodelist;
	   if(SortComScore)
		   delete []SortComScore;
      if(QueryInx)
		   delete []QueryInx;
      if(!SortComIdx)
	   	delete []SortComIdx;
   };
};

template <class _TNodeNetData>
BOOL TProcessingConnectionSubGraph<_TNodeNetData>::QueryCeps(std::list<std::string> &lListOfNodeLabels, int budget){
   if((budget == 0) || (lListOfNodeLabels.size() == 0))
      return false;
   int iCounter = 0;
   std::list<std::string>::const_iterator lIterator;
   std::string * sInputList = new std::string[lListOfNodeLabels.size()];
   for(lIterator = lListOfNodeLabels.begin(); lIterator != lListOfNodeLabels.end(); ++lIterator){
      sInputList[iCounter] = (*lIterator).c_str();
      iCounter++;
   }   
   return QueryCeps(sInputList, lListOfNodeLabels.size(), budget, lListOfNodeLabels.size());
}
template <class _TNodeNetData>
BOOL TProcessingConnectionSubGraph<_TNodeNetData>::QueryCeps(std::string * InputList, int len, int budget, int k_soft)
{
	b = budget;
	k = k_soft;


	if(!SetupComScore(InputList,len))
		return FALSE;

	/*for(int i=0;i<n;i++)
	{
		for(int j=0;j<qn;j++)
			printf("%f ",IndScore[i][j]);
		printf("%f\n",ComScore[i]);
	}*/

	
	BOOL Re = BuildCeps();

	/******output something*******/
	/******delete something*******/
	return Re;
}

template <class _TNodeNetData>
int TProcessingConnectionSubGraph<_TNodeNetData>::IsAuthor(std::string au)
{
	if(!AuthorList)
		return -1;
	
	for(int i=0;i<n;i++)
	{
      if((*(AuthorList+i)).compare(au) == 0)
			return i;
	}
	return -1;
}

template <class _TNodeNetData>
BOOL TProcessingConnectionSubGraph<_TNodeNetData>::SetQueryList(std::string * InputList, int len)
{
	if(!InputList)
	{
		qn = 0;
		QueryExist = FALSE;
		return FALSE;
	}
	qn = len;
	
	int pos;
	if(QueryInx)
		delete []QueryInx;
	QueryInx = new int[qn];
	for(int i=0;i<qn;i++)
	{
		pos = IsAuthor(*(InputList+i));
		if(pos==-1)
		{
			delete []QueryInx;
			QueryExist = FALSE;
			return FALSE;
		}
		else
			QueryInx[i] = pos;

	}
	QueryExist = TRUE;
	return TRUE;
}
template <class _TNodeNetData>
int TProcessingConnectionSubGraph<_TNodeNetData>::GetIndexInNodeNetGraph(int iNodeIndex){
   return AdjGraph.nodelist[iNodeIndex].iThisNodeIndex;
}

template <class _TNodeNetData>
int TProcessingConnectionSubGraph<_TNodeNetData>::GetIndexInHangHangGraph(int iNodeIndex, int iNNodes){
	for(int i=0; i < iNNodes; i++){
      if(AdjGraph.nodelist[i].iThisNodeIndex == iNodeIndex){
         return i;
      }
   }
   return -1;
}

template <class _TNodeNetData>
BOOL TProcessingConnectionSubGraph<_TNodeNetData>::SetIndScore()
{
	//new it in Pre_Ceps//delete it in Post_Ceps
	IndScore = new float * [n];
	for(int i=0;i<n;i++)
	{
		IndScore[i] = new float[qn];		
	}
	for(int j=0;j<qn;j++)
	{
		my_rwr.RWR(&AdjGraph,Deg,QueryInx[j]);
		for(int i=0;i<n;i++)
			IndScore[i][j] = my_rwr.r1[i];
	}
	return TRUE;
}

template <class _TNodeNetData>
float TProcessingConnectionSubGraph<_TNodeNetData>::CombineScoreOne(float * iScore, int k)
{
	float Re = 1.0 * 100000;
	if(k==qn)//And Query
	{
		for(int i=0;i<qn;i++)
			Re = Re * iScore[i];
	}
	//K_SoftAnd Has not yet implemented//
	return Re;
}

template <class _TNodeNetData>
BOOL TProcessingConnectionSubGraph<_TNodeNetData>::CombineScore()
{
	//new it in Pre_Ceps//delete it in Post_Ceps
	ComScore = new float [n];
	for(int i=0;i<n;i++)
	{
		ComScore[i] = CombineScoreOne(IndScore[i],k);
	}
	return TRUE;
}

template <class _TNodeNetData>
BOOL TProcessingConnectionSubGraph<_TNodeNetData>::SetupThre()
{
	if(SortComScore)
		delete []SortComScore;
 
	SortComScore = new float [n];
	for(int i=0;i<n;i++)
		SortComScore[i] = ComScore[i];
	if(!SortComIdx)
		delete []SortComIdx;
	SortComIdx = SortList(SortComScore,n,1);
	CandSetThre = SortComScore[CandSetSize];

	return TRUE;
}

template <class _TNodeNetData>
BOOL TProcessingConnectionSubGraph<_TNodeNetData>::SetupComScore(std::string * InputList, int len)
{
   float fTeste;
	if(!SetQueryList(InputList,len))
		return FALSE;
	if(QueryExist)
	{
		SetIndScore();
		CombineScore();
		/***************************************/
		for(int i=0;i<n;i++)
		{
			for(int j=0;j<qn;j++)
			{
            fTeste = IndScore[i][j];
				printf("%f ",IndScore[i][j]);
			}
         fTeste = ComScore[i];
			printf("%f\n", ComScore[i]);
		}
		/*************************************/

		SetupThre();

		/*for(int i=0;i<n;i++)
		{			
			printf("%d: %f\n", SortComIdx[i], SortComScore[i]);
		}*/

	}

	return TRUE;
}
template <class _TNodeNetData>
int TProcessingConnectionSubGraph<_TNodeNetData>::IsInList(int * ConList, int len, int val)
{
	if(!ConList)
		return -1;
	
	for(int i=0;i<len;i++)
	{
		if(ConList[i]==val)
			return i;
	}
	return -1;
}

template <class _TNodeNetData>
int * TProcessingConnectionSubGraph<_TNodeNetData>::SortList(float * slist, int len, int flag)
{
	if(!slist)
		return NULL;
	
	int * idx = new int [len];

	for(int i=0;i<len;i++)
	{
		idx[i] = i;
	}
	float tmp_val;
	int tmp_idx;
	for(int i=0;i<len-1;i++)
	{
		for(int j=i+1;j<len;j++)
			if(flag==1)//large first
			{
				if(slist[j]>slist[i])
				{
					tmp_val = slist[i];
					slist[i] = slist[j];
					slist[j] = tmp_val;

					tmp_idx = idx[i];
					idx[i] = idx[j];
					idx[j] = tmp_idx;

				}
			}
			else
			{
				if(slist[j]<slist[i])
				{
					tmp_val = slist[i];
					slist[i] = slist[j];
					slist[j] = tmp_val;

					tmp_idx = idx[i];
					idx[i] = idx[j];
					idx[j] = tmp_idx;
				}
			}
	}
	return idx;
}

template <class _TNodeNetData>
int TProcessingConnectionSubGraph<_TNodeNetData>::MergeList(int * idx1, int len1, int * idx2, int len2)
{
	if(!idx1||!idx2)
		return 0;
	int len = 0;
	int tmp;
	int * tmp_idx = new int[len2];
	for(int i=0;i<len2;i++)
	{
		tmp = IsInList(idx1,len1,idx2[i]);
		if(tmp==-1)
		{
			len = len + 1;
			tmp_idx[i] = idx2[i];
		}
	}

	int * idx0 = new int[len1+len];
	for(int i=0;i<len1;i++)
		idx0[i] = idx1[i];
	for(int i=len1;i<len1+len;i++)
		idx0[i] = tmp_idx[i];
	delete []tmp_idx;
	delete []idx1;

	idx1 = idx0;
	return len+len1;
}

//Setup n, ScoreMatrix, AuthorList, AdjGraph;
template <class _TNodeNetData>
BOOL TProcessingConnectionSubGraph<_TNodeNetData>::Pre_Ceps(std::string sFile, std::string aFile, std::string gFile)
{
	
	/*FILE * fp1= fopen(sFile, "r");
	if(!fp1)
	{
		fclose(fp1);
		return FALSE;
	}*/
	FILE * fp2= fopen(aFile.c_str(), "r");
	if(!fp2)
	{
		fclose(fp2);
		return FALSE;
	}
	FILE * fp3= fopen(gFile.c_str(), "r");
	if(!fp3)
	{
		fclose(fp3);
		return FALSE;
	}

	std::string au;
	int i,j;
	int idx,num;
	float weight;

	//Setup Author list
	fscanf(fp2, "%d\n", &n);
	if(AuthorList)
	{
		delete []AuthorList;
		AuthorList = NULL;
	}
	AuthorList = new std::string[n];
	//for(int i=0;i<n;i++)
	//	AuthorList[i] = "tong";
	for(int i=0;i<n;i++)
	{
		fscanf(fp2, "%s", au);
		//sprintf(AuthorList[i],"%s",au);
		//AuthorList[i].Format("%s", au);
		AuthorList[i] = au;
	}
	fclose(fp2);
	

	//Setup Graph 
	fscanf(fp3, "%d\n", &n);
	AdjGraph.nodenum = n;
	AdjGraph.nodelist = new node [n];
	Deg = new float[n];
	for(int i=0;i<n;i++)
		Deg[i] = 0.0f;
	for(int i=0;i<n;i++)
	{
		fscanf(fp3, "%d:%d\n", &idx, &num);
		AdjGraph.nodelist[i].length = num;		
		AdjGraph.nodelist[i].weight = new float [num];
		AdjGraph.nodelist[i].index = new int [num];
		for(j=0;j<num;j++)
		{
			fscanf(fp3, "%d:%f ", &idx, &weight);
			AdjGraph.nodelist[i].index[j] = idx-1;
			AdjGraph.nodelist[i].weight[j] = weight;
			Deg[idx-1] += weight;
		}
		fscanf(fp3,"\n");
	}
	fclose(fp3);

	//
	//for(int i=0;i<n;i++)
	//	printf("%f\n",Deg[i]);
	//Setup Score matrix
	/*if(ScoreMatrix)
	{
		for(int i=0;i<n;i++)
			delete []ScoreMatrix[i];
		delete []ScoreMatrix;
	}
	ScoreMatrix = new float *[n];
	for(int i=0;i<n;i++)
		ScoreMatrix[i] = new float [n];

	fscanf(fp1, "%d\n", &n);

	for(int i=0;i<n;i++)
	{
		for(j=0;j<n;j++)
		{
			fscanf(fp1,"%f ", &weight);
			ScoreMatrix[i][j] = weight;
		}
	}
	fclose(fp1);*/
	//Setup RWR
	my_rwr.SetupRWR(n, 0.9, 1e-9, 50);
	return TRUE;
}
//Destroy ScoreMatrix, AuthorList, AdjGraph;
template <class _TNodeNetData>
BOOL TProcessingConnectionSubGraph<_TNodeNetData>::Post_Ceps()
{
	//delete AuthorList
	delete []AuthorList;
	for(int i=0;i<n;i++)
	{
		delete []AdjGraph.nodelist[i].index;
		delete []AdjGraph.nodelist[i].weight;
	}
	delete [] AdjGraph.nodelist;
	delete [] Deg;
	//delete AdjGraph


	//delete Scorematrix;
	/*if(!ScoreMatrix)
	{
		for(int i=0;i<n;i++)
			delete []ScoreMatrix[i];
		delete []ScoreMatrix;
	}*/

	//Destroy RWR
	my_rwr.DestroyRWR();

	return TRUE;
}
template <class _TNodeNetData>
int TProcessingConnectionSubGraph<_TNodeNetData>::Pick_Up_One(CepsNode * CepsIdx)
{
	int CurSGSize = CepsNodeLength(CepsIdx);
	int * CurSG = new int[CurSGSize];
	CepsNode * p = CepsIdx;
	for(int i=0;i<CurSGSize;i++)
	{
		CurSG[i] = p->idx;
		p = p->next;
	}
	for(int i=0;i<CandSetSize;i++)
	{
		if(IsInList(CurSG,CurSGSize,SortComIdx[i])==-1)
		{
			delete []CurSG;
			return SortComIdx[i];
		}
	}
	delete []CurSG;
	return -1;
}
template <class _TNodeNetData>
BOOL TProcessingConnectionSubGraph<_TNodeNetData>::Gene_Cand_List(int startnode, int endnode)
{
	int pos;
	if(CandList)
		CepsNodeDelete(CandList);

	pos = IsInList(QueryInx,qn,startnode);
	if(pos==-1)
		return FALSE;

	CepsNode * p1 = CepsNodeNew(startnode);
	if(p1)
		InsertCepsNode(CandList, p1);
	p1 = CepsNodeNew(endnode);
	if(p1)
		InsertCepsNode(CandList, p1);


	float mn_is,mx_is;
	if(IndScore[startnode][pos]>IndScore[endnode][pos])
	{
		mn_is = IndScore[endnode][pos];
		mx_is = IndScore[startnode][pos];
	}
	else
	{
		mx_is = IndScore[endnode][pos];
		mn_is = IndScore[startnode][pos];
	}
	
	int idx;
	for(int i=0;i<CandSetSize&&i<n;i++)
	{
		idx = SortComIdx[i];
		if(idx!=startnode&&idx!=endnode)
//			&&IndScore[idx][pos]>=mn_is
//			&&IndScore[idx][pos]<=mx_is)
		{
			p1 = CepsNodeNew(idx);
			if(p1)
				InsertCepsNode(CandList, p1);
		}
	}
	//PrintCepsNode(CandList);
	SortCepsNode(CandList, pos,1);
	//PrintCepsNode(CandList);

	//Make sure the endnode is at the END in the case of tie
	int len = CepsNodeLength(CandList);
	p1 = CepsNodePos(CandList,len-1);
	if(p1->idx!=endnode){
		int j;
		CepsNode * p2 = CandList;
		for(int i=0;i<len;i++){
			if(p2->idx==endnode){
				j=i;
				break;
			}else{
				p2 = p2->next;
			}
		}		
		CepsNodeExchange(CandList,j,len-1);
	}

	return TRUE;
}
template <class _TNodeNetData>
BOOL TProcessingConnectionSubGraph<_TNodeNetData>::Find_Next_Path(int flag){
	int len = CepsNodeLength(CandList);
	int i,j,k;
	int kp,st,edd,pt;
	float score;
	int ** Pre_Pos = new int * [MaxPathLen];
	float ** DP_Table = new float * [MaxPathLen];
	for(int i=0;i<MaxPathLen;i++)
	{
		Pre_Pos[i] = new int [len];
		DP_Table[i] = new float [len];
	}
	for(int i=0;i<MaxPathLen;i++)
	{
		for(j=0;j<len;j++)
		{
			Pre_Pos[i][j] = -1;
			DP_Table[i][j]= 0.0f;
		}
	}

	CepsNode * p = CandList, *p1;
	DP_Table[0][0] = CandList->comscore;
	Pre_Pos[0][0] = 0;

	//Setup DP table
	for(int i=1;i<len;i++)//for every candidate node
	{
		p = p->next;
		if(CepsNodeExist(CepsIdx,p))
			st = 1;		
		else
			st = 2;
		
		if(i>=MaxPathLen-1)
			edd = MaxPathLen;
		else
			edd = i+1;
		for(k=st;k<=edd;k++)//every possible length of path
		{
			if(st==1)
				kp = k;
			else
				kp = k-1;
			score = 0.0f;
			pt = -1;
			for(j=0;j<i;j++)//everything node before i
			{
				p1 = CepsNodePos(CandList,j);
				if(isAdj(p->idx,p1->idx)&&
					DP_Table[kp-1][j]>score)//is adjacent
				{
					score = DP_Table[kp-1][j];
					pt = j;
				}
			}
			if(score>0)
			{
				DP_Table[k-1][i] = score+p->comscore;
				Pre_Pos[k-1][i] = pt;
			}
		}
	}

	//find max_score/length
	score = 0.0f;
	pt = -1;
	int pl = 0;
	if(flag==0)
		st = 2;
	else
	{
		st = 1;
		for(k=1;k<MaxPathLen;k++)
		{
			if(DP_Table[k][len-1]>0)
				st=2;
		}		
		
	}
	for(k=st;k<=MaxPathLen;k++)
	{
		if(DP_Table[k-1][len-1]/k>score)
		{
			score = DP_Table[k-1][len-1]/k;
			pl = k;
		}
	}
	if(pl==0)
		return FALSE;

	//find path
	if(PathIdx)
	{
		CepsNodeDelete(PathIdx);
		PathIdx = NULL;
	}
	k = pl;
	j = len-1;
	i=0;
	while(i<len)
	{
		i++;
		p = CepsNodePos(CandList,j);
		p = CepsNodeNew(p->idx);
		InsertCepsNode(PathIdx,p);

		if(p->idx==CandList->idx)//find the starting point
			i = len;

		j = Pre_Pos[k-1][j];
		if(!CepsNodeExist(CepsIdx,p))
			k = k-1;
	}

	//PrintCepsNode(PathIdx);


	for(int i=0;i<MaxPathLen;i++)
	{
		delete []Pre_Pos[i];
		delete []DP_Table[i];
	}
	delete []Pre_Pos;
	delete []DP_Table;
	return TRUE;

}
template <class _TNodeNetData>
BOOL TProcessingConnectionSubGraph<_TNodeNetData>::Find_Next_Steiner()
{
	int endnode = Pick_Up_One(CepsIdx);
	if(endnode==-1)
		return FALSE;
	PrintCepsNode(CepsIdx);
	for(int i=0;i<qn;i++)
	{
		Gene_Cand_List(QueryInx[i],endnode);
		/***********************************************/
		/*CepsNode *p = CandList;
		printf("%d\n",p->idx);
		while(p->next)
		{
			p = p->next;
			printf("%d\n",p->idx);

		}*/
        /***********************************************/
		//PrintCepsNode(CandList);
		Find_Next_Path(i);

		//PrintCepsNode(CepsIdx);
		//PrintCepsNode(PathIdx);
		
		MergeCepsNode(CepsIdx,PathIdx);

		//PrintCepsNode(CepsIdx);
		
		CepsNodeDelete(PathIdx);
	}
	//PrintCepsNode(CepsIdx);
	return TRUE;
}
template <class _TNodeNetData>
BOOL TProcessingConnectionSubGraph<_TNodeNetData>::BuildCeps()
{
	CepsNode * p;
	if(CepsIdx)
	{
		CepsNodeDelete(CepsIdx);
		CepsIdx = NULL;
	}
	//Include query node
	for(int i=0;i<qn;i++)
	{
		p = CepsNodeNew(QueryInx[i]);
		InsertCepsNode(CepsIdx,p);
	}
	int i = 0;
	while(CepsNodeLength(CepsIdx)<b&&i<=b)
	{
		Find_Next_Steiner();
		i++;
	}

	return TRUE;

}

template <class _TNodeNetData>
BOOL TProcessingConnectionSubGraph<_TNodeNetData>::isAdj(int idx1, int idx2)
{
	if(idx1<0||idx1>=n)
		return FALSE;
	if(idx2<0||idx2>=n)
		return FALSE;
	for(int i=0;i<AdjGraph.nodelist[idx1].length;i++)
	{
		if(AdjGraph.nodelist[idx1].index[i]==idx2)
			return TRUE;
	}
	return FALSE;
}

template <class _TNodeNetData>
float TProcessingConnectionSubGraph<_TNodeNetData>::AdjPos(int i, int j)
{
	if(i<0||i>=n)
		return -1.0f;
	if(j<0||j>=n)
		return -1.0f;
	for(int k=0;k<AdjGraph.nodelist[i].length;k++)
	{
		if(AdjGraph.nodelist[i].index[k]==j)
			return AdjGraph.nodelist[i].weight[k];
	}
	return 0.0f;

}

template <class _TNodeNetData>
float TProcessingConnectionSubGraph<_TNodeNetData>::GetEdgeWeight(int i, int j)
{
	if(i<0||i>=n)
		return -1.0f;
	if(j<0||j>=n)
		return -1.0f;
	for(int k=0;k<AdjGraph.nodelist[i].length;k++)
	{
		if(AdjGraph.nodelist[i].index[k]==j)
			return AdjGraph.nodelist[i].weight[k];
	}
	return 0.0f;

}
template <class _TNodeNetData>
int TProcessingConnectionSubGraph<_TNodeNetData>::CepsNodeLength(CepsNode * CNList)
{
	if(!CNList)
		return 0;
	CepsNode * cn = CNList;
	int len = 1;
	while(cn->next)
	{
		len = len + 1;
		cn = cn->next;
	}
	return len;
}
template <class _TNodeNetData>
BOOL TProcessingConnectionSubGraph<_TNodeNetData>::InsertCepsNode(CepsNode * &CNList, CepsNode * CN)
{
	int len = CepsNodeLength(CNList);
	if(len==0)
	{
		CNList = CN;
		return TRUE;
	}
	CepsNode * p_cn = CNList;
	while(p_cn->next)
	{		
		p_cn = p_cn->next;
	}
	p_cn->next = CN;
	return TRUE;
}

template <class _TNodeNetData>
CepsNode * TProcessingConnectionSubGraph<_TNodeNetData>::CepsNodePos(CepsNode * CNList, int i)
{
	int len = CepsNodeLength(CNList);
	if(i<0||i>len)
		return NULL;
	CepsNode * p1 = CNList;
	for(int k=0;k<i;k++)
		p1 = p1->next;
	return p1;
}

template <class _TNodeNetData>
CepsNode * TProcessingConnectionSubGraph<_TNodeNetData>::CepsNodePre(CepsNode * CNList,int i)
{
	return CepsNodePos(CNList, i-1);
}
template <class _TNodeNetData>
BOOL TProcessingConnectionSubGraph<_TNodeNetData>::CepsNodeExchange(CepsNode * CNList,int i,int j)
{
	CepsNode * p1 = CepsNodePos(CNList, i);
	if(!p1)
		return FALSE;
	CepsNode * p2 = CepsNodePos(CNList, j);
	if(!p2)
		return FALSE;
	if(i==j)
		return TRUE;

	//exchange index;
	int tmp;
	tmp = p1->idx;
	p1->idx = p2->idx;
	p2->idx = tmp;

	//exchange individual score;
	float t2;
	for(int k=0;k<p1->qn;k++)
	{
		t2 = p1->indscore[k];
		p1->indscore[k] = p2->indscore[k];
		p2->indscore[k] = t2;
	}
	//exchange combine score;
	t2 = p1->comscore;
	p1->comscore = p2->comscore;
	p2->comscore = t2;

	return TRUE;
}
template <class _TNodeNetData>
BOOL TProcessingConnectionSubGraph<_TNodeNetData>::SortCepsNode(CepsNode * CNList, int pos, int flag)
{
	//int pos = IsInList(QueryInx,qn,qidx);
	if(pos<0||pos>qn)
		return FALSE;
	int len = CepsNodeLength(CNList);
	CepsNode  *p1, *p2;
	for(int i=0;i<len-1;i++)
	{
		p1 = CepsNodePos(CNList, i);

		for(int j=i+1;j<len;j++)
		{
			p2 = CepsNodePos(CNList, j);
			if(flag==1)//largest first
			{
				if(p2->indscore[pos]>p1->indscore[pos])
					CepsNodeExchange(CNList, i,j);
			}//small first
			else
			{
				if(p2->indscore[pos]<p1->indscore[pos])
					CepsNodeExchange(CNList, i,j);
			}
		}
	}
	return TRUE;

}
template <class _TNodeNetData>
CepsNode * TProcessingConnectionSubGraph<_TNodeNetData>::CepsNodeNew(int i)
{
	if(i<0||i>n)
		return NULL;
	CepsNode * p1 = new CepsNode;//
	p1->indscore = new float [qn];
	p1->next = NULL;
	p1->idx = i;
	p1->qn = qn;
	for(int j=0;j<qn;j++)
	{
		p1->indscore[j] = IndScore[i][j];
	}
	p1->comscore = ComScore[i];
	p1->next = NULL;
	return p1;
}
template <class _TNodeNetData>
BOOL TProcessingConnectionSubGraph<_TNodeNetData>::CepsNodeExist(CepsNode * CNList, CepsNode * CN)
{
	int len = CepsNodeLength(CNList);
	if(len<=0||!CN)
		return FALSE;
	CepsNode * p = CNList;
	for(int i=0;i<len;i++)
	{
		if(p->idx==CN->idx)
			return TRUE;
		p = p->next;
	}
	return FALSE;		
}
template <class _TNodeNetData>
BOOL TProcessingConnectionSubGraph<_TNodeNetData>::MergeCepsNode(CepsNode * CNList1, CepsNode * CNList2)
{
	int len1 = CepsNodeLength(CNList1);
	if(len1<=0)
		return FALSE;
	int len2 = CepsNodeLength(CNList2);
	if(len2<=0)
		return FALSE;

	CepsNode *p = CNList2;
	CepsNode *p1;
	for(int i=0;i<len2;i++)
	{
		if(!CepsNodeExist(CNList1,p))
		{
			p1 = CepsNodeNew(p->idx);
			InsertCepsNode(CNList1,p1);
		}
		p = p->next;
	}
	return TRUE;
}
template <class _TNodeNetData>
BOOL TProcessingConnectionSubGraph<_TNodeNetData>::CepsNodeDelete(CepsNode * &CNList)
{
	if(!CNList)
		return FALSE;
	int len = CepsNodeLength(CNList);
	CepsNode * p = CNList, *p1;
	while(p->next)
	{
		p1 = p->next;
		//delete p
		delete []p->indscore;
		delete p;
		p = p1;
	}
	delete []p->indscore;
	delete p;
	CNList  = NULL;
	return TRUE;

}
template <class _TNodeNetData>
BOOL TProcessingConnectionSubGraph<_TNodeNetData>::PrintCepsNode(CepsNode * CNList)
{
	if(!CNList)
	{
		printf("empty CepsNode List\n");
		return FALSE;
	}
	CepsNode *p = CNList;
	printf("\n\n");
	printf("%d ",p->idx);
	for(int i=0;i<p->qn;i++)
		printf("%f ", p->indscore[i]);
	printf("%f\n", p->comscore);
	while(p->next)
	{
		p = p->next;
		printf("%d ",p->idx);
		for(int i=0;i<p->qn;i++)
			printf("%f ", p->indscore[i]);
		printf("%f\n", p->comscore);
	}
	printf("\n\n");
	return TRUE;
}

template <class _TNodeNetData>
BOOL TProcessingConnectionSubGraph<_TNodeNetData>::OutputCeps(std::string pre_outfile){
	if(!CepsIdx)
		return FALSE;
   /*Get size of graph*/
	int len = CepsNodeLength(CepsIdx);

   /*Reset all nodes color and visibility*/
   for(typename _TNodeNetData::TNodeI NI = pNodeNetData->BegNI(); NI < pNodeNetData->EndNI(); NI++) {
      NI().SetColor("125_125_125");
      NI().SetVisible(false);
   }  

   int iIndexInNodeNetGraph, iAdjacentIndexInNodeNetGraph;

	CepsNode * p;
	//output node file
	std::string NodeFile;
   NodeFile += pre_outfile;
   NodeFile += "_nf.txt";

   FILE * fp = fopen(NodeFile.c_str(), "w");
	if(!fp)
		return FALSE;
	p = CepsIdx;
   /*Check all nodes in Hanghang's structure*/
	for(int i=0;i<len;i++){
      /*Get index in original graph*/
      iIndexInNodeNetGraph = GetIndexInNodeNetGraph(p->idx);
      /*Write id and label*/
      fprintf(fp,"%d %s ",iIndexInNodeNetGraph, AuthorList[p->idx].c_str());
      /*If it belongs to set of query nodes*/
		if(IsInList(QueryInx,qn,p->idx)!=-1)
			fprintf(fp,"d 255_000_000\n");     /*Write shape = d, color = red*/
		else
			fprintf(fp,"c 000_000_255\n");     /*Else write shape = c, color = blue*/
      /*Next*/
		p = p->next;
	}
	fclose(fp);

	CepsNode * p1;
	float weight;

   /*Scan all nodes in answer set*/
	for(int i=0;i<len;i++){
      /*Next node in answer set*/
		p = CepsNodePos(CepsIdx,i);
      iIndexInNodeNetGraph = GetIndexInNodeNetGraph(p->idx); /*Index in the original structure*/
      /*Set color and visibility of node*/
      pNodeNetData->GetNI(iIndexInNodeNetGraph)().SetColor("000_000_255");
      pNodeNetData->GetNI(iIndexInNodeNetGraph)().SetVisible(true);
      /*Scan all nodes in answer set*/   
		for(int j=0;j<len;j++){
         /*Get index*/
			p1 = CepsNodePos(CepsIdx,j);
         /*Verify if p and p1 define an edge in the answer set*/
			weight = AdjPos(p->idx,p1->idx);
         if(weight>0){
            /*Retrieve original index*/
            iAdjacentIndexInNodeNetGraph = GetIndexInNodeNetGraph(p1->idx);
            /*Write edge in edge file*/
				fprintf(fp,"%d %d %d\n",iIndexInNodeNetGraph,iAdjacentIndexInNodeNetGraph,int(weight));
         }
		}
	}	
	fclose(fp); 
	return TRUE;
}

#endif
