#pragma once
#ifndef _ProcessingForestFire_
#define _ProcessingForestFire_

#include <Commons/stdafx.h>
#include <Dialogs/DialogParamsInput.h>

template <class _PGraph>
class TProcessingForestFire {
private:
  TRnd Rnd;
  _PGraph Graph;
  TFlt FwdBurnProb, BckBurnProb, ProbDecay;
  TIntV VectorStartupNodes;  // nodes to start fire
  TIntV VectorBurnedNodes;
  /* Statistics */
  TIntV VectorBurnedStatistics, VectorBurningStatistics, VectorNewBurnedStatistics; // total burned, currently burning, newly burned in current time step
  /* For progressive burning */
  int iNextTimeStep;
  int iBurnedNodes;
  int iExtinctFire;
  bool bHasAliveNeighbors;
  double CurrentFwdBurnProb;
  double CurrentBckBurnProb;
  TIntV VectorNewBurnedNodes;
  TIntV VectorCurrentlyBurningNodes;
  TIntH HashBurnedNodes;
private:
  UndefCopyAssign(TProcessingForestFire);
  int iTimeSteps;
  int iFrameToFrameInterval;
public:
  TProcessingForestFire() : FwdBurnProb(0.0), BckBurnProb(0.0), ProbDecay(1.0), Rnd(1), iNextTimeStep(0) {
  }
  TProcessingForestFire(const _PGraph& GraphPt, const double& ForwBurnProb, const double& BckBurnProb, const double& DecayProb, const int& RndSeed) :
  Graph(GraphPt), FwdBurnProb(ForwBurnProb), BckBurnProb(BckBurnProb), ProbDecay(DecayProb), Rnd(RndSeed), iNextTimeStep(0) {
  }
  ~TProcessingForestFire(){ };
  bool SetParametersVisually();
  int GetTimeSteps(){return iTimeSteps; };
  int GetFrameToFrameInterval(){return iFrameToFrameInterval; };
  void SetGraph(const _PGraph& GraphPt) { Graph = GraphPt; }
  _PGraph GetGraph() const { return Graph; }
  void SetBurnProb(const double& ForwBurnProb, const double& BackBurnProb) { FwdBurnProb=ForwBurnProb;  BckBurnProb=BackBurnProb; }
  void SetProbDecay(const double& DecayProb) { ProbDecay = DecayProb; }

  void Infect(const int& NodeId) { VectorStartupNodes.Gen(1,1);  VectorStartupNodes[0] = NodeId; }
  void Infect(const TIntV& InfectedNIdV) { VectorStartupNodes = InfectedNIdV; }
  void InfectAll();
  void InfectRnd(const int& NInfect);

  void BurnExpFire();  // burn each link independently with prob BurnProb (burn fixed percentage of links)
  int StepForwardForestFire();
  void BurnGeoFire();  // burn fixed number of links

  int GetFireTm() const { return VectorBurnedStatistics.Len(); } // time of fire
  int GetBurned() const { return VectorBurnedNodes.Len(); }
  int GetBurnedNId(const int& NIdN) const { return VectorBurnedNodes[NIdN]; }
  const TIntV& GetBurnedNodes() const { return VectorBurnedNodes; }
  void GetBurnedNodes(TIntV& NIdV) const { NIdV = VectorBurnedNodes; }
  void PlotFire(const TStr& Desc, const bool& PlotAllBurned = false, const bool& SavePng = false);
};

template <class _PGraph>
bool TProcessingForestFire<_PGraph>::SetParametersVisually(){
   TDialogParamsInput fpiDialogParamsInput(NULL, "Enter forest fire parameters");
   fpiDialogParamsInput.SetParam1Name("Number of time units");
   fpiDialogParamsInput.SetParam2Name("Number of initial random nodes");
   fpiDialogParamsInput.SetParam3Name("Fire neighbors probability");
   fpiDialogParamsInput.SetParam4Name("Frame to frame interval");
   fpiDialogParamsInput.SetInitialValues("20","10","0.5","300");
   fpiDialogParamsInput.MakeDialog();
   if(fpiDialogParamsInput.ShowModal() == wxID_OK){
      iTimeSteps = atoi(fpiDialogParamsInput.GetParam1());
      int iInitialFire = atoi(fpiDialogParamsInput.GetParam2());
      if(iInitialFire >= Graph->GetNodes())
         iInitialFire = Graph->GetNodes()/2;
      const double dForwBurnProb = (double)atof(fpiDialogParamsInput.GetParam3());
      const double dBckBurnProb = (double)atof(fpiDialogParamsInput.GetParam3());
      iFrameToFrameInterval = atoi(fpiDialogParamsInput.GetParam4());
      this->InfectRnd(iInitialFire);
      this->SetBurnProb(dForwBurnProb,dBckBurnProb);
      return true;
   }else{
      return false;
   }
}

template <class _PGraph>
void TProcessingForestFire<_PGraph>::InfectAll() {
  VectorStartupNodes.Gen(Graph->GetNodes());
  for (typename _PGraph::TObj::TNodeI& NI = Graph->BegNI(); NI < Graph->EndNI(); NI++) {
    VectorStartupNodes.Add(NI.GetId()); }
}

template <class _PGraph>
void TProcessingForestFire<_PGraph>::InfectRnd(const int& NInfect) {
  IAssert(NInfect < Graph->GetNodes());
  TIntV NIdV(Graph->GetNodes(), 0);
  for (typename _PGraph::TObj::TNodeI NI = Graph->BegNI(); NI < Graph->EndNI(); NI++) {
    NIdV.Add(NI.GetId()); }
  NIdV.Shuffle(Rnd);
  VectorStartupNodes.Gen(NInfect, 0);
  for (int i = 0; i < NInfect; i++) {
    VectorStartupNodes.Add(NIdV[i]); }
}

template <class _PGraph>
int TProcessingForestFire<_PGraph>::StepForwardForestFire(){
  typename _PGraph::TObj::TNodeI NI = Graph->BegNI();

  if(iNextTimeStep == 0){
     /*Initial burning nodes*/
     VectorCurrentlyBurningNodes = VectorStartupNodes;
     /*Initially, startup nodes are immediatelly burned*/
     for (int i = 0; i < VectorStartupNodes.Len(); i++) {
        HashBurnedNodes.AddDat(VectorStartupNodes[i]);
        VectorBurnedNodes.Add(HashBurnedNodes.GetKey(HashBurnedNodes.Len()-1));
     }
     iBurnedNodes = VectorStartupNodes.Len();
     /*Clear auxiliar vectors*/
     VectorBurnedStatistics.Clr();  VectorBurningStatistics.Clr();  VectorNewBurnedStatistics.Clr();
     /*Old probability buffers*/
     CurrentFwdBurnProb = FwdBurnProb;
     CurrentBckBurnProb = BckBurnProb;
     /*Initialize vector of results*/
     VectorBurnedNodes.Clr();
     iExtinctFire = 0;
  }else{ 
     if(VectorCurrentlyBurningNodes.Empty()){
        /*No more nodes to burn - fire extintic*/
        iNextTimeStep++;
        return 0;
     }
  }

  VectorNewBurnedNodes.Clr(false);
  for (int node = 0; node < VectorCurrentlyBurningNodes.Len(); node++) {
     const int& BurningNId = VectorCurrentlyBurningNodes[node];
     IAssert(HashBurnedNodes.IsKey(BurningNId));
     NI = Graph->GetNI(BurningNId);
     bHasAliveNeighbors = false;
     iExtinctFire = 0;

     /*Burn out neighbors*/
     for (int v = 0; v < NI.GetOutDeg(); v++) {
        const int OutNId = NI.GetOutNId(v);
        if (! HashBurnedNodes.IsKey(OutNId)) { // not yet burned
           bHasAliveNeighbors = true;
           /*Probability verification*/
           if (Rnd.GetUniDev() < CurrentFwdBurnProb) {
              HashBurnedNodes.AddDat(OutNId);  VectorNewBurnedNodes.Add(OutNId);  iBurnedNodes++; }
              VectorBurnedNodes.Add(HashBurnedNodes.GetKey(HashBurnedNodes.Len()-1));
        }
     }
     /*Burn in neighbors*/
     if (CurrentBckBurnProb > 0.0) {
        for (int v = 0; v < NI.GetInDeg(); v++) {
           const int InNId = NI.GetInNId(v);
           if (! HashBurnedNodes.IsKey(InNId)) { // not yet burned
              bHasAliveNeighbors = true;
              /*Probability verification*/
              if (Rnd.GetUniDev() < CurrentBckBurnProb) {
                 HashBurnedNodes.AddDat(InNId);  VectorNewBurnedNodes.Add(InNId);  iBurnedNodes++; }
                 VectorBurnedNodes.Add(HashBurnedNodes.GetKey(HashBurnedNodes.Len()-1));
           }
        }
     }
     if (! bHasAliveNeighbors) { iExtinctFire++; }
  }
  VectorBurnedStatistics.Add(iBurnedNodes);  
  VectorBurningStatistics.Add(VectorCurrentlyBurningNodes.Len() - iExtinctFire);  
  VectorNewBurnedStatistics.Add(VectorNewBurnedNodes.Len());
  //VectorCurrentlyBurningNodes.AddV(VectorNewBurnedNodes);   // node is burning eternally
  VectorCurrentlyBurningNodes = VectorNewBurnedNodes;         // node is burning just 1 time step

  /*Probability decay*/
  CurrentFwdBurnProb = CurrentFwdBurnProb * ProbDecay;
  CurrentBckBurnProb = CurrentBckBurnProb * ProbDecay;

  iNextTimeStep++;
  return VectorNewBurnedNodes.Len();
}

/* Burn each link independently with prob BurnProb */
template <class _PGraph>
void TProcessingForestFire<_PGraph>::BurnExpFire() {
  const int& NInfect = VectorStartupNodes.Len();
  const typename _PGraph::TObj& G = *Graph;

  typename _PGraph::TObj::TNodeI& NI = Graph->BegNI();
  TIntH HashBurnedNodes;                              // burned nodes
  TIntV VectorStartUpNodesCopy = VectorStartupNodes;  // currently burning nodes
  TIntV NewVectorBurnedNodes;                         // nodes newly burned in current step
  bool HasAliveNbhs;                                  // has unburned neighbors
  int NBurned = NInfect, time, v, i, NDiedFire=0;

  /*Startup nodes are immediatelly burned*/
  for (i = 0; i < VectorStartupNodes.Len(); i++) {
    HashBurnedNodes.AddDat(VectorStartupNodes[i]);
  }

  /*Clear auxiliar vectors*/
  VectorBurnedStatistics.Clr();  VectorBurningStatistics.Clr();  VectorNewBurnedStatistics.Clr();

  /*Old probability buffers*/
  const double OldFwdBurnProb = FwdBurnProb;
  const double OldBckBurnProb = BckBurnProb;

  /*Time after time*/
  for (time = 0; ; time++) {
    NewVectorBurnedNodes.Clr(false);
    for (int node = 0; node < VectorStartUpNodesCopy.Len(); node++) {
      const int& BurningNId = VectorStartUpNodesCopy[node];
      IAssert(HashBurnedNodes.IsKey(BurningNId));
      NI = G.GetNI(BurningNId);
      HasAliveNbhs = false;
      NDiedFire = 0;

      /*Burn out neighbors*/
      for (v = 0; v < NI.GetOutDeg(); v++) {
        const int OutNId = NI.GetOutNId(v);
        if (! HashBurnedNodes.IsKey(OutNId)) { // not yet burned
          HasAliveNbhs = true;
          /*Probability verification*/
          if (Rnd.GetUniDev() < FwdBurnProb) {
            HashBurnedNodes.AddDat(OutNId);  NewVectorBurnedNodes.Add(OutNId);  NBurned++; }
        }
      }
      /*Burn in neighbors*/
      if (BckBurnProb > 0.0) {
        for (v = 0; v < NI.GetInDeg(); v++) {
          const int InNId = NI.GetInNId(v);
          if (! HashBurnedNodes.IsKey(InNId)) { // not yet burned
            HasAliveNbhs = true;
            /*Probability verification*/
            if (Rnd.GetUniDev() < BckBurnProb) {
              HashBurnedNodes.AddDat(InNId);  NewVectorBurnedNodes.Add(InNId);  NBurned++; }
          }
        }
      }
      if (! HasAliveNbhs) { NDiedFire++; }
    }
    VectorBurnedStatistics.Add(NBurned);  
    VectorBurningStatistics.Add(VectorStartUpNodesCopy.Len() - NDiedFire);  
    VectorNewBurnedStatistics.Add(NewVectorBurnedNodes.Len());
    //VectorStartUpNodesCopy.AddV(NewVectorBurnedNodes);   // node is burning eternally
    VectorStartUpNodesCopy = NewVectorBurnedNodes;         // node is burning just 1 time step
    if (VectorStartUpNodesCopy.Empty()) break;

    /*Probability decay*/
    FwdBurnProb = FwdBurnProb * ProbDecay;
    BckBurnProb = BckBurnProb * ProbDecay;
  }

  /*This is the final result accessible via GetBurnedNodes method*/
  VectorBurnedNodes.Clr();
  for (i = 0; i < HashBurnedNodes.Len(); i++) {
    VectorBurnedNodes.Add(HashBurnedNodes.GetKey(i));
  }
  /*Restore original probabilities*/
  FwdBurnProb = OldFwdBurnProb;
  BckBurnProb = OldBckBurnProb;
}

// come to a node, toss biased BurnProb coin until success, burn number till success random links
template <class _PGraph>
void TProcessingForestFire<_PGraph>::BurnGeoFire() {
  const int& NInfect = VectorStartupNodes.Len();
  const typename _PGraph::TObj& G = *Graph;

  typename _PGraph::TObj::TNodeI& NI = Graph->BegNI();
  TIntH HashBurnedNodes;               // burned nodes
  TIntV VectorStartUpNodesCopy = VectorStartupNodes; // currently burning nodes
  TIntV NewVectorBurnedNodes;            // nodes newly burned in current step
  bool HasAliveInNbhs, HasAliveOutNbhs;              // has unburned neighbors
  TIntV AliveNIdV;                // NIds of alive neighbors
  int NBurned = NInfect, time, v, i;

  const double OldFwdBurnProb=FwdBurnProb;
  const double OldBckBurnProb=BckBurnProb;

  /*Startup nodes are immediatelly burned*/
  for (i = 0; i < VectorStartupNodes.Len(); i++) {
    HashBurnedNodes.AddDat(VectorStartupNodes[i]); }
  VectorBurnedStatistics.Clr();  VectorBurningStatistics.Clr();  VectorNewBurnedStatistics.Clr();

  for (time = 0; ; time++) {
    NewVectorBurnedNodes.Clr(false);
    for (int node = 0; node < VectorStartUpNodesCopy.Len(); node++) {
      const int& BurningNId = VectorStartUpNodesCopy[node];
      IAssert(HashBurnedNodes.IsKey(BurningNId));
      NI = G.GetNI(BurningNId);
      // find unburned links
      AliveNIdV.Clr(false); // unburned links
      HasAliveOutNbhs = false;
      for (v = 0; v < NI.GetOutDeg(); v++) {
        const int OutNId = NI.GetOutNId(v);
        if (! HashBurnedNodes.IsKey(OutNId)) { AliveNIdV.Add(OutNId);  HasAliveOutNbhs = true; }
      }
      // burn forward links
      const int BurnNFwdLinks = Rnd.GetGeoDev(1.0-FwdBurnProb) - 1; // number of links to burn (geometric coin). Can also burn 0 links.
      if (HasAliveOutNbhs && BurnNFwdLinks > 0) {
        AliveNIdV.Shuffle(Rnd);
        for (i = 0; i < TMath::Mn(BurnNFwdLinks, AliveNIdV.Len()); i++) {
          const int BurnNId = AliveNIdV[i];
          HashBurnedNodes.AddDat(BurnNId);  NewVectorBurnedNodes.Add(BurnNId);  NBurned++;
        }
      }
      // backward links
      if (BckBurnProb > 0) {
        // find unburned links
        AliveNIdV.Clr(false);
        HasAliveInNbhs = false;
        for (v = 0; v < NI.GetInDeg(); v++) {
          const int InNId = NI.GetInNId(v);
          if (! HashBurnedNodes.IsKey(InNId)) { AliveNIdV.Add(InNId);  HasAliveInNbhs = true; }
        }
        // burn backward links
        const int BurnNBckLinks = Rnd.GetGeoDev(1.0-BckBurnProb) - 1; // number of links to burn (geometric coin). Can also burn 0 links.
        if (HasAliveInNbhs && BurnNBckLinks > 0) {
          AliveNIdV.Shuffle(Rnd);
          for (i = 0; i < TMath::Mn(BurnNBckLinks, AliveNIdV.Len()); i++) {
            const int BurnNId = AliveNIdV[i];
            HashBurnedNodes.AddDat(BurnNId);  NewVectorBurnedNodes.Add(BurnNId);  NBurned++;
            //printf("  %d", BurnNId);
          }
        }
      }
    }
    VectorBurnedStatistics.Add(NBurned);  VectorBurningStatistics.Add(VectorStartUpNodesCopy.Len());  VectorNewBurnedStatistics.Add(NewVectorBurnedNodes.Len());
    // VectorStartUpNodesCopy.AddV(NewVectorBurnedNodes);   // node is burning eternally
    VectorStartUpNodesCopy = NewVectorBurnedNodes;   // node is burning just 1 time step
    if (VectorStartUpNodesCopy.Empty()) break;
    FwdBurnProb = FwdBurnProb * ProbDecay;
    BckBurnProb = BckBurnProb * ProbDecay;
  }
  VectorBurnedNodes.Clr();
  for (i = 0; i < HashBurnedNodes.Len(); i++) {
    VectorBurnedNodes.Add(HashBurnedNodes.GetKey(i));
  }
  FwdBurnProb = OldFwdBurnProb;
  BckBurnProb = OldBckBurnProb;
}

template <class _PGraph>
void TProcessingForestFire<_PGraph>::PlotFire(const TStr& Desc, const bool& PlotAllBurned, const bool& SavePng) {
  TGnuPlot GnuPlot("ForestFire.tab", "ForestFire.plt", TStr::Fmt("%s. ForestFire. G(%d, %d). P=%g, NInfect=%d", Desc.CStr(), Graph->GetNodes(), Graph->GetEdges(), FwdBurnProb, VectorStartupNodes.Len()), true);
  GnuPlot.SetXYLabel("Time", "Number of nodes");
  if (PlotAllBurned) GnuPlot.AddPlot(VectorBurnedStatistics, gpwLinesPoints, "All burned nodes till time");
  GnuPlot.AddPlot(VectorBurningStatistics, gpwLinesPoints, "Burning nodes at time");
  GnuPlot.AddPlot(VectorNewBurnedStatistics, gpwLinesPoints, "Newly burned nodes at time");
  GnuPlot.Plot();
  if (SavePng) GnuPlot.SavePng(TFile::GetUniqueFNm("ForestFire.#.png"));
}

#endif
