#pragma once
#ifndef _ProcessingMETISPartitioning_
#define _ProcessingMETISPartitioning_

#include <Commons/stdafx.h>
#include <Commons/stdafx.h>

#include <wx/numdlg.h>
#include <wx/filename.h>

#include <Commons/Auxiliar.h>
#include <Dialogs/DialogWarning.h>

template <class _TNodeNetData>
class TProcessingMETISPartitioning {
private:
   TPt<_TNodeNetData> pNodeNetData;
   std::string sMETISEdgeFile;
public:
   TProcessingMETISPartitioning(TPt<_TNodeNetData> pANodeNetData):pNodeNetData(pANodeNetData){
      sMETISEdgeFile = pNodeNetData->GetLoadedEdgesFileName();
      std::string sDirectory;       std::string sFileNameRoot;
      std::string sFileName;
      /*Create file - form name*/
      wxFileName wxfnFileName(sMETISEdgeFile.c_str());
      sDirectory = wxfnFileName.GetPath() + wxFileName::GetPathSeparator();
      sFileName = wxfnFileName.GetName();       /*Get file name without extension*/
      sFileName += "_EDGES_METIS.txt";          /*Add file identifier*/
      sMETISEdgeFile = sDirectory + sFileName;  /*Form final name*/
   }
   ~TProcessingMETISPartitioning(){};
   void CallMetisPartitioning();
   void ApplyMETISPartitionFile();
   bool WriteEdgeFileForMETIS();
};

template <class _TNodeNetData>
void TProcessingMETISPartitioning<_TNodeNetData>::CallMetisPartitioning(){
   /*Write input file in METIS format*/
   this->WriteEdgeFileForMETIS();
   /*Get number of partitions*/
   long lNumber = wxGetNumberFromUser( _T("Number of partitions."),
                                    _T("Type or select the number of desired partitions:"), 
                                    _T("Number of partitions"), 5, 0, 100, NULL );
   std::string sMETISPartitioningFile = sMETISEdgeFile + ".part." + TConverter<long>::ToString(lNumber);
   /*Prepare launch parameters*/
   const char * cMETISApplication = PMETIS_EXE;
   wxMessageDialog wxdmKMetisOrPMetis( NULL, _T("Use kmetis.exe instead of pmetis.exe default partitioning? (Check METIS documentation for details)"),
                                      _T("Confirmation:"), wxNO_DEFAULT|wxYES_NO );
   if (wxdmKMetisOrPMetis.ShowModal() == wxID_YES) {
      cMETISApplication = KMETIS_EXE;
   }

   char * cCurrentDirectory = new char[200];
   GetCurrentDirectory(200, cCurrentDirectory);

   std::string sCommandLine = " \"" + sMETISEdgeFile + "\" " + TConverter<long>::ToString(lNumber);
   char* cCommandLine = new char[sCommandLine.length()+1];
   strcpy(cCommandLine, sCommandLine.c_str());

   /*Query user*/
   wxMessageDialog wxdmWaitForMETIS( NULL, _T("METIS software might take long to finish partitioning. Do you want to wait for it (YES) or continue your session and have METIS in parallel (NO)?"),
                             _T("Confirmation:"), wxNO_DEFAULT|wxYES_NO );
   wxMessageDialog wxmdColorPartitionedGraph(NULL, _T("The partitioning is ready, do you want to apply the partitioning?"),
                              _T("Confirmation:"), wxNO_DEFAULT|wxYES_NO );
   switch ( wxdmWaitForMETIS.ShowModal() ){
      /*Lauch*/
      case wxID_YES:
         LaunchApp(cMETISApplication, cCommandLine, cCurrentDirectory, true);
         if(FileExists(sMETISPartitioningFile)){
            TDialogWarning dwDoneWarning("CREATED FILE: METIS PARTITIONING FILE " + sMETISPartitioningFile + " .");
            switch ( wxmdColorPartitionedGraph.ShowModal() ){
               case wxID_YES:                  
                  this->ApplyMETISPartitionFile();
                  break;
               case wxID_NO:
                  break;
            }
         }else{
            TDialogWarning dwNoFile("ERROR: Partitioning failed, check METIS documentation for accepted graphs.");
         }
         break;
      case wxID_NO:
         LaunchApp(cMETISApplication, cCommandLine, cCurrentDirectory, false);
         break;
   }
   /*Clean up*/
   delete cCurrentDirectory;
   delete cCommandLine;
};

template <class _TNodeNetData>
void TProcessingMETISPartitioning<_TNodeNetData>::ApplyMETISPartitionFile(){
   int iPartitionNumber;
   int iHighestPartitionNumber;          float fRatio;
   std::string sAMETISPartitionFile;
   typename _TNodeNetData::TNodeI NI = pNodeNetData->BegNI();

   pNodeNetData->Reset(true);
   wxFileName wxfnFileName(pNodeNetData->GetLoadedEdgesFileName().c_str());
   wxString wxFileFilter = wxfnFileName.GetName() + "*.part.*";
   wxFileDialog wxfdOpenFileDialog(NULL, "Choose METIS partitioning file",
                                   ".", "", wxFileFilter, wxOPEN);
   if ( wxfdOpenFileDialog.ShowModal() == wxID_OK ){
      sAMETISPartitionFile = wxfdOpenFileDialog.GetPath();
      wxfdOpenFileDialog.Destroy();
   }else
      return;

   std::ifstream ifsDataFile;
   /*File opening*/
   ifsDataFile.open(sAMETISPartitionFile.c_str());
   if(!ifsDataFile) {
      TDialogWarning dwWarning("Warning: partition file " + sAMETISPartitionFile + " could not be opened. No nodes properties loaded. Please, provide file " + sAMETISPartitionFile);
      return;
   }
   /*Node file*/
   iHighestPartitionNumber = -1;
   NI = pNodeNetData->BegNI();
   while ( !ifsDataFile.eof() ) {
      /*The ith line (a single number) tells to which partition the ith nodes belongs*/
      ifsDataFile >> iPartitionNumber;
      if(iPartitionNumber > iHighestPartitionNumber)
         iHighestPartitionNumber = iPartitionNumber;
      NI().SetMETISPartitionNumber(iPartitionNumber);
      NI++;
      if(NI == pNodeNetData->EndNI())
         break;
   }
   /*Have nodes color reflect the partition number*/
   for(typename _TNodeNetData::TNodeI NI = pNodeNetData->BegNI(); NI < pNodeNetData->EndNI(); NI++){
      fRatio = NI().GetMETISPartitionNumber()/(double)iHighestPartitionNumber;

      NI().SetRed(fRatio*255);   NI().SetGreen(255 - fRatio*255);   NI().SetBlue(fRatio*255);
   }
   ifsDataFile.close();
};

/*Edge files*/
template <class _TNodeNetData>
bool TProcessingMETISPartitioning<_TNodeNetData>::WriteEdgeFileForMETIS(){
   FILE *fEdgeFile;              int iDataCounting;
   int iNextNeighbor;            int iNeighbors;
   int iEdgesCounter = 0;        std::list<int>::iterator iListIterator;
   THash<TInt, TInt> hCorrespondences;
   std::list<int> lTemp;
   int iCounter;
   /*Open file for writing*/
   if ((fEdgeFile = fopen(sMETISEdgeFile.c_str(), "w+t")) == NULL){
      TDialogWarning dwWarning("Cannot create edge file " + sMETISEdgeFile + ". Please verify path and permissions.");
      return false;
   }

   /*Create correspondence between actual node ids and METIS ids*/
   iCounter = 0;

   /*METIS starts on node 1, _TNodeNetData starts on node 0 we add 1*/
   for(typename _TNodeNetData::TNodeI NI = pNodeNetData->BegNI(); NI < pNodeNetData->EndNI(); NI++){
      hCorrespondences.AddDat(TInt(NI.GetId()), TInt(iCounter+1));
      iCounter++;
   }

   fprintf(fEdgeFile, "%d ", pNodeNetData->GetNodes());
   fprintf(fEdgeFile, "%d",  pNodeNetData->GetEdges());
   fprintf(fEdgeFile, "\n");
   iDataCounting = 0;

   for(typename _TNodeNetData::TNodeI NI = pNodeNetData->BegNI(); NI < pNodeNetData->EndNI(); NI++){
      iNeighbors = NI.GetOutDeg();
      iEdgesCounter += NI.GetOutDeg();
      lTemp.clear();
      for(int i = 0; i < iNeighbors; i++){
         iNextNeighbor = NI.GetOutNId(i);
         fprintf(fEdgeFile,"%d ", hCorrespondences.GetDat(iNextNeighbor));         
      }

      iEdgesCounter += NI.GetInDeg();
      iNeighbors = NI.GetInDeg();

      for(int i = 0; i < iNeighbors; i++){
         iNextNeighbor = NI.GetInNId(i);
         fprintf(fEdgeFile,"%d ", hCorrespondences.GetDat(iNextNeighbor));         
      }
      fprintf(fEdgeFile, "\n");
      iDataCounting++;
   }
   fclose(fEdgeFile);

   if(iDataCounting > 0){
      TDialogWarning dwDoneWarning("FILE CREATED: METIS EDGE FILE " + sMETISEdgeFile + ".");
      return true;
   }else{
      //TDialogWarning dwWarning("Error: nothing written to file " + sMETISEdgeFile + ".");
      return false;
   }
};

#endif
