#ifndef _ProcessingEmbedGraphInHyperSpace_
#define _ProcessingEmbedGraphInHyperSpace_

#include "Commons\Stdafx.h"
#include "Commons\Auxiliar.h"
#include <limits>
#include <vector>
#include <set>

template <class _TNodeNetData>
class TProcessingEmbedGraphInHyperSpace {
private:
    int iNDimensions;
   _TNodeNetData* nnNodeNetData;
public:
   TProcessingEmbedGraphInHyperSpace(_TNodeNetData* nnANodeNetData):nnNodeNetData(nnANodeNetData){
      iNDimensions = 50;
   }
   ~TProcessingEmbedGraphInHyperSpace(){};
   void EmbedGraphInHyperSpace();
   void PerformBFS(int iANodeId);
};

template <class _TNodeNetData>
void TProcessingEmbedGraphInHyperSpace<_TNodeNetData>::EmbedGraphInHyperSpace(){
   std::vector<int> vPivots;
   int iFirstPivot = nnNodeNetData->GetRandomNodeId();
   int iMostDistantNode;
   int iMaxDistancesSum = -1;
   vPivots.push_back(iFirstPivot);
   for(int i = 0; i < iNDimensions; i++){
      PerformBFS(vPivots[i]);
      iMaxDistancesSum = -1;
      for(typename _TNodeNetData::TNodeI& NI = nnNodeNetData->BegNI(); NI < nnNodeNetData->EndNI(); NI++){
         NI().vHyperCoordinates.push_back(NI().iBFSDistance);
         NI().dHyperCoordinatesSum += NI().iBFSDistance;
         if(NI().dHyperCoordinatesSum > iMaxDistancesSum){
            iMaxDistancesSum = NI().dHyperCoordinatesSum;
            iMostDistantNode = NI.GetId();
         }
      }
      /*Next pivot is the most distant element to all previous pivots*/
      vPivots.push_back(iMostDistantNode);
   }
}

template <class _TNodeNetData>
void TProcessingEmbedGraphInHyperSpace<_TNodeNetData>::PerformBFS(int iANodeId){
   int iNeighbors;
   int iVisitedNode;
   int iNextNeighbor;
   std::queue<int> qBFS;
   /*The VC++ parser requires this sintax for (std::numeric_limits<int>::max)()*/
   const int maxInt = (std::numeric_limits<int>::max)();

   for(typename _TNodeNetData::TNodeI& NI = nnNodeNetData->BegNI(); NI < nnNodeNetData->EndNI(); NI++){
      NI().bBFSFlag = false;
      NI().iBFSDistance = maxInt;
   }
   nnNodeNetData->GetNI(iANodeId)().iBFSDistance = 0;
   nnNodeNetData->GetNI(iANodeId)().bBFSFlag = true;
   qBFS.push(iANodeId);
   while(!qBFS.empty()){
      iVisitedNode = qBFS.front();
      qBFS.pop();
      iNeighbors = nnNodeNetData->GetNI(iVisitedNode).GetOutDeg();
      for(int i = 0; i < iNeighbors; i++){
         iNextNeighbor = nnNodeNetData->GetNI(iVisitedNode).GetOutNId(i);                                 
         if(!nnNodeNetData->GetNI(iNextNeighbor)().bBFSFlag){
            nnNodeNetData->GetNI(iNextNeighbor)().bBFSFlag = true;
            nnNodeNetData->GetNI(iNextNeighbor)().iBFSDistance = nnNodeNetData->GetNI(iVisitedNode)().iBFSDistance + 1;
            qBFS.push(iNextNeighbor);
         }
      }
      iNeighbors = nnNodeNetData->GetNI(iVisitedNode).GetInDeg();
      for(int i = 0; i < iNeighbors; i++){
         iNextNeighbor = nnNodeNetData->GetNI(iVisitedNode).GetInNId(i);
         if(!nnNodeNetData->GetNI(iNextNeighbor)().bBFSFlag){
            nnNodeNetData->GetNI(iNextNeighbor)().bBFSFlag = true;
            nnNodeNetData->GetNI(iNextNeighbor)().iBFSDistance = nnNodeNetData->GetNI(iVisitedNode)().iBFSDistance + 1;
            qBFS.push(iNextNeighbor);
         }
      }
   }
}
#endif