#pragma once
#ifndef _ProcessingInfluentialHierarchy_
#define _ProcessingInfluentialHierarchy_

#include <limits>
#include <vector>
#include <set>

#include <Commons/stdafx.h>
#include <Commons/Auxiliar.h>

template <class _TNodeNetData>
class TProcessingInfluentialHierarchy {
private:
   class TStructTempData{
   public:
      std::set<int> sInfluenceSet;
      int iVisitingLevel;
      int iParentNodeTemp;
   };
private:
   std::hash_map<int, TStructTempData> hmMapOfTempData;
   TPt<_TNodeNetData> pNodeNetData;
   double dHierarchyMobilityRatio;
   void ComputeNodeSetInfluence(typename _TNodeNetData::TNodeI niASubjectNode);
   void TrackInfluenceSubsetsForNode(typename _TNodeNetData::TNodeI& niASubjectNode);
   int TrackNeighbors(typename _TNodeNetData::TNodeI& niASubjectNode, std::set<int>& vValidSetOfNodes, int iAInfluenceLevel);
   void IdentifyInfluentialSonsByDegree(typename _TNodeNetData::TNodeI& niASubjectNode, bool bUseInfiniteDegree = false);
   void IdentifyInfluentialSonsByPageRank(typename _TNodeNetData::TNodeI& niASubjectNode, bool bUseInfiniteDegree = false);
   void SetVisitingLevelForSetOfNodes(int iALevel, std::set<int>& vASetOfNodes);
public:
   TProcessingInfluentialHierarchy(TPt<_TNodeNetData> pANodeNetData):pNodeNetData(pANodeNetData){
      dHierarchyMobilityRatio = 0.00;
   }
   ~TProcessingInfluentialHierarchy(){};
   void ComputeNodeSetInfluence();
   void SetHierarchyMobilityRatio(double dAHierarchyMobilityRatio){
      if(dHierarchyMobilityRatio != dAHierarchyMobilityRatio){
         this->dHierarchyMobilityRatio = dAHierarchyMobilityRatio;
         this->ComputeNodeSetInfluence();
      }
   };
};

template <class _TNodeNetData>
void TProcessingInfluentialHierarchy<_TNodeNetData>::ComputeNodeSetInfluence(){
   typename std::hash_map<int,TProcessingInfluentialHierarchy::TStructTempData>::iterator pTempDataForNode = hmMapOfTempData.begin();

   /*Start up*/
   /*Initialize sons per level counter*/
   pNodeNetData->vTotalSonsForLevel.clear();
   pNodeNetData->vTotalSonsForLevel.reserve(pNodeNetData->GetNodes()*100);
   for(int iCounter = 0; iCounter < 100; iCounter++){
      pNodeNetData->vTotalSonsForLevel.push_back((int)0);
   }
   /*Initialize each node hierarchical dada*/
   pNodeNetData->iMaxInfluenceLevel = -1;
   for(typename _TNodeNetData::TNodeI NI = pNodeNetData->BegNI(); NI < pNodeNetData->EndNI(); NI++){
      NI().ClearNodeSons();
      NI().SetInfluenceLevel(-1);
      TProcessingInfluentialHierarchy::TStructTempData stdTemp;
      stdTemp.iParentNodeTemp = -1;
      stdTemp.sInfluenceSet.clear();
      hmMapOfTempData.insert(std::pair<int,TStructTempData>(NI.GetId(),stdTemp));
   }

   /*Creates a node that is to be used as the root of the influence hierarchy*/
   const int iMaxInt = (std::numeric_limits<int>::max)();
   pNodeNetData->AddNode(iMaxInt);
   typename _TNodeNetData::TNodeI niTemp = pNodeNetData->GetNI(iMaxInt);
   niTemp().SetInfluenceLevel(-1);
   TProcessingInfluentialHierarchy::TStructTempData stdTemp;
   stdTemp.iParentNodeTemp = -1;
   stdTemp.sInfluenceSet.clear();
   hmMapOfTempData.insert(std::pair<int,TStructTempData>(niTemp.GetId(),stdTemp));
   pTempDataForNode = hmMapOfTempData.find(niTemp.GetId());

   /*Its initial set of influence comprises the entire graph*/
   for(typename _TNodeNetData::TNodeI NI = pNodeNetData->BegNI(); NI < pNodeNetData->EndNI(); NI++){            
      (*pTempDataForNode).second.sInfluenceSet.insert(NI.GetId());
      NI().SetInfluenceLevel(0);
   }
   /*But not itself as it is not a real node*/
   (*pTempDataForNode).second.sInfluenceSet.erase(iMaxInt);

   ComputeNodeSetInfluence(pNodeNetData->GetNI(iMaxInt));  /*Start recursion*/

   /*Keeps reference to the list of sons (first level nodes) of the root (emulate root)*/
   pNodeNetData->sFirstLevelNodes = pNodeNetData->GetNI(iMaxInt)().GetSonNodes();
   std::set<int>::const_iterator sIterator;
   for (sIterator = pNodeNetData->GetNI(iMaxInt)().GetSonNodes().begin(); sIterator != pNodeNetData->GetNI(iMaxInt)().GetSonNodes().end(); ++sIterator){
      pNodeNetData->sFirstLevelNodes.insert(*sIterator);
   }
   /*Remove root from the graph structure*/
   pNodeNetData->DelNode(iMaxInt);
   pNodeNetData->bInfluenceDraw = true;
}


template <class _TNodeNetData>
void TProcessingInfluentialHierarchy<_TNodeNetData>::IdentifyInfluentialSonsByDegree(typename _TNodeNetData::TNodeI& niASubjectNode, bool bUseInfiniteDegree){
   typename std::hash_map<int,TProcessingInfluentialHierarchy::TStructTempData>::iterator& pTempDataForNode = hmMapOfTempData.find(niASubjectNode.GetId());
      
   int iHighestDegree = -1;   int iShiftedHighestDegree = -1;
   int iNodeDegree = -1;      int iLimitDegree;
   std::set<int>::const_iterator vNode;

   /*Only satisfied by the root of the hierarchy*/
   if(bUseInfiniteDegree)
      iLimitDegree = (std::numeric_limits<int>::max)();
   else
      iLimitDegree = niASubjectNode.GetDeg();

   /*Find highest acceptable degree: highest that is smaller than current node's*/
   for(vNode = (*pTempDataForNode).second.sInfluenceSet.begin(); vNode != (*pTempDataForNode).second.sInfluenceSet.end(); ++vNode){
      if(*vNode < 0)
         break;
      iNodeDegree = pNodeNetData->GetNI(*vNode).GetDeg();
      if((iNodeDegree > iHighestDegree) && (iNodeDegree < iLimitDegree))
         iHighestDegree = iNodeDegree;
   }
   iShiftedHighestDegree = iHighestDegree - iHighestDegree*dHierarchyMobilityRatio;
   if(iShiftedHighestDegree < 0)
      iShiftedHighestDegree = 0;

   /*Clear set of sons*/
   niASubjectNode().ClearNodeSons();
   /*No sons to add because no node with (iNodeDegree > iHighestDegree) && (iNodeDegree < iLimitDegree) was found*/
   if(iHighestDegree == -1)
      return;
   /*Find nodes in this degree interval and have them as sons of niASubjectNode - update iInfluenceLevel*/
   for(vNode = (*pTempDataForNode).second.sInfluenceSet.begin(); vNode != (*pTempDataForNode).second.sInfluenceSet.end(); ++vNode){
      iNodeDegree = pNodeNetData->GetNI(*vNode).GetDeg();
      if((iNodeDegree <= iHighestDegree) && (iNodeDegree >= iShiftedHighestDegree)){         
         pNodeNetData->GetNI(*vNode)().SetInfluenceLevel(niASubjectNode().GetInfluenceLevel() + 1);
         CountSonForLevel(pNodeNetData->GetNI(*vNode)().GetInfluenceLevel());
         niASubjectNode().AddSonToInfluenceSet(pNodeNetData->GetNI(*vNode).GetId());
         /*Just identify the maximum influence level for feedback*/
         if(pNodeNetData->GetNI(*vNode)().GetInfluenceLevel() > this->iMaxInfluenceLevel)
            this->iMaxInfluenceLevel = pNodeNetData->GetNI(*vNode)().GetInfluenceLevel();
      }
   }
};

template <class _TNodeNetData>
void TProcessingInfluentialHierarchy<_TNodeNetData>::IdentifyInfluentialSonsByPageRank(typename _TNodeNetData::TNodeI& niASubjectNode, bool bUseInfiniteDegree){
   typename std::hash_map<int,TProcessingInfluentialHierarchy::TStructTempData>::iterator pTempDataForNode = hmMapOfTempData.find(niASubjectNode.GetId());
   double dHighestPageRank = -1;   double dShiftedHighestPageRank = -1;
   double dPageRank = -1;          double dLimitPageRank;
   std::set<int>::const_iterator vNode;

   /*Only satisfied by the root of the hierarchy*/
   if(bUseInfiniteDegree)
      dLimitPageRank = (std::numeric_limits<double>::max)();
   else
      dLimitPageRank = niASubjectNode().GetPageRank();

   /*Find highest acceptable PageRank: highest that is smaller than current node's*/
   for(vNode = (*pTempDataForNode).second.sInfluenceSet.begin(); vNode != (*pTempDataForNode).second.sInfluenceSet.end(); ++vNode){
      if(*vNode < 0)
         break;
      dPageRank = pNodeNetData->GetNI(*vNode)().GetPageRank();
      if((dPageRank > dHighestPageRank) && (dPageRank < dLimitPageRank))
         dHighestPageRank = dPageRank;
   }
   dShiftedHighestPageRank = dHighestPageRank - dHighestPageRank*dHierarchyMobilityRatio;
   if(dShiftedHighestPageRank < 0)
      dShiftedHighestPageRank = 0;

   /*Clear set of sons*/
   niASubjectNode().ClearNodeSons();
   /*No sons to add because no node with (dPageRank > dHighestPageRank) && (dPageRank < dLimitPageRank) was found*/
   if(dHighestPageRank == -1)
      return;
   /*Find nodes in this PageRank interval and have them as sons of niASubjectNode - update iInfluenceLevel*/
   for(vNode = (*pTempDataForNode).second.sInfluenceSet.begin(); vNode != (*pTempDataForNode).second.sInfluenceSet.end(); ++vNode){
      dPageRank = pNodeNetData->GetNI(*vNode)().GetPageRank();
      if((dPageRank <= dHighestPageRank) && (dPageRank >= dShiftedHighestPageRank)){
         pNodeNetData->GetNI(*vNode)().SetInfluenceLevel(niASubjectNode().GetInfluenceLevel() + 1);
         pNodeNetData->CountSonForLevel(pNodeNetData->GetNI(*vNode)().GetInfluenceLevel());
         niASubjectNode().AddSonToInfluenceSet(pNodeNetData->GetNI(*vNode).GetId());
         /*Just identify the maximum influence level for feedback*/
         if(pNodeNetData->GetNI(*vNode)().GetInfluenceLevel() > pNodeNetData->iMaxInfluenceLevel)
            pNodeNetData->iMaxInfluenceLevel = pNodeNetData->GetNI(*vNode)().GetInfluenceLevel();
      }
   }
};

template <class _TNodeNetData>
void TProcessingInfluentialHierarchy<_TNodeNetData>::ComputeNodeSetInfluence(typename _TNodeNetData::TNodeI niASubjectNode){
   typename std::hash_map<int,TProcessingInfluentialHierarchy::TStructTempData>::iterator pTempDataForNode = hmMapOfTempData.find(niASubjectNode.GetId());
   std::set<int>::const_iterator sIterator;
   typename _TNodeNetData::TNodeI nitemp;

   /*Get initial (son) nodes (those with highest degree smaller than the node degree)*/
   if(niASubjectNode.GetId() == (std::numeric_limits<int>::max)()) /*root node*/
      IdentifyInfluentialSonsByPageRank(niASubjectNode, true);
   else
      IdentifyInfluentialSonsByPageRank(niASubjectNode);

   if(niASubjectNode().GetSonNodes().size() == 0)
      return;         /*Recursion stop condition: no more nodes tracked as sons*/

   TrackInfluenceSubsetsForNode(niASubjectNode);
   /*For each son (and its influence subset) call the recursion*/
   for(sIterator = niASubjectNode().GetSonNodes().begin(); sIterator != niASubjectNode().GetSonNodes().end(); ++sIterator) {
      if(*sIterator < 0)
         break;
      nitemp = pNodeNetData->GetNI(*sIterator);
      ComputeNodeSetInfluence(nitemp);
   }
};

/*Receives a node that has an influence set of nodes and calculates new subsets of influence
  each with a niASubjectNode().GetSonNodes() as root*/
template <class _TNodeNetData>
void TProcessingInfluentialHierarchy<_TNodeNetData>::TrackInfluenceSubsetsForNode(typename _TNodeNetData::TNodeI& niASubjectNode){
   typename std::hash_map<int,TProcessingInfluentialHierarchy::TStructTempData>::iterator pTempDataForNode = hmMapOfTempData.find(niASubjectNode.GetId());
   typename std::hash_map<int,TProcessingInfluentialHierarchy::TStructTempData>::iterator pTempDataForSonNode = pTempDataForNode;
   int iVisitingLevel = 0;
   int iNumberOfTrackedNodes = 0;
   std::set<int>::const_iterator sIterator;
   /*Initialize flag iParentNodeTemp that will be propagated to all tracked neighbors to niASubjectNode().GetSonNodes()*/
   for(sIterator = niASubjectNode().GetSonNodes().begin(); sIterator != niASubjectNode().GetSonNodes().end(); ++sIterator) {
      if(*sIterator < 0)
         break;
      /*Retrieve son data*/
      typename _TNodeNetData::TNodeI nitemp = pNodeNetData->GetNI(*sIterator);
      pTempDataForSonNode = hmMapOfTempData.find((*sIterator));
      /*Set iParentNodeTemp buffer variable. Not used to track parent. Instead use sSonNodes set.*/
      (*pTempDataForSonNode).second.iParentNodeTemp = nitemp.GetId();
      /*And have theirselves as part of their influence set*/
      (*pTempDataForSonNode).second.sInfluenceSet.insert(nitemp.GetId());
   }

   /*Reset all nodes in parent's influence set (niASubjectNode().sInfluenceSet) 0*/
   SetVisitingLevelForSetOfNodes(iVisitingLevel, (*pTempDataForNode).second.sInfluenceSet);
   /*Set new Influence level that will be considered*/
   iVisitingLevel++;
   /*Set Influence level for the intial set of nodes (niASubjectNode().GetSonNodes())
     These will be the running nodes - Neighbor tracking starts from each of them in pseudo-paralelism*/
   SetVisitingLevelForSetOfNodes(iVisitingLevel, niASubjectNode().GetSonNodes());
   iNumberOfTrackedNodes += niASubjectNode().GetSonNodes().size();
   /*Go through all nodes and process the nodes with the Influence level being considered*/
   /*Repeat until no neighbor node is left untracked*/
   while(iNumberOfTrackedNodes > 0){
      iNumberOfTrackedNodes = 0;
      /*Go through the nodes in the set*/
      for(sIterator = (*pTempDataForNode).second.sInfluenceSet.begin(); sIterator != (*pTempDataForNode).second.sInfluenceSet.end(); ++sIterator) {
         if((*sIterator) < 0)
            break;
         typename _TNodeNetData::TNodeI nitemp = pNodeNetData->GetNI(*sIterator);
         pTempDataForSonNode = hmMapOfTempData.find((*sIterator));
         /*Look for the Influence level being considered - Initially satisfied only by niASubjectNode().GetSonNodes()*/
         if((*pTempDataForSonNode).second.iVisitingLevel == iVisitingLevel){
            /*Set new Influence levels for neighbor nodes that haven't been tracked yet*/
            iNumberOfTrackedNodes += TrackNeighbors(nitemp, (*pTempDataForNode).second.sInfluenceSet, iVisitingLevel+1);
         }
      }
      /*Consider a new hirarchy level and go through again*/
      iVisitingLevel++;
   }
};

/*This method scans all neighbors of niASubjectNode and, in case they are in vValidSetOfNodes,
I set their parent node as iCurrentParentId, set their iAInfluenceLevel and add them for 
pNodeNetData->GetNI(iCurrentParentId)().sInfluenceSet's influence set*/
template <class _TNodeNetData>
int TProcessingInfluentialHierarchy<_TNodeNetData>::TrackNeighbors(typename _TNodeNetData::TNodeI& niASubjectNode, std::set<int>& vValidSetOfNodes, int iAInfluenceLevel){
   typename std::hash_map<int,TProcessingInfluentialHierarchy::TStructTempData>::iterator pTempDataForNode = hmMapOfTempData.find(niASubjectNode.GetId());
   typename std::hash_map<int,TProcessingInfluentialHierarchy::TStructTempData>::iterator pTempDataForNeighborNode = pTempDataForNode;
   typename std::hash_map<int,TProcessingInfluentialHierarchy::TStructTempData>::iterator pTempDataForCurrentParentNode = pTempDataForNode;
   
   int iNextNeighbor;  int iCountAffectedNodes = 0;
   int iCurrentParentId = (*pTempDataForNode).second.iParentNodeTemp;

   int iNeighbors = niASubjectNode.GetOutDeg();
   for(int i = 0; i < iNeighbors; i++){
      iNextNeighbor = niASubjectNode.GetOutNId(i);
      pTempDataForNeighborNode = hmMapOfTempData.find(iNextNeighbor);
      /*If node has not been tracked yet*/
      if((*pTempDataForNeighborNode).second.iVisitingLevel == 0){
         /*Consider only nodes in vValidSetOfNodes, the current influence set in wich we are looking for subsets*/
         if(vValidSetOfNodes.find(iNextNeighbor) != vValidSetOfNodes.end()){
            (*pTempDataForNeighborNode).second.iVisitingLevel = iAInfluenceLevel;
            (*pTempDataForNeighborNode).second.iParentNodeTemp = iCurrentParentId;
            /*Build new inluence set - subset of the current one*/
            pTempDataForCurrentParentNode = hmMapOfTempData.find(iCurrentParentId);
            (*pTempDataForCurrentParentNode).second.sInfluenceSet.insert(iNextNeighbor);
            iCountAffectedNodes++;
         }
      }
   }
   iNeighbors = niASubjectNode.GetInDeg();
   for(int i = 0; i < iNeighbors; i++){
      iNextNeighbor = niASubjectNode.GetInNId(i);
      pTempDataForNeighborNode = hmMapOfTempData.find(iNextNeighbor);
      /*If node has not been tracked yet*/
      if((*pTempDataForNeighborNode).second.iVisitingLevel == 0){
         if(vValidSetOfNodes.find(iNextNeighbor) != vValidSetOfNodes.end()){
            (*pTempDataForNeighborNode).second.iVisitingLevel = iAInfluenceLevel;
            (*pTempDataForNeighborNode).second.iParentNodeTemp = iCurrentParentId;
            /*Build new inluence set - subset of the current one*/
            pTempDataForCurrentParentNode = hmMapOfTempData.find(iCurrentParentId);
            (*pTempDataForCurrentParentNode).second.sInfluenceSet.insert(iNextNeighbor);
            iCountAffectedNodes++;
         }
      }
   }
   return iCountAffectedNodes;
};
template <class _TNodeNetData>
void TProcessingInfluentialHierarchy<_TNodeNetData>::SetVisitingLevelForSetOfNodes(int iALevel, std::set<int>& vASetOfNodes){
   typename std::hash_map<int,TProcessingInfluentialHierarchy::TStructTempData>::iterator pTempDataForNode = hmMapOfTempData.begin();
   std::set<int>::const_iterator sIterator;
   for(sIterator = vASetOfNodes.begin(); sIterator != vASetOfNodes.end(); ++sIterator) {
      pTempDataForNode = hmMapOfTempData.find((*sIterator));
      (*pTempDataForNode).second.iVisitingLevel = iALevel;
   }   
}
#endif
