#pragma once
#ifndef _ProcessingPageRank_
#define _ProcessingPageRank_

#include <sstream>

#include <Commons/stdafx.h>

template <class _TNodeNetData>
class TProcessingPageRank {
private:
   class TStructTempData{
   public:
      bool bInPageRankRecursion;
      int iIterationLevel;
   };
private:
   std::hash_map<int, TStructTempData> hmMapOfTempData;
   int iTimeSteps;
   int iSimulationTimeCounter;
   TPt<_TNodeNetData> pNodeNetData;
   double RecursiveComputePageRankForNode(typename _TNodeNetData::TNodeI &niASubjectNode, int iIterationLevel);
   double SumAllPageRanks();
public:
   TProcessingPageRank(TPt<_TNodeNetData> pANodeNetData):pNodeNetData(pANodeNetData){
   }
   ~TProcessingPageRank(){};
   void ComputePageRank();
};

template <class _TNodeNetData>
void TProcessingPageRank<_TNodeNetData>::ComputePageRank(){
   typename std::hash_map<int,TProcessingPageRank::TStructTempData>::iterator pTempDataForNode = hmMapOfTempData.begin();
   for(typename _TNodeNetData::TNodeI NI = pNodeNetData->BegNI(); NI < pNodeNetData->EndNI(); NI++){
      TProcessingPageRank::TStructTempData stdTemp;
      stdTemp.bInPageRankRecursion = false;
      stdTemp.iIterationLevel = 0;
      hmMapOfTempData.insert(std::pair<int,TStructTempData>(NI.GetId(),stdTemp));
   }
   double dNewSum;           double dOldSum;
   double dPageRanksRatio;   int iIterationsCounter = 0;
   do{
      iIterationsCounter++;
      dOldSum = SumAllPageRanks();
      for(typename _TNodeNetData::TNodeI NI = pNodeNetData->BegNI(); NI < pNodeNetData->EndNI(); NI++) {
         pTempDataForNode = hmMapOfTempData.find(NI.GetId());         
         if(pTempDataForNode->second.iIterationLevel < iIterationsCounter)
            RecursiveComputePageRankForNode(NI, iIterationsCounter);
      }
      dNewSum = SumAllPageRanks();
      dPageRanksRatio = dNewSum/dOldSum;
   }while(((dPageRanksRatio < 0.995) ||
           (dPageRanksRatio > 1.005)) && 
           (iIterationsCounter < 20));
}


template <class _TNodeNetData>
double TProcessingPageRank<_TNodeNetData>::SumAllPageRanks(){
   double dSum = 0;
   for(typename _TNodeNetData::TNodeI NI = pNodeNetData->BegNI(); NI < pNodeNetData->EndNI(); NI++) {
      dSum += NI().GetPageRank();
   }
   return dSum;
}

template <class _TNodeNetData>
double TProcessingPageRank<_TNodeNetData>::RecursiveComputePageRankForNode(typename _TNodeNetData::TNodeI &niASubjectNode, int iIterationLevel){
   typename std::hash_map<int,TProcessingPageRank::TStructTempData>::iterator pTempDataForNode = hmMapOfTempData.find(niASubjectNode.GetId());
   /*We assume an undirected graph in which every node points to each other.
     Therefore, every node is a cited and a citing node for the current one.*/

   /*If the recursion was already called for this node, quit
     calculation returning whatever the pagerank is*/
   
   if(pTempDataForNode->second.iIterationLevel >= iIterationLevel)
      return niASubjectNode().GetPageRank();

   if(pTempDataForNode->second.bInPageRankRecursion)
      return niASubjectNode().GetPageRank();

   pTempDataForNode->second.bInPageRankRecursion = true;

   /*Following the original paper. We sum the citations from other nodes to this
     one, divided by their number of citations to other nodes*/
   double dCitingNodesPageRankSum = 0;

   int iNextNeighbor;
   typename _TNodeNetData::TNodeI niCitingNode = pNodeNetData->BegNI();

   /*Visit all neighbor nodes*/
   int iNeighbors = niASubjectNode.GetOutDeg();   /*Out neighbors*/
   for(int i = 0; i < iNeighbors; i++){
      iNextNeighbor = niASubjectNode.GetOutNId(i);
      niCitingNode = pNodeNetData->GetNI(iNextNeighbor);
      dCitingNodesPageRankSum += RecursiveComputePageRankForNode(niCitingNode, iIterationLevel)/(double)niCitingNode.GetDeg();
   }

   iNeighbors = niASubjectNode.GetInDeg();        /*In neighbors*/
   for(int i = 0; i < iNeighbors; i++){
      iNextNeighbor = niASubjectNode.GetInNId(i);
      niCitingNode = pNodeNetData->GetNI(iNextNeighbor);
      dCitingNodesPageRankSum += RecursiveComputePageRankForNode(niCitingNode, iIterationLevel)/(double)niCitingNode.GetDeg();
   }
   niASubjectNode().SetPageRank(0.15 + 0.85*(dCitingNodesPageRankSum));

   /*For iteration iIterationLevel, this flag states that the node 
     has already had its pagerank calculated*/
   pTempDataForNode->second.iIterationLevel = iIterationLevel;
   /*This flag prevents circular recursion*/
   pTempDataForNode->second.bInPageRankRecursion = false;

   return niASubjectNode().GetPageRank();
}
#endif
