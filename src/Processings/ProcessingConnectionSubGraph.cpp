#include "ProcessingConnectionSubGraph.h"

RandomWalkWithRestart::RandomWalkWithRestart()
{
	r1 = NULL;
	r2 = NULL;
}

RandomWalkWithRestart::~RandomWalkWithRestart()
{

}

BOOL RandomWalkWithRestart::SetupRWR(int len, double my_c, int my_tol, int my_itemax)
{
 	length = len;
	c = my_c;
	tolorance = my_tol;
	ite_max = my_itemax;
	if(r1)
		delete []r1;
	r1 = new float[length];	
	if(r2)
		delete []r2;
	r2 = new float[length];
	return TRUE;
}
BOOL RandomWalkWithRestart::DestroyRWR()
{
	if(r1)
		delete []r1;
	if(r2)
		delete []r2;
	return TRUE;
}
float RandomWalkWithRestart::VecDiff(float *v1, float * v2)
{
	float re = 0.0f;
	for(int i=0;i<length;i++)
		re+=(v1[i]-v2[i]) * (v1[i]-v2[i]);
	re = sqrt(re);
	return re;
}
BOOL RandomWalkWithRestart::VecCopy(float * &v1, float *v2)
{
	for(int i=0;i<length;i++)
		v1[i] = v2[i];
	return TRUE;
}
BOOL RandomWalkWithRestart::VecMu(graph * gph, float * &v1, float * v2,float * Deg)
{
	if(gph->nodenum!=length)
		return FALSE;
	int idx;
	for(int i=0;i<length;i++)
		v1[i] = 0.0f;
	for(int i=0;i<length;i++)//every row of gph
	{
		for(int j=0;j<gph->nodelist[i].length;j++)
		{
			idx = gph->nodelist[i].index[j];
			if(idx==q)
			{
				int tmp;
				tmp = 0;
			}
			v1[i] += (gph->nodelist[i].weight[j] * v2[idx]/Deg[idx]);
		}
	}
	return TRUE;
}

BOOL RandomWalkWithRestart::InitRWR()
{
	for(int i=0;i<length;i++)
	{
		r1[i] = 0.0f;
		r2[i] = 0.0f;
	}
	//r1[q] = 1.0f;
	r2[q] = 1.0f;
	return TRUE;
}
BOOL RandomWalkWithRestart::IterateOne(graph * gph, float * Deg)
{
	//for(int j=0;j<length;j++)
	//		printf("%d: %f\n",j+1, r2[j]);
	if(!VecMu(gph,r1,r2,Deg))
		return FALSE;
	
	for(int i=0;i<length;i++)
		r1[i] = c * r1[i];
	r1[q] += (1-c);

	//for(j=0;j<length;j++)
	//		printf("%d: %f\n",j+1, r1[j]);

	float diff = VecDiff(r1,r2);
	if(diff<=tolorance)//stop
		return FALSE;//should stop iteration;
	VecCopy(r2,r1);

	//for(j=0;j<length;j++)
	//		printf("%d: %f\n",j+1, r2[j]);

	return TRUE;
}
BOOL RandomWalkWithRestart::RWR(graph * gph, float * Deg, int query)
{
	if(gph->nodenum!=length)
		return FALSE;
	q = query;
	InitRWR();
	int i=0, flag=1;
	while(flag==1)
	{
		if(!IterateOne(gph, Deg))
			flag=0;
		i++;
		if(i>=ite_max)
			flag=0;
		//for(int j=0;j<length;j++)
		//	printf("%d: %f\n",j+1, r1[j]);
	}
	return TRUE;
}