#pragma once
#ifndef _DialogParamsInput_
#define _DialogParamsInput_

#include <string>
#include <Commons/Auxiliar.h>

class WXDLLEXPORT TDialogParamsInput;

class WXDLLEXPORT TDialogParamsInput : public wxDialog
{
public:
   bool bShowParam1;
   bool bShowParam2;
   bool bShowParam3;
   bool bShowParam4;
   TDialogParamsInput(){};
   TDialogParamsInput(wxWindow* parent,const wxString& title);
   void MakeDialog();
   void OnClickOk(wxCommandEvent& event);
   void OnClickCancel(wxCommandEvent& event);
   void SetInitialValues(wxString sParam1 = "30", wxString sParam2 = "0.9",
                         wxString sParam3 = "15", wxString sParam4 = "300");
   void SetParam1Name(wxString wxsAName);
   void SetParam2Name(wxString wxsAName);
   void SetParam3Name(wxString wxsAName);
   void SetParam4Name(wxString wxsAName);
   wxString GetParam1();
   wxString GetParam2();
   wxString GetParam3();
   wxString GetParam4();
   int ShowModal();
private:
   int iReturnStatus;
   wxButton *wxbOkButton;
   wxButton *wxbCancelButton;
   wxStaticText *sText1;
   wxStaticText *sText2;
   wxStaticText *sText3;
   wxStaticText *sText4;
   wxTextCtrl *tcTextCtrl1;
   wxTextCtrl *tcTextCtrl2;
   wxTextCtrl *tcTextCtrl3;
   wxTextCtrl *tcTextCtrl4;
   wxFlexGridSizer *dialogSizer;
   DECLARE_EVENT_TABLE()
};

#endif