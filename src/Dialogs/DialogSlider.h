#pragma once
#ifndef _DialogSlider_
#define _DialogSlider_

#include <Commons/Auxiliar.h>

class TDialogSlider : public wxDialog
{
public:
   /*Small interface to be implemented in order to teach the DialogSlider
   how to update the exterior interface that called the slider*/
   class TFunctorUpdaterForSlider{
      protected:
         void *interfaceToBeUpdated;
      public:
         virtual void SetInterfaceToBeUpdated(void *aInterfaceToBeUpdated){
            interfaceToBeUpdated = aInterfaceToBeUpdated;
         };
         virtual void UpdateSlider(){
            if(interfaceToBeUpdated == NULL) return;
         };
         virtual void RefreshInterface(){
            if(interfaceToBeUpdated == NULL) return;
         };
   };

   TDialogSlider();
   TDialogSlider(wxWindow* parent,const wxString& title, const wxPoint& pPosition = wxDefaultPosition);
   ~TDialogSlider();
   int GetValue(){return m_slider->GetValue();};
   virtual int ShowModal();
   TFunctorUpdaterForSlider* fufsUpdater;
   TFunctorUpdaterForSlider* fufsOnCloseUpdater;
   void SetLimits(int iAHigherLimit, int iALowerLimit, int iATickFrequency = 1);
   void SetSliderPosition(int iAPosition);
private:
   wxSlider *m_slider;
   wxButton *okButton;
   wxFlexGridSizer *dialogSizer;
   void OnClickOk(wxCommandEvent& event);
   void OnCloseWindow(wxCloseEvent& event);
   void OnUpdate(wxCommandEvent& event);
   void OnMove(wxMoveEvent& event);
   DECLARE_EVENT_TABLE()
};

#endif