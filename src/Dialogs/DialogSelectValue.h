#pragma once
#ifndef _DialogSelectValue_
#define _DialogSelectValue_

#include <Commons/Auxiliar.h>
#include <wx/spinctrl.h>

class TDialogSelectValue : public wxDialog
{
public:
   /*Small interface to be implemented in order to teach the DialogSelectValue
   how to update the exterior interface that called the slider*/
   class TFunctorUpdaterForSelectValue{
      protected:
         void *interfaceToBeUpdated;
      public:
         virtual void SetInterfaceToBeUpdated(void *aInterfaceToBeUpdated){
            interfaceToBeUpdated = aInterfaceToBeUpdated;
         };
         virtual void RefreshInterface(){
            if(interfaceToBeUpdated == NULL) return;
         };
         virtual void Execute(){
            if(interfaceToBeUpdated == NULL) return;
         };
   };

   TDialogSelectValue();
   TDialogSelectValue(wxWindow* parent,const wxString& title, const wxPoint& pPosition = wxDefaultPosition);
   ~TDialogSelectValue();
   void SetValuesRange(int iLow, int iMax);
   int GetValue();
   TFunctorUpdaterForSelectValue* fufsUpdater;
private:
   bool bShowForTheFirstTime;
   wxButton *okButton;
   wxButton *cancelButton;
   wxSpinCtrl *wxscValueSpiner;
   wxFlexGridSizer *dialogSizer;
   wxFlexGridSizer *dialogDownSizer;
   void OnClickOk(wxCommandEvent& event);
   void OnClickCancel(wxCommandEvent& event);   
   void OnShow(wxShowEvent& event);
   void OnCloseWindow(wxCloseEvent& event);
   void OnMove(wxMoveEvent& event);
   DECLARE_EVENT_TABLE()
};

#endif