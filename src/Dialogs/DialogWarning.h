#pragma once
#ifndef _DialogWarning_
#define _DialogWarning_

#include <Commons/Auxiliar.h>

class TDialogWarning : public wxMessageDialog
{
public:
   TDialogWarning(std::string sAMessage, std::string sATitle = "Message dialog", bool bModal = true)
      :wxMessageDialog(NULL, _T(sAMessage.c_str()),_T(sATitle.c_str()), wxOK){
         if(bModal)
            this->ShowModal();
         else
            this->Show();
   }
};
#endif