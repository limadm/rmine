#include "DialogSelectNodes.h"

enum{
   DialogSelectNodes_Ok = wxID_LOWEST - 301,
   DialogSelectNodes_Cancel = wxID_LOWEST - 302,
   DialogSelectNodes_Updated =  wxID_LOWEST - 303,
   DialogSelectNodes_Move =  wxID_LOWEST - 304
};

BEGIN_EVENT_TABLE(TDialogSelectNodes, wxDialog)
EVT_BUTTON(DialogSelectNodes_Ok, TDialogSelectNodes::OnClickOk)
EVT_BUTTON(DialogSelectNodes_Cancel, TDialogSelectNodes::OnClickCancel)
EVT_SHOW(TDialogSelectNodes::OnShow)
EVT_CLOSE(TDialogSelectNodes::OnCloseWindow)
EVT_MOVE(TDialogSelectNodes::OnMove)
END_EVENT_TABLE()

TDialogSelectNodes::TDialogSelectNodes(wxWindow* parent,const wxString& title, const wxPoint& pPosition)
:wxDialog(parent, wxID_ANY, title, pPosition){
   bShowForTheFirstTime = true;
   this->SetSize(255,120);

   dialogUpSizer = new wxFlexGridSizer(2, 1, 10, 10);
   dialogDownSizer = new wxFlexGridSizer(1, 2, 10, 10);
   wxtcListOfSelectedNodes = new wxTextCtrl(this,-1,"",wxPoint(0, 0),wxSize(250, 50),
                             wxTE_MULTILINE|wxTE_READONLY|wxTE_WORDWRAP);
   okButton = new wxButton(this, DialogSelectNodes_Ok, "Ok", wxPoint(100, 55));
   cancelButton = new wxButton(this, DialogSelectNodes_Cancel, "Cancel", wxPoint(100, 55));
   dialogUpSizer->Add(wxtcListOfSelectedNodes, 0, wxALIGN_CENTRE_VERTICAL|wxALIGN_CENTRE_HORIZONTAL);
   dialogUpSizer->Add(dialogDownSizer, 0, wxALIGN_CENTRE_VERTICAL|wxALIGN_CENTRE_HORIZONTAL);
   dialogDownSizer->Add(okButton, 0, wxALIGN_CENTRE_VERTICAL);
   dialogDownSizer->Add(cancelButton, 0, wxALIGN_CENTRE_VERTICAL);

   fufsUpdater = NULL;

   this->SetSizer(dialogUpSizer);
   this->SetAutoLayout(true);
   this->Layout();
}

TDialogSelectNodes::~TDialogSelectNodes(){
   if(fufsUpdater != NULL)
      delete fufsUpdater;
}
void TDialogSelectNodes::AddSelectedItemForExhibition(std::string sAItemLabel){
   for(int iCounter = 0; iCounter < wxtcListOfSelectedNodes->GetNumberOfLines(); iCounter++){
      if(sAItemLabel.compare(wxtcListOfSelectedNodes->GetLineText(iCounter).c_str()) == 0)
         return;   /*Do not add already added labels*/
   }

   wxtcListOfSelectedNodes->AppendText(sAItemLabel.c_str());
   wxtcListOfSelectedNodes->AppendText("\n");
}

void TDialogSelectNodes::GetListOfSelectedNodes(std::list<std::string> &lAListOfLabels){
   lAListOfLabels.clear();
   for(int iCounter = 0; iCounter < wxtcListOfSelectedNodes->GetNumberOfLines(); iCounter++){
      if(wxtcListOfSelectedNodes->GetLineText(iCounter).compare("") == 0)
         continue;
      lAListOfLabels.push_back(std::string(wxtcListOfSelectedNodes->GetLineText(iCounter).c_str()));
   }
}

void TDialogSelectNodes::OnShow(wxShowEvent& event){
   event.Skip();
   if(bShowForTheFirstTime){ 
      wxtcListOfSelectedNodes->Clear();
      bShowForTheFirstTime = false;
   }
}

void TDialogSelectNodes::OnClickCancel(wxCommandEvent& event){ 
   event.Skip();
   this->Show(FALSE);
   if(fufsUpdater != NULL)
      (*fufsUpdater).CloseSequence();
   bShowForTheFirstTime = true;
}

void TDialogSelectNodes::OnClickOk(wxCommandEvent& event){ 
   event.Skip();
   this->Show(FALSE);
   if(fufsUpdater != NULL)
      (*fufsUpdater).Execute();
   bShowForTheFirstTime = true;
}

void TDialogSelectNodes::OnCloseWindow(wxCloseEvent& event){
  event.Skip();
  if(fufsUpdater != NULL)
      (*fufsUpdater).CloseSequence();
}
void TDialogSelectNodes::OnMove(wxMoveEvent& event){
   event.Skip();
   if(fufsUpdater != NULL)
      (*fufsUpdater).RefreshInterface();
}
