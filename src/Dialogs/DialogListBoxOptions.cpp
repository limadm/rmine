#include "DialogListBoxOptions.h"

enum{
   DialogListBox_Ok = wxID_LOWEST - 601,
   DialogListBox_Updated =  wxID_LOWEST - 602
};

BEGIN_EVENT_TABLE(TDialogListBoxOptions, wxDialog)
EVT_BUTTON(DialogListBox_Ok, TDialogListBoxOptions::OnClickOk)
EVT_CLOSE(TDialogListBoxOptions::OnCloseWindow)
EVT_LISTBOX(DialogListBox_Updated, TDialogListBoxOptions::OnUpdate)
EVT_MOVE(TDialogListBoxOptions::OnMove)
END_EVENT_TABLE()

TDialogListBoxOptions::TDialogListBoxOptions(wxWindow* parent,const wxString& title, int iWidth, int iHeight)
:wxDialog(parent, wxID_ANY, title){
   fufsUpdater = NULL;
   wxString wxsEmptyList[] = { "" };
   this->SetSize(iWidth, iHeight);
   this->Centre(wxBOTH);   
   wxcbListBox = new wxListBox(this, DialogListBox_Updated, wxDefaultPosition, 
                               wxSize(iWidth*0.6, iHeight-25), 1, wxsEmptyList, wxLB_SINGLE);

   dialogSizer = new wxFlexGridSizer(2, 2, 10, 10);
   okButton = new wxButton(this, DialogListBox_Ok, "Ok", wxDefaultPosition);                          

   dialogSizer->Add(wxcbListBox, 0, wxALIGN_CENTRE_VERTICAL);
   dialogSizer->Add(okButton, 0, wxALIGN_CENTRE_VERTICAL);

   this->SetSizer(dialogSizer);
   this->SetAutoLayout(true);
   this->Layout();

   bIsOptionsAlreadySet = false;
}
void TDialogListBoxOptions::SetNewSize(int iWidth, int iHeight){
   wxDialog::SetSize(iWidth, iHeight);
   wxcbListBox->SetSize(iWidth*0.6, iHeight-25);
   this->Layout();
}
int TDialogListBoxOptions::GetSelectedItemIndex(){
   return wxcbListBox->GetSelection();
};

std::string TDialogListBoxOptions::GetSelectedItemString(){
   return wxcbListBox->GetStringSelection().c_str();
};

bool TDialogListBoxOptions::IsOptionsAlreadySet(){
   return bIsOptionsAlreadySet;
}

void TDialogListBoxOptions::OnUpdate(wxCommandEvent& event){
   if(fufsUpdater != NULL)
      (*fufsUpdater).UpdateListBox();
   event.Skip();
}

int TDialogListBoxOptions::ShowModal(){
   wxDialog::ShowModal();
   return wxID_OK;
}

void TDialogListBoxOptions::OnClickOk(wxCommandEvent& event){
  this->Show(FALSE);
  event.Skip();
}

void TDialogListBoxOptions::OnCloseWindow(wxCloseEvent& event){
  if(fufsUpdater != NULL)
      (*fufsUpdater).RefreshInterface();
  event.Skip();
}

void TDialogListBoxOptions::SetOptions(wxArrayString& wxsOptions){
   this->wxcbListBox->Clear();
   this->wxcbListBox->InsertItems(wxsOptions, 0);
   bIsOptionsAlreadySet = true;
}

void TDialogListBoxOptions::OnMove(wxMoveEvent& event){
   if(fufsUpdater != NULL)
      (*fufsUpdater).RefreshInterface();
   event.Skip();
}