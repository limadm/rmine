#include "DialogSlider.h"

enum{
   DialogSlider_Ok = wxID_LOWEST - 301,
   DialogSlider_Updated =  wxID_LOWEST - 302,
   DialogSlider_Move =  wxID_LOWEST - 303
};

BEGIN_EVENT_TABLE(TDialogSlider, wxDialog)
EVT_BUTTON(DialogSlider_Ok, TDialogSlider::OnClickOk)
EVT_CLOSE(TDialogSlider::OnCloseWindow)
EVT_SLIDER(DialogSlider_Updated, TDialogSlider::OnUpdate)
EVT_MOVE(TDialogSlider::OnMove)
END_EVENT_TABLE()

TDialogSlider::TDialogSlider(wxWindow* parent,const wxString& title, const wxPoint& pPosition)
:wxDialog(parent, wxID_ANY, title, pPosition){
   m_slider = new wxSlider( this, DialogSlider_Updated, 10, 0, 10,
                            wxDefaultPosition, wxSize(155,wxDefaultCoord),
                            wxSL_AUTOTICKS | wxSL_LABELS);
   m_slider->SetTickFreq(1, 0);

   this->SetSize(285,100);

   dialogSizer = new wxFlexGridSizer(2, 2, 10, 10);
   okButton = new wxButton(this, DialogSlider_Ok, "Ok", wxDefaultPosition);                          

   dialogSizer->Add(m_slider, 0, wxALIGN_CENTRE_VERTICAL);
   dialogSizer->Add(okButton, 0, wxALIGN_CENTRE_VERTICAL);

   fufsUpdater = NULL;
   fufsOnCloseUpdater = NULL;

   this->SetSizer(dialogSizer);
   this->SetAutoLayout(true);
   this->Layout();
}

TDialogSlider::~TDialogSlider(){
   if(fufsUpdater != NULL)
      delete fufsUpdater;
   if(fufsOnCloseUpdater != NULL)
      delete fufsOnCloseUpdater;
/*  Parent wxSlider is responsible for this
   delete m_slider;
   delete okButton;
   delete dialogSizer;*/
}

void TDialogSlider::SetLimits(int iAHigherLimit, int iALowerLimit, int iATickFrequency){
   m_slider->SetRange(iAHigherLimit, iALowerLimit);
   m_slider->SetValue(iAHigherLimit);
   m_slider->SetTick(iATickFrequency);   
}
void TDialogSlider::SetSliderPosition(int iAPosition){
   m_slider->SetValue(iAPosition);
}   

void TDialogSlider::OnUpdate(wxCommandEvent& event){
   if(fufsUpdater != NULL)
      (*fufsUpdater).UpdateSlider();
   event.Skip();
}

int TDialogSlider::ShowModal(){
   wxDialog::ShowModal();
   return wxID_OK;
}

void TDialogSlider::OnClickOk(wxCommandEvent& event){  
   this->Show(FALSE);
   if(fufsUpdater != NULL)
      (*fufsUpdater).RefreshInterface();
   event.Skip();
}

void TDialogSlider::OnCloseWindow(wxCloseEvent& event){
  if(fufsUpdater != NULL)
      (*fufsUpdater).RefreshInterface();
  event.Skip();
}
void TDialogSlider::OnMove(wxMoveEvent& event){
  if(fufsUpdater != NULL)
      (*fufsUpdater).RefreshInterface();
   event.Skip();
}
