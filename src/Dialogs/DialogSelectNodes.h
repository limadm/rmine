#pragma once
#ifndef _DialogSelectNodes_
#define _DialogSelectNodes_

#include <Commons/Auxiliar.h>

class TDialogSelectNodes : public wxDialog
{
public:
   /*Small interface to be implemented in order to teach the DialogSelectNodes
   how to update the exterior interface that called the slider*/
   class TFunctorUpdaterForSelectNodesDialog{
      protected:
         void *interfaceToBeUpdated;
      public:
         virtual void SetInterfaceToBeUpdated(void *aInterfaceToBeUpdated){
            interfaceToBeUpdated = aInterfaceToBeUpdated;
         };
         virtual void FinishSelectionSession(){
            if(interfaceToBeUpdated == NULL) return;
         };
         virtual void RefreshInterface(){
            if(interfaceToBeUpdated == NULL) return;
         };
         virtual void Execute(){
            if(interfaceToBeUpdated == NULL) return;
         };
         virtual void CloseSequence(){
            if(interfaceToBeUpdated == NULL) return;
         }
   };
public:
   TDialogSelectNodes();
   TDialogSelectNodes(wxWindow* parent,const wxString& title, const wxPoint& pPosition = wxDefaultPosition);
   ~TDialogSelectNodes();
   void AddSelectedItemForExhibition(std::string sAItemLabel);
   void GetListOfSelectedNodes(std::list<std::string> &lAListOfLabels);
//   virtual int ShowModal();
   TFunctorUpdaterForSelectNodesDialog* fufsUpdater;
private:
   bool bShowForTheFirstTime;
   wxButton *okButton;
   wxButton *cancelButton;
   wxTextCtrl *wxtcListOfSelectedNodes;
   wxFlexGridSizer *dialogUpSizer;
   wxFlexGridSizer *dialogDownSizer;
   wxFlexGridSizer *dialogSizer;
   void OnClickOk(wxCommandEvent& event);
   void OnClickCancel(wxCommandEvent& event);   
   void OnShow(wxShowEvent& event);
   void OnCloseWindow(wxCloseEvent& event);
   void OnMove(wxMoveEvent& event);
   DECLARE_EVENT_TABLE()
};

#endif
