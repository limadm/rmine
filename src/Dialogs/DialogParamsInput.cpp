#include "DialogParamsInput.h"

enum{
   InputFrame_Ok = wxID_LOWEST - 401,
   InputFrame_Cancel = wxID_LOWEST - 402
};

BEGIN_EVENT_TABLE(TDialogParamsInput, wxDialog)
EVT_BUTTON(InputFrame_Ok, TDialogParamsInput::OnClickOk)
EVT_BUTTON(InputFrame_Cancel, TDialogParamsInput::OnClickCancel)
END_EVENT_TABLE()

TDialogParamsInput::TDialogParamsInput(wxWindow* parent,const wxString& title)
:wxDialog(parent, wxID_ANY, title){
   iReturnStatus = wxID_CANCEL;
   sText1 = new wxStaticText(this, -1, "Parameter 1");
   tcTextCtrl1 = new wxTextCtrl(this, -1);
   sText2 = new wxStaticText(this, -1, "Parameter 2");
   tcTextCtrl2 = new wxTextCtrl(this, -1);
   sText3 = new wxStaticText(this, -1, "Parameter 3");
   tcTextCtrl3 = new wxTextCtrl(this, -1);
   sText4 = new wxStaticText(this, -1, "Parameter 4");
   tcTextCtrl4 = new wxTextCtrl(this, -1);

   this->SetSize(200,200);
   this->Centre(wxBOTH);
   dialogSizer = new wxFlexGridSizer(2, 2, 10, 10);

   wxbOkButton = new wxButton(this, InputFrame_Ok, "Ok", wxDefaultPosition, wxSize(40,20));
   wxbCancelButton = new wxButton(this, InputFrame_Cancel, "Cancel", wxDefaultPosition, wxSize(40,20));

   this->SetSizer(dialogSizer);
   this->SetAutoLayout(TRUE);
}
void TDialogParamsInput::SetInitialValues(wxString sParam1, wxString sParam2,
                                          wxString sParam3, wxString sParam4){
   tcTextCtrl1->SetValue(sParam1);
   tcTextCtrl2->SetValue(sParam2);
   tcTextCtrl3->SetValue(sParam3);
   tcTextCtrl4->SetValue(sParam4);
}
void TDialogParamsInput::MakeDialog(){
   dialogSizer->Add(sText1, 0, wxALIGN_CENTRE_VERTICAL);
   dialogSizer->Add(tcTextCtrl1, 0, wxALIGN_CENTRE_VERTICAL);

   dialogSizer->Add(sText2, 0, wxALIGN_CENTRE_VERTICAL);
   dialogSizer->Add(tcTextCtrl2, 0, wxALIGN_CENTRE_VERTICAL);

   dialogSizer->Add(sText3, 0, wxALIGN_CENTRE_VERTICAL);
   dialogSizer->Add(tcTextCtrl3, 0, wxALIGN_CENTRE_VERTICAL);

   dialogSizer->Add(sText4, 0, wxALIGN_CENTRE_VERTICAL);
   dialogSizer->Add(tcTextCtrl4, 0, wxALIGN_CENTRE_VERTICAL);

   dialogSizer->Add(wxbOkButton, 0, wxALIGN_CENTRE_VERTICAL);
   dialogSizer->Add(wxbCancelButton, 0, wxALIGN_CENTRE_VERTICAL);

   this->Layout();
}
void TDialogParamsInput::SetParam1Name(wxString wxsAName){
   bShowParam1 = true;
   sText1->SetLabel(wxsAName);
}
void TDialogParamsInput::SetParam2Name(wxString wxsAName){
   bShowParam2 = true;
   sText2->SetLabel(wxsAName);
}
void TDialogParamsInput::SetParam3Name(wxString wxsAName){
   bShowParam3 = true;
   sText3->SetLabel(wxsAName);
}
void TDialogParamsInput::SetParam4Name(wxString wxsAName){
   bShowParam4 = true;
   sText4->SetLabel(wxsAName);
}

int TDialogParamsInput::ShowModal(){
   wxDialog::ShowModal();
   return iReturnStatus;
}

void TDialogParamsInput::OnClickOk(wxCommandEvent& event){
   iReturnStatus = wxID_OK;
   this->Show(FALSE);
   event.Skip();
}
void TDialogParamsInput::OnClickCancel(wxCommandEvent& event){
   iReturnStatus = wxID_CANCEL;   
   this->Show(FALSE);
   event.Skip();
}
wxString TDialogParamsInput::GetParam1(){
   if(!bShowParam1)
      return "";
   return tcTextCtrl1->GetLabel();
}
wxString TDialogParamsInput::GetParam2(){
   if(!bShowParam2)
      return "";
   return tcTextCtrl2->GetLabel();
}
wxString TDialogParamsInput::GetParam3(){
   if(!bShowParam3)
      return "";
   return tcTextCtrl3->GetLabel();
}
wxString TDialogParamsInput::GetParam4(){
   if(!bShowParam4)
      return "";
   return tcTextCtrl4->GetLabel();
}