#include "DialogSelectValue.h"

enum{
   DialogSelectValue_Ok = wxID_LOWEST - 301,
   DialogSelectValue_Cancel = wxID_LOWEST - 302,
   DialogSelectValue_Updated =  wxID_LOWEST - 303,
   DialogSelectValue_Move =  wxID_LOWEST - 304
};

BEGIN_EVENT_TABLE(TDialogSelectValue, wxDialog)
EVT_BUTTON(DialogSelectValue_Ok, TDialogSelectValue::OnClickOk)
EVT_BUTTON(DialogSelectValue_Cancel, TDialogSelectValue::OnClickCancel)
EVT_SHOW(TDialogSelectValue::OnShow)
EVT_CLOSE(TDialogSelectValue::OnCloseWindow)
EVT_MOVE(TDialogSelectValue::OnMove)
END_EVENT_TABLE()

TDialogSelectValue::TDialogSelectValue(wxWindow* parent,const wxString& title, const wxPoint& pPosition)
:wxDialog(parent, wxID_ANY, title, pPosition){
   bShowForTheFirstTime = true;
   this->SetSize(200,90);
   wxscValueSpiner = new wxSpinCtrl(this,-1);
   dialogSizer = new wxFlexGridSizer(2, 1, 10, 10);
   dialogDownSizer = new wxFlexGridSizer(1, 2, 10, 10);
   okButton = new wxButton(this, DialogSelectValue_Ok, "Ok", wxPoint(100, 55));
   cancelButton = new wxButton(this, DialogSelectValue_Cancel, "Cancel", wxPoint(100, 55));
   dialogSizer->Add(wxscValueSpiner, 0, wxALIGN_CENTRE_VERTICAL|wxALIGN_CENTRE_HORIZONTAL);
   dialogSizer->Add(dialogDownSizer, 0, wxALIGN_CENTRE_VERTICAL|wxALIGN_CENTRE_HORIZONTAL);
   dialogDownSizer->Add(okButton, 0, wxALIGN_CENTRE_VERTICAL);
   dialogDownSizer->Add(cancelButton, 0, wxALIGN_CENTRE_VERTICAL);

   fufsUpdater = NULL;

   this->SetSizer(dialogSizer);
   this->SetAutoLayout(true);
   this->Layout();
}

TDialogSelectValue::~TDialogSelectValue(){
   if(fufsUpdater != NULL)
      delete fufsUpdater;
}

void TDialogSelectValue::SetValuesRange(int iLow, int iMax){
   wxscValueSpiner->SetRange(iLow, iMax);
   wxscValueSpiner->SetValue((iLow+iMax)/2);
}

int TDialogSelectValue::GetValue(){   
   return wxscValueSpiner->GetValue();
}
void TDialogSelectValue::OnShow(wxShowEvent& event){
   event.Skip();

}
void TDialogSelectValue::OnClickCancel(wxCommandEvent& event){ 
   event.Skip();
   this->Show(FALSE);
   if(fufsUpdater != NULL)
      (*fufsUpdater).RefreshInterface();   
   bShowForTheFirstTime = true;
}
void TDialogSelectValue::OnClickOk(wxCommandEvent& event){ 
   event.Skip();
   this->Show(FALSE);
   if(fufsUpdater != NULL)
      (*fufsUpdater).Execute();
   bShowForTheFirstTime = true;
}

void TDialogSelectValue::OnCloseWindow(wxCloseEvent& event){
  event.Skip();
  if(fufsUpdater != NULL)
      (*fufsUpdater).RefreshInterface();
}
void TDialogSelectValue::OnMove(wxMoveEvent& event){
   event.Skip();
   if(fufsUpdater != NULL)
      (*fufsUpdater).RefreshInterface();
}
