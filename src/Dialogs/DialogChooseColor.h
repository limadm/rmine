#pragma once
#ifndef _DialogChooseColor_
#define _DialogChooseColor_

#include <Commons/stdafx.h>
#include <wx/generic/colrdlgg.h>

class TFunctorUpdaterDialogChooseColor;
class TDialogChooseColor : public wxGenericColourDialog
{
public:
   TDialogChooseColor(wxWindow *parent);
   TFunctorUpdaterDialogChooseColor* fufsUpdater;
private:
   void OnMouseLeftClick(wxMouseEvent& event);
   void OnRedSlider(wxCommandEvent& event);
   void OnGreenSlider(wxCommandEvent& event);
   void OnBlueSlider(wxCommandEvent& event);
   void OnCloseWindow(wxCloseEvent& event);
   DECLARE_EVENT_TABLE()
};

/*Small interface to be implemented in order to teach the DialogChooseColor
how to update the exterior interface that called the slider*/
class TFunctorUpdaterDialogChooseColor{
protected:
   void *interfaceToBeUpdated;
public:
   virtual void SetInterfaceToBeUpdated(void *aInterfaceToBeUpdated){
      interfaceToBeUpdated = aInterfaceToBeUpdated;
   }
   virtual void UpdateDialogChooseColor(){
      if(interfaceToBeUpdated == NULL) return;         
   };
   virtual void RefreshInterface(){
      if(interfaceToBeUpdated == NULL) return;
   };
};

#endif