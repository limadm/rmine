#pragma once
#ifndef _DialogListBoxOptions_
#define _DialogListBoxOptions_

#include <Commons/Auxiliar.h>

class TFunctorUpdaterForListBox;

class TDialogListBoxOptions : public wxDialog
{
public:
   TFunctorUpdaterForListBox* fufsUpdater;
public:
   TDialogListBoxOptions(wxWindow* parent,const wxString& title, int iWidth = 285, int iHeight = 100);
   int GetSelectedItemIndex();
   std::string GetSelectedItemString();
   virtual int ShowModal();
   void SetOptions(wxArrayString& wxsOptions);
   bool IsOptionsAlreadySet();
   void SetNewSize(int iWidth, int iHeight);
private:
   wxListBox *wxcbListBox;
   wxButton *okButton;
   wxFlexGridSizer *dialogSizer;
   bool bIsOptionsAlreadySet;
private:
   void OnClickOk(wxCommandEvent& event);
   void OnCloseWindow(wxCloseEvent& event);
   void OnUpdate(wxCommandEvent& event);
   void OnMove(wxMoveEvent& event);
   DECLARE_EVENT_TABLE()
};

/*Small interface to be implemented in order to teach the DialogListBoxOptions
how to update the exterior interface that called the slider*/
class TFunctorUpdaterForListBox{
   protected:
      void *interfaceToBeUpdated;   void *allPurposeObject;
   public:
      TFunctorUpdaterForListBox():allPurposeObject(NULL){};
      virtual void SetInterfaceToBeUpdated(void *aInterfaceToBeUpdated){
         interfaceToBeUpdated = aInterfaceToBeUpdated;
      }
      /*Store anything that eventually will be used in a callback*/
      virtual void SetOptionalAllPurposeObject(void *aObject){
         allPurposeObject = aObject;
      }
      virtual void* GetOptionalAllPurposeObject(){
         return allPurposeObject;
      }
      virtual void UpdateListBox(){
         if(interfaceToBeUpdated == NULL) return;
      };
      virtual void RefreshInterface(){
         if(interfaceToBeUpdated == NULL) return;
      };
};
#endif