#include "DialogChooseColor.h"

enum{
   ChooseColor_Updated =  wxID_LOWEST - 901
};

BEGIN_EVENT_TABLE(TDialogChooseColor, wxGenericColourDialog)
    EVT_SLIDER(wxID_RED_SLIDER, TDialogChooseColor::OnRedSlider)
    EVT_SLIDER(wxID_GREEN_SLIDER, TDialogChooseColor::OnGreenSlider)
    EVT_SLIDER(wxID_BLUE_SLIDER, TDialogChooseColor::OnBlueSlider)
    EVT_CLOSE(TDialogChooseColor::OnCloseWindow)
    EVT_LEFT_UP(TDialogChooseColor::OnMouseLeftClick)
END_EVENT_TABLE()

TDialogChooseColor::TDialogChooseColor(wxWindow *parent)
:wxGenericColourDialog(parent){
   fufsUpdater = NULL;
}

void TDialogChooseColor::OnCloseWindow(wxCloseEvent& event){
  if(fufsUpdater != NULL)
      (*fufsUpdater).RefreshInterface();
  event.Skip();
}


void TDialogChooseColor::OnMouseLeftClick(wxMouseEvent& event){
   wxGenericColourDialog::OnMouseEvent(event);
   if(fufsUpdater != NULL)
      (*fufsUpdater).UpdateDialogChooseColor();
   event.Skip();
}

void TDialogChooseColor::OnRedSlider(wxCommandEvent& event){
   wxGenericColourDialog::OnRedSlider(event);
   if(fufsUpdater != NULL)
      (*fufsUpdater).UpdateDialogChooseColor();
   event.Skip();
}
void TDialogChooseColor::OnGreenSlider(wxCommandEvent& event){
   wxGenericColourDialog::OnGreenSlider(event);
   if(fufsUpdater != NULL)
      (*fufsUpdater).UpdateDialogChooseColor();
   event.Skip();
}
void TDialogChooseColor::OnBlueSlider(wxCommandEvent& event){
   wxGenericColourDialog::OnBlueSlider(event);
   if(fufsUpdater != NULL)
      (*fufsUpdater).UpdateDialogChooseColor();
   event.Skip();
}