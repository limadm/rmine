#pragma once
#ifndef _DrawWxWidget_
#define _DrawWxWidget_

#include <string>
#include <time.h>

#include <Commons/Auxiliar.h>
#include <Drawings/DrawInterface.h>
#include <Frames/FrameShowImage.h>
#include <Layouts/LayoutRandom.h>

template <class _TNodeNet>
class TDrawWxWidget : implements TDrawInterface<_TNodeNet>{
public:
   TDrawWxWidget(TPt<_TNodeNet> pANodeNetData);
   ~TDrawWxWidget();
   void SetFrame(wxWindow *fAFrame);
   bool IdentifyNode(const int iX, const int iY, typename _TNodeNet::TNodeI& niANode);
   virtual bool ConvertDeviceToDrawingCoordinates(double& dX, double& dY);
   virtual void SetWidthAndHeight(wxCoord wxcAWidth,wxCoord wxcAHeight);
   virtual void SetRelativeScale(double fWidthScaleFactor,double fHeightScaleFactor,
                                 int iReferenceX, int iReferenceY);
   virtual void SetAbsoluteScale(double fWidthScaleFactor,double fHeightScaleFactor);
   virtual void SetTranslation(int iAHorizontalTranslation, int iAVerticalTranslation);
   virtual void SetAbsoluteTranslation(int iAHorizontalTranslation, int iAVerticalTranslation);
   virtual void ApplyLayout(TLayoutInterface<_TNodeNet> &gliAGraphLayout);
   virtual void DrawGraph(void *vDrawContext);
   virtual void DrawNodeDetailedData(typename _TNodeNet::TNodeI& niANodeI);
   virtual void DrawNeighborhoodDetails();
   virtual std::string GetLabelFromEmphasizedNode();
   virtual bool SetLabelForEmphasizedNode(std::string sALabel);
   virtual bool SetShapeForEmphasizedNode(TShapeType::TShapeAcronysm npsAShapeType);
   virtual bool SetColorForEmphasizedNode(wxColour& wxcAColour);
   virtual bool SetSizeForEmphasizedNode(double dASize);
   virtual double GetSizeForEmphasizedNode();
   virtual wxMDIChildFrame* DegreeDist(wxMDIParentFrame *wxmdipAParent, wxWindow *wxfACallingFrame);
   virtual wxMDIChildFrame* CummulativeDegreeDist(wxMDIParentFrame *wxmdipAParent, wxWindow *wxfACallingFrame);
   virtual wxMDIChildFrame* Hops(wxMDIParentFrame *wxmdipAParent, wxWindow *wxfACallingFrame);
   virtual wxMDIChildFrame* SingValues(wxMDIParentFrame *wxmdipAParent, wxWindow *wxfACallingFrame);
   virtual wxMDIChildFrame* WeakComponents(wxMDIParentFrame *wxmdipAParent, wxWindow *wxfACallingFrame);
   virtual wxMDIChildFrame* StrongComponents(wxMDIParentFrame *wxmdipAParent, wxWindow *wxfACallingFrame);
   virtual void UpadteGraphDeviceCoordinates();
   void SetShowNeighborHoodDetails(bool bOptionDrawNodeDetailedDataInNeighborHood,
                                   bool bShowLabelsInNeighborHoodDetails,
                                   typename _TNodeNet::TNodeI aNodeInShowLabelsInNeighborHoodDetails);
private:
   TFrameShowImage *sifImageFrame;
   /*Imagine the graph as a squared area where all the nodes and edges lie in*/
   /*wxcGraphWidth and wxcGraphHeight are boundless (all the graph or just part of the graph being showed)*/
	wxCoord wxcGraphWidth;      wxCoord wxcGraphHeight;
   /*Positional translation of the graph*/
   int iHorizontalTranslation; int iVerticalTranslation;
   /*Display limits in drawing coordinates (0.0 - 1.0)*/
   double fMinX; double fMaxX;   double fMinY; double fMaxY;
   /*Default layout*/
   TLayoutRandom<_TNodeNet> gliRandomLayout;  
   /*Internal Layout*/
   TLayoutInterface<_TNodeNet>* gliGraphLayout;
   wxBrush *wxbDrawingBrush;
   /*Color possibilties*/
   byte bRegularR;   byte bRegularG;   byte bRegularB;
   byte bInfectedR;   byte bInfectedG;   byte bInfectedB;
   byte bImmunizedR;   byte bImmunizedG;   byte bImmunizedB;
   /*Size of the display in display coordinates - used in DrawNode()*/
   int iDisplayWidth;         int iDisplayHeight;
   wxPen* wxpCustomPen;       wxDC *dcDrawContext;
   /*NeighborHoodDetails parameters*/
   bool bShowNeighborHoodDetailsOnce;  bool bDrawNodeDetailedDataInNeighborHoodDetails;
   bool bShowLabelsInNeighborHoodDetails;
   typename _TNodeNet::TNodeI nodeInShowLabelsInNeighborHoodDetails;
private:
   virtual void DrawNodes();
   virtual void DrawNode(typename _TNodeNet::TNodeI& niANodeI, bool bEmphasizeNode = false);
   virtual void DrawInfluenceNode(typename _TNodeNet::TNodeI& niANodeI, bool bEmphasizeNode = false);
   virtual void DrawEdges();
   virtual void DrawEdge(typename _TNodeNet::TEdgeI &eAEdge);
   void DetermineNodeColor(typename _TNodeNet::TNodeI& niANodeI, bool bEmphasizeNode);
   void DoDrawing(typename _TNodeNet::TNodeI& niANodeI, bool bEmphasizeNode = false);
   void DrawWeight(typename _TNodeNet::TEdgeI &eiAEdgeI);
   virtual void ConvertDrawingToDeviceCoordinates(double& fDrawingToDeviceX, double& fDrawingToDeviceY);
   void UpdateDisplaySize();
   void SetNodePosition(typename _TNodeNet::TNodeI& niANode, const int iX, const int iY);
   virtual void DrawNodeLabel(typename _TNodeNet::TNodeI& niANodeI);
};

template <class _TNodeNet>
TDrawWxWidget<_TNodeNet>::TDrawWxWidget(TPt<_TNodeNet> pANodeNetData) {
// Superclass attributes
   this->pNodeNetData = pANodeNetData;
   this->fFrameToDraw = NULL;
   this->bShowAllLabels = false;
   this->bShowWeights = false;
   this->iExibitionLevel = -1;
   this->tLowTimeLimit = 0;
// Class attributes
   dcDrawContext = NULL;
   bShowNeighborHoodDetailsOnce = false;
   bDrawNodeDetailedDataInNeighborHoodDetails = false;
   bShowLabelsInNeighborHoodDetails = false;
   wxcGraphWidth = 100;
   wxcGraphHeight = 100;
   iHorizontalTranslation = 0; iVerticalTranslation = 0;
   gliGraphLayout = NULL;
   fMinX = 100000; fMaxX = -1; fMinY = 100000; fMaxY = -1;
   wxbDrawingBrush = new wxBrush;
   bRegularR = 0;    bRegularG = 255; bRegularB = 0;
   bInfectedR = 255; bInfectedG = 0;  bInfectedB = 0;
   bImmunizedR = 0;  bImmunizedG = 0; bImmunizedB = 255;
   sifImageFrame = NULL;
   wxpCustomPen = new wxPen(wxColour(0,0,255));
};
template <class _TNodeNet>
TDrawWxWidget<_TNodeNet>::~TDrawWxWidget(){
   delete wxbDrawingBrush;
};
template <class _TNodeNet>
void TDrawWxWidget<_TNodeNet>::SetFrame(wxWindow *fAFrame){
   this->fFrameToDraw = fAFrame;
   UpdateDisplaySize();
}

template <class _TNodeNet>
void TDrawWxWidget<_TNodeNet>::SetShowNeighborHoodDetails(bool bOptionDrawNodeDetailedDataInNeighborHood,
                                                          bool bOptionShowLabelsInNeighborHoodDetails,
                                                            typename _TNodeNet::TNodeI aNodeInShowLabelsInNeighborHoodDetails){
   bShowNeighborHoodDetailsOnce = true;
   bDrawNodeDetailedDataInNeighborHoodDetails = bOptionDrawNodeDetailedDataInNeighborHood;
   bShowLabelsInNeighborHoodDetails = bOptionShowLabelsInNeighborHoodDetails;
   nodeInShowLabelsInNeighborHoodDetails = aNodeInShowLabelsInNeighborHoodDetails;
}

template <class _TNodeNet>
void TDrawWxWidget<_TNodeNet>::UpdateDisplaySize(){
   int& iWidhtTemp = iDisplayWidth;   int& iHeightTemp = iDisplayHeight;
   this->fFrameToDraw->GetSize(&iDisplayWidth, &iDisplayHeight);

   iDisplayWidth = iWidhtTemp;
   iDisplayHeight = iHeightTemp;
};
/*Imagine the graph as a squared area where all the nodes and edges lie in*/
template <class _TNodeNet>
void TDrawWxWidget<_TNodeNet>::ConvertDrawingToDeviceCoordinates(double& fDrawingToDeviceX, double& fDrawingToDeviceY){
   fDrawingToDeviceX = iHorizontalTranslation + fDrawingToDeviceX*wxcGraphWidth;
   fDrawingToDeviceY = iVerticalTranslation   + fDrawingToDeviceY*wxcGraphHeight;
}

/*Given two display coordinates, return two drawing coordinates (0.0 - 1.0)*/
template <class _TNodeNet>
bool TDrawWxWidget<_TNodeNet>::ConvertDeviceToDrawingCoordinates(double& dX, double& dY){
   double fTempMaxX = 1.0;  double fTempMinX = 0.0;
   double fTempMaxY = 1.0;  double fTempMinY = 0.0;
   /*Find the limits of the graph inside the display, in display coordinates*/
   ConvertDrawingToDeviceCoordinates(fTempMaxX,fTempMaxY);
   ConvertDrawingToDeviceCoordinates(fTempMinX,fTempMinY);

   if(((fTempMinX <= dX)&&(dX < fTempMaxX))&&
      ((fTempMinY <= dY)&&(dY < fTempMaxY))){
         dX = (dX - fTempMinX)/(fTempMaxX - fTempMinX);
         dY = (dY - fTempMinY)/(fTempMaxY - fTempMinY);
         /*Successful conversion*/
         return true;
      }else{
         /*failed - the point dX, dY is out of the graph drawing area*/
         return false;
      }
}
template <class _TNodeNet>
void TDrawWxWidget<_TNodeNet>::SetWidthAndHeight(wxCoord wxcAWidth,wxCoord wxcAHeight){
   wxcGraphWidth = wxcAWidth; wxcGraphHeight = wxcAHeight;
}
template <class _TNodeNet>
void TDrawWxWidget<_TNodeNet>::SetRelativeScale(double fWidthScaleFactor,double fHeightScaleFactor,
                                 int iReferenceX, int iReferenceY){
   /*Performs scale relative to a reference point*/

   double dXTemp = iReferenceX; double dYTemp = iReferenceY; 
   if(!ConvertDeviceToDrawingCoordinates(dXTemp, dYTemp))
      return;  /*Point out of the graph area*/
   
   /*Calculate size gain*/
   int iNewWidthSize  = fWidthScaleFactor * wxcGraphWidth;
   int iWidthIncrease = iNewWidthSize - wxcGraphWidth;

   int iNewHeightSize  = fHeightScaleFactor * wxcGraphHeight;
   int iHeightIncrease = iNewHeightSize - wxcGraphHeight;

   SetWidthAndHeight(iNewWidthSize, iNewHeightSize);
   /*Scaling increases coordinates, we decrease them to have the graph centered*/
   SetTranslation(-(float)dXTemp  * iWidthIncrease,
      -(float)dYTemp  * iHeightIncrease);
}
template <class _TNodeNet>
void TDrawWxWidget<_TNodeNet>::SetAbsoluteScale(double fWidthScaleFactor,double fHeightScaleFactor){
   this->UpdateDisplaySize();
   double dWidht = fWidthScaleFactor*wxcGraphWidth;
   double dHeight = fHeightScaleFactor*wxcGraphHeight;

   iHorizontalTranslation = fWidthScaleFactor*iHorizontalTranslation;
   iVerticalTranslation   = fHeightScaleFactor*iVerticalTranslation;

   SetWidthAndHeight(dWidht, dHeight);
   UpadteGraphDeviceCoordinates();
}
template <class _TNodeNet>
void TDrawWxWidget<_TNodeNet>::SetTranslation(int iAHorizontalTranslation, int iAVerticalTranslation){
   iHorizontalTranslation += iAHorizontalTranslation;
   iVerticalTranslation += iAVerticalTranslation;
   UpadteGraphDeviceCoordinates();
}
template <class _TNodeNet>
void TDrawWxWidget<_TNodeNet>::SetAbsoluteTranslation(int iAHorizontalTranslation, int iAVerticalTranslation){
   iHorizontalTranslation = iAHorizontalTranslation;
   iVerticalTranslation = iAVerticalTranslation;
   UpadteGraphDeviceCoordinates();
}
template <class _TNodeNet>
void TDrawWxWidget<_TNodeNet>::ApplyLayout(TLayoutInterface<_TNodeNet> &gliAGraphLayout){
   gliRandomLayout.ApplyLayout(this->pNodeNetData);
   gliAGraphLayout.ApplyLayout(this->pNodeNetData);
}

template <class _TNodeNet>
void TDrawWxWidget<_TNodeNet>::DrawGraph(void *vDrawContext){
   dcDrawContext = (wxDC*)vDrawContext;
   this->DrawEdges();
   this->DrawNodes();
   if(bShowNeighborHoodDetailsOnce)
      DrawNeighborhoodDetails();
   dcDrawContext = NULL;
}

template <class _TNodeNet>
void TDrawWxWidget<_TNodeNet>::DrawNodes(){
   if(dcDrawContext == NULL)
      return;
   typename _TNodeNet::TNodeI niTemp;

   std::set<int>::iterator vIterator;

   /*Retrack drawing limits at every draw inside 
     DrawNode then DoDrawing method*/
   fMinX = 100000; fMaxX = -1; fMinY = 100000; fMaxY = -1;
   /*Influence visualization*/
   if(this->pNodeNetData->bInfluenceDraw){
      /*Draw lines to son nodes*/
      wxBLACK_PEN->SetWidth(1);
      dcDrawContext->SetPen(*wxBLACK_PEN);
      for(typename _TNodeNet::TNodeI NI = this->pNodeNetData->BegNI(); NI < this->pNodeNetData->EndNI(); NI++){
         if( (NI().GetInfluenceLevel() < this->iExibitionLevel) )
            /*Go through all sons*/
            for (vIterator = NI().GetSonNodes().begin(); vIterator != NI().GetSonNodes().end(); vIterator++){
               dcDrawContext->DrawLine(NI().GetDeviceX(),
                                       NI().GetDeviceY(),
                                       this->pNodeNetData->GetNI(*vIterator)().GetDeviceX(),
                                       this->pNodeNetData->GetNI(*vIterator)().GetDeviceY());
               typename _TNodeNet::TNodeI node = this->pNodeNetData->GetNI((*vIterator));
               this->DrawNode(node);
            }
      }
      /*Draw the nodes*/
      for(typename _TNodeNet::TNodeI NI = this->pNodeNetData->BegNI(); NI < this->pNodeNetData->EndNI(); NI++){
         if(NI().GetInfluenceLevel() <= this->iExibitionLevel){
            /*Draw nodes*/
            this->DrawNode(NI);
         }
      }
   /*Regular visualization*/
   }else{
      for(typename _TNodeNet::TNodeI NI = this->pNodeNetData->BegNI(); NI < this->pNodeNetData->EndNI(); NI++){
         if(NI().GetVisible())
            this->DrawNode(NI);
      }
   }

   /*Draw focused node*/
   if(this->pNodeNetData->GetEmphasizedNode(niTemp))
      this->DrawNode(niTemp, true);
};
template <class _TNodeNet>
void TDrawWxWidget<_TNodeNet>::DrawEdges(){
   if(dcDrawContext == NULL)
      return;
   if(this->pNodeNetData->bInfluenceDraw){
      wxMEDIUM_GREY_PEN->SetWidth(1);
      dcDrawContext->SetPen(*wxMEDIUM_GREY_PEN);
   }else{
      wxBLACK_PEN->SetWidth(1);
      dcDrawContext->SetPen(*wxBLACK_PEN);
   }
   for (typename _TNodeNet::TEdgeI EI = this->pNodeNetData->BegEI(); EI != this->pNodeNetData->EndEI(); EI++) {
      if((this->pNodeNetData->GetNDat(EI.GetSrcNId()).GetVisible())&&(this->pNodeNetData->GetNDat(EI.GetDstNId()).GetVisible()))
         this->DrawEdge(EI);
   }
};

template <class _TNodeNet>
void TDrawWxWidget<_TNodeNet>::DrawInfluenceNode(typename _TNodeNet::TNodeI& niANodeI, bool bEmphasizeNode){
   this->DetermineNodeColor(niANodeI, bEmphasizeNode);
   this->DoDrawing(niANodeI);
};

template <class _TNodeNet>
void TDrawWxWidget<_TNodeNet>::DrawNode(typename _TNodeNet::TNodeI& niANodeI, bool bEmphasizeNode){
   if(niANodeI().GetVisible()){
      this->DetermineNodeColor(niANodeI, bEmphasizeNode);
      this->DoDrawing(niANodeI, bEmphasizeNode);
   }
};
template <class _TNodeNet>
void TDrawWxWidget<_TNodeNet>::DrawEdge(typename _TNodeNet::TEdgeI &eAEdge){
   dcDrawContext->DrawLine(this->pNodeNetData->GetNDat(eAEdge.GetSrcNId()).GetDeviceX(),
                           this->pNodeNetData->GetNDat(eAEdge.GetSrcNId()).GetDeviceY(),
                           this->pNodeNetData->GetNDat(eAEdge.GetDstNId()).GetDeviceX(),
                           this->pNodeNetData->GetNDat(eAEdge.GetDstNId()).GetDeviceY());

   if(this->bShowWeights)
      DrawWeight(eAEdge);
};
/*------------------------------------------------------------------------------------------*/
template <class _TNodeNet>
bool TDrawWxWidget<_TNodeNet>::IdentifyNode(const int iX, const int iY, typename _TNodeNet::TNodeI& niANode){
   double dTempX = iX; double dTempY = iY;
   if(this->ConvertDeviceToDrawingCoordinates(dTempX, dTempY)){
      for(typename _TNodeNet::TNodeI NI = this->pNodeNetData->BegNI(); NI < this->pNodeNetData->EndNI(); NI++) {
         if(NI().GetVisible()){
            if(NI().IsPointOver((double)dTempX, (double)dTempY)){
               niANode = NI;
               return true;
            }
         }
      }
   }
   return false;
};
/*------------------------------------------------------------------------------------------*/
template <class _TNodeNet>
bool TDrawWxWidget<_TNodeNet>::SetShapeForEmphasizedNode(TShapeType::TShapeAcronysm npsAShapeType){
   typename _TNodeNet::TNodeI niTemp;
   if(!this->pNodeNetData->GetEmphasizedNode(niTemp))
      return false;
   niTemp().SetShape(npsAShapeType);
   return true;
};

template <class _TNodeNet>
bool TDrawWxWidget<_TNodeNet>::SetColorForEmphasizedNode(wxColour& wxcAColour){
   typename _TNodeNet::TNodeI niTemp;
   if(!this->pNodeNetData->GetEmphasizedNode(niTemp))
      return false;
   niTemp().SetRed(wxcAColour.Red());
   niTemp().SetGreen(wxcAColour.Green());
   niTemp().SetBlue(wxcAColour.Blue());
   return true;
}

template <class _TNodeNet>
bool TDrawWxWidget<_TNodeNet>::SetSizeForEmphasizedNode(double dASize){
   typename _TNodeNet::TNodeI niTemp;
   if(!this->pNodeNetData->GetEmphasizedNode(niTemp))
      return false;
   niTemp().SetSize(dASize);
   return true;   
};

template <class _TNodeNet>
double TDrawWxWidget<_TNodeNet>::GetSizeForEmphasizedNode(){
   typename _TNodeNet::TNodeI niTemp;
   if(!this->pNodeNetData->GetEmphasizedNode(niTemp))
      return 0.0;
   return niTemp().GetSize();
};

template <class _TNodeNet>
std::string TDrawWxWidget<_TNodeNet>::GetLabelFromEmphasizedNode(){
   typename _TNodeNet::TNodeI niTemp;
   if(!this->pNodeNetData->GetEmphasizedNode(niTemp))
      return "";
   return niTemp().GetLabel();
}

template <class _TNodeNet>
bool TDrawWxWidget<_TNodeNet>::SetLabelForEmphasizedNode(std::string sALabel){
   typename _TNodeNet::TNodeI niTemp;
   if(!this->pNodeNetData->GetEmphasizedNode(niTemp))
      return false;
   niTemp().SetLabel(sALabel);
   return true;
}
/*------------------------------------------------------------------------------------------*/
template <class _TNodeNet>
void TDrawWxWidget<_TNodeNet>::DetermineNodeColor(typename _TNodeNet::TNodeI& niANodeI, bool bEmphasizeNode){
   /*Color of the node*/
   if(niANodeI().GetInfected())
      wxbDrawingBrush->SetColour(bInfectedR, bInfectedG, bInfectedB);
   else if(niANodeI().GetImmunized())
      wxbDrawingBrush->SetColour(bImmunizedR, bImmunizedG, bImmunizedB);
   else if(this->pNodeNetData->bInfluenceDraw){
      wxbDrawingBrush->SetColour((byte)(niANodeI().GetInfluenceLevel()/(float)this->pNodeNetData->GetMaxInfluenceLevel()*255),
                                 (byte)(niANodeI().GetInfluenceLevel()/(float)this->pNodeNetData->GetMaxInfluenceLevel()*255), 
                                 (byte)(niANodeI().GetInfluenceLevel()/(float)this->pNodeNetData->GetMaxInfluenceLevel()*255));      
   }else{
      if(bEmphasizeNode)
         wxbDrawingBrush->SetColour(255-niANodeI().GetRed(), 255-niANodeI().GetGreen(), 255-niANodeI().GetBlue());
      else
         wxbDrawingBrush->SetColour(niANodeI().GetRed(), niANodeI().GetGreen(), niANodeI().GetBlue());
   }
   dcDrawContext->SetBrush(*wxbDrawingBrush);

   wxBLACK_PEN->SetWidth(1);
   dcDrawContext->SetPen(*wxBLACK_PEN);
};

template <class _TNodeNet>
void TDrawWxWidget<_TNodeNet>::DrawNodeDetailedData(typename _TNodeNet::TNodeI& niANodeI){
   unsigned int iBiggestStringSize = 0;
   int iLabelFrameWidth, iLabelFrameHeight;
   int iXPos = niANodeI().GetDeviceX();
   int iYPos = niANodeI().GetDeviceY();

   /*Calculate small translation in order not to write over the node*/
   double dHShapeTranslation;
   /*The graph fits in the screen*/
   if((iDisplayWidth > wxcGraphWidth) || (iDisplayHeight > wxcGraphHeight))
      dHShapeTranslation = (niANodeI().GetSize()/2.0)*wxcGraphWidth;
   else /*The graph does not fit in the screen*/
      dHShapeTranslation = (niANodeI().GetSize()/2.0)*iDisplayWidth;
   iXPos += dHShapeTranslation;

   /*Labels*/
   std::string sId = "Id = " + TConverter<int>::ToString(niANodeI.GetId());
   iBiggestStringSize = sId.size();
   std::string sLabel = "Label = " + niANodeI().GetLabel();
   if(sLabel.size() > iBiggestStringSize)
      iBiggestStringSize = sLabel.size();
   std::string sHLevel = "Hierarchy level = " + TConverter<int>::ToString(niANodeI().GetInfluenceLevel());
   if(sHLevel.size() > iBiggestStringSize)
      iBiggestStringSize = sHLevel.size();
   std::string sPRank = "PageRank = " + TConverter<double>::ToString(niANodeI().GetPageRank());
   if(sPRank.size() > iBiggestStringSize)
      iBiggestStringSize = sPRank.size();
   std::string sMETISPartition = "METIS Part = " + TConverter<int>::ToString(niANodeI().GetMETISPartitionNumber());
   if(sMETISPartition.size() > iBiggestStringSize)
      iBiggestStringSize = sMETISPartition.size();
   wxBLACK_PEN->SetWidth(1);
   dcDrawContext->SetPen(*wxBLACK_PEN);
   wxbDrawingBrush->SetColour(255, 255, 255);
   dcDrawContext->SetBrush(*wxbDrawingBrush);
   iLabelFrameWidth = iBiggestStringSize*8 + 2;
   if(((iXPos + iLabelFrameWidth) > iDisplayWidth) && (iXPos > iLabelFrameWidth*0.85))
      iXPos -= iLabelFrameWidth;
   iLabelFrameHeight = (5 + 1) * 14;
   if(((iYPos + iLabelFrameHeight) > iDisplayHeight) && (iYPos > iLabelFrameHeight*0.85))
      iYPos -= iLabelFrameHeight;
   dcDrawContext->DrawRectangle(iXPos, iYPos, iLabelFrameWidth, iLabelFrameHeight);
  
   iXPos += 2;
   dcDrawContext->DrawText(sId.c_str(),iXPos, iYPos + 6);
   dcDrawContext->DrawText(sLabel.c_str(),iXPos, iYPos + 20);
   dcDrawContext->DrawText(sHLevel.c_str(),iXPos, iYPos + 34);
   dcDrawContext->DrawText(sPRank.c_str(),iXPos, iYPos + 48);
   dcDrawContext->DrawText(sMETISPartition.c_str(),iXPos, iYPos + 62);
}

template <class _TNodeNet>
void TDrawWxWidget<_TNodeNet>::DrawNeighborhoodDetails(){
   if(dcDrawContext == NULL)
      return;
   int iNextNeighbor;      int iEdgeIndex;
   int iNeighbors = nodeInShowLabelsInNeighborHoodDetails.GetOutDeg();

   for(int i = 0; i < iNeighbors; i++){
      iNextNeighbor = nodeInShowLabelsInNeighborHoodDetails.GetOutNId(i);
      if(this->pNodeNetData->GetNI(iNextNeighbor)().GetVisible()){
         this->pNodeNetData->IsEdge(nodeInShowLabelsInNeighborHoodDetails.GetId(),iNextNeighbor,iEdgeIndex);

         wxpCustomPen->SetWidth(3);
         wxpCustomPen->SetColour(255-nodeInShowLabelsInNeighborHoodDetails().GetRed(), 255-nodeInShowLabelsInNeighborHoodDetails().GetGreen(), 255-nodeInShowLabelsInNeighborHoodDetails().GetBlue());
         dcDrawContext->SetPen(*wxpCustomPen);

         typename _TNodeNet::TEdgeI edge = this->pNodeNetData->GetEI(iEdgeIndex);
         this->DrawEdge(edge);
         this->DrawWeight(edge);

         typename _TNodeNet::TNodeI node = this->pNodeNetData->GetNI(iNextNeighbor);
         this->DrawNode(node, true);
         if(bShowLabelsInNeighborHoodDetails)
            this->DrawNodeLabel(node);
      }
   }
   iNeighbors = nodeInShowLabelsInNeighborHoodDetails.GetInDeg();
   for(int i = 0; i < iNeighbors; i++){
      iNextNeighbor = nodeInShowLabelsInNeighborHoodDetails.GetInNId(i);
      if(this->pNodeNetData->GetNI(iNextNeighbor)().GetVisible()){

         this->pNodeNetData->IsEdge(iNextNeighbor,nodeInShowLabelsInNeighborHoodDetails.GetId(),iEdgeIndex);
         wxpCustomPen->SetWidth(3);
         wxpCustomPen->SetColour(255-nodeInShowLabelsInNeighborHoodDetails().GetRed(), 255-nodeInShowLabelsInNeighborHoodDetails().GetGreen(), 255-nodeInShowLabelsInNeighborHoodDetails().GetBlue());
         dcDrawContext->SetPen(*wxpCustomPen);

         typename _TNodeNet::TEdgeI edge = this->pNodeNetData->GetEI(iEdgeIndex);
         this->DrawEdge(edge);
         this->DrawWeight(edge);

         typename _TNodeNet::TNodeI node = this->pNodeNetData->GetNI(iNextNeighbor);
         this->DrawNode(node, true);
         if(bShowLabelsInNeighborHoodDetails)
            this->DrawNodeLabel(node);
      }
   }
   this->DrawNode(nodeInShowLabelsInNeighborHoodDetails, true);
   if(bShowLabelsInNeighborHoodDetails)
      this->DrawNodeLabel(nodeInShowLabelsInNeighborHoodDetails);

   if(bDrawNodeDetailedDataInNeighborHoodDetails)
      DrawNodeDetailedData(nodeInShowLabelsInNeighborHoodDetails);
   //bShowNeighborHoodDetailsOnce = false;
}

template <class _TNodeNet>
void TDrawWxWidget<_TNodeNet>::DrawWeight(typename _TNodeNet::TEdgeI &eiAEdgeI){
   int dWeightX = (this->pNodeNetData->GetNI(eiAEdgeI.GetDstNId())().GetDeviceX() - this->pNodeNetData->GetNI(eiAEdgeI.GetSrcNId())().GetDeviceX())/2 + this->pNodeNetData->GetNI(eiAEdgeI.GetSrcNId())().GetDeviceX();
   int dWeightY = (this->pNodeNetData->GetNI(eiAEdgeI.GetDstNId())().GetDeviceY() + this->pNodeNetData->GetNI(eiAEdgeI.GetSrcNId())().GetDeviceY())/2;

   std::string sWeight = TConverter<int>::ToString(eiAEdgeI().GetWeight());

   wxBLACK_PEN->SetWidth(1);
   dcDrawContext->SetPen(*wxBLACK_PEN);
   wxbDrawingBrush->SetColour(255, 255, 255);
   dcDrawContext->SetBrush(*wxbDrawingBrush);
   dcDrawContext->DrawRectangle(dWeightX, dWeightY, sWeight.size()*8 + 4, 18);

   dcDrawContext->DrawText(sWeight.c_str(),dWeightX+2, dWeightY+2);
}

template <class _TNodeNet>
void TDrawWxWidget<_TNodeNet>::DoDrawing(typename _TNodeNet::TNodeI& niANodeI, bool bEmphasizeNode){
   int iHShapeTranslation;       int iVShapeTranslation;
   int iNodeWidthDisplaySize;    int iNodeHeightDisplaySize;
   /*Size of the nodes in display coordinates*/
   int iMinimumSize = 5;

   /*Calculate positioning*/

   /*If the graph fits in the screen*/
   if((iDisplayWidth > wxcGraphWidth) || (iDisplayHeight > wxcGraphHeight)){
      iNodeWidthDisplaySize = niANodeI().GetSize()*wxcGraphWidth;
      iNodeHeightDisplaySize = niANodeI().GetSize()*wxcGraphHeight;
   }else{ /*The graph does not fit in the screen*/
      iNodeWidthDisplaySize = niANodeI().GetSize()*iDisplayWidth;
      iNodeHeightDisplaySize = niANodeI().GetSize()*iDisplayHeight;
   }
   if(bEmphasizeNode){
      iNodeWidthDisplaySize *= 1.6;
      iNodeHeightDisplaySize *= 1.6;
   }

   /*Force minimum size for drawing*/
   iNodeWidthDisplaySize = TUtil<int>::GetMax(iNodeWidthDisplaySize, iMinimumSize);
   iNodeHeightDisplaySize = TUtil<int>::GetMax(iNodeHeightDisplaySize, iMinimumSize);

   iHShapeTranslation = iNodeWidthDisplaySize/2;
   iVShapeTranslation = iNodeHeightDisplaySize/2;

   int iXPos = niANodeI().GetDeviceX();
   int iYPos = niANodeI().GetDeviceY();
  
   /*Drawing*/
   if(niANodeI().GetShapeType() == TShapeType::saSquare){
      dcDrawContext->DrawRectangle(iXPos - iHShapeTranslation,
         iYPos - iVShapeTranslation,
         iNodeWidthDisplaySize, iNodeHeightDisplaySize);
   }else if(niANodeI().GetShapeType() == TShapeType::saCircle){
      dcDrawContext->DrawEllipse(iXPos - iHShapeTranslation,
                                   iYPos - iVShapeTranslation, 
                                   iNodeWidthDisplaySize, iNodeHeightDisplaySize);
   }else if(niANodeI().GetShapeType() == TShapeType::saDiamond){
      wxPoint wxpPoints[] = {wxPoint(iXPos - iHShapeTranslation,iYPos),wxPoint(iXPos,iYPos - iVShapeTranslation),
                             wxPoint(iXPos + iHShapeTranslation,iYPos), wxPoint(iXPos, iYPos + iVShapeTranslation)};
      dcDrawContext->DrawPolygon(4, wxpPoints);
   }
   /*Labeling*/
   if(this->bShowAllLabels)
      DrawNodeLabel(niANodeI);
   
   //Code under investigation: this code has no use after 03/04
   //because it is depricated. Since then obseve for positional bugs
   /*Positional limits tracking - used by method ConvertDeviceToDrawingCoordinates*/
   if(this->pNodeNetData->GetNodes() == 1){
      fMinX = niANodeI().GetX() - 2*niANodeI().GetSize();
      fMaxX = niANodeI().GetX() + 2*niANodeI().GetSize();
      fMinY = niANodeI().GetY() - 2*niANodeI().GetSize();
      fMaxY = niANodeI().GetY() + 2*niANodeI().GetSize();
   }else{
      if(niANodeI().GetX() < fMinX) fMinX = niANodeI().GetX();
      if(niANodeI().GetX() > fMaxX) fMaxX = niANodeI().GetX();
      if(niANodeI().GetY() < fMinY) fMinY = niANodeI().GetY();
      if(niANodeI().GetY() > fMaxY) fMaxY = niANodeI().GetY();
   }
}
template <class _TNodeNet>
void TDrawWxWidget<_TNodeNet>::DrawNodeLabel(typename _TNodeNet::TNodeI& niANodeI){
   int iXPos = niANodeI().GetDeviceX();
   int iYPos = niANodeI().GetDeviceY();

   int iNodeWidthDisplaySize = niANodeI().GetSize()*wxcGraphWidth;
   int iHShapeTranslation = iNodeWidthDisplaySize/2;

   int iFirstNeighborId;
   typename _TNodeNet::TNodeI niFirstNeighbor = this->pNodeNetData->BegNI();
   std::string sLabel = niANodeI().GetLabel().c_str();
   dcDrawContext->SetBrush(*wxWHITE_BRUSH);
   if(this->pNodeNetData->GetFirstKnownNeighbor(niANodeI,iFirstNeighborId)){
      niFirstNeighbor = this->pNodeNetData->GetNI(iFirstNeighborId);
      /*Decide whether left or write of node*/
      wxBLACK_PEN->SetWidth(1);
      dcDrawContext->SetPen(*wxBLACK_PEN);
      if(niFirstNeighbor().GetDeviceX() > niANodeI().GetDeviceX()){
         dcDrawContext->DrawRectangle(iXPos + iHShapeTranslation - ((sLabel.size()+2) * 8) - 10,iYPos-8,((sLabel.size()+2) * 8)-4, 18);
         dcDrawContext->DrawText(sLabel.c_str(),iXPos + iHShapeTranslation - ((sLabel.size()+2) * 8) - 6, iYPos - 8);
      }else{
         dcDrawContext->DrawRectangle(iXPos + iHShapeTranslation, iYPos-8,((sLabel.size()+2) * 8) - 4, 18);
         dcDrawContext->DrawText(sLabel.c_str(),iXPos + iHShapeTranslation + 6, iYPos - 8);
      }
   }
}
/*------------------------------------------------------------------------------------------*/
template <class _TNodeNet>
wxMDIChildFrame* TDrawWxWidget<_TNodeNet>::DegreeDist(wxMDIParentFrame *wxmdipAParent, wxWindow *wxfACallingFrame){
   TGPlot::PlotDegDist(this->pNodeNetData, "Plot", "Temp", true, true, true, true, true, true, IMAGE_HORIZONTAL_RESOLUTION, IMAGE_VERTICAL_RESOLUTION);
   sifImageFrame = new TFrameShowImage(wxmdipAParent, "Degree distribution",50,50,200,200,wxfACallingFrame);
   sifImageFrame->ShowImage("dds.Plot.png");
   sifImageFrame->Restore();
   return sifImageFrame;   
};
template <class _TNodeNet>
wxMDIChildFrame* TDrawWxWidget<_TNodeNet>::CummulativeDegreeDist(wxMDIParentFrame *wxmdipAParent, wxWindow *wxfACallingFrame){
   TGPlot::PlotCumDegDist(this->pNodeNetData, "Plot", "Temp", true, true, false, true, IMAGE_HORIZONTAL_RESOLUTION, IMAGE_VERTICAL_RESOLUTION);
   sifImageFrame = new TFrameShowImage(wxmdipAParent, "Cumulative degree distribution",250,50,200,200,wxfACallingFrame);
   sifImageFrame->ShowImage("dds.Plot.png");
   sifImageFrame->Restore();
   return sifImageFrame;   
};
template <class _TNodeNet>
wxMDIChildFrame* TDrawWxWidget<_TNodeNet>::Hops(wxMDIParentFrame *wxmdipAParent, wxWindow *wxfACallingFrame){
   TGPlot::PlotHops(this->pNodeNetData, false, "Plot", "Temp", true, IMAGE_HORIZONTAL_RESOLUTION, IMAGE_VERTICAL_RESOLUTION);
   sifImageFrame = new TFrameShowImage(wxmdipAParent, "Hops",50,250,200,200,wxfACallingFrame);
   sifImageFrame->ShowImage("hop.Plot.png");
   sifImageFrame->Restore();
   return sifImageFrame;
};
template <class _TNodeNet>
wxMDIChildFrame* TDrawWxWidget<_TNodeNet>::SingValues(wxMDIParentFrame *wxmdipAParent, wxWindow *wxfACallingFrame){
/*      const int Vals = pNodeNetData->GetNodes()/2 > 500 ? 500 : pNodeNetData->GetNodes()/2;
   TGPlot::PlotSngVals(pNodeNetData, Vals, "Plot", "Temp", true, IMAGE_HORIZONTAL_RESOLUTION, IMAGE_VERTICAL_RESOLUTION);
   sifImageFrame = new TFrameShowImage(wxmdipAParent, "Singular values",250,250,200,200,wxfACallingFrame);
   sifImageFrame->ShowImage("sval.Plot.png");
   sifImageFrame->Restore();
   return sifImageFrame;
   /*  std::string stTemp = "\\Ju\\RMine\\data\\graph.bin";
   const PGraph Graph = TNGraph::Load(TFIn(TStr::Fmt(stTemp.c_str())));   
   */
   return NULL;
};

template <class _TNodeNet>
wxMDIChildFrame* TDrawWxWidget<_TNodeNet>::WeakComponents(wxMDIParentFrame *wxmdipAParent, wxWindow *wxfACallingFrame){
   TGPlot::PlotWccDist(this->pNodeNetData, "Plot", "Temp", true, IMAGE_HORIZONTAL_RESOLUTION, IMAGE_VERTICAL_RESOLUTION);
   sifImageFrame = new TFrameShowImage(wxmdipAParent, "Weak components distribution",50,450,200,200,wxfACallingFrame);
   sifImageFrame->ShowImage("wcc.Plot.png");
   sifImageFrame->Restore();
   return sifImageFrame;
};

template <class _TNodeNet>
wxMDIChildFrame* TDrawWxWidget<_TNodeNet>::StrongComponents(wxMDIParentFrame *wxmdipAParent, wxWindow *wxfACallingFrame){
   TGPlot::PlotSccDist(this->pNodeNetData, "Plot", "Temp", true, IMAGE_HORIZONTAL_RESOLUTION, IMAGE_VERTICAL_RESOLUTION);
   sifImageFrame = new TFrameShowImage(wxmdipAParent, "Strong components distribution",250,450,200,200,wxfACallingFrame);
   sifImageFrame->ShowImage("scc.Plot.png");
   sifImageFrame->Restore();
   return sifImageFrame;
};
/*------------------------------------------------------------------------------------------*/
template <class _TNodeNet>
void TDrawWxWidget<_TNodeNet>::SetNodePosition(typename _TNodeNet::TNodeI& niANode, const int iX, const int iY){
   double dXPos = iX; double dYPos = iY;
   /*The new position may be invalid*/
   if(this->ConvertDeviceToDrawingCoordinates(dXPos, dYPos)){
      niANode().SetX(dXPos);
      niANode().SetY(dYPos);
      niANode().SetDeviceX(iX);
      niANode().SetDeviceY(iY);
   }
}
template <class _TNodeNet>
void TDrawWxWidget<_TNodeNet>::UpadteGraphDeviceCoordinates(){
   double dXPos, dYPos;
   for(typename _TNodeNet::TNodeI NI = this->pNodeNetData->BegNI(); NI < this->pNodeNetData->EndNI(); NI++){     
      dXPos = NI().GetX();
      dYPos = NI().GetY();
      this->ConvertDrawingToDeviceCoordinates(dXPos, dYPos);
      NI().SetDeviceX(dXPos);
      NI().SetDeviceY(dYPos);
   }
}
#endif







