#pragma once
#ifndef _DrawInterface_
#define _DrawInterface_

#include <wx/cmndata.h>

#include <Commons/Auxiliar.h>
#include <Layouts/LayoutInterface.h>

template <class _TNodeNet>
DeclareInterface(TDrawInterface) {
protected:
   int iExibitionLevel;
   time_t tLowTimeLimit;
   TPt<_TNodeNet> pNodeNetData;
   wxWindow *fFrameToDraw;
public:
   bool bShowAllLabels;                   bool bShowWeights;
   virtual void SetFrame(wxWindow *fAFrame) = 0;
   virtual bool IdentifyNode(const int iX, const int iY, typename _TNodeNet::TNodeI& niANode) = 0;
   virtual void SetNodePosition(typename _TNodeNet::TNodeI& niANode, const int iX, const int iY) = 0;
   virtual void SetWidthAndHeight(wxCoord wxcAWidth,wxCoord wxcAHeight) = 0;
   virtual void ConvertDrawingToDeviceCoordinates(double& fDrawingToDeviceX, double& fDrawingToDeviceY) = 0;
   virtual bool ConvertDeviceToDrawingCoordinates(double& dX, double& dY) = 0;   
   virtual void SetAbsoluteScale(double fWidthScaleFactor,double fHeightScaleFactor) = 0;
   virtual void SetRelativeScale(double fWidthScaleFactor,double fHeightScaleFactor,
                         int iReferenceX, int iReferenceY) = 0;
   virtual void SetAbsoluteTranslation(int iAHorizontalTranslation, int iAVerticalTranslation) = 0;
   virtual void SetTranslation(int iWidthScaleFactor,int iHeightScaleFactor) = 0;   
   virtual void UpdateDisplaySize() = 0;
   virtual void ApplyLayout(TLayoutInterface<_TNodeNet> &gliAGraphLayout) = 0;
   virtual void DrawGraph(void *vDrawContext) = 0;
   virtual void DrawNodes() = 0;
   virtual void DrawInfluenceNode(typename _TNodeNet::TNodeI& niANodeI, bool bEmphasizeNode = false) = 0;
   virtual void DrawNode(typename _TNodeNet::TNodeI& niANodeI, bool bEmphasizeNode = false) = 0;
   virtual void DrawEdges() = 0;
   virtual void DrawEdge(typename _TNodeNet::TEdgeI &eAEdge) = 0;
   virtual bool SetShapeForEmphasizedNode(TShapeType::TShapeAcronysm npsAShapeType) = 0;
   virtual bool SetSizeForEmphasizedNode(double dASize) = 0;
   virtual double GetSizeForEmphasizedNode() = 0;
   virtual bool SetLabelForEmphasizedNode(std::string sALabel) = 0;
   virtual std::string GetLabelFromEmphasizedNode() = 0;
   virtual void SetShowNeighborHoodDetails(bool bOptionDrawNodeDetailedDataInNeighborHood,
                                           bool bOptionShowLabelsInNeighborHoodDetails,
                                           typename _TNodeNet::TNodeI aNodeInShowLabelsInNeighborHoodDetails) = 0;
   virtual void DrawNodeDetailedData(typename _TNodeNet::TNodeI& niANodeI) = 0;
   virtual void DrawNodeLabel(typename _TNodeNet::TNodeI& niANodeI) = 0;
   virtual void DrawNeighborhoodDetails() = 0;
   virtual int GetExibitionLevel(){ return iExibitionLevel;};
   virtual TPt<_TNodeNet>& GetPointerToData(){ return pNodeNetData; };
   virtual void SetExibitionLevel(int iAExibitionLevel){ iExibitionLevel = iAExibitionLevel;};
   virtual void SetLowTimeLimit(int iALowTimeLimit){ tLowTimeLimit = iALowTimeLimit; };
   virtual bool SetColorForEmphasizedNode(wxColour& wxcAColour) = 0;
   virtual wxMDIChildFrame* DegreeDist(wxMDIParentFrame *wxmdipAParent, wxWindow *wxfACallingFrame) = 0;
   virtual wxMDIChildFrame* CummulativeDegreeDist(wxMDIParentFrame *wxmdipAParent, wxWindow *wxfACallingFrame) = 0;
   virtual wxMDIChildFrame* Hops(wxMDIParentFrame *wxmdipAParent, wxWindow *wxfACallingFrame) = 0;
   virtual wxMDIChildFrame* SingValues(wxMDIParentFrame *wxmdipAParent, wxWindow *wxfACallingFrame) = 0;
   virtual wxMDIChildFrame* WeakComponents(wxMDIParentFrame *wxmdipAParent, wxWindow *wxfACallingFrame) = 0;
   virtual wxMDIChildFrame* StrongComponents(wxMDIParentFrame *wxmdipAParent, wxWindow *wxfACallingFrame) = 0;
   virtual void UpadteGraphDeviceCoordinates() = 0;
EndInterface

#endif