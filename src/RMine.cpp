#include "RMine.h"

#include "Frames/FrameRMine.h"
#include <DebugWindow.h>
#include <wx/splash.h>
#include <wx/msgdlg.h>

// Create a new application object: this macro will allow wxWidgets to create
// the application object during program execution (it's better than using a
// static object for many reasons) and also implements the accessor function
// wxGetApp() which will return the reference of the right type (i.e. MyApp and
// not wxApp)
IMPLEMENT_APP(RMine)

// ----------------------------------------------------------------------------
// the application class
// ----------------------------------------------------------------------------

bool ErrorDialog(const char* errorDetails = NULL) {
	std::string message = "A fatal error ocurred, aborting.";
	if (errorDetails) {
		message += "\nDetails:\"";
		message += errorDetails;
		message += "\"";
	}
#ifndef _DEBUG
	wxMessageDialog(NULL, message).ShowModal();
#endif
	return false;
}

bool ErrorDialog(char* msg) {
	return ErrorDialog(const_cast<const char*>(msg));
}

void ErrorDialog(const TStr& msg) {
	ErrorDialog(msg.CStr());
}

// 'Main program' equivalent: the program execution "starts" here
bool RMine::OnInit() {
	rmine::wx::Initialize();
	TOnExeStop::PutOnExeStopF(ErrorDialog);
	TExcept::PutOnExceptF(ErrorDialog);
	
	// Show splash screen
	int iTimeOut = 1200;
	wxBitmap bmSplashImage;
	if (bmSplashImage.LoadFile("assets/Splash.bmp", wxBITMAP_TYPE_BMP)) {
		wxSplashScreen spSplash(bmSplashImage,
				wxSPLASH_CENTRE_ON_SCREEN|wxSPLASH_TIMEOUT,
				iTimeOut, NULL, -1, wxDefaultPosition, wxDefaultSize,
				wxSIMPLE_BORDER|wxSTAY_ON_TOP);
		wxYield();
		wxMilliSleep(iTimeOut);
	}
	
	// create the main application window
	wxWindow *main = new TFrameRMine("RMine Interface");
	SetTopWindow(main);

	// and show it (the frames, unlike simple controls, are not shown when
	// created initially)
	GetTopWindow()->Show();
	
	// success: wxApp::OnRun() will be called which will enter the main message
	// loop and the application will run. If we returned false here, the
	// application would exit immediately.
	return true;
}

int RMine::OnRun() {
	try {
		return wxApp::OnRun();
	} catch (...) {
		ErrorDialog();
		return -1;
	}
}