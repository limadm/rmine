#pragma once
#include <wx/wx.h>

class DebugWindow : public wxFrame {
public:
	DebugWindow(wxWindow *parent);
	void Clear();
	wxTextCtrl text;
	
	DebugWindow & operator << (const wxString &);
	DebugWindow & operator << (char);
	DebugWindow & operator << (long);
	DebugWindow & operator << (unsigned long);
	DebugWindow & operator << (double);
};

DebugWindow& debug();