#include <DebugWindow.h>
#include <Commons/Auxiliar.h>

DebugWindow::DebugWindow(wxWindow *parent) :
	wxFrame(parent, wxID_ANY, "Debug window", wxPoint(0,0), wxSize(300,200),
		wxFRAME_TOOL_WINDOW | wxRESIZE_BORDER | wxCAPTION | wxCLIP_CHILDREN | wxFRAME_FLOAT_ON_PARENT),
	text(this, wxID_ANY, "", wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE)
{
}

void DebugWindow::Clear()
{
	text.SetValue("");
}

DebugWindow & DebugWindow::operator << (const wxString &s)
{
	text.SetValue(text.GetValue() + s);
	text.SetInsertionPointEnd();
	return *this;
}

DebugWindow & DebugWindow::operator << (char x)
{
	return (*this) << TConverter<char>::ToString(x);
}
DebugWindow & DebugWindow::operator << (long x)
{
	return (*this) << TConverter<long>::ToString(x);
}
DebugWindow & DebugWindow::operator << (unsigned long x)
{
	return (*this) << TConverter<unsigned long>::ToString(x);
}
DebugWindow & DebugWindow::operator << (double x)
{
	return (*this) << TConverter<double>::ToString(x);
}

DebugWindow & debug()
{
	static DebugWindow *x = 0;
	if (!x) {
		x = new DebugWindow(NULL);
	}
	x->Show();
	return *x;
}