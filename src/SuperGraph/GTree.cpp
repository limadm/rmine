#ifdef _SUPERGRAPHCOMPILATION

#include "GTree.h"

#include <time.h>

#include <wx/dir.h>
#include <wx/filename.h>
#include <wx/progdlg.h>

#include <Commons/Storage.h>
#include <SuperGraph/DrawSuperGraph.h>

using namespace rmine;
using namespace rmine::disk;

/*---------------------------------------------------------------------------------*/
TGTree::TGTree(std::string sATreeWorkingDirectory)
	: sTreeWorkingDirectory(sATreeWorkingDirectory),
	  sTreeSaveDirectory(sTreeWorkingDirectory + wxFileName::GetPathSeparator() + "TreeBuild"),
	  iTotalNodes(0),
	  iTotalEdges(0),
	  iTotalLevels(0),
	  storage(NULL)
{};
TGTree::~TGTree()
{
	gtnRoot.clear();
	if (storage != NULL) {
		delete storage;
	}
	hmIdIndex.clear();
	lLabelIndex.clear();
	sSetOfSuperNodes.clear();
};
TGTreeSuperNodeEntry* TGTree::GetRoot()
{
	return &(*gtnRoot.begin());
};
void TGTree::ExpandAllParents(TGTreeSuperNodeEntry* gtsneASuperNode)
{
	if(gtsneASuperNode == NULL) {
		return;
	}
	gtsneASuperNode->SetExpanded(true);
	TGTreeSuperNodeEntry* gtsneTemp = gtsneASuperNode->gtsneParent;
	while(gtsneTemp != NULL) {
		gtsneTemp->SetExpanded(true);
		gtsneTemp = gtsneTemp->gtsneParent;
	}
}

void TGTree::AddSuperNodeLabelToSetOfLabels(std::string sALabel)
{
	if(sALabel.compare("") == 0) {
		return;
	}
	sSetOfSuperNodes.insert(sALabel);
}

TGTreeSuperNodeEntry* TGTree::FindSuperNodeEntry(std::string sId)
{
	return FindSuperNodeEntryRecursive(&gtnRoot, sId);
}

TGTreeSuperNodeEntry* TGTree::FindSuperNodeEntryRecursive(TGTreeNode* gtneBaseNode, std::string sId)
{
	if(gtneBaseNode == NULL) {
		return NULL;
	}
	TGTreeSuperNodeEntry* gtneReturnValue = NULL;
	gtneReturnValue = gtneBaseNode->ScanEntries(sId);
	if(gtneReturnValue != NULL) {
		return gtneReturnValue;
	}

	std::list<TGTreeSuperNodeEntry>::iterator iIterator;
	for(iIterator = gtneBaseNode->begin(); iIterator != gtneBaseNode->end(); ++iIterator) {
		gtneReturnValue = FindSuperNodeEntryRecursive(&iIterator->gtnSubTree, sId);
		if(gtneReturnValue != NULL) {
			return gtneReturnValue;
		}
	}
	return NULL;
};
/*---------------------------------------------------------------------------------*/
void TGTree::PrintTree()
{
	this->PrintTreeRecursive(&this->gtnRoot);
}
void TGTree::PrintTreeRecursive(TGTreeNode* gtneBaseNode)
{
	gtneBaseNode->PrintTreeNode();
	std::list<TGTreeSuperNodeEntry>::iterator iIterator;
	for(iIterator = gtneBaseNode->begin(); iIterator != gtneBaseNode->end(); ++iIterator) {
		if(iIterator->gtnSubTree.size() > 0) {
			PrintTreeRecursive(&iIterator->gtnSubTree);
		}
	}
}
/*---------------------------------------------------------------------------------*/
void TGTree::ApplyTreeMap()
{
	this->ApplyTreeMapRecursive(&this->gtnRoot, false, 1.0, 1.0, 0.0, 0.0);
}
void TGTree::ApplyTreeMapRecursive(TGTreeNode* gtneBaseNode, bool bHorizontalSplit,
                                   double dHorizontalSizeToShare, double dVerticalSizeToShare,
                                   double dInitialX, double dInitialY)
{
	double dSonsDiameterShare;
	double dNextXPos = dInitialX;
	double dNextYPos = dInitialY;

	if(bHorizontalSplit) {
		dSonsDiameterShare = dHorizontalSizeToShare/(double)gtneBaseNode->size();
	} else {
		dSonsDiameterShare = dVerticalSizeToShare/(double)gtneBaseNode->size();
	}
	std::list<TGTreeSuperNodeEntry>::iterator iIterator;
	for(iIterator = gtneBaseNode->begin(); iIterator != gtneBaseNode->end(); ++iIterator) {
		if(bHorizontalSplit) {
			iIterator->dHorizontalSize = dSonsDiameterShare;
			iIterator->dVerticalSize = dVerticalSizeToShare;
			iIterator->dX = dNextXPos;
			iIterator->dY = dNextYPos;
			dNextXPos += dSonsDiameterShare;
		} else {
			iIterator->dHorizontalSize = dHorizontalSizeToShare;
			iIterator->dVerticalSize = dSonsDiameterShare;
			iIterator->dX = dNextXPos;
			iIterator->dY = dNextYPos;
			dNextYPos += dSonsDiameterShare;
		}
		if(iIterator->gtnSubTree.size() > 0)
			this->ApplyTreeMapRecursive(&iIterator->gtnSubTree, !bHorizontalSplit,
			                            iIterator->dHorizontalSize, iIterator->dVerticalSize,
			                            iIterator->dX, iIterator->dY);
	}
}

void TGTree::TrackNodeForeignNeighbors(int iANodeId,
                                       TGTreeSuperNodeEntry* gtsneASuperNode,
                                       wxArrayString& wxasArrayOfForeingNodesLabels)
{
	/*	clock_t start, end;
		double dElapsedTime;
		start = clock();
	*/
	if(gtsneASuperNode->gtsneParent == NULL) { /*Root has no parent*/
		return;
	}
	std::list<int>::iterator lIterator;
	std::list<int> lListOfForeignNeighborsIds;
	TGTreeSuperNodeEntry* gtsneANodeParent = gtsneASuperNode;
	/*Way up the tree - find all neighbors ids*/
	while(gtsneANodeParent->IsOpenNode(iANodeId)) {
		gtsneANodeParent->gtsneParent->TrackNodeSuperEdgesNeighbors(lListOfForeignNeighborsIds, iANodeId, gtsneANodeParent);
		gtsneANodeParent = gtsneANodeParent->gtsneParent;
	}
	/*Find the correspondent labels*/
#ifndef PERFORMANCE
	for(lIterator = lListOfForeignNeighborsIds.begin(); lIterator != lListOfForeignNeighborsIds.end(); lIterator++) {
		wxasArrayOfForeingNodesLabels.Add(this->GetNodeLabel(*lIterator).c_str());
	}
#endif
	/*	end = clock();
		dElapsedTime = (double)(end - start)/ (double)CLOCKS_PER_SEC;
		TDialogWarning dwDoneWarning("Elapsed time = " + TConverter<double>::ToString(dElapsedTime));
	*/
}
/*---------------------------------------------------------------------------------*/
void TGTree::ApplyDistributionLayout()
{
	/* toss off
		std::list<TGTreeSuperNodeEntry>::iterator iIterator = this->gtnRoot.begin();
		iIterator->dX = 0;	 iIterator->dY = 0;
		iIterator->dHorizontalSize = 1.0;
		iIterator->dVerticalSize = 1.0;
		*/
	this->ApplyDistributionLayoutRecursive(this->GetRoot());
}
void TGTree::ApplyDistributionLayoutRecursive(TGTreeSuperNodeEntry* gsnGraphSuperNode)
{
	/*Center of parent supernode's ellipse*/
	double dParentXCenter = gsnGraphSuperNode->dX;
	double dParentYCenter = gsnGraphSuperNode->dY;
	/*Radius (axis) of supernode's ellipse*/
	double dParentHorizontalRadius = gsnGraphSuperNode->dHorizontalSize/2.0;
	double dParentVerticalRadius = gsnGraphSuperNode->dVerticalSize/2.0;

	double dXRadiusExtreme;
	double dYRadiusExtreme;
	double dSonAngle;
	double dSonRadius;
	double dRadiusAtAngle;

	/*Biggest son cardinality (biggest son magnitude)*/
	int iBiggestNumberOfGrandSons = GetBiggestNumberOfGrandSons(gsnGraphSuperNode);

	/*Size of the biggest son supernode - all the others will be proportionally smaller*/
	//double dSonRatioSize = iBiggestNumberOfGrandSons/(double)gsnGraphSuperNode->iTotalNumberOfGraphNodesBeneath;
	//double dHorizontalBiggestSize = dSonRatioSize * gsnGraphSuperNode->dHorizontalSize;
	//double dVerticalBiggestSize = dSonRatioSize * gsnGraphSuperNode->dVerticalSize;

	double dMinSizeRatio = std::min(0.2, 1.0 / gsnGraphSuperNode->gtnSubTree.size());
	double dMaxSizeRatio = std::min(0.6, iBiggestNumberOfGrandSons/(double)gsnGraphSuperNode->iTotalNumberOfGraphNodesBeneath);

	double dSonSizeRatio;

	double dSonXCenter;
	double dSonYCenter;

	/*Angle share for each son supernodes*/
	double dAngleShare = _2PI/(double)gsnGraphSuperNode->gtnSubTree.size();
	double dAcumulatedAngle = 0;
	/*Go through all sons (supernodes)*/
	std::list<TGTreeSuperNodeEntry>::iterator iIterator;
	for(iIterator = gsnGraphSuperNode->gtnSubTree.begin(); iIterator != gsnGraphSuperNode->gtnSubTree.end(); ++iIterator) {
		/*We are gonna use elliptical coordinates*/
		/*We need the pair (radius, angle) to detemine the position of the center of this son's ellipse*/

		/*First, the angle*/
		dSonAngle = dAcumulatedAngle;
		dAcumulatedAngle += dAngleShare;	/*Next son's angle*/

		/*Then the radius - a little more complicated*/

		/*Size (horizontal and vertical) of this son. Proportional to the biggest son supernode.*/
		dSonSizeRatio = std::max(dMinSizeRatio, std::min(dMaxSizeRatio, iIterator->iTotalNumberOfGraphNodesBeneath / (double) gsnGraphSuperNode->iTotalNumberOfGraphNodesBeneath));
		iIterator->dHorizontalSize = dSonSizeRatio * gsnGraphSuperNode->dHorizontalSize;
		iIterator->dVerticalSize   = dSonSizeRatio * gsnGraphSuperNode->dVerticalSize;

		/*Calculate the size of the radius at dSonAngle - the radius varies, as it is an ellipse*/
		/*Point on the ellipse at dSonAngle*/
		dXRadiusExtreme = dParentHorizontalRadius * cos(dSonAngle);
		dYRadiusExtreme = dParentVerticalRadius * sin(dSonAngle);
		/*Size of the radius at dSonAngle*/
		dRadiusAtAngle = sqrt( pow(dXRadiusExtreme, 2) + pow(dYRadiusExtreme, 2) );

		/*This son's radius = (parent's radius at dSonAngle) - (max(son's,hzise, son's vsize)/2) */
		dSonRadius = (dRadiusAtAngle - TUtil<double>::GetMax(iIterator->dHorizontalSize, iIterator->dVerticalSize)/2.0)*0.95;

		/*Now put in a format that wxwidgets can use and tranlate it to window (not device) coordinates*/
		/*This son's ellipse center*/
		dSonXCenter = dSonRadius * cos(dSonAngle);
		dSonYCenter = dSonRadius * sin(dSonAngle);
		/*This son's x, y positions (top left) used by wxwidgets to draw the ellipse*/
		iIterator->dX = dSonXCenter - iIterator->dHorizontalSize/2.0;
		iIterator->dY = dSonYCenter - iIterator->dVerticalSize/2.0;
		/*Tranlation*/
		iIterator->dX += gsnGraphSuperNode->dX + dParentHorizontalRadius;
		iIterator->dY += gsnGraphSuperNode->dY + dParentVerticalRadius;

		/*Recursion*/
		if(!iIterator->bIsLeaf) {
			ApplyDistributionLayoutRecursive(&(*iIterator));
		}
	}
}
/*---------------------------------------------------------------------------------*/
int TGTree::GetNumberOfPartitionsPerLevel()
{
	/*We assume all the level have the same number of partitions*/
	return this->GetRoot()->gtnSubTree.size();
}
int TGTree::GetBiggestNumberOfGrandSons(TGTreeSuperNodeEntry* gsnGraphSuperNode)
{
	std::list<TGTreeSuperNodeEntry>::iterator iIterator;
	int iBiggestNumberOfGrandSons = 0;
	for(iIterator = gsnGraphSuperNode->gtnSubTree.begin(); iIterator != gsnGraphSuperNode->gtnSubTree.end(); ++iIterator) {
		int iTemp = iIterator->iTotalNumberOfGraphNodesBeneath;
		if(iTemp > iBiggestNumberOfGrandSons) {
			iBiggestNumberOfGrandSons = iTemp;
		}
	}
	return iBiggestNumberOfGrandSons;
}

std::string TGTree::GetNodeLabel(int iANodeId)
{
#ifndef PERFORMANCE
	return hmIdIndex.find(iANodeId)->second.GetLabel();
#else
	return "";
#endif
}

#ifndef PERFORMANCE
void TGTree::FillNodeNetWithLabelData(TPt< TNodeNetTests<TNodePlot, TEdgePlot> > pANodeNetData)
{
	pANodeNetData->FillNodeNetWithLabelDataFromHashMap(hmIdIndex);
#else
void TGTree::FillNodeNetWithLabelData(TPt< TNodeNetTests<TNodePerformance, TEdgePlot> > pANodeNetData)
{
#endif
}

void TGTree::BuildLabelIndex()
{
#ifndef PERFORMANCE
	std::hash_map<int, TNodesAuxiliar>::iterator iHashIterator;
	for(iHashIterator = hmIdIndex.begin(); iHashIterator != hmIdIndex.end(); iHashIterator++) {
		lLabelIndex.push_back(std::pair<std::string, int>(iHashIterator->second.GetLabel(),iHashIterator->first));
	}
	/*Sort in alphabetical order*/
	lLabelIndex.sort( sortByStringField< std::pair<std::string, int> >() );
#endif
}

TGTreeSuperNodeEntry* TGTree::FindNodeSuperNodeEntry(std::string sANodeLabel)
{
	int iTheNodeId = GetNodeIdFromNodeLabel(sANodeLabel);
	if(iTheNodeId == -1) {
		return NULL;
	} else {
		return FindNodeSuperNodeEntry(iTheNodeId);
	}
}
TGTreeSuperNodeEntry* TGTree::FindNodeSuperNodeEntry(int iANodeId)
{
	std::hash_map<int, TNodesAuxiliar>::iterator iHashIterator;
	iHashIterator = hmIdIndex.find(iANodeId);
	if(hmIdIndex.find(iANodeId) == hmIdIndex.end()) {
		return NULL;  /*id non existent*/
	} else {
		return (TGTreeSuperNodeEntry*)iHashIterator->second.GetSuperNodeEntry();
	}
}
int TGTree::GetNodeIdFromNodeLabel(std::string sANodeLabel, int iNthOccurrence)
{
	int iTheNodeId = -1, iOccurrencesCounter = 0;

	/*this->lLabelIndex is a list of pairs (label, id) -- (first, second)*/
	/*here we declare an iterator for this->lLabelIndex */
	std::list< std::pair<std::string, int> >::iterator iListIterator;

	wxString sTemp1, sTemp2(sANodeLabel.c_str());
	/*Sequential search for sANodeLabel*/
	for(iListIterator = this->lLabelIndex.begin(); iListIterator != this->lLabelIndex.end(); ++iListIterator) {
		sTemp1 = (iListIterator->first).c_str();
		/*We try a substring match between items in the list and the search item*/
		if(sTemp1.Upper().Find(sTemp2.Upper()) != -1) {
			iOccurrencesCounter++;
			if(iOccurrencesCounter == iNthOccurrence) {
				iTheNodeId = iListIterator->second;
				break;
			}
		}
	}
	return iTheNodeId;
}
int TGTree::GetTotalNodes()
{
	return iTotalNodes;
}
std::string TGTree::GetIthNodeLabelFromParallelLabelIndex(int iAIndex)
{
	std::list< std::pair<std::string, int> >::iterator iListIterator = lLabelIndex.begin();
	for(int iCounter = 0; iCounter < iAIndex; iCounter++) {
		iListIterator++;
	}
	return iListIterator->first;
}

/*---------------------------------------------------------------------------------*/
TGTreeSuperNodeEntry* TGTree::FindSuperNodeEntry(double dAX, double dAY)
{
	return this->FindSuperNodeEntryRecursive(this->GetRoot(), dAX, dAY);
};
TGTreeSuperNodeEntry* TGTree::FindSuperNodeEntryRecursive(TGTreeSuperNodeEntry* gtneBaseNode, double dAX, double dAY)
{
	if(gtneBaseNode->IsPointOver(dAX, dAY)) {
		/*Check for exact match, ends recursion*/
		if(gtneBaseNode->bIsLeaf || !gtneBaseNode->IsExpanded()) { /*To understand, put in a logic table*/
			return gtneBaseNode;
		}
		/*If a son is focused, skip over other son nodes*/
		if(gtneBaseNode->gtsneFocusedNode != NULL) {
			return FindSuperNodeEntryRecursive(gtneBaseNode->gtsneFocusedNode, dAX, dAY);
		}
		/*Recursion*/
		std::list<TGTreeSuperNodeEntry>::iterator iIterator;
		for(iIterator = gtneBaseNode->gtnSubTree.begin(); iIterator != gtneBaseNode->gtnSubTree.end(); ++iIterator) {
			TGTreeSuperNodeEntry* n = FindSuperNodeEntryRecursive(&(*iIterator), dAX, dAY);
			if (n != NULL) {
				return n;
			}
		}
		/*In case we reach this point, we didn't find an exact match for dAX, dAY*/
		/*We return the best match, the last SuperNode that contains dAX, dAY*/
		return gtneBaseNode;
	}
	return NULL;
};
/*---------------------------------------------------------------------------------*/
void TGTree::AddHierarchyLine(std::string sALine)
{
	TGTreeSuperNodeEntry* gtneParentNode;
	char cGraphNodes[500];
	char *cNextGraphNode = NULL;
	strcpy(cGraphNodes, sALine.c_str());
	/*The first string identifies the parent graphnode*/
	cNextGraphNode = strtok(cGraphNodes," \t\n\r");	/*Tokenizer, get next graphnode*/
	/*Search for parent node*/
	gtneParentNode = this->FindSuperNodeEntry(cNextGraphNode);
	if(gtneParentNode == NULL) {					 /*Didn't find it, insert into the root*/
		AddSuperNodeLabelToSetOfLabels(this->gtnRoot.AddSuperNodeEntry(cNextGraphNode, NULL));
		/*Retrieve parent node*/
		gtneParentNode = this->FindSuperNodeEntry(cNextGraphNode);
	}
	cNextGraphNode = strtok(NULL," \t\n\r");
	std::string sTemp = cNextGraphNode;
	/*Non-leaf*/
	if(sTemp.find(".IN.part.",0) == -1) {
		while(cNextGraphNode != NULL) {
			AddSuperNodeLabelToSetOfLabels(gtneParentNode->AddTreeNodeEntry(cNextGraphNode));
			cNextGraphNode = strtok(NULL," \t\n\r");
		}
	} else {	/*Leaf*/
		std::string sSubGraphEntirePath = this->sTreeWorkingDirectory + wxFileName::GetPathSeparator() + sTemp;
		gtneParentNode->SetAsLeafNode(sSubGraphEntirePath);
		gtneParentNode->SetExpanded(false);
	}
};
/*---------------------------------------------------------------------------------*/
bool TGTree::BuildTreeFromPartitionDirectory()
{
	std::list<std::string> lListOfSuperNodeIds;
	std::list<std::string> lListOfFiles;
	int iValidFilesCounter = 0;
	GetFilesFromDir(sTreeWorkingDirectory, &lListOfFiles);
	if(lListOfFiles.size() < 2) {
		TDialogWarning dwDoneWarning("Error: partition files not found, please check your partition data.");
		return false;
	}

	std::list<std::string>::iterator iLIterator;
	std::list<std::string>::iterator iLIteratorIds;
	std::string sFileName;

	for(iLIterator = lListOfFiles.begin(); iLIterator != lListOfFiles.end(); iLIterator++) {
		sFileName = (*iLIterator);
		/*Look for partition file name pattern*/
		if((sFileName.find(".IN.part.", 0) != -1) && (sFileName.find("_ef.txt", 0) != -1)) {
			GetPathToTreeLeafNodeFromFileName((*iLIterator), &lListOfSuperNodeIds);
			for(iLIteratorIds = lListOfSuperNodeIds.begin(); iLIteratorIds != lListOfSuperNodeIds.end(); iLIteratorIds++) {
				this->AddHierarchyLine((*(iLIteratorIds)));
			}
			iValidFilesCounter++;
		}
	}
	if(iValidFilesCounter > 0) {
		/*Data successfully loaded, now other preparations*/
		this->SetLevelsForTreeNodes();
#ifndef PERFORMANCE
		this->SetAllSuperNodesColor();
		//this->PrepareTreeData();
#endif
		this->PrepareSuperGraphData();
		//this->ReleaseAllSubgraphData();
		return true;
	}
	return false;
};
/*---------------------------------------------------------------------------------*/
/*This function has been integrated to PrepareSuperGraphData()
  It is being kept only temporarily*/
void TGTree::PrepareTreeData()
{
	this->GetRoot()->iTotalNumberOfGraphNodesBeneath = PrepareTreeDataRecursive(this->GetRoot());
	this->BuildLabelIndex();
};
int TGTree::PrepareTreeDataRecursive(TGTreeSuperNodeEntry* gsnGraphSuperNode)
{
	int iCumulativeSumOfSons = 0;
	std::list<TGTreeSuperNodeEntry>::iterator iIterator;
	for(iIterator = gsnGraphSuperNode->gtnSubTree.begin(); iIterator != gsnGraphSuperNode->gtnSubTree.end(); ++iIterator) {
		if(iIterator->bIsLeaf) {
			if(iIterator->pNodeNetSubGraph.GetRefs() <= 0) {
				iIterator->LoadGraphTextFile();
				iIterator->pNodeNetSubGraph->CopyLabelDataToHashMap(hmIdIndex, (void *)(&(*iIterator)));
			}
			iCumulativeSumOfSons += iIterator->iTotalNumberOfGraphNodesBeneath;
		} else {
			iIterator->iTotalNumberOfGraphNodesBeneath = PrepareTreeDataRecursive(&(*iIterator));
			iCumulativeSumOfSons += iIterator->iTotalNumberOfGraphNodesBeneath;
		}
	}
	return iCumulativeSumOfSons;
};
/*---------------------------------------------------------------------------------*/
void TGTree::PrepareSuperGraphData()
{
	if((iTotalNodes == 0) && (iTotalEdges == 0)) {
		this->GetRoot()->iTotalNumberOfGraphNodesBeneath = PrepareSuperGraphDataRecursive(this->GetRoot());
	}
#ifndef PERFORMANCE
	this->BuildLabelIndex();
	LoadAllSuperNodesProperties();
#endif
	TDialogWarning dwDoneWarning("Super Graph created: " + TConverter<int>::ToString(iTotalNodes) + " nodes and " + TConverter<int>::ToString(iTotalEdges) + " edges");
};
int TGTree::PrepareSuperGraphDataRecursive(TGTreeSuperNodeEntry* gsnGraphSuperNode)
{
	int iCumulativeSumOfSons = 0;
	std::list< TPt<TGTreeSuperEdge> >::iterator iSuperEdges;
	std::list<TGTreeSuperNodeEntry>::iterator iIterator;
	for(iIterator = gsnGraphSuperNode->gtnSubTree.begin(); iIterator != gsnGraphSuperNode->gtnSubTree.end(); ++iIterator) {
		if(iIterator->bIsLeaf) {
			/*Internal connections*/
			if(iIterator->pNodeNetSubGraph.GetRefs() <= 0) {
				iIterator->LoadGraphTextFile();
			}

			/*External connections - not persistent (we consume this data during tree build)*/
			iIterator->LoadExternalConnectionsFile();

			iIterator->pNodeNetSubGraph->CopyLabelDataToHashMap(hmIdIndex, (void *)(&(*iIterator)));

			iCumulativeSumOfSons += iIterator->iTotalNumberOfGraphNodesBeneath;
			/*We compute internal nodes and internal edges (resolved by definition)*/
			iTotalNodes += iIterator->pNodeNetSubGraph->GetNodes();
			iTotalEdges += iIterator->pNodeNetSubGraph->GetEdges();
		} else {
			iIterator->iTotalNumberOfGraphNodesBeneath = PrepareSuperGraphDataRecursive(&(*iIterator));
			iCumulativeSumOfSons += iIterator->iTotalNumberOfGraphNodesBeneath;
		}
	}
	gsnGraphSuperNode->ResolveOpenNodesFromSons();
	/*Release the subgraph of correspondent LeafSuperNodes (if any)*/
	/*if(gsnGraphSuperNode->gtnSubTree.begin()->bIsLeaf){
		for(iIterator = gsnGraphSuperNode->gtnSubTree.begin(); iIterator != gsnGraphSuperNode->gtnSubTree.end(); ++iIterator)
			iIterator->ReleaseSubGraph(false);
	}*/
	/*Here we compute external edges that were resolved (all else)*/
	for(iSuperEdges = gsnGraphSuperNode->lListSuperEdges.begin(); iSuperEdges != gsnGraphSuperNode->lListSuperEdges.end(); ++iSuperEdges) {
		iTotalEdges += (*iSuperEdges)->GetWeight();
	}
	return iCumulativeSumOfSons;
};
/*----------------------------------------------------------------------*/
void TGTree::SaveTreeBuildToDir()
{
	CpuTimeLog log("GTree::SaveToDir");
	EmptyAndRemoveDirectory(this->sTreeSaveDirectory);
	CreateDirectory(this->sTreeSaveDirectory.c_str(), NULL);

	std::string sTreeFile = this->sTreeSaveDirectory + wxFileName::GetPathSeparator() + "Tree";
	TFInOut fioTreeFile(sTreeFile.c_str(), faCreate, true);
	TInt tiTotalNodes = iTotalNodes;
	tiTotalNodes.Save(fioTreeFile);
	TInt tiTotalEdges = iTotalEdges;
	tiTotalEdges.Save(fioTreeFile);
	TInt tiTotalLevels = iTotalLevels;
	tiTotalLevels.Save(fioTreeFile);

	SaveTreeBuildToDirRecursive(this->GetRoot());
}
void TGTree::SaveTreeBuildToDirRecursive(TGTreeSuperNodeEntry* gtsneBase)
{
	gtsneBase->Save(this->sTreeSaveDirectory);
	if(gtsneBase->bIsLeaf) {
		return;
	}
	std::list<TGTreeSuperNodeEntry>::iterator iIterator;
	for(iIterator = gtsneBase->gtnSubTree.begin(); iIterator != gtsneBase->gtnSubTree.end(); ++iIterator) {
		SaveTreeBuildToDirRecursive(&(*iIterator));
	}
}
bool TGTree::LoadTreeBuildFromDir()
{
	if(!DirectoryExists(this->sTreeSaveDirectory)) {
		return false;
	}
	std::string sTreeFile = this->sTreeSaveDirectory + wxFileName::GetPathSeparator() + "Tree";
	TFInOut fioTreeFile(sTreeFile.c_str(), faRdOnly, true);
	TInt tiTotalNodes(fioTreeFile);
	iTotalNodes = tiTotalNodes.Val;
	TInt tiTotalEdges(fioTreeFile);
	iTotalEdges = tiTotalEdges.Val;
	TInt tiTotalLevels(fioTreeFile);
	iTotalLevels = tiTotalLevels.Val;

	std::hash_set<std::string> lListOfFiles;
	wxString wxsFilename;
	/*Track SuperNodes dirs*/
	wxDir dir(this->sTreeSaveDirectory.c_str());
	bool bHasDirs = dir.GetFirst(&wxsFilename, wxEmptyString, wxDIR_DIRS);
	while(bHasDirs) {
		lListOfFiles.insert(std::string(wxsFilename));
		bHasDirs = dir.GetNext(&wxsFilename);
	}
	/*check for valid number*/
	if(lListOfFiles.size() <= 0) {
		TDialogWarning dwDoneWarning("Error: no tree build data available.");
		return false;
	}
	/*Start recursion*/
	AddSuperNodeLabelToSetOfLabels(this->gtnRoot.AddSuperNodeEntry("s0",NULL));
	LoadTreeBuildFromDirRecursive(this->GetRoot(), &lListOfFiles);
	/*Prepare presentation and structure*/
#ifndef PERFORMANCE
	this->SetAllSuperNodesColor();
#endif
	this->PrepareTreeData();

	return true;
};
void TGTree::LoadTreeBuildFromDirRecursive(TGTreeSuperNodeEntry* gtsneBase, std::hash_set<std::string> *lListOfFiles)
{
	int iSonCounter = 0;
	std::hash_set<std::string>::iterator iLIterator, iTemp;
	std::string sDirName, sNextDir;
	/*First recreate list of sons for gtsneBase*/
	sNextDir = gtsneBase->sSuperNodeId + TConverter<int>::ToString(iSonCounter);
	iLIterator = lListOfFiles->find(sNextDir);
	while(iLIterator != lListOfFiles->end()) {
		AddSuperNodeLabelToSetOfLabels(gtsneBase->AddTreeNodeEntry(sNextDir));
		LoadTreeBuildFromDirRecursive(gtsneBase->gtnSubTree.ScanEntries(sNextDir),lListOfFiles);
		iTemp = iLIterator;
		iLIterator++;
		lListOfFiles->erase(iTemp);
		iSonCounter++;
		sNextDir = gtsneBase->sSuperNodeId + TConverter<int>::ToString(iSonCounter);
		iLIterator = lListOfFiles->find(sNextDir);
	}
	/*Then load respective data from file for gtsneBase*/
	gtsneBase->Load(this->sTreeSaveDirectory);
}
/*---------------------------------------------------------------------------------*/
void TGTree::GetPathToTreeLeafNodeFromFileName(std::string sAFileName, std::list<std::string>* lListOfSuperNodeIds)
{
	/*Given a file name, returns the path in the tree to the corresponding tree leaf node*/

	lListOfSuperNodeIds->clear();
	char *cNextSuperNodeIdTemp = NULL;
	std::string sNextSuperNodeId1;
	std::string sNextSuperNodeId2;
	std::string sId;

	int iStart = sAFileName.rfind(".IN.part.") + std::string(".IN.part.").size();
	int iEnd = sAFileName.rfind("_ef.txt") - iStart;
	std::string sFileIdentifier = sAFileName.substr(iStart, iEnd);

	char cFileName[500];
	strcpy(cFileName, sFileIdentifier.c_str());

	cNextSuperNodeIdTemp = strtok(cFileName,"_");
	sNextSuperNodeId1 = "s";
	sId = cNextSuperNodeIdTemp;
	sNextSuperNodeId1 += cNextSuperNodeIdTemp;
	while(true) {
		cNextSuperNodeIdTemp = strtok(NULL,"_");
		if(cNextSuperNodeIdTemp == 0) {
			break;
		}
		sId += cNextSuperNodeIdTemp;
		sNextSuperNodeId2 = "s" + sId;
		lListOfSuperNodeIds->push_back(sNextSuperNodeId1 + " " + sNextSuperNodeId2);
		sNextSuperNodeId1 = sNextSuperNodeId2;
	}
	lListOfSuperNodeIds->push_back(sNextSuperNodeId2 + " " + sAFileName);
};

/*The graph nodes (beneath the leaves) are not counted*/
void TGTree::SetLevelsForTreeNodes()
{
	SetLevelsForTreeNodesRecursive(this->GetRoot(),0);
}
void TGTree::SetLevelsForTreeNodesRecursive(TGTreeSuperNodeEntry* gtsneBase, int iLevelCounter)
{
	gtsneBase->iLevel = iLevelCounter;
	if(iLevelCounter > iTotalLevels) {
		iTotalLevels = iLevelCounter;
	}
	if(gtsneBase->bIsLeaf) {
		return;
	}
	std::list<TGTreeSuperNodeEntry>::iterator iIterator;
	iLevelCounter++;
	for(iIterator = gtsneBase->gtnSubTree.begin(); iIterator != gtsneBase->gtnSubTree.end(); ++iIterator) {
		SetLevelsForTreeNodesRecursive(&(*iIterator),iLevelCounter);
	}
}
/*----------------------------------------------------------------------*/
void TGTree::WriteSuperNodeProperties()
{
	FILE *fSuperNodePropertiesFile;
	/*Create SuperNode properites file*/
	std::string sPropertiesFileName = this->sTreeWorkingDirectory;
	std::string sGTreeRootName = sTreeWorkingDirectory.substr(sTreeWorkingDirectory.find_last_of(wxFileName::GetPathSeparator())+1,
	                             sTreeWorkingDirectory.size());
	sPropertiesFileName = sPropertiesFileName + wxFileName::GetPathSeparator() + sGTreeRootName + ".snf.txt";
	/*Try to open/create file*/
	if ((fSuperNodePropertiesFile = fopen(sPropertiesFileName.c_str(), "w+t")) == NULL) {
		/*This file is not mandatory*/
		return;
	}
	fprintf(fSuperNodePropertiesFile,"%s","#lines started by # are ignored\n");
	fprintf(fSuperNodePropertiesFile,"%s","#this is an snf or a super nodes files - do not change the order of lines\n");
	fprintf(fSuperNodePropertiesFile,"%s","#supernode_id supernode_label\n");
	/*Recursion*/
	WriteSuperNodePropertiesRecursive(this->GetRoot(), fSuperNodePropertiesFile);
	/*Close file*/
	fclose(fSuperNodePropertiesFile);
};
void TGTree::WriteSuperNodePropertiesRecursive(TGTreeSuperNodeEntry* gsnGraphSuperNode, FILE * fAPropertiesFile)
{
	fprintf(fAPropertiesFile, "%s ", gsnGraphSuperNode->sSuperNodeId.c_str());
	fprintf(fAPropertiesFile, "%s", gsnGraphSuperNode->GetLabel().c_str());
	fprintf(fAPropertiesFile, "\n");
	if(gsnGraphSuperNode->bIsLeaf) {
		return;
	}
	std::list<TGTreeSuperNodeEntry>::iterator iIterator;
	for(iIterator = gsnGraphSuperNode->gtnSubTree.begin(); iIterator != gsnGraphSuperNode->gtnSubTree.end(); ++iIterator) {
		WriteSuperNodePropertiesRecursive(&(*iIterator), fAPropertiesFile);
	}
};
/*----------------------------------------------------------------------*/
void TGTree::LoadAllSuperNodesProperties()
{
	char cLineTrash[1000];
	std::string sTemp;
	TGTreeSuperNodeEntry* gsnGraphSuperNode;
	std::ifstream ifsDataFile;
	/*Read SuperNode properites file*/
	std::string sPropertiesFileName = this->sTreeWorkingDirectory;
	std::string sGTreeRootName = sTreeWorkingDirectory.substr(sTreeWorkingDirectory.find_last_of(wxFileName::GetPathSeparator())+1,
	                             sTreeWorkingDirectory.size());

	sPropertiesFileName = sPropertiesFileName + wxFileName::GetPathSeparator() + sGTreeRootName + ".snf.txt";
	ifsDataFile.open(sPropertiesFileName.c_str());
	if(!ifsDataFile) {
		return;    /*This file is not mandatory, it is ok*/
	}

	/*Percorre todas as linhas do arquivo*/
	while ( !ifsDataFile.eof() ) {
		ifsDataFile >> sTemp;
		if(sTemp.find("#",0) != -1) {
			ifsDataFile.getline(cLineTrash, 1000);  /*Read and discard full line*/
			continue;					 /*Found scape caracter, jump to next line*/
		}
		gsnGraphSuperNode = FindSuperNodeEntry(sTemp);
		if(gsnGraphSuperNode == NULL) {
			continue;
		}
		ifsDataFile >> sTemp;  /*Now we get the label*/
		Replace(sTemp,'_',' ');
		gsnGraphSuperNode->SetLabel(sTemp);  /*Label set*/
	}
	/*Close file*/
	ifsDataFile.close();
};
/*----------------------------------------------------------------------*/
void TGTree::SetAllSuperNodesColor()
{
	int i = 0;
	TGTreeNode::iterator iIterator;
	for(iIterator = GetRoot()->gtnSubTree.begin(); iIterator != GetRoot()->gtnSubTree.end(); ++iIterator, ++i) {
		double h = 1.0 / GetRoot()->gtnSubTree.size() * i,
		       s = 0.40,
		       v = 0.75;
		iIterator->SetColor(h,s,v);
		SetAllSuperNodesColorRecursive(&(*iIterator), h, 1);
	}
};
void TGTree::SetAllSuperNodesColorRecursive(TGTreeSuperNodeEntry* node, double h, int level)
{
	if(node->bIsLeaf) {
		return;
	}
	double s = 0.40 - 0.20/iTotalLevels*level,
	       v = 0.75 + 0.25/iTotalLevels*level;
	int i = 0;
	TGTreeNode::iterator iIterator;
	for(iIterator = node->gtnSubTree.begin(); iIterator != node->gtnSubTree.end(); ++iIterator, ++i) {
		iIterator->SetColor(h,s,v);

		SetAllSuperNodesColorRecursive(&(*iIterator), h, level+1);
	}
};
/*----------------------------------------------------------------------*/
/*This method, besides expanding and collapsing super nodes, it keeps track
of which super nodes are to be interconnected*/
void TGTree::ExpandSuperNode(TGTreeSuperNodeEntry* gtsneBase, bool bOption)
{
	std::list<TGTreeSuperNodeEntry>::iterator iIterator;
	if(bOption) {
		gtsneBase->SetExpanded(bOption);  /*if expand == true expand only this guy*/
	} else {
		/*If collapse (bOption == false) collapse everything beneath*/
		gtsneBase->Release(false);
	}
}
/*----------------------------------------------------------------------*/
bool TGTree::TranslateSuperNode(TGTreeSuperNodeEntry* gtsneBase, double dTempX, double dTempY)
{
	return TranslateSuperNodeRecursive(gtsneBase, dTempX, dTempY);
}
void TGTree::TranslateSuperNodeSubGraph(TGTreeSuperNodeEntry* gtsneBase, int iX, int iY)
{
	TranslateSuperNodeSubGraphRecursive(gtsneBase, iX, iY);
}
bool TGTree::TranslateSuperNodeRecursive(TGTreeSuperNodeEntry* gtsneBase, double dTempX, double dTempY)
{
	std::list<TGTreeSuperNodeEntry>::iterator iIterator;
	if(!gtsneBase->TranslateSuperNode(dTempX, dTempY)) {
		return false;
	}
	if(gtsneBase->bIsLeaf) {
		return true;
	}
	for(iIterator = gtsneBase->gtnSubTree.begin(); iIterator != gtsneBase->gtnSubTree.end(); ++iIterator) {
		TranslateSuperNodeRecursive(&(*iIterator), dTempX, dTempY);
	}
	return true;
}
void TGTree::TranslateSuperNodeSubGraphRecursive(TGTreeSuperNodeEntry* gtsneBase, int iX, int iY)
{
	std::list<TGTreeSuperNodeEntry>::iterator iIterator;
	if(gtsneBase->diDrawer != NULL) {
		gtsneBase->diDrawer->SetTranslation(iX, iY);
	}
	if(gtsneBase->bIsLeaf) {
		return;
	}
	for(iIterator = gtsneBase->gtnSubTree.begin(); iIterator != gtsneBase->gtnSubTree.end(); ++iIterator) {
		TranslateSuperNodeSubGraphRecursive(&(*iIterator), iX, iY);
	}
}
/*----------------------------------------------------------------------*/
/*Not used to free memory, but to clean up the subgraphs on the fly*/
void TGTree::ReleaseAllSubgraphData()
{
	ReleaseAllSubgraphDataRecursive(this->GetRoot());
}
void TGTree::ReleaseAllSubgraphDataRecursive(TGTreeSuperNodeEntry* gtsneBase)
{
	if(gtsneBase->bIsLeaf) {
		gtsneBase->ReleaseSubGraph(false);
		return;
	}
	std::list<TGTreeSuperNodeEntry>::iterator iIterator;
	for(iIterator = gtsneBase->gtnSubTree.begin(); iIterator != gtsneBase->gtnSubTree.end(); ++iIterator) {
		ReleaseAllSubgraphDataRecursive(&(*iIterator));
	}
}
void TGTree::ClearAllNodes()
{
	ClearAllNodesRecursive(this->GetRoot());
}
void TGTree::ClearAllNodesRecursive(TGTreeSuperNodeEntry* gtsneBase)
{
	if(gtsneBase->bIsLeaf) {
		return;
	}
	std::list<TGTreeSuperNodeEntry>::iterator iIterator;
	for(iIterator = gtsneBase->gtnSubTree.begin(); iIterator != gtsneBase->gtnSubTree.end(); ++iIterator) {
		if(!iIterator->bIsLeaf) {
			ClearAllNodesRecursive(&(*iIterator));
		}
	}
	gtsneBase->gtnSubTree.clear();
}
TGTreeSuperNodeEntry* TGTree::GetFirstCommonParent(TGTreeSuperNodeEntry* gtsneANode, TGTreeSuperNodeEntry* gtsneAnotherNode)
{
	if((gtsneANode == NULL) || (gtsneAnotherNode == NULL)) {
		return this->GetRoot();
	}
	int iLevelANode = gtsneANode->iLevel;
	int iLevelAnotherNode = gtsneAnotherNode->iLevel;
	int iLevelDifference = ABS((iLevelANode - iLevelAnotherNode));
	if(iLevelANode > iLevelAnotherNode)
		for(int i = 0; i < iLevelDifference; i++) {
			gtsneANode = gtsneANode->gtsneParent;
		}
	else
		for(int i = 0; i < iLevelDifference; i++) {
			gtsneAnotherNode = gtsneAnotherNode->gtsneParent;
		}

	while(gtsneANode != gtsneAnotherNode) {
		gtsneANode = gtsneANode->gtsneParent;
		gtsneAnotherNode = gtsneAnotherNode->gtsneParent;
	}
	return gtsneANode;
}

TGTreeSuperNodeEntry* TGTree::GetFirstNodeInPath(TGTreeSuperNodeEntry* gtsneFrom, TGTreeSuperNodeEntry* gtsneTo)
{
	if((gtsneFrom == gtsneTo) || (gtsneTo->gtsneParent == gtsneFrom)) {
		return gtsneTo;
	}
	TGTreeSuperNodeEntry* gtsneTemp = gtsneTo->gtsneParent;
	while(gtsneTemp->gtsneParent != gtsneFrom) {
		gtsneTemp = gtsneTemp->gtsneParent;
	}
	return gtsneTemp;
}
void TGTree::GetIntersectionOfSets(std::hash_set<int>& lResultSet, std::hash_set<int>& lFirstSet, std::hash_set<int>& lSecondSet)
{
	/*Trace intersection between lFirstSet and lSecondSet and put it in lResultSet*/
	lResultSet.clear();
	std::hash_set<int>::iterator iIterator;
	for(iIterator = lFirstSet.begin(); iIterator != lFirstSet.end(); iIterator++) {
		if(lSecondSet.find(*iIterator) != lSecondSet.end()) {
			lResultSet.insert(*iIterator);
		}
	}
}

#define PERCENTAGE_OF_NODES 0.5
void TGTree::ForeignNeighborsTest()
{
	TGTreeSuperNodeEntry* gtneTemp = NULL;
	wxArrayString wxasArrayOfForeingNodesLabels;
	clock_t start, end;
	double dElapsedTime;
	int iPercent = this->GetTotalNodes()*PERCENTAGE_OF_NODES;
	int iNextId = 1;

	start = clock();
	for(int iTemp = 1; iTemp <= iPercent;) {
		gtneTemp = this->FindNodeSuperNodeEntry(iNextId);
		if(gtneTemp == NULL) {
			/*nop*/
		} else {
			this->TrackNodeForeignNeighbors(iTemp, gtneTemp, wxasArrayOfForeingNodesLabels);
			iTemp++;
		}
		iNextId++;
	}
	end = clock();

	dElapsedTime = (double)(end - start);
	dElapsedTime = dElapsedTime/ (double)CLOCKS_PER_SEC;
	TDialogWarning dwDoneWarning("Elapsed time = " + TConverter<double>::ToString(dElapsedTime));
}

#define NUMBER_OF_SNC_COMPUTATIONS 1000
#define LEVEL_FOR_SNC 2
void TGTree::GetConnectivityOfSuperNodes()
{
	int iPartitionsPerLevel = this->GetNumberOfPartitionsPerLevel();
	int iNumberOfCalculations = 0;
	double dTotalElapsedTime = 0;
	std::string sPairsOfSuperNodes = "";
	double dElapsedTime = 0;
	bool bCalculusExecuted;
	int _totaledges = iTotalEdges;
	iTotalEdges = 0;

	stack<TGTreeSuperNodeEntry*> s1;
	s1.push(GetRoot());
	while (!s1.empty() && iNumberOfCalculations < NUMBER_OF_SNC_COMPUTATIONS) {
		TGTreeSuperNodeEntry* node1 = s1.top();
		s1.pop();
		node1->Load(false);
		for (TGTreeNode::iterator ni = node1->gtnSubTree.begin(); ni != node1->gtnSubTree.end(); ++ni) {
			s1.push(&*ni);
		}
		stack<TGTreeSuperNodeEntry*> s2;
		s2.push(GetRoot());
		while (!s2.empty() && iNumberOfCalculations < NUMBER_OF_SNC_COMPUTATIONS) {
			TGTreeSuperNodeEntry* node2 = s2.top();
			s2.pop();
			node2->Load(false);
			for (TGTreeNode::iterator ni = node2->gtnSubTree.begin(); ni != node2->gtnSubTree.end(); ++ni) {
				s2.push(&*ni);
			}
			if((node1->iLevel == LEVEL_FOR_SNC) && (node2->iLevel == LEVEL_FOR_SNC)) {
				this->GetConnectivity(node1,node2,dElapsedTime,bCalculusExecuted);
				if(bCalculusExecuted) {
					sPairsOfSuperNodes += "(" + node1->GetLabel() + ", " + node2->GetLabel() + ") - ";
					iNumberOfCalculations++;
					dTotalElapsedTime += dElapsedTime;
				}
			}
		}
	}
	dTotalElapsedTime = dTotalElapsedTime/(double)CLOCKS_PER_SEC;
	TDialogWarning dwDoneWarning("Elapsed time for "
	                             + ToString(iNumberOfCalculations)
	                             + " SNC computations = "
	                             + ToString(dTotalElapsedTime)
	                             + " - Average time: "
	                             + ToString(dTotalElapsedTime/iNumberOfCalculations)
	                             + " - Average edges: "
	                             + ToString(iTotalEdges/(double)iNumberOfCalculations)
	                             + " - Considered pairs: "
	                             + ToString(sPairsOfSuperNodes));
	iTotalEdges = _totaledges;
}

TPt<TGTreeSuperEdge> TGTree::GetConnectivity(TGTreeSuperNodeEntry* gtsneANode, TGTreeSuperNodeEntry* gtsneAnotherNode, double &dTime, bool &bCalculusExecuted)
{
	bCalculusExecuted = false;
	if( /*The same SuperNodes*/
	    (gtsneANode == gtsneAnotherNode)
#ifndef PERFORMANCE
	    /*Same parent - brothers connectivity already in the tree*/
	    || (gtsneANode->gtsneParent == gtsneAnotherNode->gtsneParent)
#endif
	) {
		return TGTreeSuperEdge::gtseNULLSuperEdge;    /*Quit*/
	}

	/*No connectivity between SuperNodes in the same path*/
	if(gtsneANode->iLevel > gtsneAnotherNode->iLevel) {
		if(gtsneANode->IsAncestor(gtsneAnotherNode)) {
			return TGTreeSuperEdge::gtseNULLSuperEdge;
		}
	} else {
		if(gtsneAnotherNode->IsAncestor(gtsneANode)) {
			return TGTreeSuperEdge::gtseNULLSuperEdge;
		}
	}
	clock_t start, end;

	TPt<TGTreeSuperEdge> gtseTemp = TGTreeSuperEdge::gtseNULLSuperEdge;
	TGTreeSuperNodeEntry* gtsneFirstCommonParent = GetFirstCommonParent(gtsneANode, gtsneAnotherNode);
	TGTreeSuperNodeEntry* gtsneCommonBrotherANode = GetFirstNodeInPath(gtsneFirstCommonParent, gtsneANode);
	TGTreeSuperNodeEntry* gtsneCommonBrotherAnotherNode = GetFirstNodeInPath(gtsneFirstCommonParent, gtsneAnotherNode);

	/*We find the super edge corresponding to the first sons of gtsneFirstCommonParent
	  that are in the path from gtsneANode and gtsneAnotherNode to the their first common parent*/
	/*Note the false parameter in FindSuperEdgeEntry. We deal with undirected super edges*/
	TPt<TGTreeSuperEdge> gtseSuperEdge = gtsneFirstCommonParent->FindSuperEdgeEntry(gtsneCommonBrotherANode, gtsneCommonBrotherAnotherNode, false);

	/*No super edge, no connectivity*/
	if(gtseSuperEdge == TGTreeSuperEdge::gtseNULLSuperEdge) {
		return TGTreeSuperEdge::gtseNULLSuperEdge;
	}

	/*Create the connectivity SuperEdge (just once)*/
	if(gtseTemp == TGTreeSuperEdge::gtseNULLSuperEdge) {
		gtseTemp = TPt<TGTreeSuperEdge>(new TGTreeSuperEdge(gtsneANode, gtsneAnotherNode));
	}

	start = clock();

	std::string logString = "TGTree::GetConnectivity(\"" + gtsneANode->GetAncestorsLabel() + "\",\"" + gtsneAnotherNode->GetAncestorsLabel() + "\")";
	CpuTimeLog log(logString.c_str());

	/*We check all the edges of the connectivity SuperEdge verifying
	  if their nodes are openNodes for gtsneANode and gtsneAnotherNode*/
	for (TGTreeSuperEdge::TEdgeI EI = gtseSuperEdge->BegEI(); EI != gtseSuperEdge->EndEI(); EI++) {
		if(gtsneANode->hsListOpenNodes.find(EI.GetSrcNId()) != gtsneANode->hsListOpenNodes.end()) {
			if(gtsneAnotherNode->hsListOpenNodes.find(EI.GetDstNId()) != gtsneAnotherNode->hsListOpenNodes.end()) {
				gtseTemp->AddGTreeEdge(EI.GetSrcNId(), EI.GetDstNId(), EI().GetWeight());
			}
			continue;
		}

		if(gtsneANode->hsListOpenNodes.find(EI.GetDstNId()) != gtsneANode->hsListOpenNodes.end()) {
			if(gtsneAnotherNode->hsListOpenNodes.find(EI.GetSrcNId()) != gtsneAnotherNode->hsListOpenNodes.end()) {
				gtseTemp->AddGTreeEdge(EI.GetSrcNId(), EI.GetDstNId(), EI().GetWeight());
			}
		}
	}
	end = clock();

	dTime = (double)(end - start);
	bCalculusExecuted = true;
//	TDialogWarning dwDoneWarning("Elapsed time = " + TConverter<double>::ToString(dElapsedTime));

	iTotalEdges += gtseTemp->GetEdges();

	/*No connectivity*/
	if(gtseTemp->GetEdges() == 0) {
		return TGTreeSuperEdge::gtseNULLSuperEdge;
	}

#ifdef PERFORMANCE
	gtseTemp = TGTreeSuperEdge::gtseNULLSuperEdge;
#endif
	return gtseTemp;
}
int TGTree::GetBrothersConnectivity(TGTreeSuperNodeEntry* gtsneANode, TGTreeSuperNodeEntry* gtsneAnotherNode)
{
	if(gtsneANode->gtsneParent == gtsneAnotherNode->gtsneParent) {
		TPt<TGTreeSuperEdge> gtseTemp = gtsneANode->gtsneParent->FindSuperEdgeEntry(gtsneANode,gtsneAnotherNode, false);
		if(gtseTemp != TGTreeSuperEdge::gtseNULLSuperEdge) { /*Brothers with connectivity*/
			return gtseTemp->GetWeight();
		} else {
			return 0;    /*Brothers, but no connectivity*/
		}
	}
	return -1;  /*Not brothers*/
}
TPt<TGTreeSuperEdge> TGTree::IdentifySuperEdge(double dAX, double dAY, TDrawSuperGraph *dsgADrawer)
{
	TGTreeSuperNodeEntry* gtsneTemp;
	TPt<TGTreeSuperEdge> gtseTemp;
	double x = dAX, y = dAY;
	if(dsgADrawer->ConvertDeviceToDrawingCoordinates(dAX, dAY)) {
		/*Get SuperNode over which the mouse is*/
		gtsneTemp = this->FindSuperNodeEntry(dAX, dAY);
		if(gtsneTemp != NULL) {	/*Found?*/
			if(gtsneTemp->IsExpanded()) { /*SuperEdges being showed?*/
				gtseTemp = gtsneTemp->IdentifySuperEdge(x, y, dsgADrawer);
				if(gtseTemp != TGTreeSuperEdge::gtseNULLSuperEdge) {
					return gtseTemp;    /*Found*/
				} else {
					return IdentifySuperEdgeRecursive(gtsneTemp, x, y, dsgADrawer);
				}
			}
		}
	}
	return TGTreeSuperEdge::gtseNULLSuperEdge;
}
TPt<TGTreeSuperEdge> TGTree::IdentifySuperEdgeRecursive(TGTreeSuperNodeEntry* gtsneBase,double dAX, double dAY, TDrawSuperGraph *dsgADrawer)
{
	/*Try to find Connectivity SuperEdge (between any son beneath)*/
	/*Recursion then*/
	TPt<TGTreeSuperEdge> gtseTemp = gtsneBase->IdentifyConnectivitySuperEdge(dAX, dAY, dsgADrawer);
	if(gtseTemp != TGTreeSuperEdge::gtseNULLSuperEdge) {
		return gtseTemp;    /*Found*/
	}
	if(gtsneBase->bIsLeaf) {
		return TGTreeSuperEdge::gtseNULLSuperEdge;
	}
	std::list<TGTreeSuperNodeEntry>::iterator iIterator;
	for(iIterator = gtsneBase->gtnSubTree.begin(); iIterator != gtsneBase->gtnSubTree.end(); ++iIterator) {
		gtseTemp = IdentifySuperEdgeRecursive(&(*iIterator), dAX, dAY, dsgADrawer);
		if(gtseTemp != TGTreeSuperEdge::gtseNULLSuperEdge) {
			return gtseTemp;
		}
	}
	return TGTreeSuperEdge::gtseNULLSuperEdge;
}
/*----------------------------------------------------------------------*/
// io operations

void TGTree::Load()
{
	CpuTimeLog log("GTree::LoadFromFile");
	if (storage) {
		delete storage;
	}
	storage = new InputStorage(sTreeWorkingDirectory.c_str());
	*storage >> iTotalNodes;
	*storage >> iTotalEdges;
	*storage >> iTotalLevels;
	uint count;
	*storage >> count;
	while (count--) {
		int id;
		std::string label;
		*storage >> id >> label;
		hmIdIndex.insert(std::pair<int,TNodesAuxiliar>(id, (TNodesAuxiliar(label,NULL))));
	}
	gtnRoot.push_back((TGTreeSuperNodeEntry(*storage, NULL)));
}

void UpdateNodesStorage(TGTreeSuperNodeEntry &n, InputStorage &s)
{
	n.SetInputStorage(s);
	for (TGTreeNode::iterator i = n.gtnSubTree.begin(); i != n.gtnSubTree.end(); ++i) {
		UpdateNodesStorage(*i, s);
	}
}

void TGTree::Save()
{
	std::string sFile = sTreeWorkingDirectory + ".gtree";
	std::string sBkpFile = sFile + ".bkp";
	{
		// flush and close file at ~OutputStorage
		CpuTimeLog log("GTree::SaveToFile");
		OutputStorage out (sBkpFile.c_str());
		out << iTotalNodes;
		out << iTotalEdges;
		out << iTotalLevels;
		uint count = hmIdIndex.size();
		out << count;
		for (hash_map<int,TNodesAuxiliar>::iterator it = hmIdIndex.begin(); it != hmIdIndex.end(); ++it) {
			out << it->first << it->second.GetLabel();
		}
		this->GetRoot()->Save(out);
	}
	if (storage) {
		delete storage;
	}
	wxRenameFile(sBkpFile, sFile);
	storage = new InputStorage(sFile.c_str());
	UpdateNodesStorage(*GetRoot(), *storage);
}

#endif
