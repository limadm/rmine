#pragma once
#ifndef _ResolutionComponent_
#define _ResolutionComponent_

#include <Commons/Auxiliar.h>

#define SELECTION_THICKNESS 2;

class TResolutionComponent{
private:
   int iX1, iX2; int iY1, iY2;
   int iNumberOfTicks;    int iCurrentTick;
   int iLineThickness;    bool bVisible;
public:
   wxDC *wxdcDeviceContext;
   wxPen* wxpMyPen;
   TResolutionComponent(int iANumberOfTicks);
   void SetDeviceContext(wxDC *wxdcADeviceContext);
   void Draw();
   bool IsMouseOver(int iAX, int iAY);
   void SetCurrentTick(int iATickPosition);
   int  GetCurrentTick();
   void SetNumberOfTicks(int iANumberOfTicks);
   void ProcessClick(int iAX, int iAY);
   void SetVisible(bool bAOption);
};
#endif
