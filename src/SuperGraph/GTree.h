#pragma once
#ifdef _SUPERGRAPHCOMPILATION

#ifndef _GTree_
#define _GTree_

#include <hash_map>

#include <Commons/Auxiliar.h>
#include <SuperGraph/GTreeNode.h>

class TDrawSuperGraph;

static double dInitializer;
static bool bInitializer;

class TGTree{   
public:
   /*Created right here, filled in AddHierarchyLine during the building process*/
   TGTreeNode gtnRoot; /*---Descriptive---*/
public:
   TGTree(std::string sATreeWorkingDirectory);
   ~TGTree();

   void Load();
   void Save();

   int GetNumberOfPartitionsPerLevel();
   bool BuildTreeFromPartitionDirectory();
   bool LoadTreeBuildFromDir();
   void SaveTreeBuildToDir();
   TGTreeSuperNodeEntry* FindSuperNodeEntry(std::string sId);
   TGTreeSuperNodeEntry* FindSuperNodeEntry(double dAX, double dAY);
   TPt<TGTreeSuperEdge> IdentifySuperEdge(double dAX, double dAY, TDrawSuperGraph *dsgADrawer);
   void SetAllSuperNodesColor();
   void PrintTree();
   void ApplyTreeMap();
   void ApplyDistributionLayout();
   void ApplyDistributionLayoutRecursive(TGTreeSuperNodeEntry* gsnGraphSuperNode);
   bool TranslateSuperNode(TGTreeSuperNodeEntry* gtsneBase, double dTempX, double dTempY);
   void ForeignNeighborsTest();
   void GetConnectivityOfSuperNodes();
   TPt<TGTreeSuperEdge> GetConnectivity(TGTreeSuperNodeEntry* gtsneANode, TGTreeSuperNodeEntry* gtsneAnotherNode, double &dTime = dInitializer, bool &bCalculusExecuted = bInitializer);
   void ExpandSuperNode(TGTreeSuperNodeEntry* gtsneBase, bool bOption); 
   std::string GetNodeLabel(int iANodeId);

#ifndef PERFORMANCE
   void FillNodeNetWithLabelData(TPt< TNodeNetTests<TNodePlot, TEdgePlot> > pANodeNetData);
#else
   void FillNodeNetWithLabelData(TPt< TNodeNetTests<TNodePerformance, TEdgePlot> > pANodeNetData);
#endif
   TGTreeSuperNodeEntry* FindNodeSuperNodeEntry(int iANodeId);
   int GetNodeIdFromNodeLabel(std::string sANodeLabel, int iNthOccurrence = 1);
   TGTreeSuperNodeEntry* FindNodeSuperNodeEntry(std::string sANodeLabel);
   int GetTotalNodes();
   std::string GetIthNodeLabelFromParallelLabelIndex(int iAIndex);
   void ExpandAllParents(TGTreeSuperNodeEntry* gtsneASuperNode);
   void WriteSuperNodeProperties();
   void LoadAllSuperNodesProperties();
   void TrackNodeForeignNeighbors(int iANodeId, TGTreeSuperNodeEntry* gtsneASuperNode, wxArrayString& wxasArrayOfForeingNodesLabels);
   TGTreeSuperNodeEntry* GetFirstCommonParent(TGTreeSuperNodeEntry* gtsneANode, TGTreeSuperNodeEntry* gtsneAnotherNode);
   TGTreeSuperNodeEntry* GetRoot();
private:
   /*Necessary for constructor*/
   std::string sTreeWorkingDirectory; /*---Descriptive---*/
   std::string sTreeSaveDirectory;

   /*Filled after creation in PrepareTreeData*/
   std::hash_map<int, TNodesAuxiliar> hmIdIndex;
   /*Filled after creation in BuildLabelIndex*/   
   std::list< std::pair<std::string, int> > lLabelIndex;
   /*These are used to compute the total of simple nodes and simple edges*/
   /*Both are filled after creation in PrepareSuperGraphData*/
   int iTotalNodes;           int iTotalEdges;
   /*Both Filled after creation in SetLevelsForTreeNodes*/
   int iTotalLevels;
   std::set<std::string> sSetOfSuperNodes;
   rmine::disk::InputStorage *storage;
private:
   void PrepareTreeData();
   void PrepareSuperGraphData();
   void ReleaseAllSubgraphData();
   void SetLevelsForTreeNodes();
   int PrepareSuperGraphDataRecursive(TGTreeSuperNodeEntry* gsnGraphSuperNode);
   int PrepareTreeDataRecursive(TGTreeSuperNodeEntry* gsnGraphSuperNode);
   void BuildLabelIndex();
   void AddSuperNodeLabelToSetOfLabels(std::string sALabel);
   void ApplyTreeMapRecursive(TGTreeNode* gtneBaseNode, bool bHorizontalSplit,
                              double dHorizontalSizeToShare, double dVerticalSizeToShare,
                              double dInitialX, double dInitialY);
   int GetBiggestNumberOfGrandSons(TGTreeSuperNodeEntry* gsnGraphSuperNode);
   void AddHierarchyLine(std::string sALine);
   void PrintTreeRecursive(TGTreeNode* gtneBaseNode);
   TGTreeSuperNodeEntry* FindSuperNodeEntryRecursive(TGTreeNode* gtneBaseNode, std::string sId);
   TGTreeSuperNodeEntry* FindSuperNodeEntryRecursive(TGTreeSuperNodeEntry* gtneBaseNode, double dAX, double dAY);
   TPt<TGTreeSuperEdge> IdentifySuperEdgeRecursive(TGTreeSuperNodeEntry* gtsneBase,double dAX, double dAY, TDrawSuperGraph *dsgADrawer);
   bool TranslateSuperNodeRecursive(TGTreeSuperNodeEntry* gtsneBase, double dTempX, double dTempY);
   void TranslateSuperNodeSubGraph(TGTreeSuperNodeEntry* gtsneBase, int iX, int iY);
   void TranslateSuperNodeSubGraphRecursive(TGTreeSuperNodeEntry* gtsneBase, int iX, int iY);
   int GetBrothersConnectivity(TGTreeSuperNodeEntry* gtsneANode, TGTreeSuperNodeEntry* gtsneAnotherNode);
   void GetPathToTreeLeafNodeFromFileName(std::string sAFileName, std::list<std::string>* lListOfSuperNodeIds);
   void ClearAllNodes();
   TGTreeSuperNodeEntry* GetFirstNodeInPath(TGTreeSuperNodeEntry* gtsneFrom, TGTreeSuperNodeEntry* gtsneTo);
   void GetIntersectionOfSets(std::hash_set<int>& lResultSet, std::hash_set<int>& lFirstSet, std::hash_set<int>& lSecondSet);
   void SetLevelsForTreeNodesRecursive(TGTreeSuperNodeEntry* gtsneBase, int iLevelCounter);
   void ReleaseAllSubgraphDataRecursive(TGTreeSuperNodeEntry* gtsneBase);
   void ClearAllNodesRecursive(TGTreeSuperNodeEntry* gtsneBase);
   void SetAllSuperNodesColorRecursive(TGTreeSuperNodeEntry* gsnGraphSuperNode, double hue, int level);
   void WriteSuperNodePropertiesRecursive(TGTreeSuperNodeEntry* gsnGraphSuperNode, FILE * fAPropertiesFile);
   void SaveTreeBuildToDirRecursive(TGTreeSuperNodeEntry* gtsneBase);
   void LoadTreeBuildFromDirRecursive(TGTreeSuperNodeEntry* gtsneBase, std::hash_set<std::string> *lListOfFiles);
};
/*-------------------------------------------------------------------------------*/
#endif

#endif
