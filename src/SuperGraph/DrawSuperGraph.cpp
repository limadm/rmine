#ifdef _SUPERGRAPHCOMPILATION

#include "DrawSuperGraph.h"

#include <Graphs/NodeNetTests.h>
#include <Graphs/NodePlot.h>
#include <Graphs/EdgePlot.h>
#include <Layouts/LayoutCircular.h>
#include <SuperGraph/FrameDrawSuperGraph.h>

TDrawSuperGraph::TDrawSuperGraph(TGTree* gtASuperGraphTree)
	:	gtGraphTree(gtASuperGraphTree),
	    wxcGraphWidth(100),
	    wxcGraphHeight(100),
	    iHorizontalTranslation(0),
	    iVerticalTranslation(0),
	    bAutoFocus(false),
	    bVisibleSuperEdges(true)
{
	dcDeviceContext = NULL;
	fdsgFrameForLeafSuperNode = NULL;
	fMinX = 0;
	fMaxX = 1.0;
	fMinY = 0;
	fMaxY = 1.0;
	wxbDrawingBrush = new wxBrush(wxColour(0xFFFFFFFF));
	gtneEmphasizedNode = NULL;
	fFrameToDraw = NULL;
	gtseEmphasizedSuperEdge = TGTreeSuperEdge::gtseNULLSuperEdge;
	lConnectivities = new std::list<int>();
	rcResolutionTracker = new TResolutionComponent(1);
	iCurrentResolution = 0;
};
TDrawSuperGraph::~TDrawSuperGraph()
{
	delete lConnectivities;
	delete wxbDrawingBrush;
	if(rcResolutionTracker != NULL) {
		delete rcResolutionTracker;
	}
};

void TDrawSuperGraph::SetFrame(wxWindow *fAFrame)
{
	fFrameToDraw = fAFrame;
	UpdateDisplaySize();
}

void TDrawSuperGraph::UpdateDisplaySize()
{
	int& iWidhtTemp = iDisplayWidth;
	int& iHeightTemp = iDisplayHeight;
	fFrameToDraw->GetSize(&iDisplayWidth, &iDisplayHeight);
	iDisplayWidth = iWidhtTemp;
	iDisplayHeight = iHeightTemp;
};

void TDrawSuperGraph::SetWidthAndHeight(wxCoord wxcAWidth,wxCoord wxcAHeight)
{
	wxcGraphWidth = wxcAWidth;
	wxcGraphHeight = wxcAHeight;
}

#ifndef PERFORMANCE
void TDrawSuperGraph::SetInterfaceForLeafSuperNodes(TFrameDrawGraph< TNodeNetTests<TNodePlot, TEdgePlot> > *fdsgAFrame)
{
#else
void TDrawSuperGraph::SetInterfaceForLeafSuperNodes(TFrameDrawGraph< TNodeNetTests<TNodePerformance, TEdgePlot> > *fdsgAFrame)
{
#endif
	fdsgFrameForLeafSuperNode = fdsgAFrame;
}

TGTreeSuperNodeEntry* TDrawSuperGraph::IdentifySuperNode(const int iX, const int iY)
{
	double dTempX = iX;
	double dTempY = iY;
	if(this->ConvertDeviceToDrawingCoordinates(dTempX, dTempY)) {
		return gtGraphTree->FindSuperNodeEntry(dTempX, dTempY);
	}
	return NULL;
};

TPt<TGTreeSuperEdge> TDrawSuperGraph::IdentifySuperEdge(const int iX, const int iY)
{
	return gtGraphTree->IdentifySuperEdge(iX, iY, this);
};


void TDrawSuperGraph::SetRelativeScale(double fWidthScaleFactor, double fHeightScaleFactor,
                                       int iReferenceX, int iReferenceY)
{
	double dXTemp = iReferenceX;
	double dYTemp = iReferenceY;
	if(!ConvertDeviceToDrawingCoordinates(dXTemp, dYTemp)) {
		return;    /*Point out of the graph area*/
	}

	/*Calculate new size - nearly the screen area*/
	int iNewWidthSize  = fWidthScaleFactor * wxcGraphWidth;
	int iNewHeightSize  = fHeightScaleFactor * wxcGraphHeight;
	this->SetWidthAndHeight(iNewWidthSize, iNewHeightSize);
	/*Put the reference point at the center of the screen
	  We note that the coordinates of the SuperGraph drawing do
	  not change for scaling. What we do is scaling of the drawing
	  space not of the objects.*/
	CentralizeDrawingPoint(dXTemp, dYTemp);
};

void TDrawSuperGraph::SetAbsoluteScale(double fWidthScaleFactor, double fHeightScaleFactor)
{
	this->UpdateDisplaySize();
	double dWidht = fWidthScaleFactor*wxcGraphWidth;
	double dHeight = fHeightScaleFactor*wxcGraphHeight;

	iHorizontalTranslation = fWidthScaleFactor*iHorizontalTranslation;
	iVerticalTranslation   = fHeightScaleFactor*iVerticalTranslation;

	SetWidthAndHeight(dWidht, dHeight);
};

void TDrawSuperGraph::SetTranslation(int iAHorizontalTranslation, int iAVerticalTranslation)
{
	iHorizontalTranslation += iAHorizontalTranslation;
	iVerticalTranslation   += iAVerticalTranslation;
};

bool TDrawSuperGraph::TranslateEmphasizedNode(int iTX, int iTY)
{
	if(gtneEmphasizedNode == NULL) {
		return false;
	}
	double dTempX = iTX;
	double dTempY = iTY;
	ConvertDeviceSizeToDrawingSize(dTempX, dTempY);
	/*Translate super nodes below it*/
	if(!this->gtGraphTree->TranslateSuperNode(gtneEmphasizedNode, dTempX, dTempY)) {
		gtneEmphasizedNode = NULL;
	}
	return true;
};
void TDrawSuperGraph::ExpandNodeTomaHawk(TGTreeSuperNodeEntry* gtneAGraphSuperNode)
{
	/*ExpandNodeTomaHawk: focus on a nodes' grand parent,
	show its parent and brothes, show its sons. It they exist.*/
	if(gtneAGraphSuperNode->gtsneParent != NULL) {
		if(gtneAGraphSuperNode->gtsneParent->gtsneParent != NULL) {
			ExpandNode(gtneAGraphSuperNode->gtsneParent->gtsneParent, false);
			ExpandNode(gtneAGraphSuperNode->gtsneParent->gtsneParent, true);
			ExpandNode(gtneAGraphSuperNode->gtsneParent, false);
			ExpandNode(gtneAGraphSuperNode->gtsneParent, true);
			ExpandNode(gtneAGraphSuperNode, false);
			ExpandNode(gtneAGraphSuperNode, true);
			FocusInSuperNode(gtneAGraphSuperNode->gtsneParent->gtsneParent);
		} else {
			ExpandNode(gtneAGraphSuperNode->gtsneParent, false);
			ExpandNode(gtneAGraphSuperNode->gtsneParent, true);
			ExpandNode(gtneAGraphSuperNode, false);
			ExpandNode(gtneAGraphSuperNode, true);
			FocusInSuperNode(gtneAGraphSuperNode->gtsneParent);
		}
	} else {
		ExpandNode(gtneAGraphSuperNode, false);
		ExpandNode(gtneAGraphSuperNode, true);
		FocusInSuperNode(gtneAGraphSuperNode);
	}
}

void TDrawSuperGraph::ExpandNode(TGTreeSuperNodeEntry* gtneAGraphSuperNode, bool bOption)
{
	if(bOption) { /*Expand*/
		this->gtGraphTree->ExpandSuperNode(gtneAGraphSuperNode, bOption);
	} else {		 /*Collapse*/
		this->gtGraphTree->ExpandSuperNode(gtneAGraphSuperNode, bOption);
		/*Identify context exhibition problem*/
		if(gtneAGraphSuperNode->iLevel < iCurrentResolution) {
			/*Correct it*/
			this->FocusInSuperNode(gtneAGraphSuperNode);
		}
	}
	gtneEmphasizedNode = NULL;
};

void TDrawSuperGraph::DrawNode(TGTreeSuperNodeEntry* gtneAGraphSuperNode, bool bEmphasizeNode)
{
	if(dcDeviceContext == NULL) {
		return;
	}
	double dXPos;
	double dYPos;
	double dHorizontalSize;
	double dVerticalSize;

	TGTreeSuperNodeEntry* gtsneParent = gtneAGraphSuperNode->gtsneParent;

	dXPos = gtneAGraphSuperNode->dX;
	dYPos = gtneAGraphSuperNode->dY;
	dHorizontalSize = gtneAGraphSuperNode->dHorizontalSize;
	dVerticalSize = gtneAGraphSuperNode->dVerticalSize;

	this->ConvertDrawingToDeviceCoordinates(dXPos, dYPos);
	this->ConvertDrawingSizeToDeviceSize(dHorizontalSize, dVerticalSize);

	dcDeviceContext->SetPen(wxPen());
	dcDeviceContext->SetBrush(wxBrush());

	wxGraphicsContext *graphics = dcDeviceContext->GetGraphicsContext();
	wxPen              pen;
	wxGraphicsBrush    brush;

	if(bEmphasizeNode) {
		/*
			// Draw parent's bounding with differentiation color
			if(gtsneParent != NULL){
				wxBLACK_PEN->SetWidth(2);
				dcDeviceContext->SetPen(*wxBLACK_PEN);
				dcDeviceContext->SetBrush(*wxTRANSPARENT_BRUSH);
				double dXPosTemp = gtsneParent->dX;
				double dYPosTemp = gtsneParent->dY;
				double dHSizeTemp = gtsneParent->dHorizontalSize;
				double dVSizeTemp = gtsneParent->dVerticalSize;
				this->ConvertDrawingToDeviceCoordinates(dXPosTemp, dYPosTemp);
				this->ConvertDrawingSizeToDeviceSize(dHSizeTemp, dVSizeTemp);
				dcDeviceContext->DrawEllipse(dXPosTemp, dYPosTemp,
											 dHSizeTemp, dVSizeTemp);
			}

			wxGREEN_PEN->SetWidth(5);
			dcDeviceContext->SetPen(*wxGREEN_PEN);
			wxbDrawingBrush->SetColour(255, 255, 255);
			dcDeviceContext->SetBrush(*wxbDrawingBrush);
		*/
		pen   = wxPen(*wxGREEN, 4);
		brush = graphics->CreateBrush(*wxWHITE_BRUSH);
	} else {
		/*
			wxBLACK_PEN->SetWidth(2);
			dcDeviceContext->SetPen(*wxBLACK_PEN);
			wxbDrawingBrush->SetColour(gtneAGraphSuperNode->bRed,
									   gtneAGraphSuperNode->bGreen,
									   gtneAGraphSuperNode->bBlue);
			dcDeviceContext->SetBrush(*wxbDrawingBrush);
		*/
		int r = gtneAGraphSuperNode->bRed,
		    g = gtneAGraphSuperNode->bGreen,
		    b = gtneAGraphSuperNode->bBlue;
		pen   = wxPen(wxColour(r,g,b), 2);

		brush = graphics->CreateLinearGradientBrush(
		            dXPos, dYPos,
		            dXPos, dYPos + dVerticalSize,
		            wxColour(
		                std::min<int>(255, 1.2 * r),
		                std::min<int>(255, 1.2 * g),
		                std::min<int>(255, 1.2 * b)),
		            wxColour(
		                0.8 * r,
		                0.8 * g,
		                0.8 * b));
	}
	graphics->SetPen(pen);
	graphics->SetBrush(brush);
	graphics->DrawEllipse(dXPos, dYPos, dHorizontalSize, dVerticalSize);

	graphics->SetPen(*wxBLACK_PEN);
	if (!gtneAGraphSuperNode->IsExpanded()) {
		std::string sLabel = gtneAGraphSuperNode->GetCompactLabel();

		wxDouble w,h,descent,leading;
		graphics->GetTextExtent(sLabel, &w, &h, &descent, &leading);
		int iLabelXPos = (dHorizontalSize - w)/2.0;
		int iLabelYPos = (dVerticalSize   - h)/2.0;

		graphics->DrawText(sLabel,
		                   dXPos + iLabelXPos,
						   dYPos + iLabelYPos);
	} else if (gtneAGraphSuperNode->gtsneFocusedNode == NULL
	           && gtneAGraphSuperNode->gtsneParent != NULL) {

		std::string sAncestorsLabel = gtneAGraphSuperNode->GetAncestorsLabel();

		wxDouble w,h,descent,leading;
		graphics->GetTextExtent(sAncestorsLabel, &w, &h, &descent, &leading);
		int iLabelXPos = (dHorizontalSize - w)/2.0;

		graphics->DrawText(sAncestorsLabel,
		                   dXPos + iLabelXPos,
		                   dYPos + dVerticalSize + 1);
	}
	DrawBiggestInnerRectangle(gtneAGraphSuperNode);

	/*Positional limits tracking - used by method ConvertDeviceToDrawingCoordinates*/

	this->ConvertDeviceToDrawingCoordinates(dXPos, dYPos);
	this->ConvertDeviceSizeToDrawingSize(dHorizontalSize, dVerticalSize);

	if(dXPos < fMinX) {
		fMinX = dXPos;
	}
	if((dXPos + dHorizontalSize) > fMaxX) {
		fMaxX = (dXPos + dHorizontalSize);
	}
	if(dYPos < fMinY) {
		fMinY = dYPos;
	}
	if((dYPos + dVerticalSize) > fMaxY) {
		fMaxY = (dYPos + dVerticalSize);
	}
};

void TDrawSuperGraph::DrawLeafNode(TGTreeSuperNodeEntry* gtneAGraphSuperNode)
{
#ifndef PERFORMANCE
	TFrameDrawSuperGraph< TNodeNetTests<TNodePlot, TEdgePlot> > *fgsgTemp = (TFrameDrawSuperGraph< TNodeNetTests<TNodePlot, TEdgePlot> >*)this->fdsgFrameForLeafSuperNode;
#else
	TFrameDrawSuperGraph< TNodeNetTests<TNodePerformance, TEdgePlot> > *fgsgTemp = (TFrameDrawSuperGraph< TNodeNetTests<TNodePerformance, TEdgePlot> >*)this->fdsgFrameForLeafSuperNode;
#endif
	double dXPos;
	double dYPos;
	double dHorizontalSize;
	double dVerticalSize;

	dXPos = gtneAGraphSuperNode->dX;
	dYPos = gtneAGraphSuperNode->dY;
	dHorizontalSize = gtneAGraphSuperNode->dHorizontalSize;
	dVerticalSize = gtneAGraphSuperNode->dVerticalSize;

	/*Update subgraph drawer size to reflect any scaling*/
	this->ConvertDrawingSizeToDeviceSize(dHorizontalSize, dVerticalSize);
	/*Update subgraph drawer position to reflect any translation*/
	this->ConvertDrawingToDeviceCoordinates(dXPos, dYPos);

	/*Check if the LeafSuperNode is in the visible screen*/
	if(((dXPos + dHorizontalSize) < 0) || (dXPos > iDisplayWidth) ||
	        ((dYPos + dVerticalSize)   < 0) || (dYPos > iDisplayHeight)) {
		return;
	}

	/*Subgraph not loaded, we load it now*/
	if(gtneAGraphSuperNode->pNodeNetSubGraph.GetRefs() <= 0) {
#ifndef PERFORMANCE
		TPt< TNodeNetTests<TNodePlot, TEdgePlot> > pNodeNetTemp(new TNodeNetTests<TNodePlot, TEdgePlot>());
#else
		TPt< TNodeNetTests<TNodePerformance, TEdgePlot> > pNodeNetTemp(new TNodeNetTests<TNodePerformance, TEdgePlot>());
#endif
		gtneAGraphSuperNode->pNodeNetSubGraph = pNodeNetTemp;
		gtneAGraphSuperNode->LoadGraphTextFile();
	}
	/*We have one diDrawer per leaf supernode to draw the respective
	  subgraph. All are drawn on the same FrameDrawSuperGraph:FrameDrawgraph*/
	if(gtneAGraphSuperNode->diDrawer == NULL) {
#ifndef PERFORMANCE
		gtneAGraphSuperNode->diDrawer = new TDrawWxWidget< TNodeNetTests<TNodePlot, TEdgePlot> >(gtneAGraphSuperNode->pNodeNetSubGraph);
#else
		gtneAGraphSuperNode->diDrawer = new TDrawWxWidget< TNodeNetTests<TNodePerformance, TEdgePlot> >(gtneAGraphSuperNode->pNodeNetSubGraph);
#endif
		gtneAGraphSuperNode->diDrawer->SetFrame(fgsgTemp);

		if(!gtneAGraphSuperNode->pNodeNetSubGraph->bLayoutFlag) {
#ifndef PERFORMANCE
			TLayoutCircular< TNodeNetTests<TNodePlot, TEdgePlot> > tempLayout;
#else
			TLayoutCircular< TNodeNetTests<TNodePerformance, TEdgePlot> > tempLayout;
#endif
			gtneAGraphSuperNode->diDrawer->ApplyLayout(tempLayout);	/*Layout information is kept in pNodeNetSubGraph*/
		}
	}
	/*Set size and place to draw subgraph data*/
	gtneAGraphSuperNode->diDrawer->SetWidthAndHeight((int)dHorizontalSize, (int)dVerticalSize);
	gtneAGraphSuperNode->diDrawer->SetAbsoluteTranslation((int)dXPos, (int)dYPos);
	/*Update drawing*/
	fgsgTemp->DrawLeafSuperNodeData(gtneAGraphSuperNode, dcDeviceContext);
};

void TDrawSuperGraph::DrawSuperGraph(wxGCDC *vDrawContext)
{
	dcDeviceContext = vDrawContext;
	dcDeviceContext->SetFont(font::sNodes);
	/*Draw all nodes*/
	DrawSuperGraphRecursive(this->gtGraphTree->GetRoot());
	/*Draw connectivity on demand SuperEdges*/
	GetDeviceContext()->SetFont(font::sEdges);
	DrawConnectivitySuperEdge();
	/*Redraw emphasized SuperEdge on top of everything else*/
	if(gtseEmphasizedSuperEdge != TGTreeSuperEdge::gtseNULLSuperEdge) {
		gtseEmphasizedSuperEdge->Draw(this);
	}
	if (!bAutoFocus) {
		/*Resolution Tracker Component*/
		rcResolutionTracker->SetDeviceContext(vDrawContext);
		rcResolutionTracker->Draw();
	}
};

void TDrawSuperGraph::DrawSuperGraphRecursive(TGTreeSuperNodeEntry* gtneBaseNode)
{
	if (gtneBaseNode->IsVisible()) {
		if (gtneBaseNode->gtsneParent != NULL) { // root is not drawn
			bool emphasized = (gtneBaseNode == gtneEmphasizedNode);
			// Draw outline and label.
			DrawNode(gtneBaseNode, emphasized);
		}
		if (gtneBaseNode->IsExpanded()) {
			if (gtneBaseNode->bIsLeaf) {
				DrawLeafNode(gtneBaseNode);
			} else {
				if (gtneBaseNode->gtsneFocusedNode != NULL) {
					// The focused node is drawn over its parent.
					DrawSuperGraphRecursive(gtneBaseNode->gtsneFocusedNode);
				} else {
					// Draw SuperEdges between sons.
					GetDeviceContext()->SetFont(font::sEdges);
					gtneBaseNode->DrawSuperEdges(this, gtneEmphasizedNode);
					GetDeviceContext()->SetFont(font::sNodes);
					/*Draw all son nodes*/
					for (TGTreeNode::iterator iIterator = gtneBaseNode->gtnSubTree.begin(); iIterator != gtneBaseNode->gtnSubTree.end(); ++iIterator) {
						DrawSuperGraphRecursive(&(*iIterator)); /*Recursion*/
					}
				}
			}
		}
	}
};
/*-------------------------------------------------------------*/
void TDrawSuperGraph::DrawConnectivitySuperEdge()
{
	DrawConnectivitySuperEdgeRecursive(this->gtGraphTree->GetRoot());
};
void TDrawSuperGraph::DrawConnectivitySuperEdgeRecursive(TGTreeSuperNodeEntry* gtneBaseNode)
{
	std::list<TGTreeSuperNodeEntry>::iterator iIterator;
	std::list< TPt<TGTreeSuperEdge> >::iterator it;
	/*Draw SuperEdge*/
	for (it = gtneBaseNode->mmConnectivitySuperEdges.begin(); it != gtneBaseNode->mmConnectivitySuperEdges.end(); ++it) {
		if((*it)->GetSuperNode2()->gtsneParent->IsExpanded()) {
			(*it)->Draw(this, gtneEmphasizedNode);
		}
	}
	/*Recursion*/
	if(!gtneBaseNode->bIsLeaf) {
		for(iIterator = gtneBaseNode->gtnSubTree.begin(); iIterator != gtneBaseNode->gtnSubTree.end(); ++iIterator) {
			if(gtneBaseNode->IsExpanded()) {
				DrawConnectivitySuperEdgeRecursive(&(*iIterator));
			}
		}
	}
};
/*-------------------------------------------------------------*/
void TDrawSuperGraph::DrawNodeDetailedData(TGTreeSuperNodeEntry* gtneAGraphSuperNode)
{
	if(dcDeviceContext == NULL) {
		return;
	}
	unsigned int iBiggestStringSize = 0;
	double dXPos = gtneAGraphSuperNode->GetCenterX();
	double dYPos = gtneAGraphSuperNode->GetCenterY();
	this->ConvertDrawingToDeviceCoordinates(dXPos, dYPos);

	/*Labels*/
	std::string sId = "Id = " + gtneAGraphSuperNode->sSuperNodeId;
	iBiggestStringSize = sId.size();

	std::string sNumberOfSons = TConverter<int>::ToString(gtneAGraphSuperNode->iTotalNumberOfGraphNodesBeneath);

	wxBLACK_PEN->SetWidth(1);
	dcDeviceContext->SetPen(*wxBLACK_PEN);
	wxbDrawingBrush->SetColour(255, 255, 255);
	dcDeviceContext->SetBrush(*wxbDrawingBrush);
	//dcDeviceContext->DrawRectangle(dXPos,
	//dYPos, iBiggestStringSize*8 + 2, (5 + 1) * 14);
	dcDeviceContext->DrawRectangle(dXPos, dYPos, 300, 150);

	dXPos += 2;

	dcDeviceContext->DrawText(gtneAGraphSuperNode->sSuperNodeId.c_str(),dXPos, dYPos + 6);
	dcDeviceContext->DrawText(sNumberOfSons.c_str(), dXPos, dYPos + 20);

	std::list< TPt<TGTreeSuperEdge> >::iterator iSuperEdges;
	int i = dYPos + 50;
	std::string sTemp = "Edges: ";
	for(iSuperEdges = gtneAGraphSuperNode->lListSuperEdges.begin(); iSuperEdges != gtneAGraphSuperNode->lListSuperEdges.end(); ++iSuperEdges) {
		sTemp = (*iSuperEdges)->GetSuperNode1()->sSuperNodeId + " - " +
		        (*iSuperEdges)->GetSuperNode2()->sSuperNodeId;
#ifndef PERFORMANCE
		for(TNodeNetTests<TNodePlot, TEdgePlot>::TEdgeI EI = (*iSuperEdges)->BegEI();
#else
		for(TNodeNetTests<TNodePerformance, TEdgePlot>::TEdgeI EI = (*iSuperEdges)->BegEI();
#endif
		        EI != (*iSuperEdges)->EndEI(); EI++) {
			sTemp += " " + TConverter<int>::ToString(EI.GetSrcNId()) + " - " + TConverter<int>::ToString(EI.GetDstNId()) + "  ";
		}
		dcDeviceContext->DrawText(sTemp.c_str(), dXPos, i);
		//		dcDeviceContext->DrawText(sNumberOfSons.c_str(), dXPos, dYPos + 20);
		i += 14;
	}
	std::hash_set<int>::iterator iHashIterator;
	sTemp = "Open: ";
	for(iHashIterator = gtneAGraphSuperNode->hsListOpenNodes.begin(); iHashIterator!= gtneAGraphSuperNode->hsListOpenNodes.end(); iHashIterator++) {
		sTemp += TConverter<int>::ToString(*iHashIterator) + " - ";
	}
	i += 14;
	dcDeviceContext->DrawText(sTemp.c_str(), dXPos, i);

	std::multimap<unsigned int, TEdgeTemp*>::iterator eIterator;
	sTemp = "UEdges: ";
	for(eIterator = gtneAGraphSuperNode->sUnresolvedEdges.begin(); eIterator != gtneAGraphSuperNode->sUnresolvedEdges.end(); ++eIterator) {
		sTemp += TConverter<int>::ToString((*eIterator).second->GetSource()) + " - " +
		         TConverter<int>::ToString((*eIterator).second->GetDestination()) + "  ";
	}

	i += 14;
	dcDeviceContext->DrawText(sTemp.c_str(), dXPos, i);
	if(gtneAGraphSuperNode->bIsLeaf) {
		sTemp = "LEAF";
	} else {
		sTemp = "NOT LEAF";
	}
	i += 14;
	dcDeviceContext->DrawText(sTemp.c_str(), dXPos, i);
};

void TDrawSuperGraph::ConvertDrawingToDeviceCoordinates(double& fDrawingToDeviceX, double& fDrawingToDeviceY)
{
	fDrawingToDeviceX = iHorizontalTranslation
	                    + fDrawingToDeviceX*wxcGraphWidth;
	fDrawingToDeviceY = iVerticalTranslation
	                    + fDrawingToDeviceY*wxcGraphHeight;
};

void TDrawSuperGraph::ConvertDrawingSizeToDeviceSize(double& dWidht, double& dHeight)
{
	dWidht = dWidht*wxcGraphWidth;
	dHeight = dHeight*wxcGraphHeight;
};

bool TDrawSuperGraph::ConvertDeviceToDrawingCoordinates(double& dX, double& dY)
{
	double fTempMaxX = fMaxX;
	double fTempMinX = fMinX;
	double fTempMaxY = fMaxY;
	double fTempMinY = fMinY;
	/*Find the limits of the graph inside the display, in display coordinates*/
	ConvertDrawingToDeviceCoordinates(fTempMaxX,fTempMaxY);
	ConvertDrawingToDeviceCoordinates(fTempMinX,fTempMinY);

	dX = (dX - fTempMinX)/(fTempMaxX - fTempMinX);
	dY = (dY - fTempMinY)/(fTempMaxY - fTempMinY);

	return true;
};

void TDrawSuperGraph::ConvertDeviceSizeToDrawingSize(double& dWidht, double& dHeight)
{
	dWidht = dWidht/(double)wxcGraphWidth;
	dHeight = dHeight/(double)wxcGraphHeight;
};

void TDrawSuperGraph::FocusInSuperNode(TGTreeSuperNodeEntry* gtneASuperNode)
{
	double dScaleFactor;
	double dDeviceWidth = gtneASuperNode->dHorizontalSize;
	double dDeviceHeight = gtneASuperNode->dVerticalSize;
	this->ConvertDrawingSizeToDeviceSize(dDeviceWidth, dDeviceHeight);
	/*Here we calculate the scale factor that will determine the
	  SuperNode as big (as large or as tall) as the screen.*/
	if(this->iDisplayWidth > this->iDisplayHeight) {
		dScaleFactor = iDisplayWidth/dDeviceWidth;
	} else {
		dScaleFactor = iDisplayHeight/dDeviceHeight;
	}

	/*10% decrease to grant a minimum context by not
	  having a full screen SuperNode drawing*/
	dScaleFactor *= 0.9;

	/*Calculate center*/
	double dXPos = gtneASuperNode->GetCenterX();
	double dYPos = gtneASuperNode->GetCenterY();
	this->ConvertDrawingToDeviceCoordinates(dXPos, dYPos);
	/*Perform scale relative to a point*/
	this->SetRelativeScale(dScaleFactor,dScaleFactor, dXPos, dYPos);
	/*Update resolution tracking*/
	SetCurrentResolution(gtneASuperNode->iLevel);
}

void TDrawSuperGraph::FocusOutSuperNode(TGTreeSuperNodeEntry* gtneASuperNode)
{
	double dDeviceWidth;
	double dDeviceHeight;
	double dXPos;
	double dYPos;
	double dScaleFactor;
	if(gtneASuperNode == this->gtGraphTree->GetRoot()) {
		/*Root uses its ownself as reference*/
		dDeviceWidth = gtneASuperNode->dHorizontalSize;
		dDeviceHeight = gtneASuperNode->dVerticalSize;
		SetCurrentResolution(gtneASuperNode->iLevel);
	} else {
		/*Other nodes*/
		dDeviceWidth = gtneASuperNode->gtsneParent->dHorizontalSize;
		dDeviceHeight = gtneASuperNode->gtsneParent->dVerticalSize;
		SetCurrentResolution(gtneASuperNode->gtsneParent->iLevel);
	}

	this->ConvertDrawingSizeToDeviceSize(dDeviceWidth, dDeviceHeight);
	if(this->iDisplayWidth > this->iDisplayHeight) {
		dScaleFactor = iDisplayWidth/dDeviceWidth;
	} else {
		dScaleFactor = iDisplayHeight/dDeviceHeight;
	}

	dScaleFactor *= 0.9;

	if(gtneASuperNode == gtGraphTree->GetRoot()) {
		/*Root uses its ownself as reference*/
		dXPos = gtneASuperNode->GetCenterX();
		dYPos = gtneASuperNode->GetCenterY();
	} else {
		/*Other nodes*/
		dXPos = gtneASuperNode->gtsneParent->GetCenterX();
		dYPos = gtneASuperNode->gtsneParent->GetCenterY();
	}
	this->ConvertDrawingToDeviceCoordinates(dXPos, dYPos);
	this->SetRelativeScale(dScaleFactor,dScaleFactor, dXPos, dYPos);
}

void TDrawSuperGraph::CentralizeDrawingPoint(double dADrawingX, double dADrawingY)
{
	double dDeviceX = dADrawingX;
	double dDeviceY = dADrawingY;
	ConvertDrawingToDeviceCoordinates(dDeviceX, dDeviceY);
	CentralizePoint((int)dDeviceX, (int)dDeviceY);
}

void TDrawSuperGraph::CentralizePoint(int iADeviceX, int iADeviceY)
{
	/*Put (iADeviceX, iADeviceY) at the center of the display device*/
	int iDeviceCenterX, iDeviceCenterY;
	iDeviceCenterX = this->iDisplayWidth/2;
	iDeviceCenterY = this->iDisplayHeight/2;
	this->SetTranslation(-(iADeviceX - iDeviceCenterX),
	                     -(iADeviceY - iDeviceCenterY));
}

void TDrawSuperGraph::DrawBiggestInnerRectangle(TGTreeSuperNodeEntry* gtneAGraphSuperNode)
{
	// TODO empty method
};

bool TDrawSuperGraph::SetShapeForEmphasizedNode(TShapeType::TShapeAcronysm npsAShapeType)
{
	return true;
};


bool TDrawSuperGraph::SetColorForEmphasizedNode(wxColour& wxcAColour)
{
	return true;
};


bool TDrawSuperGraph::SetSizeForEmphasizedNode(double dASize)
{
	return true;
};


double TDrawSuperGraph::GetSizeForEmphasizedNode()
{
	return 1.0;
};


std::string TDrawSuperGraph::GetLabelFromEmphasizedNode()
{
	return "";
};


bool TDrawSuperGraph::SetLabelForEmphasizedNode(std::string sALabel)
{
	return true;
};

TGTreeSuperNodeEntry* TDrawSuperGraph::GetEmphasizedSuperNode()
{
	return gtneEmphasizedNode;
};

void TDrawSuperGraph::SetEmphasizedSuperNode(TGTreeSuperNodeEntry* gtneAGraphSuperNode)
{
	if(dcDeviceContext == NULL) {
		return;
	}
	gtneEmphasizedNode = gtneAGraphSuperNode;
	if(gtneEmphasizedNode != NULL) {
		rcResolutionTracker->SetVisible(true);
		rcResolutionTracker->SetNumberOfTicks(gtneEmphasizedNode->iLevel+1);
		/*Select background parent SuperNode in a focused visualization*/
		if(gtneEmphasizedNode->iLevel < iCurrentResolution) {
			this->FocusInSuperNode(gtneEmphasizedNode);
		}
	} else {
		rcResolutionTracker->SetVisible(false);
	}
};
void TDrawSuperGraph::SetEmphasizedSuperEdge(TPt<TGTreeSuperEdge> gtseAGraphSuperEdge)
{
	if(gtseAGraphSuperEdge == TGTreeSuperEdge::gtseNULLSuperEdge) {
		gtseEmphasizedSuperEdge = TGTreeSuperEdge::gtseNULLSuperEdge;
		return;
	}
	/*Previous emphasized SuperEdge*/
	if(gtseEmphasizedSuperEdge != TGTreeSuperEdge::gtseNULLSuperEdge) {
		gtseEmphasizedSuperEdge->SetEmphasis(false);
	}
	gtseEmphasizedSuperEdge = gtseAGraphSuperEdge;
	gtseEmphasizedSuperEdge->SetEmphasis(true);
}
TPt<TGTreeSuperEdge> TDrawSuperGraph::GetEmphasizedSuperEdge()
{
	return gtseEmphasizedSuperEdge;
}
bool TDrawSuperGraph::IsMouseOverResolutionTracker(int iAX, int iAY)
{
	bool bReturn = false;
	if(rcResolutionTracker == NULL) {
		return bReturn;
	} else {
		bReturn = rcResolutionTracker->IsMouseOver(iAX, iAY);
		return bReturn;
	}
};
void TDrawSuperGraph::AnimatedFocusInSuperNode(int iLevelToFocus, TGTreeSuperNodeEntry* gtsneASuperNodeToFocus, bool bExpandAll)
{
#ifndef PERFORMANCE
	TFrameDrawSuperGraph< TNodeNetTests<TNodePlot, TEdgePlot> > *fgsgTemp = (TFrameDrawSuperGraph< TNodeNetTests<TNodePlot, TEdgePlot> >*)this->fdsgFrameForLeafSuperNode;
#else
	TFrameDrawSuperGraph< TNodeNetTests<TNodePerformance, TEdgePlot> > *fgsgTemp = (TFrameDrawSuperGraph< TNodeNetTests<TNodePerformance, TEdgePlot> >*)this->fdsgFrameForLeafSuperNode;
#endif
	fgsgTemp->DisableMouse();
	if(bExpandAll) {
		gtGraphTree->ExpandAllParents(gtsneASuperNodeToFocus);
		fgsgTemp->Refresh();
		wxMilliSleep(800);
	}
	int iFocusTransitions = iLevelToFocus - iCurrentResolution;
	if(iFocusTransitions == 0) { /*Nothing to do*/
		this->SetEmphasizedSuperNode(gtsneASuperNodeToFocus);
		fgsgTemp->EnableMouse();
		return;
	}
	TGTreeSuperNodeEntry* gtsneTemp = NULL;
	TGTreeSuperNodeEntry* gtsneCurrent = NULL;
	int iToGetToCurrent;
	if(iFocusTransitions > 0) { /*Way down the tree*/
		/*iFocusTransitions transitions*/
		for(int iCounter = 0; iCounter < iFocusTransitions; iCounter++) {
			gtsneTemp = gtsneASuperNodeToFocus;
			/*Make the way back from the reference supernode up the tree*/
			for(int iCounter2 = (gtsneASuperNodeToFocus->iLevel-1); iCounter2 > iCurrentResolution; iCounter2--) {
				gtsneTemp = gtsneTemp->gtsneParent;
			}
			FocusInSuperNode(gtsneTemp);  /*Forces iCurrentResolution update */
			fgsgTemp->Refresh();
			wxMilliSleep(800);
		}
	} else {	/*Way up the tree - simpler*/
		iFocusTransitions = ABS(iFocusTransitions);

		/*Find the SuperNode that currently has the focus (goes up the tree)*/
		/*The focus node is the biggest SuperNode currently in the screen*/
		gtsneCurrent = gtsneASuperNodeToFocus;
		iToGetToCurrent = (gtsneASuperNodeToFocus->iLevel) - iCurrentResolution;
		for(int iCounter = 0; iCounter < iToGetToCurrent; iCounter++) {
			gtsneCurrent = gtsneCurrent->gtsneParent;
		}
		if(iToGetToCurrent >= 0) {
			/*Focus successively on the SuperNodes up the tree
			up to the iFocusTransitions*/
			gtsneTemp = gtsneCurrent;
			for(int iCounter = 0; iCounter < iFocusTransitions; iCounter++) {
				gtsneTemp = gtsneTemp->gtsneParent;
				FocusInSuperNode(gtsneTemp);
				fgsgTemp->Refresh();
				wxMilliSleep(800);
			}
		} else { /*SuperNode to focus is out of sight*/
			gtsneTemp = this->GetEmphasizedSuperNode();
			/*Up to current SuperNode relative to EmphasizedSuperNode*/
			for(int iCounter = 0; iCounter < gtsneTemp->iLevel - iCurrentResolution; iCounter++) {
				gtsneTemp = gtsneTemp->gtsneParent;
			}

			while(gtsneTemp != gtsneASuperNodeToFocus) {
				gtsneTemp = gtsneTemp->gtsneParent;
				FocusInSuperNode(gtsneTemp);
				fgsgTemp->Refresh();
				wxMilliSleep(800);
			}
		}
	}
	fgsgTemp->EnableMouse();
	this->SetEmphasizedSuperNode(gtsneASuperNodeToFocus);
}
void TDrawSuperGraph::UpdateResolutionTracker(int iAX, int iAY)
{
	if(gtneEmphasizedNode == NULL) {
		return;
	}
	rcResolutionTracker->SetVisible(true);
	rcResolutionTracker->ProcessClick(iAX, iAY);
	AnimatedFocusInSuperNode(rcResolutionTracker->GetCurrentTick(),
	                         gtneEmphasizedNode);

};
void TDrawSuperGraph::SetCurrentResolution(int iACurrentResolution)
{
	iCurrentResolution = iACurrentResolution;
	rcResolutionTracker->SetCurrentTick(iCurrentResolution);
};
int TDrawSuperGraph::GetCurrentResolution()
{
	return iCurrentResolution;
};

void TDrawSuperGraph::SetGlobalBrushColor(const unsigned char red, const unsigned char green, const unsigned char blue)
{
	this->wxbDrawingBrush->SetColour(wxColour(red, green, blue));
	this->dcDeviceContext->SetBrush(*this->wxbDrawingBrush);
	this->dcDeviceContext->GetGraphicsContext()->SetBrush(*this->wxbDrawingBrush);
};

#endif
