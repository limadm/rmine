#pragma once
#ifndef _FrameDrawSuperGraphEventsEnumeration_
#define _FrameDrawSuperGraphEventsEnumeration_

enum{
   DrawSuperGraph_EnableSuperGraphEdition = wxID_HIGHEST + 201,
   DrawSuperGraph_EnableConnectivityOnExpand,
   DrawSuperGraph_EnableAutoFocus,
   DrawSuperGraph_ShowSuperEdges,
   DrawSuperGraph_ResetView,
   DrawSuperGraph_SetExpandCollapseNode,
   DrawSuperGraph_PickSuperNode,
   DrawSuperGraph_TomahawkPerspective,
   DrawSuperGraph_ConnectivityOnDemand,
   DrawSuperGraph_LabelQuery,
   DrawSuperGraph_DeleteConnectivitySuperEdge,
   DrawGraph_SetSuperNodeLabel,
   DrawGraph_TrackNodeForeignNeighbors,
   DrawGraph_NextLabelOccurrence,
   DrawSuperGraph_SuperGraphButton,
   DrawSuperGraph_ConnectivityTests,
   DrawSuperGraph_ForeignNeighborsTests
};

#endif