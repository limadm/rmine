#pragma once
#ifdef _SUPERGRAPHCOMPILATION

#ifndef _FrameDrawSuperGraph_
#define _FrameDrawSuperGraph_

#include <Commons/stdafx.h>

#include <algorithm>
#include <vector>

#include <wx/cmndata.h>

#include <Commons/Auxiliar.h>
#include <Frames/FrameDrawGraph.h>
#include <Layouts/LayoutBiPartite.h>
#include <SuperGraph/DrawSuperGraph.h>
#include <SuperGraph/FrameDrawSuperGraphEventsEnumeration.h>

namespace font {
	extern wxFont sNodes;
	extern wxFont sEdges;
}

template <class _TNodeNet>
class TFunctorDelSuperEdgeGraphWindow;

template <class _TNodeNet>
class TFunctorFocusUpdaterForListBox;

template <class _TNodeNet>
class TFrameDrawSuperGraph : public TFrameDrawGraph<_TNodeNet>
{
public:
	TFrameDrawSuperGraph(wxMDIParentFrame *parent, const wxPoint& wxpAPosition, const wxSize& wxsAsize, std::string sAPathToGTreeData);
	~TFrameDrawSuperGraph();
	bool BuildGTree();
	bool LoadBuildGTree();
	void Refresh();
	void DrawLeafSuperNodeData(TGTreeSuperNodeEntry* gtneASuperNode, wxGCDC *vDrawContext);
	bool SucceessfullyLoaded();
	bool Show(bool show = true);
	void EnableMouse();
	void DisableMouse();
private:
	TGTree *gtGraphTree;
	TDrawSuperGraph *dsgDrawer;
	std::string sPathToGTreeData;
	TGTreeSuperNodeEntry* gtnePickedNodeTarget;
	std::vector< TFrameDrawGraph<_TNodeNet>* > vSuperEdgeSubBraphWindows;
	TFunctorDelSuperEdgeGraphWindow<_TNodeNet>* fdsewTemp;
	wxMenu *wxmEditSuperGraph;
	TDialogListBoxOptions *dlboTemp;
	TFunctorFocusUpdaterForListBox<_TNodeNet>* ffufTemp;
	bool bSucceessfullyLoaded;
	bool bConnectivityInstructions;
	std::string sLabelQueryBuffer;
	TGTreeSuperNodeEntry* gtsneLastEmphasizedSuperNode;
#ifdef NOTIGNORE
	TTestGTreeDisk *tgTest;
#endif
	wxBitmapButton *wxbbSuperGraph;
	bool bMouseEnabled;
	int iLabelQueryOccurrencesCounter;
private:
	void Init();
	void CreateSuperEdgeGraphWindow(TPt<TGTreeSuperEdge>);
	void SwitchOperationsSupportedOnlyBySubGraphs(bool bAOption);
	void SwitchOperationsNotSupportedBySubGraphs(bool bAOption);
	TGTreeSuperNodeEntry* GetEmphasizedSuperNode();
	void SetEmphasizedSuperNode(TGTreeSuperNodeEntry* gtneASuperNode);
	void InitializeControls();
	bool FocusInGraphNode(std::string sAGraphNodeLabel, TGTreeSuperNodeEntry* gtsneReference = NULL, int iNthOccurrence = 1);
	void FocusSuperNode(TGTreeSuperNodeEntry* gtneIdentifiedSuperNode);
	void OnClickNextLabelOccurrence(wxCommandEvent& event);
	void OnClickLabelQuery(wxCommandEvent& event);
	virtual void OnPaint(wxPaintEvent& event);
	void OnShow(wxShowEvent& event);
	void OnReSize(wxSizeEvent& event);
	void OnContextMenu(wxContextMenuEvent& event);
	void OnClickEnableNodesEdition(wxCommandEvent& event);
	void OnClickEnableGraphEdition(wxCommandEvent& event);
	void OnClickEnableSuperGraphEdition(wxCommandEvent& event);
	void OnClickNodeButton(wxCommandEvent& event);
	void OnClickGraphButton(wxCommandEvent& event);
	void OnClickSuperGraphButton(wxCommandEvent& event);
	void EnableNodesEdition(wxCommandEvent& event = wxCommandEvent(wxEVT_NULL));
	void EnableGraphEdition(wxCommandEvent& event = wxCommandEvent(wxEVT_NULL));
	void EnableSuperGraphEdition(wxCommandEvent& event = wxCommandEvent(wxEVT_NULL));
	void OnClickSetNodeLabel(wxCommandEvent& event);
	void OnClickTrackNodeNeighbors(wxCommandEvent& event);
	void OnClickSuperNodeConnectivityTests(wxCommandEvent& event);
	void OnClickForeignNeighborsTests(wxCommandEvent& event);
	void OnClickResetView(wxCommandEvent& event);
	void RefreshLayout(wxCommandEvent& event);
	void ExpandCollapseNode(wxMouseEvent& event, TGTreeSuperNodeEntry* gtneIdentifiedSuperNode);

	void OnClickCloseWindow(wxCommandEvent& event) {
		event.Skip();
		this->Close();
	};

	void OnMove(wxMoveEvent& event) {
		event.Skip();
		this->Refresh();
	};

	void OnSetFocus(wxFocusEvent& event) {
		event.Skip();
		this->Refresh();
	}
	void OnKillFocus(wxFocusEvent& event);
	void OnMouseEvent(wxMouseEvent& event);
	void OnClose(wxCloseEvent& event);

	DECLARE_EVENT_TABLE()

	/********wxWidgets stuf, ignore if you do not intend to deal with interface events********/
	/*static const wxEventTableEntry sm_eventTableEntries[];
	static const wxEventTable sm_eventTable;
	virtual const wxEventTable* GetEventTable() const;
	static wxEventHashTable sm_eventHashTable;
	virtual wxEventHashTable& GetEventHashTable() const;*/
	/****************************************************************************************/
	friend class TFunctorDelSuperEdgeGraphWindow<_TNodeNet>;
	friend class TFunctorFocusUpdaterForListBox<_TNodeNet>;
};

template <class _TNodeNet>
TFrameDrawSuperGraph<_TNodeNet>::TFrameDrawSuperGraph(wxMDIParentFrame *parent, const wxPoint& wxpAPosition, const wxSize& wxsAsize, std::string sAPathToGTreeData)
	: TFrameDrawGraph<_TNodeNet>(parent, wxpAPosition, wxsAsize, NULL)
{
#ifdef NOTIGNORE
	tgTest = new TTestGTreeDisk();
#endif
	dsgDrawer = NULL;
	wxmEditSuperGraph = NULL;
	gtnePickedNodeTarget = NULL;
	sLabelQueryBuffer = "";
	fdsewTemp = NULL;
	dlboTemp = NULL;
	ffufTemp = NULL;
	bConnectivityInstructions = true;
	bMouseEnabled = true;
	sPathToGTreeData = sAPathToGTreeData;
	wxbbSuperGraph = NULL;
	gtsneLastEmphasizedSuperNode = NULL;
	iLabelQueryOccurrencesCounter = 1;
};

template <class _TNodeNet>
TFrameDrawSuperGraph<_TNodeNet>::~TFrameDrawSuperGraph()
{
	typename std::vector< TFrameDrawGraph<_TNodeNet> *>::iterator x;
	for (x = vSuperEdgeSubBraphWindows.begin(); x < vSuperEdgeSubBraphWindows.end(); ++x) {
		delete (*x)->diDrawer;
		delete (*x);
	}
	vSuperEdgeSubBraphWindows.clear();
	if (bSucceessfullyLoaded) {
		wxMessageDialog dialog( NULL, _T("Save Graph-tree?"),_T("Confirmation:"), wxNO_DEFAULT|wxYES_NO);
		if (dialog.ShowModal() == wxID_YES) {
			gtGraphTree->Save();
			//gtGraphTree->SaveTreeBuildToDir();
		}
	}
	if (dsgDrawer != NULL) {
		delete dsgDrawer;
	}
	if (gtGraphTree != NULL) {
		delete gtGraphTree;
	}
	if (dlboTemp != NULL) {
		delete dlboTemp;
	}
	if (ffufTemp != NULL) {
		delete ffufTemp;
	}
	if (fdsewTemp != NULL) {
		delete fdsewTemp;
	}
}

template <class _TNodeNet>
void TFrameDrawSuperGraph<_TNodeNet>::Init()
{
	if (bSucceessfullyLoaded) {
		dsgDrawer = new TDrawSuperGraph(gtGraphTree);
		dsgDrawer->SetFrame(this);
		dsgDrawer->SetInterfaceForLeafSuperNodes(this);
		/*This avoids the message to confirm saving edition and
		layout information in FrameDrawGraph. For SuperGraphs we automatically
		save this information for every subgraph by default.*/
		this->bConfirmSaveModifications = false;
		fdsewTemp = new TFunctorDelSuperEdgeGraphWindow<_TNodeNet > ();
		dlboTemp = new TDialogListBoxOptions(this, "Choose data for query", 300, 100);
		ffufTemp = new TFunctorFocusUpdaterForListBox<_TNodeNet > ();
	}
}

template <class _TNodeNet>
bool TFrameDrawSuperGraph<_TNodeNet>::SucceessfullyLoaded()
{
	return bSucceessfullyLoaded;
}

template <class _TNodeNet>
bool TFrameDrawSuperGraph<_TNodeNet>::Show(bool show = true)
{
	bool bShowStatus;
	if (show) {
		if (this->dsgDrawer == NULL) {
			return false;
		}
		bShowStatus = TFrameDrawGraph<_TNodeNet>::Show(show);
		if (bShowStatus && show) {
			this->InitializeControls();
		}
	} else { /*Show(false)*/
		bShowStatus = TFrameDrawGraph<_TNodeNet>::Show(show);
	}
	return bShowStatus;
}

/*-----------------------------------------------------------*/
template <class _TNodeNet>
bool TFrameDrawSuperGraph<_TNodeNet>::BuildGTree()
{
	CpuTimeLog log("Frame::BuildGTree");
	gtGraphTree = new TGTree(sPathToGTreeData);
	if (gtGraphTree->BuildTreeFromPartitionDirectory()) {
		gtGraphTree->ApplyTreeMap();
		gtGraphTree->ApplyDistributionLayout();
		gtGraphTree->GetRoot()->SetExpanded(true);
		bSucceessfullyLoaded = true;
		Init();
	} else {
		TDialogWarning dwWarning("This directory does not contain SuperGraph data.", "Wrong data source");
		bSucceessfullyLoaded = false;
	}
	return bSucceessfullyLoaded;
}

template <class _TNodeNet>
bool TFrameDrawSuperGraph<_TNodeNet>::LoadBuildGTree()
{
	CpuTimeLog log("Frame::LoadGTree");
	gtGraphTree = new TGTree(sPathToGTreeData);

	//if (gtGraphTree->LoadTreeBuildFromDir()) {
	try {
		gtGraphTree->Load();
		gtGraphTree->GetRoot()->SetExpanded(true);
		bSucceessfullyLoaded = true;
#ifndef PERFORMANCE
		Init();
#endif
		//} else {
	} catch(std::exception) {
		TDialogWarning dwWarning("This directory does not contain pre build SuperGraph data. Please, first run File->Build and draw super graph dir", "Wrong data source");
		bSucceessfullyLoaded = false;
	}
	return bSucceessfullyLoaded;
}

/*-----------------------------------------------------------*/
template <class _TNodeNet>
void TFrameDrawSuperGraph<_TNodeNet>::Refresh()
{
	if (dsgDrawer == NULL) {
		return;
	}
	wxMDIChildFrame::Refresh();
}

template <class _TNodeNet>
void TFrameDrawSuperGraph<_TNodeNet>::OnPaint(wxPaintEvent& event)
{
	event.Skip();
	if (this->dsgDrawer == NULL) {
		return;
	}

	wxAutoBufferedPaintDC deviceContext(this);
	wxGCDC graphics(deviceContext);

	graphics.SetBackground(*wxWHITE_BRUSH);
	graphics.Clear();

	dsgDrawer->bAutoFocus         = wxmEditSuperGraph->IsChecked(DrawSuperGraph_EnableAutoFocus);
	dsgDrawer->bVisibleSuperEdges = wxmEditSuperGraph->IsChecked(DrawSuperGraph_ShowSuperEdges );
	dsgDrawer->DrawSuperGraph(&graphics);

	/*Interaction with emphasized SuperNode subgraph is possible
	  due to the following code and to ancestor call
	  "TFrameDrawGraph<_TNodeNet>::OnMouseEvent(event);"
	  at TFrameDrawSuperGraph<_TNodeNet>::OnMouseEvent
	 */
	if (this->GetEmphasizedSuperNode() != NULL) {
		if (this->GetEmphasizedSuperNode()->bIsLeaf &&
		        this->GetEmphasizedSuperNode()->IsExpanded()) {
			/*In other words, we have interaction only when
			a subgraph has been selected*/
			if (this->GetEmphasizedSuperNode()->pNodeNetSubGraph.GetRefs() > 0) {
				this->SetGraphData(this->GetEmphasizedSuperNode()->pNodeNetSubGraph);
			}
			if (this->GetEmphasizedSuperNode()->diDrawer != NULL) {
				this->SetDrawer(this->GetEmphasizedSuperNode()->diDrawer);
				// this->diDrawer->DrawGraph(&wxpdcSuperGraphDeviceContext);
			}
			return;
		}
	}
	/*Force subgraph data to NULL -  no subgraph has been selected*/
#ifndef PERFORMANCE
	TPt< TNodeNetTests<TNodePlot, TEdgePlot> > pNodeNetTemp(NULL);
#else
	TPt< TNodeNetTests<TNodePerformance, TEdgePlot> > pNodeNetTemp(NULL);
#endif
	this->SetGraphData(pNodeNetTemp);
	this->SetDrawer(NULL);
};

template <class _TNodeNet>
void TFrameDrawSuperGraph<_TNodeNet>::OnClickNextLabelOccurrence(wxCommandEvent& event)
{
	if (sLabelQueryBuffer.compare("") == 0) {
		OnClickLabelQuery(event);
	} else {
		iLabelQueryOccurrencesCounter++;
		if (!FocusInGraphNode(sLabelQueryBuffer, dsgDrawer->GetEmphasizedSuperNode(), iLabelQueryOccurrencesCounter)) {
			/*Did not find it*/
			iLabelQueryOccurrencesCounter = 1;
			sLabelQueryBuffer = "";
		}
	}
}

template <class _TNodeNet>
void TFrameDrawSuperGraph<_TNodeNet>::OnClickLabelQuery(wxCommandEvent& event)
{
	if (this->dsgDrawer == NULL) {
		return;
	}
	/*If there are too many items, we use an edit box to type the label to be searched.*/
	if (gtGraphTree->GetTotalNodes() > 1000) {
		wxTextEntryDialog wxedTemp(this, "Type label to look for. Press F3 for next occurrence.", "Type-in label dialog");
		wxedTemp.SetValue(sLabelQueryBuffer.c_str());
		if (wxedTemp.ShowModal() == wxID_OK) {
			/*Data for next label occurrence (F3)*/
			iLabelQueryOccurrencesCounter = 1;
			sLabelQueryBuffer = wxedTemp.GetValue();
			if (dsgDrawer->GetEmphasizedSuperNode() == NULL) {
				dsgDrawer->SetEmphasizedSuperNode(gtsneLastEmphasizedSuperNode);
			}
			FocusInGraphNode(sLabelQueryBuffer, dsgDrawer->GetEmphasizedSuperNode(), iLabelQueryOccurrencesCounter);
		}
	} else { /*Otherwise, we use a list box for picking up the label*/
		wxArrayString wxasTemp;
		for (int iCounter = 0; iCounter < gtGraphTree->GetTotalNodes(); iCounter++) {
			wxasTemp.Add(gtGraphTree->GetIthNodeLabelFromParallelLabelIndex(iCounter).c_str());
		}
		this->dlboTemp->fufsUpdater = this->ffufTemp;
		this->dlboTemp->fufsUpdater->SetInterfaceToBeUpdated(this);
		this->dlboTemp->SetOptions(wxasTemp);
		this->dlboTemp->Show();
	}
}

template <class _TNodeNet>
void TFrameDrawSuperGraph<_TNodeNet>::OnKillFocus(wxFocusEvent& event)
{
	event.Skip();
	this->Refresh();
};

template <class _TNodeNet>
void TFrameDrawSuperGraph<_TNodeNet>::FocusSuperNode(TGTreeSuperNodeEntry* gtneIdentifiedSuperNode)
{
	TGTreeSuperNodeEntry* gtsneParent = gtneIdentifiedSuperNode->gtsneParent;
	if (gtsneParent != NULL && gtsneParent != gtGraphTree->GetRoot()) {
		dsgDrawer->SetEmphasizedSuperEdge(NULL);
		if (gtsneParent->gtsneFocusedNode != NULL) {
			gtsneParent->gtsneFocusedNode = NULL;
			gtGraphTree->ApplyDistributionLayoutRecursive(gtsneParent);
		} else {
			gtsneParent->gtsneFocusedNode = gtneIdentifiedSuperNode;
			gtneIdentifiedSuperNode->dX = gtsneParent->dX;
			gtneIdentifiedSuperNode->dY = gtsneParent->dY;
			gtneIdentifiedSuperNode->dHorizontalSize = gtsneParent->dHorizontalSize;
			gtneIdentifiedSuperNode->dVerticalSize   = gtsneParent->dVerticalSize;
			gtGraphTree->ApplyDistributionLayoutRecursive(gtneIdentifiedSuperNode);
		}
	}
}

template <class _TNodeNet>
void TFrameDrawSuperGraph<_TNodeNet>::OnMouseEvent(wxMouseEvent& event)
{
	//event.Skip();
	if (!bMouseEnabled) {
		return;
	}
	if (dsgDrawer == NULL) {
		return;
	}

	/*Resolution tracker - works when there is a SuperNode Emphasized*/
	if (this->GetEmphasizedSuperNode() != NULL) {
		if (dsgDrawer->IsMouseOverResolutionTracker(event.GetPosition().x, event.GetPosition().y)) {
			if (event.LeftDown()) {
				dsgDrawer->UpdateResolutionTracker(event.GetPosition().x, event.GetPosition().y);
				this->Refresh();
			}
			return;
		}
	}

	/*SuperGraph edtion is not enabled?*/
	if (!wxmEditSuperGraph->IsChecked(DrawSuperGraph_EnableSuperGraphEdition)) {
		/*We send the processing to FrameDrawGraph ancestor for subgraph interaction processing*/
		TFrameDrawGraph<_TNodeNet>::OnMouseEvent(event);
		return;
	}

	TGTreeSuperNodeEntry* gtneIdentifiedSuperNode;
	/*SuperGraph interaction*/
	TPt<TGTreeSuperEdge> gtnseTemp;
	if (event.LeftDown()) {
		gtnseTemp = dsgDrawer->GetEmphasizedSuperEdge();
		if (gtnseTemp == TGTreeSuperEdge::gtseNULLSuperEdge
			|| !gtnseTemp->IsMouseOver(event.GetPosition().x, event.GetPosition().y, dsgDrawer)) {

			gtneIdentifiedSuperNode = dsgDrawer->IdentifySuperNode(event.GetPosition().x, event.GetPosition().y);

			if (wxmEditSuperGraph->IsChecked(DrawSuperGraph_PickSuperNode)) {
				bool newIdSuperNode = gtneIdentifiedSuperNode != GetEmphasizedSuperNode();
				this->SetEmphasizedSuperNode(gtneIdentifiedSuperNode);
				if (gtneIdentifiedSuperNode != NULL && gtneIdentifiedSuperNode != gtGraphTree->GetRoot()) {
					if (event.ControlDown()) {
						FocusSuperNode(gtneIdentifiedSuperNode);
					}
					if (newIdSuperNode) {
						gtnseTemp = dsgDrawer->GetEmphasizedSuperEdge();
						if (gtnseTemp != TGTreeSuperEdge::gtseNULLSuperEdge
							&& gtnseTemp->GetSuperNode1() != gtneIdentifiedSuperNode
							&& gtnseTemp->GetSuperNode2() != gtneIdentifiedSuperNode) {
								gtnseTemp->SetEmphasis(false);
								dsgDrawer->SetEmphasizedSuperEdge(TGTreeSuperEdge::gtseNULLSuperEdge);
						}
					}
					this->Refresh();
				}
			} else if (wxmEditSuperGraph->IsChecked(DrawSuperGraph_ConnectivityOnDemand)) {
				if (event.ShiftDown()) { /*On shift set target for connectivity (source-target)*/
					if ((this->GetEmphasizedSuperNode() != NULL) && (gtneIdentifiedSuperNode != NULL)) {
						this->GetEmphasizedSuperNode()->AddConnectivitySuperEdge(gtneIdentifiedSuperNode, gtGraphTree);
						this->Refresh();
					}
				} else {
					this->SetEmphasizedSuperNode(gtneIdentifiedSuperNode);
					if (this->GetEmphasizedSuperNode() != NULL) {
						this->Refresh();
					}
				}
			} else if (wxmEditSuperGraph->IsChecked(DrawSuperGraph_SetExpandCollapseNode)) {
				this->ExpandCollapseNode(event, gtneIdentifiedSuperNode);
			} else if (wxmEditSuperGraph->IsChecked(DrawSuperGraph_TomahawkPerspective)) {
				this->SetEmphasizedSuperNode(gtneIdentifiedSuperNode);
				if (this->GetEmphasizedSuperNode() != NULL) {
					dsgDrawer->ExpandNodeTomaHawk(this->GetEmphasizedSuperNode());
					this->Refresh();
				}
			} else if (wxmEditSuperGraph->IsChecked(DrawSuperGraph_DeleteConnectivitySuperEdge)) {
				gtnseTemp = dsgDrawer->IdentifySuperEdge(event.GetPosition().x, event.GetPosition().y);
				if (gtnseTemp != NULL) {
					if (!gtnseTemp->GetSuperNode1()->DeleteConnectivitySuperEdge(gtnseTemp)) {
						gtnseTemp->GetSuperNode2()->DeleteConnectivitySuperEdge(gtnseTemp);
					}
					gtnseTemp = NULL;
					dsgDrawer->SetEmphasizedSuperEdge(gtnseTemp);
					this->Refresh();
				}
			}
		}
		this->iIntialTranlationX = event.GetPosition().x;
		this->iIntialTranlationY = event.GetPosition().y;
	} else if (event.LeftDClick()) {
		wxSetCursor(*wxHOURGLASS_CURSOR);
		gtnseTemp = dsgDrawer->GetEmphasizedSuperEdge();
		if (gtnseTemp != NULL &&
			!gtnseTemp->IsMouseOver(event.GetPosition().x,
									event.GetPosition().y,
									dsgDrawer)) {
			gtnseTemp = dsgDrawer->IdentifySuperEdge(event.GetPosition().x, event.GetPosition().y);
		}
		if (gtnseTemp != NULL) {
			this->SetEmphasizedSuperNode(NULL);
			dsgDrawer->SetEmphasizedSuperEdge(gtnseTemp);
			CreateSuperEdgeGraphWindow(gtnseTemp);
		} else {
			gtneIdentifiedSuperNode = dsgDrawer->IdentifySuperNode(event.GetPosition().x, event.GetPosition().y);
			this->ExpandCollapseNode(event, gtneIdentifiedSuperNode);
		}
		wxSetCursor(*wxSTANDARD_CURSOR);
	} else if (event.Dragging()) {
		if (wxmEditSuperGraph->IsChecked(DrawSuperGraph_PickSuperNode)) {
			dsgDrawer->TranslateEmphasizedNode(event.GetPosition().x - this->iIntialTranlationX,
			                                   event.GetPosition().y - this->iIntialTranlationY);
		}
		this->iIntialTranlationX = event.GetPosition().x;
		this->iIntialTranlationY = event.GetPosition().y;
		this->Refresh();
	} else if (event.Moving()) {
		gtnseTemp = dsgDrawer->IdentifySuperEdge(event.GetPosition().x, event.GetPosition().y);
		if (gtnseTemp != NULL) {
			if (gtnseTemp != dsgDrawer->GetEmphasizedSuperEdge()) {
				/*New emphasized SuperEdge*/
				dsgDrawer->SetEmphasizedSuperEdge(gtnseTemp);
				/*Redraw*/
				this->Refresh();
			}
		}
	}
}

template <class _TNodeNet>
void TFrameDrawSuperGraph<_TNodeNet>::ExpandCollapseNode(wxMouseEvent& event, TGTreeSuperNodeEntry* gtneIdentifiedSuperNode)
{
	if (gtneIdentifiedSuperNode != NULL
	        && gtneIdentifiedSuperNode != this->gtGraphTree->GetRoot()) {		
		if ((event.LeftDClick() && gtneIdentifiedSuperNode->IsExpanded())
			|| (event.LeftDown() && event.ShiftDown())) { // if (event.ShiftDown()) { /*Collapse*/
			dsgDrawer->ExpandNode(gtneIdentifiedSuperNode, false);
			if (wxmEditSuperGraph->IsChecked(DrawSuperGraph_EnableAutoFocus)) {
				FocusSuperNode(gtneIdentifiedSuperNode);
			}
			if (wxmEditSuperGraph->IsChecked(DrawSuperGraph_EnableConnectivityOnExpand)) {
				TGTreeNode & gtnSubTree = gtneIdentifiedSuperNode->gtnSubTree;
				for (TGTreeNode::iterator i = gtnSubTree.begin(); i != gtnSubTree.end(); ++i) {
					i->mmConnectivitySuperEdges.clear();
				}
			}
		} else { /*Expand*/
			/*Already expanded, we pick it for dragging*/
			if (gtneIdentifiedSuperNode->IsExpanded()) {
				wxmEditSuperGraph->Check(DrawSuperGraph_PickSuperNode, true);
			}/*Not expanded, expand it*/
			else {
				dsgDrawer->ExpandNode(gtneIdentifiedSuperNode, true);
				if (wxmEditSuperGraph->IsChecked(DrawSuperGraph_EnableAutoFocus)) {
					FocusSuperNode(gtneIdentifiedSuperNode);
				}
				// automatically compute connectivities between each son and each sibling
				if (wxmEditSuperGraph->IsChecked(DrawSuperGraph_EnableConnectivityOnExpand)) {
					CpuTimeLog log("Frame::ConnectivityOnDemand");
					//gtneIdentifiedSuperNode->DeleteConnectivitySuperEdgesRecursive(gtGraphTree->GetRoot());
					gtneIdentifiedSuperNode->AddConnectivitySuperEdgesRecursive(gtGraphTree->GetRoot(), gtGraphTree);
					this->Refresh();
				}
			}
		}
	}
	this->SetEmphasizedSuperNode(gtneIdentifiedSuperNode);
}

template <class _TNodeNet>
void TFrameDrawSuperGraph<_TNodeNet>::DrawLeafSuperNodeData(TGTreeSuperNodeEntry* gtneALeafSuperNode, wxGCDC *vDrawContext)
{
	/*This FrameDrawSuperGraph descends from FrameDrawGraph and can
	  also draw a simple graph. We use this feature to draw the subgraphs
	  one at a time at every redraw cycle.*/
	/*Set the graph data to draw at this time*/
	this->SetGraphData(gtneALeafSuperNode->pNodeNetSubGraph);
	/*Set the drawer object that draws over FrameDrawSuperGraph:FrameDrawGraph*/
	this->SetDrawer(gtneALeafSuperNode->diDrawer);
	/*Draw*/
	this->diDrawer->DrawGraph(vDrawContext);
}

template <class _TNodeNet>
TGTreeSuperNodeEntry* TFrameDrawSuperGraph<_TNodeNet>::GetEmphasizedSuperNode()
{
	return dsgDrawer->GetEmphasizedSuperNode();
}

template <class _TNodeNet>
void TFrameDrawSuperGraph<_TNodeNet>::SetEmphasizedSuperNode(TGTreeSuperNodeEntry* gtneASuperNode)
{
	dsgDrawer->SetEmphasizedSuperNode(gtneASuperNode);
	this->Refresh();
	if (gtneASuperNode == NULL) {
		wxmEditSuperGraph->Enable(DrawGraph_SetSuperNodeLabel, false);
		this->EnableSuperGraphEdition();
	} else {
		wxmEditSuperGraph->Enable(DrawGraph_SetSuperNodeLabel, true);
	}
}

template <class _TNodeNet>
void TFrameDrawSuperGraph<_TNodeNet>::SwitchOperationsSupportedOnlyBySubGraphs(bool bAOption)
{
	this->menuBar->EnableTop(this->menuBar->FindMenu("Graph"), !bAOption);
	this->menuBar->EnableTop(this->menuBar->FindMenu("Nodes"), !bAOption);
	this->menuBar->EnableTop(this->menuBar->FindMenu("Actions"), !bAOption);
	this->menuBar->EnableTop(this->menuBar->FindMenu("Plot"), !bAOption);
	this->menuBar->Refresh();
}

template <class _TNodeNet>
void TFrameDrawSuperGraph<_TNodeNet>::SwitchOperationsNotSupportedBySubGraphs(bool bAOption)
{
	this->wxmEditGraph->Enable(DrawGraph_Scale_Down, bAOption);
	this->wxmEditGraph->Enable(DrawGraph_Scale_Up, bAOption);
	this->wxmEditGraph->Enable(DrawGraph_Translate, bAOption);
	this->wxmEditGraph->Enable(DrawGraph_AddEdge, false);
	this->wxmEditGraph->Enable(DrawGraph_DeleteEdge, false);
	this->wxmEditNodes->Enable(DrawGraph_AddNode, false);
	this->wxmEditNodes->Enable(DrawGraph_DeleteNode, false);
}

template <class _TNodeNet>
void TFrameDrawSuperGraph<_TNodeNet>::OnShow(wxShowEvent& event)
{
	event.Skip();
	if (dsgDrawer == NULL) {
		return;
	}
	this->wxcOldWidth = this->GetClientSize().GetWidth();
	this->wxcOldHeight = this->GetClientSize().GetHeight();
	dsgDrawer->SetWidthAndHeight(this->GetSize().GetWidth(), this->GetSize().GetHeight());
};

template <class _TNodeNet>
void TFrameDrawSuperGraph<_TNodeNet>::OnReSize(wxSizeEvent& event)
{
	event.Skip();
	if (dsgDrawer == NULL) {
		return;
	}
	wxMDIChildFrame::OnSize(event);
	dsgDrawer->SetAbsoluteScale(((double) this->GetSize().GetWidth()) / (double) this->wxcOldWidth,
	                            ((double) this->GetSize().GetHeight()) / (double) this->wxcOldHeight);
	this->wxcOldWidth = this->GetSize().GetWidth();
	this->wxcOldHeight = this->GetSize().GetHeight();
	if (this->wxpButtonsPanel != NULL) {
		this->wxpButtonsPanel->Move(this->GetClientSize().GetWidth() - this->wxpButtonsPanel->GetSize().GetWidth() - 10,
		                            this->GetClientSize().GetHeight() - this->wxpButtonsPanel->GetSize().GetHeight() - 10);
	}
	this->Refresh();
};

template <class _TNodeNet>
void TFrameDrawSuperGraph<_TNodeNet>::OnContextMenu(wxContextMenuEvent& event)
{
	event.Skip();
	if (dsgDrawer == NULL) {
		return;
	}

	if (wxmEditSuperGraph->IsChecked(DrawSuperGraph_EnableSuperGraphEdition)) {
		this->PopupMenu(wxmEditSuperGraph);
	} else {
		TFrameDrawGraph<_TNodeNet>::OnContextMenu(event);
	}
};

template <class _TNodeNet>
void TFrameDrawSuperGraph<_TNodeNet>::InitializeControls()
{
	wxmEditSuperGraph = new wxMenu();
	wxmEditSuperGraph->Append(DrawSuperGraph_ResetView, _T("&Reset visualization"), _T("Reset visualization collapsing all super nodes"));
	wxmEditSuperGraph->AppendSeparator();
	wxmEditSuperGraph->AppendCheckItem(DrawSuperGraph_EnableSuperGraphEdition, _T("&Enable super nodes edition"), _T("Enable super nodes edition"));
	wxmEditSuperGraph->AppendCheckItem(DrawSuperGraph_EnableConnectivityOnExpand, _T("&Auto-Connectivity on Expand"), _T("When expanding a super node, compute sons connectivities to other super nodes sons"));
	wxmEditSuperGraph->AppendCheckItem(DrawSuperGraph_EnableAutoFocus, _T("&Auto-Focus on Expand"), _T("When expanding a super node, draw it over its parent"));
	wxmEditSuperGraph->AppendCheckItem(DrawSuperGraph_ShowSuperEdges, _T("&Show all super edges"), _T("Show all super edges"));
	wxmEditSuperGraph->AppendSeparator();
	wxmEditSuperGraph->AppendRadioItem(DrawSuperGraph_PickSuperNode, _T("&Pick (click) / Toggle Focus (ctrl+click) super node"), _T("Pick/Focus super node"));
	wxmEditSuperGraph->AppendRadioItem(DrawSuperGraph_SetExpandCollapseNode, _T("&Expand (click) / Collapse (shift+click) node"), _T("Expand/Collapse node"));
	wxmEditSuperGraph->AppendRadioItem(DrawSuperGraph_TomahawkPerspective, _T("&Tomahawk perspective"), _T("Tomahawk perspective"));
	wxmEditSuperGraph->AppendRadioItem(DrawSuperGraph_ConnectivityOnDemand, _T("&Connectivity on demand"), _T("Connectivity on demand"));
	wxmEditSuperGraph->AppendRadioItem(DrawSuperGraph_DeleteConnectivitySuperEdge, _T("&Delete connectivity super edge"), _T("Delete connectivity super edge"));
#ifdef _CLOCKING
	wxmEditSuperGraph->Append(DrawSuperGraph_ConnectivityTests, _T("&SuperNodes Connectivity - All combinations of the SuperNodes at level 3"), _T("SuperNodes Connectivity - All combinations of the SuperNodes at level 3"));
	wxmEditSuperGraph->Append(DrawSuperGraph_ForeignNeighborsTests, _T("&Graph Nodes Connectivity - 20% of all nodes"), _T("Graph Nodes Connectivity - 20% of all nodes"));
#endif
	wxmEditSuperGraph->Append(DrawGraph_SetSuperNodeLabel, _T("&Set super node label"), _T("Set super node label"));
	wxmEditSuperGraph->Append(DrawSuperGraph_LabelQuery, _T("&Label query\tCtrl-f"), _T("Label query"));
	wxmEditSuperGraph->Append(DrawGraph_TrackNodeForeignNeighbors, _T("&Track node neighbors"), _T("Track node neighbors"));
	wxmEditSuperGraph->Append(DrawGraph_NextLabelOccurrence, _T("&Next label occurrence\tF3"), _T("Next label occurence"));

	wxmEditSuperGraph->Check(DrawSuperGraph_EnableSuperGraphEdition, true);
	wxmEditSuperGraph->Check(DrawSuperGraph_EnableConnectivityOnExpand, true);
	wxmEditSuperGraph->Check(DrawSuperGraph_EnableAutoFocus, true);
	wxmEditSuperGraph->Check(DrawSuperGraph_PickSuperNode, true);

	this->wxmEditNodes->Check(DrawGraph_EnableNodesEdition, false);
	this->wxmEditGraph->Check(DrawGraph_EnableGraphEdition, false);

	this->menuBar->Append(wxmEditSuperGraph, _T("&Super Graph"));
	/*SuperGraphButton*/
	wxbbSuperGraph = new wxBitmapButton(
	    this->wxpButtonsPanel,
	    DrawSuperGraph_SuperGraphButton,
	    wxBitmap("assets/SuperGraph.bmp", wxBITMAP_TYPE_BMP),
	    wxDefaultPosition,
	    wxDefaultSize,
	    wxNO_BORDER);
	wxbbSuperGraph->SetBitmapDisabled(wxBitmap("assets/SuperGraphActive.bmp", wxBITMAP_TYPE_BMP));
	this->wxfgsButtonsSizer->Add(wxbbSuperGraph, wxALIGN_CENTER);

	this->wxpButtonsPanel->SetSize(wxSize(this->wxbbNode->GetSize().GetWidth() + this->wxbbGraph->GetSize().GetWidth() + wxbbSuperGraph->GetSize().GetWidth() + 20,
	                                      this->wxbbNode->GetSize().GetHeight()));
	this->wxpButtonsPanel->Move(this->GetClientSize().GetWidth() - this->wxpButtonsPanel->GetSize().GetWidth() - 10,
	                            this->GetClientSize().GetHeight() - this->wxpButtonsPanel->GetSize().GetHeight() - 10);
	this->EnableSuperGraphEdition();
	this->SetTitle("R-Mine System");
#ifdef NOTIGNORE
	tgTest->Run();
#endif
};

/*----------------*/
template <class _TNodeNet>
void TFrameDrawSuperGraph<_TNodeNet>::OnClickNodeButton(wxCommandEvent& event)
{
	event.Skip();
	this->EnableNodesEdition();
}

template <class _TNodeNet>
void TFrameDrawSuperGraph<_TNodeNet>::OnClickGraphButton(wxCommandEvent& event)
{
	event.Skip();
	EnableGraphEdition();
}

template <class _TNodeNet>
void TFrameDrawSuperGraph<_TNodeNet>::OnClickSuperGraphButton(wxCommandEvent& event)
{
	event.Skip();
	EnableSuperGraphEdition();
}

/*----------------*/
template <class _TNodeNet>
void TFrameDrawSuperGraph<_TNodeNet>::OnClickEnableNodesEdition(wxCommandEvent& event)
{
	event.Skip();
	this->EnableNodesEdition(event);
};

template <class _TNodeNet>
void TFrameDrawSuperGraph<_TNodeNet>::OnClickEnableGraphEdition(wxCommandEvent& event)
{
	event.Skip();
	EnableGraphEdition(event);
};

template <class _TNodeNet>
void TFrameDrawSuperGraph<_TNodeNet>::OnClickEnableSuperGraphEdition(wxCommandEvent& event)
{
	event.Skip();
	EnableSuperGraphEdition(event);
};

template <class _TNodeNet>
void TFrameDrawSuperGraph<_TNodeNet>::RefreshLayout(wxCommandEvent& event)
{
	event.Skip();
	Refresh();
};

/*----------------*/
template <class _TNodeNet>
void TFrameDrawSuperGraph<_TNodeNet>::EnableNodesEdition(wxCommandEvent& event)
{
	if (this->pNodeNetData.GetRefs() <= 0) {
		return;
	}
	TFrameDrawGraph<_TNodeNet>::EnableNodesEdition(event);
	wxmEditSuperGraph->Check(DrawSuperGraph_EnableSuperGraphEdition, false);
	wxbbSuperGraph->Enable();
	wxmEditSuperGraph->Enable(DrawGraph_TrackNodeForeignNeighbors, true);
	SwitchOperationsSupportedOnlyBySubGraphs(false);
	this->menuBar->Refresh();
}

template <class _TNodeNet>
void TFrameDrawSuperGraph<_TNodeNet>::EnableGraphEdition(wxCommandEvent& event)
{
	if (this->pNodeNetData.GetRefs() <= 0) {
		return;
	}
	TFrameDrawGraph<_TNodeNet>::EnableGraphEdition(event);
	wxmEditSuperGraph->Check(DrawSuperGraph_EnableSuperGraphEdition, false);
	wxbbSuperGraph->Enable();
	wxmEditSuperGraph->Enable(DrawGraph_TrackNodeForeignNeighbors, false);
	SwitchOperationsSupportedOnlyBySubGraphs(false);
	this->menuBar->Refresh();
}

template <class _TNodeNet>
void TFrameDrawSuperGraph<_TNodeNet>::EnableSuperGraphEdition(wxCommandEvent& event)
{
	wxmEditSuperGraph->Check(DrawSuperGraph_EnableSuperGraphEdition, true);
	this->wxmEditNodes->Check(DrawGraph_EnableNodesEdition, false);
	this->wxmEditGraph->Check(DrawGraph_EnableGraphEdition, false);
	wxmEditSuperGraph->Enable(DrawGraph_TrackNodeForeignNeighbors, false);
	/*Set no SuperNode as Emphasized*/
	gtsneLastEmphasizedSuperNode = dsgDrawer->GetEmphasizedSuperNode();
	dsgDrawer->SetEmphasizedSuperNode(NULL);
	this->Refresh();

	if (wxmEditSuperGraph->IsChecked(DrawSuperGraph_ConnectivityOnDemand)) {
		if (bConnectivityInstructions) { /*Show just once*/
			bConnectivityInstructions = false;
			TDialogWarning("Click to choose first SuperNode, shift + click to choose second SuperNode", "Instructions");
		}
	}
	this->wxbbNode->Enable();
	this->wxbbGraph->Enable();
	this->wxbbSuperGraph->Disable();

	if (event.GetEventType() != wxEVT_NULL) {
		wxmEditSuperGraph->Check(event.GetId(), true);
	}
	SwitchOperationsSupportedOnlyBySubGraphs(true);
	SwitchOperationsNotSupportedBySubGraphs(false);
	this->menuBar->Refresh();
}

/*----------------*/
template <class _TNodeNet>
void TFrameDrawSuperGraph<_TNodeNet>::OnClickSetNodeLabel(wxCommandEvent& event)
{
	event.Skip();
	if (dsgDrawer == NULL) {
		return;
	}
	TGTreeSuperNodeEntry* gtsneTemp = this->GetEmphasizedSuperNode();
	if (gtsneTemp != NULL) {
		wxTextEntryDialog dialog(this, _T("Type the new label:"), _T("New label for super node"), _T(""), wxOK | wxCANCEL);
		dialog.SetValue((gtsneTemp->GetLabel()).c_str());
		if (dialog.ShowModal() == wxID_OK) {
			gtsneTemp->SetLabel(dialog.GetValue().c_str());
		}
		this->Refresh();
	}
}

template <class _TNodeNet>
void TFrameDrawSuperGraph<_TNodeNet>::OnClickForeignNeighborsTests(wxCommandEvent& event)
{
	event.Skip();
	gtGraphTree->ForeignNeighborsTest();
}

template <class _TNodeNet>
void TFrameDrawSuperGraph<_TNodeNet>::OnClickSuperNodeConnectivityTests(wxCommandEvent& event)
{
	event.Skip();
	gtGraphTree->GetConnectivityOfSuperNodes();
}

template <class _TNodeNet>
void TFrameDrawSuperGraph<_TNodeNet>::OnClickTrackNodeNeighbors(wxCommandEvent& event)
{
	event.Skip();
	TGTreeSuperNodeEntry* gtneTemp = this->GetEmphasizedSuperNode();
	if (gtneTemp == NULL) {
		return;
	}
	typename _TNodeNet::TNodeI niTemp;
	if (gtneTemp->GetEmphasizedNodeInSubGraph(niTemp)) {
		wxArrayString wxasArrayOfForeingNodesLabels;

		gtGraphTree->TrackNodeForeignNeighbors(niTemp.GetId(), gtneTemp, wxasArrayOfForeingNodesLabels);
		this->dlboTemp->fufsUpdater = this->ffufTemp;
		this->dlboTemp->fufsUpdater->SetInterfaceToBeUpdated(this);
		this->dlboTemp->fufsUpdater->SetOptionalAllPurposeObject(gtneTemp);
		this->dlboTemp->SetOptions(wxasArrayOfForeingNodesLabels);
		std::string sTitle = std::string("Foreign neighbors for ") + gtGraphTree->GetNodeLabel(niTemp.GetId());
		this->dlboTemp->SetTitle(sTitle.c_str());
		this->dlboTemp->Show();
	} else {
		TDialogWarning("Please, first select a node in the subgraph.", "Track node neighbors");
	}
}

template <class _TNodeNet>
void TFrameDrawSuperGraph<_TNodeNet>::OnClickResetView(wxCommandEvent& event)
{
	event.Skip();
	wxSetCursor(*wxHOURGLASS_CURSOR);
	dsgDrawer->SetEmphasizedSuperEdge(TGTreeSuperEdge::gtseNULLSuperEdge);
	dsgDrawer->SetEmphasizedSuperNode(NULL);
	TGTreeNode &xs = this->gtGraphTree->GetRoot()->gtnSubTree;
	for (TGTreeNode::iterator x = xs.begin(); x != xs.end(); ++x) {
		x->SetExpanded(false);
	}
	this->Refresh();
	wxSetCursor(*wxSTANDARD_CURSOR);
}

template <class _TNodeNet>
void TFrameDrawSuperGraph<_TNodeNet>::OnClose(wxCloseEvent& event)
{
	event.Skip();
	wxSetCursor(*wxHOURGLASS_CURSOR);
	//TFrameDrawGraph<_TNodeNet>::OnClose(event);
	dsgDrawer->SetEmphasizedSuperEdge(TGTreeSuperEdge::gtseNULLSuperEdge);
	dsgDrawer->SetEmphasizedSuperNode(NULL);
	typename std::vector< TFrameDrawGraph<_TNodeNet> *>::iterator vIterator;
	for (vIterator = vSuperEdgeSubBraphWindows.begin(); vIterator < vSuperEdgeSubBraphWindows.end(); ++vIterator) {
		(*vIterator)->Show(false);
	}
	gtGraphTree->GetRoot()->SetExpanded(false);
	wxSetCursor(*wxSTANDARD_CURSOR);
	this->Destroy();
}

template <class _TNodeNet>
void TFrameDrawSuperGraph<_TNodeNet>::EnableMouse()
{
	bMouseEnabled = true;
}

template <class _TNodeNet>
void TFrameDrawSuperGraph<_TNodeNet>::DisableMouse()
{
	bMouseEnabled = false;
}

template <class _TNodeNet>
void TFrameDrawSuperGraph<_TNodeNet>::CreateSuperEdgeGraphWindow(TPt<TGTreeSuperEdge> gtnseTemp)
{
	TPt<_TNodeNet> pNodeNetTemp(&(*gtnseTemp));
	/*Prepare colors*/
	gtnseTemp->PrepareContentForPresentation();
	/*Prepare labels - SuperEdges are not labeled till now*/
	gtGraphTree->FillNodeNetWithLabelData(pNodeNetTemp);
	TFrameDrawGraph< _TNodeNet > *dgfFrameDrawGraph;
	dgfFrameDrawGraph =
	    new TFrameDrawGraph< _TNodeNet >
	((wxMDIParentFrame *)this->GetParent(), wxPoint(100, 100), wxSize(400, 400), pNodeNetTemp,
	 wxDEFAULT_FRAME_STYLE | wxMAXIMIZE_BOX, this);
	dgfFrameDrawGraph->furwDelSuperEdgeGraphWindow = fdsewTemp;
	dgfFrameDrawGraph->furwDelSuperEdgeGraphWindow->SetInterfaceToBeUpdated(this);
	dgfFrameDrawGraph->Centre();
	dgfFrameDrawGraph->SetDrawer(new TDrawWxWidget< _TNodeNet > (pNodeNetTemp));
	dgfFrameDrawGraph->SetConfirmModificationsDialog(false);
	TLayoutBiPartite<_TNodeNet> layout;
	dgfFrameDrawGraph->ApplyLayout(layout);
	dgfFrameDrawGraph->SetTitle((gtnseTemp->GetSuperNode1()->GetLabel() + " x " + gtnseTemp->GetSuperNode2()->GetLabel()).c_str());
	dgfFrameDrawGraph->Show(true);
	dgfFrameDrawGraph->Restore();
	dgfFrameDrawGraph->SetFocus();
	dgfFrameDrawGraph->SetShowLabel(true);
	vSuperEdgeSubBraphWindows.push_back(dgfFrameDrawGraph);
}

/********wxWidgets stuf, ignore if you do not intend to deal with interface events********/
BEGIN_EVENT_TABLE_TEMPLATE1(TFrameDrawSuperGraph, TFrameDrawGraph<T>, T)

/*All mouse events to the same function*/
EVT_LEFT_DOWN  (TFrameDrawSuperGraph<T>::OnMouseEvent)
EVT_RIGHT_DOWN (TFrameDrawSuperGraph<T>::OnMouseEvent)
EVT_LEFT_UP    (TFrameDrawSuperGraph<T>::OnMouseEvent)
EVT_MOTION     (TFrameDrawSuperGraph<T>::OnMouseEvent)
EVT_LEFT_DCLICK(TFrameDrawSuperGraph<T>::OnMouseEvent)

/*Window events*/
EVT_PAINT     (TFrameDrawSuperGraph<T>::OnPaint)
EVT_SIZE      (TFrameDrawSuperGraph<T>::OnReSize)
EVT_SHOW      (TFrameDrawSuperGraph<T>::OnShow)
EVT_MOVE      (TFrameDrawSuperGraph<T>::OnMove)
EVT_CLOSE     (TFrameDrawSuperGraph<T>::OnClose)
EVT_SET_FOCUS (TFrameDrawSuperGraph<T>::OnSetFocus)
EVT_KILL_FOCUS(TFrameDrawSuperGraph<T>::OnKillFocus)

/*Menu events*/
EVT_MENU(DrawGraph_CloseWindow, TFrameDrawSuperGraph<T>::OnClickCloseWindow)

EVT_MENU(DrawSuperGraph_ShowSuperEdges, TFrameDrawSuperGraph<T>::RefreshLayout)

EVT_MENU(DrawGraph_EnableGraphEdition, TFrameDrawSuperGraph<T>::OnClickEnableGraphEdition)
EVT_MENU(DrawGraph_Scale_Up,           TFrameDrawSuperGraph<T>::OnClickEnableGraphEdition)
EVT_MENU(DrawGraph_Scale_Down,         TFrameDrawSuperGraph<T>::OnClickEnableGraphEdition)
EVT_MENU(DrawGraph_Translate,          TFrameDrawSuperGraph<T>::OnClickEnableGraphEdition)
EVT_MENU(DrawGraph_AddEdge,            TFrameDrawSuperGraph<T>::OnClickEnableGraphEdition)
EVT_MENU(DrawGraph_DeleteEdge,         TFrameDrawSuperGraph<T>::OnClickEnableGraphEdition)
EVT_MENU(DrawGraph_ShowAllLabels,      TFrameDrawSuperGraph<T>::OnClickEnableGraphEdition)
EVT_MENU(DrawGraph_ShowEdgeWeights,    TFrameDrawSuperGraph<T>::OnClickEnableGraphEdition)

EVT_MENU(DrawGraph_EnableNodesEdition, TFrameDrawSuperGraph<T>::OnClickEnableNodesEdition)
EVT_MENU(DrawGraph_PickNode,           TFrameDrawSuperGraph<T>::OnClickEnableNodesEdition)
EVT_MENU(DrawGraph_AddNode,            TFrameDrawSuperGraph<T>::OnClickEnableNodesEdition)
EVT_MENU(DrawGraph_DeleteNode,         TFrameDrawSuperGraph<T>::OnClickEnableNodesEdition)
EVT_MENU(DrawGraph_InfectNode,         TFrameDrawSuperGraph<T>::OnClickEnableNodesEdition)
EVT_MENU(DrawGraph_SetNodeLabel,       TFrameDrawSuperGraph<T>::OnClickSetNodeLabel)
EVT_MENU(DrawGraph_ShowLabels,         TFrameDrawSuperGraph<T>::OnClickEnableNodesEdition)

EVT_MENU(DrawSuperGraph_EnableSuperGraphEdition,     TFrameDrawSuperGraph<T>::OnClickEnableSuperGraphEdition)
EVT_MENU(DrawSuperGraph_PickSuperNode,               TFrameDrawSuperGraph<T>::OnClickEnableSuperGraphEdition)
EVT_MENU(DrawSuperGraph_SetExpandCollapseNode,       TFrameDrawSuperGraph<T>::OnClickEnableSuperGraphEdition)
EVT_MENU(DrawSuperGraph_TomahawkPerspective,         TFrameDrawSuperGraph<T>::OnClickEnableSuperGraphEdition)
EVT_MENU(DrawSuperGraph_ConnectivityOnDemand,        TFrameDrawSuperGraph<T>::OnClickEnableSuperGraphEdition)
EVT_MENU(DrawSuperGraph_DeleteConnectivitySuperEdge, TFrameDrawSuperGraph<T>::OnClickEnableSuperGraphEdition)
EVT_MENU(DrawSuperGraph_LabelQuery,                  TFrameDrawSuperGraph<T>::OnClickLabelQuery)
EVT_MENU(DrawGraph_SetSuperNodeLabel,                TFrameDrawSuperGraph<T>::OnClickSetNodeLabel)
EVT_MENU(DrawGraph_TrackNodeForeignNeighbors,        TFrameDrawSuperGraph<T>::OnClickTrackNodeNeighbors)
EVT_MENU(DrawGraph_NextLabelOccurrence,              TFrameDrawSuperGraph<T>::OnClickNextLabelOccurrence)
EVT_MENU(DrawSuperGraph_ConnectivityTests,           TFrameDrawSuperGraph<T>::OnClickSuperNodeConnectivityTests)
EVT_MENU(DrawSuperGraph_ForeignNeighborsTests,       TFrameDrawSuperGraph<T>::OnClickForeignNeighborsTests)

EVT_MENU(DrawSuperGraph_ResetView,       TFrameDrawSuperGraph<T>::OnClickResetView)

/*Popup menu*/
EVT_CONTEXT_MENU(TFrameDrawSuperGraph<T>::OnContextMenu)

/*Button events*/
EVT_BUTTON(DrawGraph_NodeButton,            TFrameDrawSuperGraph<T>::OnClickNodeButton)
EVT_BUTTON(DrawGraph_GraphButton,           TFrameDrawSuperGraph<T>::OnClickGraphButton)
EVT_BUTTON(DrawSuperGraph_SuperGraphButton, TFrameDrawSuperGraph<T>::OnClickSuperGraphButton)

END_EVENT_TABLE()

template <class _TNodeNet>
bool TFrameDrawSuperGraph<_TNodeNet>::FocusInGraphNode(std::string sAGraphNodeLabel, TGTreeSuperNodeEntry* gtsneReference, int iNthOccurrence)
{
	int iNodeId;
	/*Try to find string sAGraphNodeLabel*/
	iNodeId = gtGraphTree->GetNodeIdFromNodeLabel(sAGraphNodeLabel, iNthOccurrence);
	TGTreeSuperNodeEntry* gtsneSuperNodeToFocus = gtGraphTree->FindNodeSuperNodeEntry(iNodeId);

	if (gtsneSuperNodeToFocus == NULL) { /*Didn't find*/
		TDialogWarning("Label not found.", "Label query message");
		return false;
	}

	/*The reference SuperNode -- the current node with focus --
	  is used to determine the viewing perspective*/
	if (gtsneReference == NULL) {
		gtsneReference = this->gtGraphTree->GetRoot();
	}

	/*If the supernode where the graph node is, is the same that has current focus,
	  avoid perspective animation*/
	if (gtsneSuperNodeToFocus != gtsneReference) {
		/*Find common parent of the reference and the correspondent SuperNode*/
		TGTreeSuperNodeEntry* gtsneFirstCommonParent = this->gtGraphTree->GetFirstCommonParent(gtsneReference, this->GetEmphasizedSuperNode());
		TGTreeSuperNodeEntry* gtsneFirstCommonParent2 = this->gtGraphTree->GetFirstCommonParent(gtsneReference, gtsneSuperNodeToFocus);
		if (gtsneFirstCommonParent2->iLevel < gtsneFirstCommonParent->iLevel) {
			gtsneFirstCommonParent = gtsneFirstCommonParent2;
		}
		/*Focus out to overview first common parent*/
		dsgDrawer->AnimatedFocusInSuperNode(TUtil<int>::GetMax(0, gtsneFirstCommonParent->iLevel - 1), gtsneFirstCommonParent, true);
		/*Focus in SuperNode gtsneSuperNodeToFocus two levels above*/
		dsgDrawer->AnimatedFocusInSuperNode(TUtil<int>::GetMax(0, gtsneSuperNodeToFocus->iLevel), gtsneSuperNodeToFocus, true);
	}
	/*Emphasize selected node*/
	gtsneSuperNodeToFocus->pNodeNetSubGraph->GetNI(iNodeId)().SetShape(TShapeType::saDiamond);
	gtsneSuperNodeToFocus->pNodeNetSubGraph->GetNI(iNodeId)().SetSize(0.06);
	gtsneSuperNodeToFocus->pNodeNetSubGraph->GetNI(iNodeId)().SetColor("255_000_000");
	typename _TNodeNet::TNodeI node = gtsneSuperNodeToFocus->pNodeNetSubGraph->GetNI(iNodeId);
	gtsneSuperNodeToFocus->pNodeNetSubGraph->SetEmphasizedNode(node);
	this->SetEmphasizedSuperNode(gtsneSuperNodeToFocus);
	return true;
};

/*************************************************************************************/

/*Small class that teaches the (SuperEdges subgraph) TFrameDrawGraph
  to make TFrameSuperDrawGraph aware of its deletion*/
template <class _TNodeNet>
class TFunctorDelSuperEdgeGraphWindow : public TFunctorUpdaterForRootWindow<_TNodeNet>
{

	void ExteriorProcessing(TFrameDrawGraph<_TNodeNet>* fsiAFrame) {
		TFrameDrawSuperGraph<_TNodeNet> *fdgTemp = ((TFrameDrawSuperGraph<_TNodeNet> *)this->interfaceToBeUpdated);
		typename std::vector< TFrameDrawGraph<_TNodeNet>* >::iterator vIterator;
		for (vIterator = fdgTemp->vSuperEdgeSubBraphWindows.begin(); vIterator < fdgTemp->vSuperEdgeSubBraphWindows.end(); ++vIterator) {
			if ((*vIterator) == fsiAFrame) {
				/*This code is being called from within *vIterator so we cannot
				delete *vIterator. It will delete itself on its OnClose event*/
				if ((*vIterator)->diDrawer != NULL) {
					delete (*vIterator)->diDrawer;    /*Created in here, deleted in here*/
				}
				(*vIterator)->diDrawer = NULL;
				/*Erase from list of SubGraph windows*/
				fdgTemp->vSuperEdgeSubBraphWindows.erase(vIterator);
				break;
			}
		}
		fdgTemp->gtGraphTree->SetAllSuperNodesColor();
		fdgTemp->Refresh();
	};
};

/*Small class that teaches the node listbox of labels how to update the TFrameDrawSuperGraph*/
template <class _TNodeNet>
class TFunctorFocusUpdaterForListBox : public TFunctorUpdaterForListBox
{

	void UpdateListBox() {
		TFunctorUpdaterForListBox::UpdateListBox();
		TFrameDrawSuperGraph<_TNodeNet> *fdsgTemp = ((TFrameDrawSuperGraph<_TNodeNet> *)interfaceToBeUpdated);
		fdsgTemp->FocusInGraphNode(fdsgTemp->dlboTemp->GetSelectedItemString(),
		                           (TGTreeSuperNodeEntry*)this->allPurposeObject);
	}

	void RefreshInterface() {
		TFunctorUpdaterForListBox::RefreshInterface();
		TFrameDrawSuperGraph<_TNodeNet> *fdsgTemp = ((TFrameDrawSuperGraph<_TNodeNet> *)interfaceToBeUpdated);
		fdsgTemp->Refresh();
	}
};


#endif

#endif
