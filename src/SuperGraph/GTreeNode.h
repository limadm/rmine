#pragma once
#ifndef _GTreeNode_
#define _GTreeNode_

#include <fstream>
#include <map>
#include <list>
#include <hash_set>
#include <hash_map>

#include <Commons/stdafx.h>
#include <Commons/Auxiliar.h>
#include <Commons/Storage.h>
#include <Drawings/DrawInterface.h>
#include <Graphs/NodeNetTests.h>
#include <Graphs/NodePlot.h>
#include <Graphs/EdgePlot.h>

class TGTree;
class TGTreeSuperNodeEntry;
class TDrawSuperGraph;

/*-------------------------------------------------------------------------------*/

#ifndef PERFORMANCE
	class TGTreeSuperEdge : public TNodeNetTests<TNodePlot, TEdgePlot>{
#else
	class TGTreeSuperEdge : public TNodeNetTests<TNodePerformance, TEdgePlot>{
#endif
public:
	/*Static null auxiliar*/
	static TPt<TGTreeSuperEdge> gtseNULLSuperEdge;
public:
	TGTreeSuperEdge(TGTreeSuperNodeEntry* gtsneASuperNode1 = NULL,
	                TGTreeSuperNodeEntry* gtsneASuperNode2 = NULL);
	TGTreeSuperEdge(TGTreeSuperNodeEntry* gtsneASuperNode1,
	                TGTreeSuperNodeEntry* gtsneASuperNode2,
	                TSIn &SIn);
	~TGTreeSuperEdge();
	TGTreeSuperNodeEntry* GetSuperNode1();
	TGTreeSuperNodeEntry* GetSuperNode2();
	bool operator == (const TGTreeSuperEdge& TreeSuperEdge);
	bool operator != (const TGTreeSuperEdge& TreeSuperEdge);
	int GetWeight();
	int GetAccumulatedWeight();
	void AddGTreeEdge(int iASource, int iADestination, int iAWeight = 0);
	bool IsIt(TGTreeSuperNodeEntry* gtsneASuperNode1, TGTreeSuperNodeEntry* gtsneASuperNode2);
	/*There is an initial support for directed super edges, but everything has been
	  done considering undirected super edges, that is, (sId1, sId2) = (sId2, sId1)*/
#ifndef PERFORMANCE
	bool FindEdge(int iSource, int iDestination, bool bDirected, TNodeNetTests<TNodePlot, TEdgePlot>::TEdgeI& eiAEdge);
#else
	bool FindEdge(int iSource, int iDestination, bool bDirected, TNodeNetTests<TNodePerformance, TEdgePlot>::TEdgeI& eiAEdge);
#endif
	void Draw(void *dsgADrawer, TGTreeSuperNodeEntry* gtsneEmphasizedNode=NULL);
	bool IsMouseOver(double dAX, double dAY, TDrawSuperGraph *dsgADrawer);
	void SetEmphasis(bool bAOption);
	void PrepareContentForPresentation();
	void TrackNodeNeighbors(int iANodeId, std::list<int>& lListOfForeignNodeIds);
	void Save(TSOut& SOut);
	void Load(std::string sAFileName);
	friend class TPt< TGTreeSuperEdge >;
private:
	TGTreeSuperNodeEntry* gtsneSuperNode1; /*---Descriptive---We save and restore these ones*/
	TGTreeSuperNodeEntry* gtsneSuperNode2; /*---Descriptive---*/
	int iAccumulatedWeight;                /*---Descriptive---*/
	bool bEmphasized;
	double dPosX, dPosY, dWidth, dHeight;
private:
	void CalculateEdgeLimits(TGTreeSuperNodeEntry* gtneANode1,
	                         TGTreeSuperNodeEntry* gtneANode2,
	                         double &dX1, double &dY1, double &dX2, double &dY2);
	void CalculateSimpleLabelRectangleParameters(double& dPosX, double& dPosY, double& dWidth, double& dHeight,
	                                             double dXPosSource, double dYPosSource, double dXPosDestination, double dYPosDestination);
	void CalculateCompleteLabelRectangleParameters(double& dPosX, double& dPosY, double& dWidth, double& dHeight,
	                                               double dXPosSource, double dYPosSource, double dXPosDestination, double dYPosDestination);
	void DrawCompleteLabel(void *dsgADrawer, double dXPosSource, double dYPosSource, double dXPosDestination, double dYPosDestination);
	void DrawSimpleLabel(void *dsgADrawer, double dXPosSource, double dYPosSource, double dXPosDestination, double dYPosDestination);
};
/*-------------------------------------------------------------------------------*/
class TGTreeNode : public std::list<TGTreeSuperNodeEntry>{
public:
	TGTreeNode();
	TGTreeSuperNodeEntry* ScanEntries(std::string sId);
	void PrintTreeNode();
	std::string AddSuperNodeEntry(std::string sId, TGTreeSuperNodeEntry* gtsneAParent);
};
/*-------------------------------------------------------------------------------*/
class TGTreeSuperNodeEntry{
public:
	/*For leaf and non-leaf nodes*/
	/*---Descriptive---We save and restore these ones*/
	TGTreeSuperNodeEntry*gtsneParent;
	TFileHash_set<int> hsListOpenNodes;
	std::list< TPt<TGTreeSuperEdge> > lListSuperEdges;
	std::string sSuperNodeId;
	double dX, dY;
	double dHorizontalSize, dVerticalSize;
	int iTotalNumberOfGraphNodesBeneath;
	int iLevel;
	byte bRed, bGreen, bBlue;
	/*---Temporary---*/
	std::list <  TPt<TGTreeSuperEdge>  > mmConnectivitySuperEdges;
//	std::hash_map < unsigned __int64, TEdgeTemp* > sUnresolvedEdges;  /*---Temporary---*/
	std::multimap<unsigned int, TEdgeTemp*> sUnresolvedEdges;  /*---Temporary---*/
	/*For non-leaf nodes only*/
	TGTreeNode gtnSubTree; /*---Descriptive---We save and restore these ones*/
	/*For leaf nodes only*/
	/*---Descriptive---*/
	TGTreeSuperNodeEntry* gtsneFocusedNode;
	bool bVisible;
	bool bIsLeaf;
	/*---Temporary---*/

#ifndef PERFORMANCE
	TDrawInterface< TNodeNetTests<TNodePlot, TEdgePlot> >* diDrawer;
	TPt< TNodeNetTests<TNodePlot, TEdgePlot> > pNodeNetSubGraph;
#else
	TDrawInterface< TNodeNetTests<TNodePerformance, TEdgePlot> >* diDrawer;
	TPt< TNodeNetTests<TNodePerformance, TEdgePlot> > pNodeNetSubGraph;
#endif

public:
	TGTreeSuperNodeEntry(std::string sId, TGTreeSuperNodeEntry*gtsneAParent);
	TGTreeSuperNodeEntry(rmine::disk::InputStorage & s, TGTreeSuperNodeEntry* parent);
	~TGTreeSuperNodeEntry();
	bool operator == (const TGTreeSuperNodeEntry& TreeSuperNodeEntry);
	std::string AddTreeNodeEntry(std::string sId);
	void AddConnectivitySuperEdge(TGTreeSuperNodeEntry*gtsneANode, TPt<TGTreeSuperEdge>  gtseASuperEdge);
	void AddConnectivitySuperEdge(TGTreeSuperNodeEntry * gtsneANode, TGTree * gtGraphTree);
	void AddConnectivitySuperEdgesRecursive(TGTreeSuperNodeEntry * gtsneANode, TGTree * gtGraphTree);
	bool HasConnectivitySuperEdgeAlready(TGTreeSuperNodeEntry*gtsneANode);
	bool DeleteConnectivitySuperEdge(TPt<TGTreeSuperEdge>  gtseASuperEdge);
	bool DeleteConnectivitySuperEdge(TGTreeSuperNodeEntry * gtsneANode);
	void DeleteConnectivitySuperEdgesRecursive(TGTreeSuperNodeEntry * gtsneANode);
	TPt<TGTreeSuperEdge> IdentifySuperEdge(double dAX, double dAY, TDrawSuperGraph *dsgADrawer);
	TPt<TGTreeSuperEdge> IdentifyConnectivitySuperEdge(double dAX, double dAY, TDrawSuperGraph *dsgADrawer);
	void SetAsLeafNode(std::string sASubGraphFile);
	bool IsPointOver(double dAX, double dAY);
	bool IsPointOverMBB(double dAX, double dAY);
	bool IsExpanded();
	void SetExpanded(bool bAOption);
	bool TranslateSuperNode(double dTX, double dTY);
	TPt<TGTreeSuperEdge> FindSuperEdgeEntry(TGTreeSuperNodeEntry* gtsneASuperNode1, TGTreeSuperNodeEntry* gtsneASuperNode2, bool bDirected);
	void ResolveOpenNodesFromSons();
	void ReleaseSubGraph(bool bRefreshFiles);
	bool IsAncestor(TGTreeSuperNodeEntry* gtsneASuperNode);
	std::string GetAncestorsLabel();
	std::string GetCompactLabel();
	double GetCenterX();
	double GetCenterY();
	void DrawSuperEdges(void *dsgADrawer, TGTreeSuperNodeEntry* gtsneEmphasizedNode=NULL);
	void SetColor(std::string sAColorRGBCode);
	void SetColor(double h, double s, double v);
	bool IsOpenNode(int iANodeId);
	void SetLabel(std::string sALabel);
	std::string GetLabel();
	void TrackNodeSuperEdgesNeighbors(std::list<int>& lListOfForeignNeighborsIds, int iANodeId, TGTreeSuperNodeEntry* gtsneSuperNodeWhereNodeIsAOpenNode);

#ifndef PERFORMANCE
	bool GetEmphasizedNodeInSubGraph(TNodeNetTests<TNodePlot, TEdgePlot>::TNodeI& niANode);
#else
	bool GetEmphasizedNodeInSubGraph(TNodeNetTests<TNodePerformance, TEdgePlot>::TNodeI& niANode);
#endif
	bool IsVisible();
	void Save(std::string sADirectory);
	void Load(std::string sADirectory);
	void Save(rmine::disk::OutputStorage &s);
	void Load(bool recursive);
	void Release(bool saveGraph);
	void SetInputStorage(rmine::disk::InputStorage &s) { storage = &s; }
	bool LoadGraphTextFile();
	bool LoadExternalConnectionsFile();
private:
	/*---Descriptive---*/
	std::string sSubGraphFile;
	std::string sExternalEdgesFile;
	std::string sLabel;
	bool bExpanded;
	rmine::disk::InputStorage *storage;
	stPageID page;
	bool bLoaded;
	/* Transform a pair of integers into a unique integer N x N -> N
	   Google: Pairing function */
	unsigned int CantorFunction(unsigned int k1, unsigned int k2){
		/* Actually we are using Pigen pairing function, which is faster than Cantor
		   pairing function (commented below) - for details see it at MathWorld */
/*	 
		unsigned _int64 iPaired = (_int64)0;
		for (int i = 0; i <= 31; i++){
			iPaired = iPaired >> 1;
			if(k1 & 0x1)
				iPaired = iPaired | 0x8000000000000000;
			k1 = k1 >> 1;

			iPaired = iPaired >> 1;
			if(k2 & 0x1)
				iPaired = iPaired | 0x8000000000000000;
			k2 = k2 >> 1;
		}
		return iPaired;
//-----------------------------------------------------
		Count number of bits 1 - for debuging
		int iCount = 0;
		 for (int i = 0; i <= 63; i++){
			  if(iPaired & 0x1)
					iCount++;
			  iPaired = iPaired >> 1;
		 }
//-----------------------------------------------------
		// Another pairing function: http://szudzik.com/ElegantPairing.pdf
		if(k1 > k2)
			return (unsigned __int64)(0.5*(k1 + k2)*(k1 + k2 + 1) + k1); // This conditional calculus is only for the sake of disregarding the order
		else
			return (unsigned __int64)(0.5*(k1 + k2)*(k1 + k2 + 1) + k2); // this is the original Cantor function, sensitive to the order
*/
		if(k1 > k2)
			return wxUint64(k1)*k1 + k1 + k2;
		else
			return wxUint64(k2)*k2 + k1;
	};
};
/*-------------------------------------------------------------------------------*/
#endif
