#pragma once
#ifdef _SUPERGRAPHCOMPILATION

#ifndef _DrawSuperGraph_
#define _DrawSuperGraph_

#include <Commons/Auxiliar.h>
#include <Frames/FrameDrawGraph.h>
#include <Graphs/NodePlot.h>
#include <Graphs/EdgePlot.h>
#include <Graphs/NodeNetTests.h>
#include <SuperGraph/ResolutionComponent.h>
#include <SuperGraph/GTree.h>

class TDrawSuperGraph{
public:
   TDrawSuperGraph(TGTree* gtASuperGraphTree);
   ~TDrawSuperGraph();
   void SetFrame(wxWindow *fAFrame);
   wxGCDC* GetDeviceContext(){ return dcDeviceContext; };
   /*Imagine the graph as a squared area where all the nodes and edges lie in*/
   void ConvertDrawingToDeviceCoordinates(double& fDrawingToDeviceX, double& fDrawingToDeviceY);
   /*Given two display coordinates, 1two drawing coordinates (0.0 - 1.0)*/
   bool ConvertDeviceToDrawingCoordinates(double& dX, double& dY);
   void SetWidthAndHeight(wxCoord wxcAWidth,wxCoord wxcAHeight);
   void SetRelativeScale(double fWidthScaleFactor,double fHeightScaleFactor,
                         int iReferenceX, int iReferenceY);
   void SetAbsoluteScale(double fWidthScaleFactor,double fHeightScaleFactor);
   void SetTranslation(int iAHorizontalTranslation, int iAVerticalTranslation);
   void DrawLeafNode(TGTreeSuperNodeEntry* gtneAGraphSuperNode);
   void ExpandNode(TGTreeSuperNodeEntry* gtneAGraphSuperNode, bool bOption);
   void ExpandNodeTomaHawk(TGTreeSuperNodeEntry* gtneAGraphSuperNode);
   void DrawNodeDetailedData(TGTreeSuperNodeEntry* gtneAGraphSuperNode);
   void DrawBiggestInnerRectangle(TGTreeSuperNodeEntry* gtneAGraphSuperNode);
   void DrawSuperGraph(wxGCDC *vDrawContext);
   void DrawConnectivitySuperEdgeRecursive(TGTreeSuperNodeEntry* gtneBaseNode);
   void DrawConnectivitySuperEdge();
   bool TranslateEmphasizedNode(int iTX, int iTY);
   bool SetShapeForEmphasizedNode(TShapeType::TShapeAcronysm npsAShapeType);
   bool SetColorForEmphasizedNode(wxColour& wxcAColour);
   bool SetSizeForEmphasizedNode(double dASize);
   void FocusInSuperNode(TGTreeSuperNodeEntry* gtneASuperNode);
   void FocusOutSuperNode(TGTreeSuperNodeEntry* gtneASuperNode);
   double GetSizeForEmphasizedNode();
   bool SetLabelForEmphasizedNode(std::string sALabel);
   std::string GetLabelFromEmphasizedNode();
#ifndef PERFORMANCE
   void SetInterfaceForLeafSuperNodes(TFrameDrawGraph< TNodeNetTests<TNodePlot, TEdgePlot> > *fdsgAFrame);
#else
   void SetInterfaceForLeafSuperNodes(TFrameDrawGraph< TNodeNetTests<TNodePerformance, TEdgePlot> > *fdsgAFrame);
#endif
   TGTreeSuperNodeEntry* IdentifySuperNode(const int iX, const int iY);
   TPt<TGTreeSuperEdge> IdentifySuperEdge(const int iX, const int iY);
   TGTreeSuperNodeEntry* GetEmphasizedSuperNode();
   void SetEmphasizedSuperNode(TGTreeSuperNodeEntry* gtneAGraphSuperNode);
   void SetEmphasizedSuperEdge(TPt<TGTreeSuperEdge> gtseAGraphSuperEdge);
   TPt<TGTreeSuperEdge> GetEmphasizedSuperEdge();
   bool IsMouseOverResolutionTracker(int iAX, int iAY);
   void UpdateResolutionTracker(int iAX, int iAY);
   void AnimatedFocusInSuperNode(int iLevelToFocus, TGTreeSuperNodeEntry* gtsneASuperNodeToFocus, bool bExpandAll = false);
   void SetGlobalBrushColor(const unsigned char red, const unsigned char green, const unsigned char blue);
   void ConvertDeviceSizeToDrawingSize(double& dWidht, double& dHeight);
   void ConvertDrawingSizeToDeviceSize(double& dWidht, double& dHeight);
   int GetCurrentResolution();
public:
   bool bAutoFocus;
   bool bVisibleSuperEdges;
private:
   wxGCDC *dcDeviceContext;
   wxWindow *fFrameToDraw;
   
   TGTree* gtGraphTree;
   TGTreeSuperNodeEntry* gtneEmphasizedNode;
   TPt<TGTreeSuperEdge> gtseEmphasizedSuperEdge;
#ifndef PERFORMANCE
   TFrameDrawGraph< TNodeNetTests<TNodePlot, TEdgePlot> > *fdsgFrameForLeafSuperNode;
#else
   TFrameDrawGraph< TNodeNetTests<TNodePerformance, TEdgePlot> > *fdsgFrameForLeafSuperNode;
#endif

   /*Device context for drawing*/   
   wxBrush *wxbDrawingBrush;
   /*Imagine the graph as a squared area where all the nodes and edges lie in*/
   /*wxcGraphWidth and wxcGraphHeight are boundless (all the graph or just part of the graph being showed)*/
   wxCoord wxcGraphWidth;      wxCoord wxcGraphHeight;
   /*Positional translation of the graph*/
   int iHorizontalTranslation; int iVerticalTranslation;
   /*Display limits in drawing coordinates (0.0 - 1.0)*/
   double fMinX; double fMaxX;   double fMinY; double fMaxY;
   /*Holds the connectivity for the supernodes to display*/
   std::list<int>* lConnectivities;
   /*Size of the display in display coordinates - used in DrawNode()*/
   TResolutionComponent* rcResolutionTracker;
   int iCurrentResolution;
   int iDisplayWidth;         int iDisplayHeight;
private:
   void SetCurrentResolution(int iACurrentResolution);
   void DrawNode(TGTreeSuperNodeEntry* gtneAGraphSuperNode, bool bEmphasizeNode = false);
   void CentralizeDrawingPoint(double dADrawingX, double dADrawingY);
   void CentralizePoint(int iADeviceX, int iADeviceY);
   void DrawSuperGraphRecursive(TGTreeSuperNodeEntry* gtneBaseNode);
   void UpdateDisplaySize();
};
/*------------------------------------------------------------------------------------------*/
#endif

#endif
