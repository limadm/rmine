#ifdef PERFORMANCE
#include "NodePerformance.h"

void TNodePerformance::SetColor(std::string sAColorRGBCode){
   /*Deprecated - Backward compatibility*/

}

std::string TNodePerformance::GetColor(){
   return "000_000_000";
}

void TNodePerformance::SetLabel(std::string sALabel){

};

void TNodePerformance::AddSonToInfluenceSet(int iANodeIndex){

};

TNodePerformance& TNodePerformance::operator = (const TNodePerformance& NodePlot) { 
   return *this;
}

void TNodePerformance::ScaleSize(double dAScaleValue){   

}

void TNodePerformance::InfectNode(){

};

bool TNodePerformance::IsPointOver(double _X, double _Y){
	return false;
}

void TNodePerformance::Save(TSOut& SOut) const{
}
TRectangle TNodePerformance::GetBoundingBox(){
   TRectangle NodeBoundingBox;
   return NodeBoundingBox;
}

void TNodePerformance::SetRed(byte bARedValue){
}
void TNodePerformance::SetGreen(byte bAGreeValue){
}
void TNodePerformance::SetBlue(byte bABlueValue){
}

byte TNodePerformance::GetRed(){
   return 0;
}
byte TNodePerformance::GetGreen(){
   return 0;
}
byte TNodePerformance::GetBlue(){
   return 0;
}

PXmlTok TNodePerformance::GetXmlTok() const {
  PXmlTok NodeTok = TXmlTok::New("NodePlot");
  return NodeTok;
}
#endif
