#include "ResolutionComponent.h"

TResolutionComponent::TResolutionComponent(int iANumberOfTicks){
   wxdcDeviceContext = NULL;
   iX1 = 30; iX2 = 60; iY1 = 30; iY2 = 120;
   wxpMyPen = wxGREY_PEN;        bVisible = true;
   iNumberOfTicks = iANumberOfTicks;
   iCurrentTick = 0;
};
int TResolutionComponent::GetCurrentTick(){
   return iCurrentTick;
};

void TResolutionComponent::ProcessClick(int iAX, int iAY){
   iAY = iAY - iY1;
   int iTicksInterval = (iY2 - iY1)/(iNumberOfTicks-1);
   float fRatio = iAY/(float)iTicksInterval;
   iCurrentTick = (iNumberOfTicks-1) - ROUND(fRatio);
};

void TResolutionComponent::Draw(){
   if((iNumberOfTicks <= 1)||(!bVisible))  /*Nothing to select*/
      return;
   int iXPos = iX1 + (iX2 - iX1)/2;
   int iTicksInterval = (iY2 - iY1)/(iNumberOfTicks-1);
   int iNextTickPos;
   int iTemp = SELECTION_THICKNESS;          iTemp *= 3;
                                    
   /*Prepare redraw*/
   wxpMyPen->SetWidth(2);
   wxdcDeviceContext->SetPen(*wxpMyPen);
   /*Redraw*/
   wxdcDeviceContext->DrawLine(iXPos,iY1,iXPos,iY2-2);
   for(int iCounter = 0; iCounter < iNumberOfTicks; iCounter++){
      iNextTickPos = iY1+iCounter*iTicksInterval;
      wxdcDeviceContext->DrawLine(iX1+6, iNextTickPos, iX2-6, iNextTickPos);
      wxdcDeviceContext->DrawText(TConverter<int>::ToString((iNumberOfTicks-1)-iCounter).c_str(),
                                  iXPos+8, iNextTickPos);
   }
   /*Draw selected tick*/
   wxpMyPen->SetWidth(5);
   wxdcDeviceContext->SetPen(*wxpMyPen);
   iNextTickPos = iY1 + ((iNumberOfTicks-1)-iCurrentTick)*iTicksInterval;   
   wxdcDeviceContext->DrawLine(iX1+5, iNextTickPos, iX2-5, iNextTickPos);
};

bool TResolutionComponent::IsMouseOver(int iAX, int iAY){
   if(( (iAX > iX1) && (iAX < iX2) ) &&
      ( (iAY > iY1) && (iAY < iY2) )){
         wxpMyPen = wxBLACK_PEN;
         return true;
      }else{
         wxpMyPen = wxMEDIUM_GREY_PEN;
         return false;
      }
};
void TResolutionComponent::SetDeviceContext(wxDC *wxdcADeviceContext){
   wxdcDeviceContext = wxdcADeviceContext;
}
void TResolutionComponent::SetNumberOfTicks(int iANumberOfTicks){
   iNumberOfTicks = iANumberOfTicks;
};
void TResolutionComponent::SetCurrentTick(int iATickPosition){
   iCurrentTick = iATickPosition;
}
void TResolutionComponent::SetVisible(bool bAOption){
   bVisible = bAOption;
};