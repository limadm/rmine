#define _SUPERGRAPHCOMPILATION
#ifdef _SUPERGRAPHCOMPILATION

#include "GTreeNode.h"

#include <algorithm>

#include <SuperGraph/DrawSuperGraph.h>
#include <SuperGraph/GTree.h>

using namespace rmine;
using namespace rmine::disk;

bool IsSuperEdgeVisible(TGTreeSuperNodeEntry *a, TGTreeSuperNodeEntry *b, TDrawSuperGraph *dsg)
{
	return a->IsVisible()
	       && b->IsVisible()
		   && ((a->bIsLeaf || !a->IsExpanded()) && a->gtsneParent && a->gtsneParent->gtsneFocusedNode == NULL)
	       && ((b->bIsLeaf || !b->IsExpanded()) && b->gtsneParent && b->gtsneParent->gtsneFocusedNode == NULL)
		   && (dsg->bVisibleSuperEdges || a == dsg->GetEmphasizedSuperNode() || b == dsg->GetEmphasizedSuperNode());
}

/*--------------------------------------------------------------------------------*/
TGTreeNode::TGTreeNode() : std::list<TGTreeSuperNodeEntry>()
{
};

std::string TGTreeNode::AddSuperNodeEntry(std::string sId, TGTreeSuperNodeEntry*gtsneAParent)
{
	TGTreeSuperNodeEntry temp(sId,gtsneAParent);
	this->push_back(temp);
	return sId;
};

TGTreeSuperNodeEntry* TGTreeNode::ScanEntries(std::string sId)
{
	std::list<TGTreeSuperNodeEntry>::iterator iListIterator;
	for(iListIterator = this->begin(); iListIterator != this->end(); ++iListIterator) {
		if(iListIterator->sSuperNodeId.compare(sId) == 0) {
			return &(*iListIterator);
		}
	}
	return NULL;
};

void TGTreeNode::PrintTreeNode()
{
	std::list<TGTreeSuperNodeEntry>::iterator iListIterator;
	std::cout << "\n";
	for(iListIterator = this->begin(); iListIterator != this->end(); ++iListIterator) {
		std::cout << iListIterator->sSuperNodeId.c_str();
	}
};
/*--------------------------------------------------------------------------------*/
TPt<TGTreeSuperEdge> TGTreeSuperEdge::gtseNULLSuperEdge = TPt<TGTreeSuperEdge>(NULL);

TGTreeSuperEdge::TGTreeSuperEdge(TGTreeSuperNodeEntry* gtsneASuperNode1,
                                 TGTreeSuperNodeEntry* gtsneASuperNode2)
#ifndef PERFORMANCE
	:TNodeNetTests<TNodePlot, TEdgePlot>()
{
#else
	:
	TNodeNetTests<TNodePerformance, TEdgePlot>()
{
#endif

	this->gtsneSuperNode1 = gtsneASuperNode1;
	this->gtsneSuperNode2 = gtsneASuperNode2;
	this->bEmphasized = false;
	iAccumulatedWeight = 0;
}

TGTreeSuperEdge::TGTreeSuperEdge(TGTreeSuperNodeEntry* gtsneASuperNode1,
                                 TGTreeSuperNodeEntry* gtsneASuperNode2,
                                 TSIn &SIn)
#ifndef PERFORMANCE
	:TNodeNetTests<TNodePlot, TEdgePlot>(SIn)
{
#else
	:
	TNodeNetTests<TNodePerformance, TEdgePlot>(SIn)
{
#endif
	this->gtsneSuperNode1 = gtsneASuperNode1;
	this->gtsneSuperNode2 = gtsneASuperNode2;
	this->bEmphasized = false;
	TInt iAccumulatedWeight(SIn);
	this->iAccumulatedWeight = iAccumulatedWeight.Val;
}

void TGTreeSuperEdge::Save(TSOut& SOut)
{
#ifndef PERFORMANCE
	TNodeNetTests<TNodePlot, TEdgePlot>::Save(SOut);
#else
	TNodeNetTests<TNodePerformance, TEdgePlot>::Save(SOut);
#endif

	TInt iTemp = this->iAccumulatedWeight;
	iTemp.Save(SOut);
}

TGTreeSuperEdge::~TGTreeSuperEdge()
{
}
bool TGTreeSuperEdge::operator == (const TGTreeSuperEdge& TreeSuperEdge)
{
	return ((&(*this)) == &TreeSuperEdge);
}
bool TGTreeSuperEdge::operator != (const TGTreeSuperEdge& TreeSuperEdge)
{
	return !((*this) == TreeSuperEdge);
}
TGTreeSuperNodeEntry* TGTreeSuperEdge::GetSuperNode1()
{
	return this->gtsneSuperNode1;
};
TGTreeSuperNodeEntry* TGTreeSuperEdge::GetSuperNode2()
{
	return this->gtsneSuperNode2;
};
/*------------------------------------------------------------*/
int TGTreeSuperEdge::GetWeight()
{
	return this->GetEdges();
}
int TGTreeSuperEdge::GetAccumulatedWeight()
{
	return this->iAccumulatedWeight;
}
void TGTreeSuperEdge::AddGTreeEdge(int iASource, int iADestination, int iAWeight)
{
	/*Add edge*/
	if(!this->IsEdge(iASource,iADestination)) {
		/*Add source nodes and set their sizes, shapes and colors*/
		if(!this->IsNode(iASource)) {
			this->AddNode(iASource);
		}
		/*Add destination nodes and set their sizes, shapes and colors*/
		if(!this->IsNode(iADestination)) {
			this->AddNode(iADestination);
		}
		this->GetEI(this->AddEdge(iASource,iADestination))().SetWeight(TInt(iAWeight));
		iAccumulatedWeight += iAWeight;
	}
}

bool TGTreeSuperEdge::IsIt(TGTreeSuperNodeEntry* gtsneASuperNode1, TGTreeSuperNodeEntry* gtsneASuperNode2)
{
	if(
	    (this->gtsneSuperNode1 == gtsneASuperNode1) &&
	    (this->gtsneSuperNode2 == gtsneASuperNode2)
	) {
		return true;
	}
	return false;
}
/*There is an initial support for directed super edges, but everything has been
done considering undirected super edges, that is, (sId1, sId2) = (sId2, sId1)*/
#ifndef PERFORMANCE
bool TGTreeSuperEdge::FindEdge(int iSource, int iDestination, bool bDirected, TNodeNetTests<TNodePlot, TEdgePlot>::TEdgeI& eiAEdge)
{
#else
bool TGTreeSuperEdge::FindEdge(int iSource, int iDestination, bool bDirected, TNodeNetTests<TNodePerformance, TEdgePlot>::TEdgeI& eiAEdge)
{
#endif
	int iEdgeIndex;
	if(!(this->IsNode(iSource) && this->IsNode(iDestination))) {
		return false;
	}
	if(bDirected) {
		if(!this->IsEdge(iSource,iDestination,iEdgeIndex)) {
			return false;
		}
	} else {
		if(!this->IsEdge(iSource,iDestination,iEdgeIndex) &&
		        !this->IsEdge(iDestination,iSource,iEdgeIndex)) {
			return false;
		}
	}
	eiAEdge = this->GetEI(iEdgeIndex);
	return true;
};

bool TGTreeSuperEdge::IsMouseOver(double dAX, double dAY, TDrawSuperGraph *dsgADrawer)
{
	if((this->gtsneSuperNode1 == this->gtsneSuperNode2)
		|| !IsSuperEdgeVisible(this->gtsneSuperNode1,this->gtsneSuperNode2, dsgADrawer)) {
		return false;
	}
	/*We can accept that the mouse is over the SuperEdge
		if the mouse is over the SuperEdge label*/
	if ((dAX >= dPosX) && (dAX <= (dPosX+dWidth)) &&
		(dAY >= dPosY) && (dAY <= (dPosY+dHeight))) {
		return true;
	}

	return false; // TODO Review click/edge intersection
#if 0
	/*The mouse stil may be over the SuperEdge line*/
	dsgTemp->ConvertDeviceToDrawingCoordinates(dAX, dAY);

	double dPrecisionRatio = 1.0/pow(10.0,this->GetSuperNode1()->iLevel+1);
	double dUpperPrecisionRation = 1.0 + dPrecisionRatio;
	double dLowerPrecisionRation = 1.0 - dPrecisionRatio;

	double dXPosSource, dYPosSource;
	double dXPosDestination, dYPosDestination;
	double dM, dDeltaX, dDeltaY, dABS_deltaY, dY0, dTemp, dACalculatedY;

	/*Calculate the SuperEdge bounds - dXPosSource < dXPosDestination, necessarily*/
	this->CalculateEdgeLimits(this->gtsneSuperNode1, this->gtsneSuperNode2,
	                          dXPosSource,dYPosSource,
	                          dXPosDestination,dYPosDestination);

	/*Calculate line parameters*/
	dDeltaX = dXPosDestination - dXPosSource;
	dDeltaY = dYPosDestination - dYPosSource;
	dABS_deltaY = ABS(dDeltaY);
	/*Vertical line, or close to it*/
	if((dDeltaX <= 0.1)&&(dDeltaX >= 0.0)) {
		dTemp = (dXPosSource + dXPosDestination)/2;
		/*Validate X*/
		if((dAX < dTemp*0.99) || (dAX > dTemp*1.01)) {
			return false;
		} else {	/*Validate Y*/
			if((dAY < std::min(dYPosSource,dYPosDestination)*0.99)
			        || (dAY > std::max(dYPosSource,dYPosDestination)*1.01)) {
				return false;
			}
		}
		/*Horizontal line, or close to it*/
	} else if((dABS_deltaY <= 0.1)&&(dABS_deltaY >= 0.0)) {
		dTemp = (dYPosSource + dYPosDestination)/2;
		/*Validate Y*/
		if((dAY < dTemp*0.999) || (dAY > dTemp*1.001)) {
			return false;
		} else {	/*Validate X*/
			if((dAX < std::min(dXPosSource,dXPosDestination)*0.99)
			        || (dAX > std::max(dXPosSource,dXPosDestination)*1.01)) {
				return false;
			}
		}
		/*Non-vertical and non-horizontal line*/
	} else {
		dM = dDeltaY/dDeltaX;
		dY0 = dYPosSource - (dM*dXPosSource);
		dACalculatedY = dM*dAX + dY0;
		if(dACalculatedY < (dAY*dLowerPrecisionRation) || dACalculatedY > (dAY*dUpperPrecisionRation)) {
			return false;
		}
	}
	return true;
#endif
};
void TGTreeSuperEdge::Draw(void *dsgADrawer, TGTreeSuperNodeEntry* gtsneEmphasizedNode)
{
	assert(this->gtsneSuperNode1);
	assert(this->gtsneSuperNode2);
	if(!IsSuperEdgeVisible(this->gtsneSuperNode1,this->gtsneSuperNode2,(TDrawSuperGraph*)dsgADrawer)) {
		return;
	}

	if(this->GetWeight() == 0)	{
		return;    /*No weight, no draw*/
	}

	TDrawSuperGraph* dsgTemp = (TDrawSuperGraph*)dsgADrawer;
	double dXPosSource, dYPosSource;
	double dXPosDestination, dYPosDestination;
	std::string sWeight;
	/*Calculate extreme points of this SuperEdge so that it won't overlap the correspondent supernodes*/
	this->CalculateEdgeLimits(this->gtsneSuperNode1, this->gtsneSuperNode2,dXPosSource,dYPosSource,dXPosDestination,dYPosDestination);
	/*Convert to device coordinates*/
	dsgTemp->ConvertDrawingToDeviceCoordinates(dXPosSource, dYPosSource);
	dsgTemp->ConvertDrawingToDeviceCoordinates(dXPosDestination, dYPosDestination);

	/*Set color and style for drawing*/
	wxPen pen;
	bool bHighlight = (this->GetSuperNode1() == gtsneEmphasizedNode || this->GetSuperNode2() == gtsneEmphasizedNode);
	if (this->bEmphasized || bHighlight) {
		pen = wxPen(wxColour(0,255,0,127));
	} else {
		pen = wxPen(wxColour(0,0,0,100));
	}
	double dLineWidth = ROUND(log((double)this->GetWeight()));
	pen.SetWidth(dLineWidth);
	dsgTemp->GetDeviceContext()->SetPen(pen);

	if(dsgTemp->bVisibleSuperEdges || bEmphasized || bHighlight) {
		/*Draw line*/
		dsgTemp->GetDeviceContext()->DrawLine(dXPosSource, dYPosSource, dXPosDestination, dYPosDestination);
		if(this->bEmphasized) {
			DrawCompleteLabel(dsgADrawer, dXPosSource, dYPosSource, dXPosDestination, dYPosDestination);
		} else {
			DrawSimpleLabel(dsgADrawer, dXPosSource, dYPosSource, dXPosDestination, dYPosDestination);
		}
	}
};
void TGTreeSuperEdge::DrawSimpleLabel(void *dsgADrawer, double dXPosSource, double dYPosSource, double dXPosDestination, double dYPosDestination)
{
	TDrawSuperGraph* dsgTemp = (TDrawSuperGraph*)dsgADrawer;
	std::string sWeight = ToString(this->GetWeight());
	wxSize size = dsgTemp->GetDeviceContext()->GetTextExtent(sWeight);
	dWidth  = size.GetWidth()  + 4;
	dHeight = size.GetHeight() + 4;
	dPosX = (dXPosSource+dXPosDestination-dWidth )/2.0;
	dPosY = (dYPosSource+dYPosDestination-dHeight)/2.0;
	/*Set color and style for writting*/
	wxBLACK_PEN->SetWidth(1);
	dsgTemp->GetDeviceContext()->SetPen(*wxBLACK_PEN);
	/*Label with number of edges*/
	dsgTemp->SetGlobalBrushColor(255, 255, 255);
	dsgTemp->GetDeviceContext()->DrawRectangle(dPosX, dPosY, dWidth, dHeight);
	/*Write weight*/
	dsgTemp->GetDeviceContext()->DrawText(sWeight, dPosX+2, dPosY+2);
	
}
void TGTreeSuperEdge::DrawCompleteLabel(void *dsgADrawer, double dXPosSource, double dYPosSource, double dXPosDestination, double dYPosDestination)
{
	TDrawSuperGraph* dsgTemp = (TDrawSuperGraph*)dsgADrawer;
	std::string line1 = "Edges: "+ToString(this->GetWeight()),
	            line2 = "WeightSum: "+ToString(this->GetAccumulatedWeight());
	wxSize size1 = dsgTemp->GetDeviceContext()->GetTextExtent(line1),
	       size2 = dsgTemp->GetDeviceContext()->GetTextExtent(line2);
	dWidth  = std::max(size1.GetWidth(), size2.GetWidth()) + 4;
	dHeight = size1.GetHeight() + size2.GetHeight() + 4;
	dPosX = (dXPosSource+dXPosDestination-dWidth )/2.0;
	dPosY = (dYPosSource+dYPosDestination-dHeight)/2.0;
	/*Set color and style for writting*/
	wxBLACK_PEN->SetWidth(1);
	dsgTemp->GetDeviceContext()->SetPen(*wxBLACK_PEN);
	dsgTemp->SetGlobalBrushColor(0, 255, 0);
	dsgTemp->GetDeviceContext()->DrawRectangle(dPosX, dPosY, dWidth, dHeight);
	/*Write weight and Accumulated Weight*/
	dsgTemp->GetDeviceContext()->DrawText(line1, dPosX+2, dPosY+2);
	dsgTemp->GetDeviceContext()->DrawText(line2, dPosX+2, dPosY+size1.GetHeight()+2);
};
void TGTreeSuperEdge::CalculateSimpleLabelRectangleParameters(double& dPosX, double& dPosY, double& dWidth, double& dHeight,
        double dXPosSource, double dYPosSource, double dXPosDestination, double dYPosDestination)
{
	dPosX = (dXPosSource + dXPosDestination)/(double)2;
	dPosY = (dYPosSource + dYPosDestination)/(double)2;
	dWidth = TConverter<int>::ToString(this->GetWeight()).size()*8 + 4;
	dHeight = 18;
};
void TGTreeSuperEdge::CalculateCompleteLabelRectangleParameters(double& dPosX, double& dPosY, double& dWidth, double& dHeight,
        double dXPosSource, double dYPosSource, double dXPosDestination, double dYPosDestination)
{
	/*Calculate weight positioning*/
	dPosX = (dXPosSource + dXPosDestination)/(double)2.0;
	dPosY = (dYPosSource + dYPosDestination)/(double)2.0;
	dWidth = (TConverter<int>::ToString(iAccumulatedWeight).size()+11)*8 + 4;
	dHeight = 36;
};

void TGTreeSuperEdge::PrepareContentForPresentation()
{
	/*We set the color for the SuperNodes based on which of them
	  holds source nodes and which do not*/
#ifndef PERFORMANCE
	TNodeNetTests<TNodePlot, TEdgePlot>::TEdgeI eiEdgeTemp = this->BegEI();
#else
	TNodeNetTests<TNodePerformance, TEdgePlot>::TEdgeI eiEdgeTemp = this->BegEI();
#endif
	/*We use the first edge as sample*/
	/*If the source of the first edge is an OpenNode for gtsneSuperNode1
	  then it is not in gtsneSuperNode1*/
	if(this->gtsneSuperNode1->IsOpenNode(eiEdgeTemp.GetSrcNId())) {
		this->gtsneSuperNode1->SetColor("255_000_000");
		this->gtsneSuperNode2->SetColor("050_120_255");
	} else {
		this->gtsneSuperNode1->SetColor("050_120_255");
		this->gtsneSuperNode2->SetColor("255_000_000");
	}
	/*Traverse all edges - Build subgraph and prepare presentation*/
	for(eiEdgeTemp = this->BegEI(); eiEdgeTemp != this->EndEI(); eiEdgeTemp++) {
		/*Sources in red*/
		this->GetNI(eiEdgeTemp.GetSrcNId())().SetShape("c"); /*Circle*/
		this->GetNI(eiEdgeTemp.GetSrcNId())().SetSize(0.05);
		this->GetNI(eiEdgeTemp.GetSrcNId())().SetColor("255_000_000");  /*Red*/
		/*Destinations in blue*/
		this->GetNI(eiEdgeTemp.GetDstNId())().SetShape("s"); /*Square*/
		this->GetNI(eiEdgeTemp.GetDstNId())().SetSize(0.03);
		this->GetNI(eiEdgeTemp.GetDstNId())().SetColor("050_120_255"); /*Blue*/
	}
}


void TGTreeSuperEdge::SetEmphasis(bool bAOption)
{
	bEmphasized = bAOption;
};

void TGTreeSuperEdge::CalculateEdgeLimits(TGTreeSuperNodeEntry* gtneANode1,
        TGTreeSuperNodeEntry* gtneANode2,
        double &dX1, double &dY1, double &dX2, double &dY2)
{

	double dRadius1, dRadius2, dCenterX1, dCenterX2, dCenterY1, dCenterY2;
	/*Set ___1 variables for the smallest X SuperNode*/
	if(gtneANode1->GetCenterX() < gtneANode2->GetCenterX()) { /*Ok*/
		dX1 = gtneANode1->GetCenterX();
		dY1 = gtneANode1->GetCenterY();
		dRadius1 = gtneANode1->dHorizontalSize/2.0; /*dHorizontalSize = dVerticalSize*/
		dCenterX1 = gtneANode1->GetCenterX();
		dCenterY1 = gtneANode1->GetCenterY();
		dX2 = gtneANode2->GetCenterX();
		dY2 = gtneANode2->GetCenterY();
		dRadius2 = gtneANode2->dHorizontalSize/2.0; /*dHorizontalSize = dVerticalSize*/
		dCenterX2 = gtneANode2->GetCenterX();
		dCenterY2 = gtneANode2->GetCenterY();
	} else {	/*We have to invert attributes*/
		dX1 = gtneANode2->GetCenterX();
		dY1 = gtneANode2->GetCenterY();
		dRadius1 = gtneANode2->dHorizontalSize/2.0; /*dHorizontalSize = dVerticalSize*/
		dCenterX1 = gtneANode2->GetCenterX();
		dCenterY1 = gtneANode2->GetCenterY();
		dX2 = gtneANode1->GetCenterX();
		dY2 = gtneANode1->GetCenterY();
		dRadius2 = gtneANode1->dHorizontalSize/2.0; /*dHorizontalSize = dVerticalSize*/
		dCenterX2 = gtneANode1->GetCenterX();
		dCenterY2 = gtneANode1->GetCenterY();
	}
	/*Line parameters*/
	double dDeltaX = dX2 - dX1;
	double dDeltaY = dY2 - dY1;
	dDeltaY = ABS(dDeltaY);

	if(dDeltaX <= 0.001) {
		/*dX1 and dX2 won't change*/
		if(dCenterY1 > dCenterY2) {
			dY1 = dCenterY1-dRadius1;
			dY2 = dCenterY2+dRadius2;
		} else {
			dY1 = dCenterY1+dRadius1;
			dY2 = dCenterY2-dRadius2;
		}
	} else if(dDeltaY <= 0.001) {
		/*dY1 and dY2 won't change*/
		dX1 = dCenterX1 + dRadius1;
		dX2 = dCenterX2 - dRadius2;
	} else {
		double dM = dDeltaY/dDeltaX;

		/*Line/circle intersection -> x=(r^2/(m^2 + 1))^0.5, y=mx*/
		/*(m^2 + 1) as factor1*/
		double dFactor1 = (pow(dM,2.0)+1);
		/*Node 1 - calculus at the origin*/
		dX1 = sqrt(pow(dRadius1,2.0)/dFactor1);
		dY1 = dM*dX1;
		/*Node 2 - calculus at the origin*/
		dX2 = sqrt(pow(dRadius2,2.0)/dFactor1);
		dY2 = dM*dX2;

		/*Now the correspondent translation - highest y at the bottom of the screen*/
		dX1 = dCenterX1+dX1;
		dX2 = dCenterX2-dX2;
		if(dCenterY1 > dCenterY2) {
			dY1 = dCenterY1-dY1;
			dY2 = dCenterY2+dY2;
		} else {
			dY1 = dCenterY1+dY1;
			dY2 = dCenterY2-dY2;
		}
	}
};

void TGTreeSuperEdge::TrackNodeNeighbors(int iANodeId, std::list<int>& lListOfForeignNodeIds)
{
#ifndef PERFORMANCE
	TNodeNetTests<TNodePlot, TEdgePlot>::TEdgeI eiEdgeTemp = this->BegEI();
#else
	TNodeNetTests<TNodePerformance, TEdgePlot>::TEdgeI eiEdgeTemp = this->BegEI();
#endif
	if(!this->IsNode(iANodeId)) {
		return;
	}
	TNodeI niANodeI = this->GetNI(iANodeId);
	int iNeighbors = niANodeI.GetOutDeg();
	int iNextNeighbor;
	for(int i = 0; i < iNeighbors; i++) {
		iNextNeighbor = niANodeI.GetOutNId(i);
		lListOfForeignNodeIds.push_back(iNextNeighbor);
	}
	iNeighbors = niANodeI.GetInDeg();
	for(int i = 0; i < iNeighbors; i++) {
		iNextNeighbor = niANodeI.GetInNId(i);
		lListOfForeignNodeIds.push_back(iNextNeighbor);
	}
}

/*--------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------*/
TGTreeSuperNodeEntry::TGTreeSuperNodeEntry(std::string sId, TGTreeSuperNodeEntry*gtsneAParent)
{
	this->sSuperNodeId = sId;
	this->sSubGraphFile = "";
	this->sExternalEdgesFile = "";
	this->dX = 0.0;
	this->dY = 0.0;
	this->dHorizontalSize = 0.0;
	this->dVerticalSize = 0.0;
	this->bVisible = true;
	this->bIsLeaf = false;
	this->iLevel = 0;
	this->diDrawer = NULL;
	this->sLabel = sId;
	this->bExpanded = false;
	this->gtsneParent = gtsneAParent;
	this->iTotalNumberOfGraphNodesBeneath = 0;
	this->gtsneFocusedNode = NULL;
	this->storage = NULL;
	this->bLoaded = true;
};
TGTreeSuperNodeEntry::TGTreeSuperNodeEntry(InputStorage & s, TGTreeSuperNodeEntry* parent)
	: bVisible(true),
	  diDrawer(NULL),
	  bExpanded(false),
	  gtsneParent(parent),
	  gtsneFocusedNode(NULL),
	  hsListOpenNodes(),
	  storage(&s),
	  bLoaded(false)
{
	// SuperNode properties
	s >> sSuperNodeId;
	s >> sLabel;
	s >> dX;
	s >> dY;
	if (gtsneParent) {
		dX += gtsneParent->dX;
		dY += gtsneParent->dY;
	}
	s >> dHorizontalSize;
	s >> dVerticalSize;
	s >> iTotalNumberOfGraphNodesBeneath;
	s >> iLevel;
	s >> bIsLeaf;
	s >> bRed;
	s >> bGreen;
	s >> bBlue;
	// list of open nodes
	uint count;
	s >> count;
	while (count--) {
		int  i;
		s >> i;
		hsListOpenNodes.insert(i);
	}
	s >> page;
}
TGTreeSuperNodeEntry::~TGTreeSuperNodeEntry()
{
	this->Release(false);
};
bool TGTreeSuperNodeEntry::operator == (const TGTreeSuperNodeEntry& TreeSuperNodeEntry)
{
	/*Compare only by id because these ids are supposed to be unique*/
	if(this->sSuperNodeId.compare(TreeSuperNodeEntry.sSuperNodeId) == 0) {
		return true;
	}
	return false;
};
bool TGTreeSuperNodeEntry::IsVisible()
{
	if(this->gtsneParent == NULL) {
		return true;
	}
	return this->bVisible && this->gtsneParent->IsExpanded();
};

std::string TGTreeSuperNodeEntry::AddTreeNodeEntry(std::string sId)
{
	/*Prevent duplicates*/
	if(this->gtnSubTree.ScanEntries(sId) == NULL) {
		return this->gtnSubTree.AddSuperNodeEntry(sId, this);
	} else {
		return "";
	}
};
void TGTreeSuperNodeEntry::AddConnectivitySuperEdge(TGTreeSuperNodeEntry*gtsneANode, TPt<TGTreeSuperEdge> gtseASuperEdge)
{
	mmConnectivitySuperEdges.push_back(gtseASuperEdge);
}
void TGTreeSuperNodeEntry::AddConnectivitySuperEdge(TGTreeSuperNodeEntry * gtsneANode, TGTree * gtGraphTree)
{
	/*Not calculated connectivity (GetEmphasizedSuperNode()-gtneIdentifiedSuperNode) nor vice-versa?*/
	if (!this->HasConnectivitySuperEdgeAlready(gtsneANode) && !gtsneANode->HasConnectivitySuperEdgeAlready(this)) {
		/*Calculate*/
		TPt<TGTreeSuperEdge> e = gtGraphTree->GetConnectivity(this, gtsneANode);
		/*Store if calculus was valid*/
		if (e != TGTreeSuperEdge::gtseNULLSuperEdge) {
			this->AddConnectivitySuperEdge(gtsneANode, e);
		}
	}
}
void TGTreeSuperNodeEntry::AddConnectivitySuperEdgesRecursive(TGTreeSuperNodeEntry * gtsneANode, TGTree * gtGraphTree)
{
	if (this != gtsneANode) {
		if (this->IsExpanded()) {
			for (TGTreeNode::iterator i = this->gtnSubTree.begin(); i != this->gtnSubTree.end(); ++i) {
				i->AddConnectivitySuperEdgesRecursive(gtsneANode, gtGraphTree);
			}
		} else if (gtsneANode->IsExpanded()) {
			for (TGTreeNode::iterator i = gtsneANode->gtnSubTree.begin(); i != gtsneANode->gtnSubTree.end(); ++i) {
				this->AddConnectivitySuperEdgesRecursive(&(*i), gtGraphTree);
			}
		} else {
			this->AddConnectivitySuperEdge(gtsneANode, gtGraphTree);
		}
	}
}
bool TGTreeSuperNodeEntry::HasConnectivitySuperEdgeAlready(TGTreeSuperNodeEntry*gtsneANode)
{
	std::list< TPt<TGTreeSuperEdge> >::iterator it;
	for (it = this->mmConnectivitySuperEdges.begin(); it != this->mmConnectivitySuperEdges.end(); ++it) {
		if(((*it)->GetSuperNode1() == gtsneANode)||
		        ((*it)->GetSuperNode2() == gtsneANode)
		  ) {
			return true;
		}
	}
	return false;
}
bool TGTreeSuperNodeEntry::DeleteConnectivitySuperEdge(TPt<TGTreeSuperEdge> gtseASuperEdge)
{
	std::list< TPt<TGTreeSuperEdge> >::iterator it;
	for (it = this->mmConnectivitySuperEdges.begin(); it != this->mmConnectivitySuperEdges.end(); ++it) {
		if((*it) == gtseASuperEdge) {
			this->mmConnectivitySuperEdges.erase(it);
			return true;
		}
	}
	return false;
}
bool TGTreeSuperNodeEntry::DeleteConnectivitySuperEdge(TGTreeSuperNodeEntry * gtsneANode)
{
	std::list< TPt<TGTreeSuperEdge> >::iterator it;
	for (it = this->mmConnectivitySuperEdges.begin(); it != this->mmConnectivitySuperEdges.end(); ++it) {
		if (((*it)->GetSuperNode1() == gtsneANode) ||
		        ((*it)->GetSuperNode2() == gtsneANode)) {
			this->mmConnectivitySuperEdges.erase(it);
			return true;
		}
	}
	return false;
}
void TGTreeSuperNodeEntry::DeleteConnectivitySuperEdgesRecursive(TGTreeSuperNodeEntry * gtsneANode)
{
	if (this != gtsneANode) {
		gtsneANode->DeleteConnectivitySuperEdge(this);
		this->DeleteConnectivitySuperEdge(gtsneANode);
		if (gtsneANode->IsExpanded()) {
			for (TGTreeNode::iterator i = gtsneANode->gtnSubTree.begin(); i != gtsneANode->gtnSubTree.end(); ++i) {
				this->DeleteConnectivitySuperEdgesRecursive(&(*i));
			}
		}
	}
}
TPt<TGTreeSuperEdge> TGTreeSuperNodeEntry::IdentifySuperEdge(double dAX, double dAY, TDrawSuperGraph *dsgADrawer)
{
	std::list< TPt<TGTreeSuperEdge> >::iterator iIterator = this->lListSuperEdges.begin();
	for(iIterator = this->lListSuperEdges.begin(); iIterator != this->lListSuperEdges.end(); ++iIterator) {
		if((*iIterator)->IsMouseOver(dAX, dAY,dsgADrawer)) {
			return (*iIterator);
		}
	}
	return TGTreeSuperEdge::gtseNULLSuperEdge;
}

TPt<TGTreeSuperEdge> TGTreeSuperNodeEntry::IdentifyConnectivitySuperEdge(double dAX, double dAY, TDrawSuperGraph *dsgADrawer)
{
	std::list< TPt<TGTreeSuperEdge> >::iterator it = this->lListSuperEdges.begin();
	for (it = this->mmConnectivitySuperEdges.begin(); it != this->mmConnectivitySuperEdges.end(); ++it) {
		if((*it)->IsMouseOver(dAX, dAY, dsgADrawer)) {
			return (*it);
		}
	}
	return TGTreeSuperEdge::gtseNULLSuperEdge;
}
void TGTreeSuperNodeEntry::SetAsLeafNode(std::string sASubGraphFile)
{
	this->sSubGraphFile = sASubGraphFile;
	std::string sTokenToSearch = ".IN.";
	int iStart = sASubGraphFile.rfind(sTokenToSearch, sASubGraphFile.length()-1);
	int iEnd = iStart + 4;
	sExternalEdgesFile = sASubGraphFile.substr(0, iStart) + ".OUT." + sASubGraphFile.substr(iEnd, sASubGraphFile.length() - iEnd);
	this->bIsLeaf = true;
};
bool TGTreeSuperNodeEntry::IsPointOverMBB(double dAX, double dAY)
{
	if(((dAX > this->dX) && (dAX < (this->dX + this->dHorizontalSize)))&&
	        ((dAY > this->dY) && (dAY < (this->dY + this->dVerticalSize)))
	  ) {
		return true;
	}
	return false;
}
bool TGTreeSuperNodeEntry::IsPointOver(double dAX, double dAY)
{
	dAX -= this->GetCenterX();
	dAY -= this->GetCenterY();
	if((pow(dAX,2.0) + pow(dAY,2.0)) <= pow(this->dHorizontalSize/2.0,2.0)) {
		return true;
	}
	return false;
};
bool TGTreeSuperNodeEntry::IsExpanded()
{
	return bExpanded;
};
void TGTreeSuperNodeEntry::SetExpanded(bool bAOption)
{
	bExpanded = bAOption;
	gtsneFocusedNode = NULL;
	if (bExpanded && !bLoaded && storage != NULL) {
		Load(false);
	} else if (!bExpanded && bLoaded && storage != NULL) {
		Release(false);
	}
};
bool TGTreeSuperNodeEntry::LoadGraphTextFile()
{
	if(!this->bIsLeaf) { /*This is exclusively for leaf nodes*/
		return false;
	}
	if (sSubGraphFile.empty()) {
		Load(false);
		return true;
	}

	/*Check necessary instances*/
	if(this->pNodeNetSubGraph.GetRefs() <= 0) {

#ifndef PERFORMANCE
		TPt< TNodeNetTests<TNodePlot, TEdgePlot> > pNodeNetTemp(new TNodeNetTests<TNodePlot, TEdgePlot>());
#else
		TPt< TNodeNetTests<TNodePerformance, TEdgePlot> > pNodeNetTemp(new TNodeNetTests<TNodePerformance, TEdgePlot>());
#endif
		this->pNodeNetSubGraph = pNodeNetTemp;
	}
	/*Initialization and loading*/
	this->iTotalNumberOfGraphNodesBeneath = 0;

	if(!this->pNodeNetSubGraph->LoadGraphTextFile(this->sSubGraphFile)) {
		return false;
	}

	this->iTotalNumberOfGraphNodesBeneath = this->pNodeNetSubGraph->GetNodes();
	return true;
};
void TGTreeSuperNodeEntry::ReleaseSubGraph(bool bRefreshFiles)
{
	if(this->pNodeNetSubGraph.GetRefs() > 0) {
		if(bRefreshFiles) {
			this->pNodeNetSubGraph->WriteNodesFile();
			this->pNodeNetSubGraph->WriteLayoutFile();
		}
		/*Force pointer to NULL*/
#ifndef PERFORMANCE
		TPt< TNodeNetTests<TNodePlot, TEdgePlot> > pNodeNetTemp(NULL);
#else
		TPt< TNodeNetTests<TNodePerformance, TEdgePlot> > pNodeNetTemp(NULL);
#endif
		this->pNodeNetSubGraph = pNodeNetTemp;
		if(this->diDrawer != NULL) {
			delete this->diDrawer;
		}
		this->diDrawer = NULL;
	}
};
void TGTreeSuperNodeEntry::Release(bool saveGraph)
{
	for (TGTreeNode::iterator i = gtnSubTree.begin(); i != gtnSubTree.end(); ++i) {
		i->Release(saveGraph);
	}
	//mmConnectivitySuperEdges.clear();
	lListSuperEdges.clear();
	gtnSubTree.clear();
	sUnresolvedEdges.clear();
	ReleaseSubGraph(saveGraph);
	bExpanded = false;
	bLoaded = false;
};
void TGTreeSuperNodeEntry::SetLabel(std::string sALabel)
{
	sLabel = sALabel;
};
std::string TGTreeSuperNodeEntry::GetLabel()
{
	return sLabel;
};

bool TGTreeSuperNodeEntry::LoadExternalConnectionsFile()
{
	if(!this->bIsLeaf) {	/*This is exclusively for leaf nodes*/
		return false;
	}
	if (sExternalEdgesFile.empty()) {
		Load(false);
		return true;
	}
#ifndef PERFORMANCE
	TPt< TNodeNetTests<TNodePlot, TEdgePlot> > nnExternalEdgesTemp(new TNodeNetTests<TNodePlot, TEdgePlot>());
#else
	TPt< TNodeNetTests<TNodePerformance, TEdgePlot> > nnExternalEdgesTemp(new TNodeNetTests<TNodePerformance, TEdgePlot>());
#endif
	/*Check data*/
	if(this->sExternalEdgesFile.compare("") == 0) {
		return false;
	} else {
		/*A Super node may or may not have out edges*/
		if(FileExists(this->sExternalEdgesFile)) {
			nnExternalEdgesTemp->Clr();
			sUnresolvedEdges.clear();

			/*Read file*/
			nnExternalEdgesTemp->LoadGraphTextFile(sExternalEdgesFile);
			/*Track open nodes for the list of open nodes*/
			nnExternalEdgesTemp->GetAllSourceNodes(hsListOpenNodes);
			/*Add edges to the list of unresolved edges*/
#ifndef PERFORMANCE
			for (TNodeNetTests<TNodePlot, TEdgePlot>::TEdgeI EI = nnExternalEdgesTemp->BegEI(); EI != nnExternalEdgesTemp->EndEI(); EI++) {
#else
			for (TNodeNetTests<TNodePerformance, TEdgePlot>::TEdgeI EI = nnExternalEdgesTemp->BegEI(); EI != nnExternalEdgesTemp->EndEI(); EI++) {
#endif
				sUnresolvedEdges.insert(std::pair<unsigned int,TEdgeTemp*>(
				                            EI.GetSrcNId() + EI.GetDstNId(),
				                            new TEdgeTemp(EI.GetSrcNId(), EI.GetDstNId(),EI().GetWeight())));
			}
			/*Release nnExternalEdgesTemp*/
// TODO verify TPt dtor
#ifndef PERFORMANCE
			TPt< TNodeNetTests<TNodePlot, TEdgePlot> > pNodeNetTemp(NULL);
#else
			TPt< TNodeNetTests<TNodePerformance, TEdgePlot> > pNodeNetTemp(NULL);
#endif
			nnExternalEdgesTemp = pNodeNetTemp;
		}
		return true;
	}
}

bool TGTreeSuperNodeEntry::TranslateSuperNode(double dTX, double dTY)
{
	if(this->gtsneParent == NULL) { /*For the root, never do it*/
		return false;
	} else if( // If node is on first level (parent is root with null parent), or is inside its parent bounds
	    this->gtsneParent->gtsneParent == NULL
	    || (this->gtsneParent->IsPointOverMBB(dX + dTX, dY + dTY) &&
	        this->gtsneParent->IsPointOverMBB(dX + dTX + this->dHorizontalSize,
	                dY + dTY + this->dVerticalSize))) {
		this->dX += dTX;
		this->dY += dTY;
		return true;
	}
	return false;
}
void TGTreeSuperNodeEntry::DrawSuperEdges(void *dsgADrawer, TGTreeSuperNodeEntry* gtsneEmphasizedNode)
{
	std::list< TPt<TGTreeSuperEdge> >::iterator iIterator = lListSuperEdges.begin();
	for(iIterator = lListSuperEdges.begin(); iIterator != lListSuperEdges.end(); ++iIterator) {
		(*iIterator)->Draw(dsgADrawer, gtsneEmphasizedNode);
	}
}
TPt<TGTreeSuperEdge> TGTreeSuperNodeEntry::FindSuperEdgeEntry(TGTreeSuperNodeEntry* gtsneASuperNode1, TGTreeSuperNodeEntry* gtsneASuperNode2, bool bDirected)
{
	std::list< TPt<TGTreeSuperEdge> >::iterator iIterator = lListSuperEdges.begin();
	for(iIterator = lListSuperEdges.begin(); iIterator != lListSuperEdges.end(); ++iIterator) {
		if(bDirected) { /*Directed - it is here but has not been used*/
			if((*iIterator)->IsIt(gtsneASuperNode1, gtsneASuperNode2)) {
				return (*iIterator);
			}
		} else {			/*Undirected - check both possibilities*/
			if((*iIterator)->IsIt(gtsneASuperNode1, gtsneASuperNode2) || (*iIterator)->IsIt(gtsneASuperNode2, gtsneASuperNode1)) {
				return (*iIterator);
			}
		}
	}
	return TGTreeSuperEdge::gtseNULLSuperEdge;
}

void TGTreeSuperNodeEntry::ResolveOpenNodesFromSons()
{
	if(this->bIsLeaf) {
		return;
	}
	TPt<TGTreeSuperEdge> gtseTemp;
	/*Initialized reference iterators*/
	std::list<TGTreeSuperNodeEntry>::iterator iIterator = this->gtnSubTree.begin();
	std::list<TGTreeSuperNodeEntry>::iterator iIterator2 = this->gtnSubTree.begin();
	std::multimap<unsigned int,TEdgeTemp*>::iterator iTemp = iIterator->sUnresolvedEdges.begin();
	std::multimap<unsigned int,TEdgeTemp*>::iterator EI = iIterator->sUnresolvedEdges.begin();
	std::multimap<unsigned int,TEdgeTemp*>::iterator EI2 = iIterator->sUnresolvedEdges.begin();
	std::pair<std::multimap<unsigned int,TEdgeTemp*>::iterator,std::multimap<unsigned int,TEdgeTemp*>::iterator> ret;
	bool bMatch;

	/*Go through all pairs of sons*/
	for(iIterator = this->gtnSubTree.begin(); iIterator != this->gtnSubTree.end(); iIterator++) {
		/*Same loop, starting later - define all pairs*/
		iIterator2 = iIterator;
		iIterator2++;
		for(; iIterator2 != this->gtnSubTree.end(); iIterator2++) {
			/*Retrieve SuperEdge corresponding to SuperNodes (iIterator, iIterator2)*/
			gtseTemp = this->FindSuperEdgeEntry(&(*iIterator), &(*iIterator2), false);
			/*If non-existent, create and add to this' lListSuperEdges*/
			if(gtseTemp == TGTreeSuperEdge::gtseNULLSuperEdge) {	/*Create super edge for the pair (iIterator->sId, iIterator->sId2)*/
				/*Create SuperEdge*/
				gtseTemp = TPt<TGTreeSuperEdge>(new TGTreeSuperEdge(&(*iIterator), &(*iIterator2)));
				/*Store it*/
				lListSuperEdges.push_back(gtseTemp);
			}

			/*For each pair of son nodes, trace every combination of edges in their sUnresolvedEdges*/
			for (EI = iIterator->sUnresolvedEdges.begin(); EI != iIterator->sUnresolvedEdges.end();) {
				bMatch = false;
				ret = iIterator2->sUnresolvedEdges.equal_range((*EI).second->GetDestination() + (*EI).second->GetSource());
				for (EI2 = ret.first; EI2!=ret.second; ++EI2) {
					if(((*EI2).second->GetDestination() == (*EI).second->GetSource() &&
					        (*EI2).second->GetSource() == (*EI).second->GetDestination())
					  ) {
						bMatch = true;
						break;
					}
				}

				//find(
				//	CantorFunction((*EI).second->GetDestination(),(*EI).second->GetSource())
				//);
				/*if something was found*/
				if(bMatch) {
					/*Add just found edge to correspondent SuperEdge*/
					gtseTemp->AddGTreeEdge((*EI).second->GetSource(), (*EI).second->GetDestination(), (*EI).second->GetWeight());
					iTemp = EI;
					EI++;
					/*Clean up*/
					delete (*iTemp).second;
					iIterator->sUnresolvedEdges.erase(iTemp);
					delete (*EI2).second;
					iIterator2->sUnresolvedEdges.erase(EI2);
				} else {
					EI++;
				}
			}
			/*No edges found between these two son SuperNodes*/
			if(gtseTemp->GetEdges() == 0) {
				lListSuperEdges.pop_back();    /*Unstore the new SuperEdge*/
			}
		}
	}

	/*After resolving the edges, we build the list of unresolved edges and open nodes for this*/
	this->hsListOpenNodes.clear();
	this->sUnresolvedEdges.clear();
	for(iIterator = this->gtnSubTree.begin(); iIterator != this->gtnSubTree.end(); iIterator++) {
		/*Copy remaining unresolved edges a level up*/
		/*Set list of open nodes*/
		for (EI = iIterator->sUnresolvedEdges.begin(); EI != iIterator->sUnresolvedEdges.end(); EI++) {
			this->sUnresolvedEdges.insert(
			    std::pair<unsigned int,TEdgeTemp*>(
			        (*EI).second->GetSource() + (*EI).second->GetDestination(), (*EI).second)
			);
			this->hsListOpenNodes.insert((*EI).second->GetSource());
		}
	}

	for(iIterator = this->gtnSubTree.begin(); iIterator != this->gtnSubTree.end(); iIterator++) {
		/*for (EI = iIterator->sUnresolvedEdges.begin(); EI != iIterator->sUnresolvedEdges.end(); EI++){
		 delete (*EI).second;
		iIterator->sUnresolvedEdges.erase(EI);
		}*/
		iIterator->sUnresolvedEdges.clear();
	}
};
bool TGTreeSuperNodeEntry::IsOpenNode(int iANodeId)
{
	std::hash_set<int>::iterator iHashIterator;
	iHashIterator = this->hsListOpenNodes.find(iANodeId);
	if(iHashIterator == this->hsListOpenNodes.end()) {
		return false;
	}
	return true;
}
#ifndef PERFORMANCE
bool TGTreeSuperNodeEntry::GetEmphasizedNodeInSubGraph(TNodeNetTests<TNodePlot, TEdgePlot>::TNodeI& niANode)
{
#else
bool TGTreeSuperNodeEntry::GetEmphasizedNodeInSubGraph(TNodeNetTests<TNodePerformance, TEdgePlot>::TNodeI& niANode)
{
#endif
	return this->pNodeNetSubGraph->GetEmphasizedNode(niANode);
}

bool TGTreeSuperNodeEntry::IsAncestor(TGTreeSuperNodeEntry* gtsneASuperNode)
{
	TGTreeSuperNodeEntry* gtsneTemp = this->gtsneParent;
	while(gtsneTemp != NULL) {
		if(gtsneTemp == gtsneASuperNode) {
			return true;
		}
		gtsneTemp = gtsneTemp->gtsneParent;
	}
	return false;

}
void TGTreeSuperNodeEntry::SetColor(std::string sAColorRGBCode)
{
	std::string sComponent;
	/*Color string - xxx_xxx_xxx - 3 decimal digits for each RGB color component*/
	sComponent = sAColorRGBCode.substr(0, 3);
	this->bRed = (unsigned char)TConverter<int>::FromString(sComponent);
	sComponent = sAColorRGBCode.substr(4, 3);
	this->bGreen = (unsigned char)TConverter<int>::FromString(sComponent);
	sComponent = sAColorRGBCode.substr(8, 3);
	this->bBlue = (unsigned char)TConverter<int>::FromString(sComponent);
}

void TGTreeSuperNodeEntry::SetColor(double h, double s, double v)
{
	wxImage::HSVValue hsv (h,s,v);
	wxImage::RGBValue rgb = wxImage::HSVtoRGB(hsv);
	bRed   = rgb.red;
	bGreen = rgb.green;
	bBlue  = rgb.blue;
}

void TGTreeSuperNodeEntry::TrackNodeSuperEdgesNeighbors(std::list<int>& lListOfForeignNeighborsIds, int iANodeId, TGTreeSuperNodeEntry* gtsneSuperNodeWhereNodeIsAOpenNode)
{
	std::list< TPt<TGTreeSuperEdge> >::iterator iIterator = lListSuperEdges.begin();
	for(iIterator = lListSuperEdges.begin(); iIterator != lListSuperEdges.end(); ++iIterator) {
		/*Optmization: only check SuperEdges that have gtsneSuperNodeWhereNodeIsAOpenNode
		  as one of its SuperNodes.*/
		if(((*iIterator)->GetSuperNode1() == gtsneSuperNodeWhereNodeIsAOpenNode) ||
		        ((*iIterator)->GetSuperNode2() == gtsneSuperNodeWhereNodeIsAOpenNode)
		  ) {
			(*iIterator)->TrackNodeNeighbors(iANodeId, lListOfForeignNeighborsIds);
		}
	}
}
/*--------------------------------------------------------------------------------*/
double TGTreeSuperNodeEntry::GetCenterX()
{
	return this->dX + this->dHorizontalSize/2.0;
}
double TGTreeSuperNodeEntry::GetCenterY()
{
	return this->dY + this->dVerticalSize/2.0;
}
std::string TGTreeSuperNodeEntry::GetCompactLabel()
{
	size_t i = sLabel.find('=');
	if (i < 0 || i == -1) {
		return sLabel;
	} else {
		return sLabel.substr(i+1);
	}
}
std::string TGTreeSuperNodeEntry::GetAncestorsLabel()
{
	TGTreeSuperNodeEntry* ancestor = this;
	std::string sAncestorsLabel = sLabel;

	TGTreeNode::iterator it = this->gtnSubTree.begin();
	if (it != this->gtnSubTree.end()) {
		std::string sSonLabel = it->sLabel;
		size_t i = sSonLabel.find('=');
		if (i > 0 && i != -1) {
			sAncestorsLabel += " : " + sSonLabel.substr(0,i) + "?";
		}
	}
	for (ancestor = ancestor->gtsneParent;
	        ancestor->gtsneParent != NULL;
	        ancestor = ancestor->gtsneParent) {
		sAncestorsLabel = ancestor->sLabel + " : " + sAncestorsLabel;
	}
	return sAncestorsLabel;
}
/*--------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------*/
/*For pre-build operations*/
/*Save saved SuperNode from disk*/
void TGTreeSuperNodeEntry::Save(std::string sADirectory)
{
	std::string sSuperEdgeFile;
	std::string sSuperNodesIds;
	/*Prepare directory*/
	std::string sNodeEntryDir = sADirectory + wxFileName::GetPathSeparator() + this->sSuperNodeId;
	EmptyAndRemoveDirectory(sNodeEntryDir);
	CreateDirectory(sNodeEntryDir.c_str(), NULL);
	/*Save list of open nodes*/
	hsListOpenNodes.Save(sNodeEntryDir + wxFileName::GetPathSeparator() + "OpenNodes.bin");
	/*Save all SuperEdges*/
	std::list< TPt<TGTreeSuperEdge> >::iterator iIterator = this->lListSuperEdges.begin();
	for(iIterator = this->lListSuperEdges.begin(); iIterator != this->lListSuperEdges.end(); ++iIterator) {
		/*Build file name*/
		sSuperNodesIds = (*iIterator)->GetSuperNode1()->sSuperNodeId + "_" +(*iIterator)->GetSuperNode2()->sSuperNodeId;
		sSuperEdgeFile = sNodeEntryDir + wxFileName::GetPathSeparator() + "SuperEdge" + "_" +sSuperNodesIds;
		/*Save*/
		TFInOut out(sSuperEdgeFile.c_str(), faCreate, true);
		(*iIterator)->Save(out);
	}

	/*Create file for this SuperNode specifically*/
	std::string sSuperNodeFileName = sNodeEntryDir + wxFileName::GetPathSeparator() + "SuperNode" + this->sSuperNodeId;
	TFInOut fioSuperNodeFile(sSuperNodeFileName.c_str(), faCreate, true);
	/*Save properties*/
	TStr sTemp;
	sTemp = ExtractFileName(sSubGraphFile).c_str();
	sTemp.Save(fioSuperNodeFile);
	sTemp = ExtractFileName(sExternalEdgesFile).c_str();
	sTemp.Save(fioSuperNodeFile);
	sTemp = sLabel.c_str();
	sTemp.Save(fioSuperNodeFile);
	sTemp = sSuperNodeId.c_str();
	sTemp.Save(fioSuperNodeFile);
	TFlt lfTemp;
	lfTemp.Val = dX;
	lfTemp.Save(fioSuperNodeFile);
	lfTemp.Val = dY;
	lfTemp.Save(fioSuperNodeFile);
	lfTemp.Val = dHorizontalSize;
	lfTemp.Save(fioSuperNodeFile);
	lfTemp.Val = dVerticalSize;
	lfTemp.Save(fioSuperNodeFile);
	TInt tiTemp;
	tiTemp.Val = iTotalNumberOfGraphNodesBeneath;
	tiTemp.Save(fioSuperNodeFile);
	tiTemp.Val = iLevel;
	tiTemp.Save(fioSuperNodeFile);
	tiTemp.Val = (int)bIsLeaf;
	tiTemp.Save(fioSuperNodeFile);
}
/*Load saved SuperNode from disk*/
void TGTreeSuperNodeEntry::Load(std::string sADirectory)
{
	std::list<std::string> lListOfFiles;
	/*Build directory name*/
	std::string sNodeEntryDir = sADirectory + wxFileName::GetPathSeparator() + this->sSuperNodeId;
	hsListOpenNodes.Load(sNodeEntryDir + wxFileName::GetPathSeparator() + "OpenNodes.bin");
	/*Read all files*/
	GetFilesFromDir(sNodeEntryDir, &lListOfFiles);

	std::list<std::string>::iterator iLIterator;
	std::string sFileName;
	std::string sId1, sId2;
	int iStart, iEnd;
	std::string sTokenToSearch = "_";
	/*Look for SuperEdges files, one for each pair of sons stored in this->gtnSubTree*/
	for(iLIterator = lListOfFiles.begin(); iLIterator != lListOfFiles.end(); iLIterator++) {
		sFileName = (*iLIterator);
		/*Look for SuperEdge file name pattern*/
		if(sFileName.find("SuperEdge", 0) != -1) {
			/*Parse Ids*/
			iStart = sFileName.rfind(sTokenToSearch, sFileName.length()-1);
			iEnd = sFileName.rfind(sTokenToSearch, iStart-1);
			sId1 = sFileName.substr(iEnd+1, (iStart-1) - iEnd);
			sId2 = sFileName.substr(iStart+1, sFileName.length() - iEnd);
			/*Create and fill SuperEdge data from correspondent SuperEdge file*/
			std::string sSuperEdgeFile = sNodeEntryDir + wxFileName::GetPathSeparator() + sFileName;
			TFInOut SIn(sSuperEdgeFile.c_str(), faRdOnly, false);
			TPt<TGTreeSuperEdge> ptseTemp(new TGTreeSuperEdge(this->gtnSubTree.ScanEntries(sId1),
			                              this->gtnSubTree.ScanEntries(sId2),
			                              SIn));
			/*Store it*/
			lListSuperEdges.push_back(ptseTemp);
		}
	}
	/*File name for this SuperNode*/
	std::string sSuperNodeFileName = sNodeEntryDir + wxFileName::GetPathSeparator() + "SuperNode" + this->sSuperNodeId;
	TFInOut fioSuperNodeFile(sSuperNodeFileName.c_str(), faRdOnly, false);
	/*Read properties*/
	std::string sDirectoryForGraphFiles = sADirectory.substr(0,sADirectory.rfind(wxFileName::GetPathSeparator()));
	TStr strSubGraphFile(fioSuperNodeFile);
	sSubGraphFile = strSubGraphFile.CStr();
	sSubGraphFile = sDirectoryForGraphFiles + wxFileName::GetPathSeparator() + ExtractFileName(sSubGraphFile);

	TStr strExternalEdgesFile(fioSuperNodeFile);
	sExternalEdgesFile = strExternalEdgesFile.CStr();
	sExternalEdgesFile = sDirectoryForGraphFiles + wxFileName::GetPathSeparator() + ExtractFileName(sExternalEdgesFile);

	TStr strLabel(fioSuperNodeFile);
	sLabel = strLabel.CStr();
	TStr strId(fioSuperNodeFile);
	sSuperNodeId = strId.CStr();
	TFlt lfX(fioSuperNodeFile);
	dX = lfX.Val;
	TFlt lfY(fioSuperNodeFile);
	dY = lfY.Val;
	TFlt lfHorizontalSize(fioSuperNodeFile);
	dHorizontalSize = lfHorizontalSize.Val;
	TFlt lfVerticalSize(fioSuperNodeFile);
	dVerticalSize = lfVerticalSize.Val;
	TInt tiTotalNumberOfGraphNodesBeneath(fioSuperNodeFile);
	iTotalNumberOfGraphNodesBeneath = tiTotalNumberOfGraphNodesBeneath.Val;
	TInt tiLevel(fioSuperNodeFile);
	iLevel = tiLevel.Val;
	TInt tiIsLeaf(fioSuperNodeFile);
	bIsLeaf = (bool)tiIsLeaf.Val;
}
void TGTreeSuperNodeEntry::Save(OutputStorage & s)
{
	// SuperNode properties
	s << sSuperNodeId;
	s << sLabel;
	double p_dX = gtsneParent ? gtsneParent->dX : 0,
	       p_dY = gtsneParent ? gtsneParent->dY : 0;
	s << dX - p_dX;
	s << dY - p_dY;
	s << dHorizontalSize;
	s << dVerticalSize;
	s << iTotalNumberOfGraphNodesBeneath;
	s << iLevel;
	s << bIsLeaf;
	s << bRed;
	s << bGreen;
	s << bBlue;
	// list of open nodes
	uint count = hsListOpenNodes.size();
	s << count;
	for (TFileHash_set<int>::iterator i = hsListOpenNodes.begin(); i != hsListOpenNodes.end(); ++i) {
		s << *i;
	}
	s.NewPage(this->page);
	// list of child nodes
	count = gtnSubTree.size();
	s << count;
	for (TGTreeNode::iterator i = gtnSubTree.begin(); i != gtnSubTree.end(); ++i) {
		bool refresh = !i->bLoaded;
		if (refresh) {
			i->Load(false); // i->bLoaded changed inside i->Load()
		}
		i->Save(s);
		if (refresh) {
			i->Release(true);
		}
	}
	// list of SuperEdges
	count = lListSuperEdges.size();
	s << count;
	for(std::list< TPt<TGTreeSuperEdge> >::iterator i = lListSuperEdges.begin(); i != lListSuperEdges.end(); ++i) {
		TGTreeSuperNodeEntry *n1 = (*i)->GetSuperNode1();
		TGTreeSuperNodeEntry *n2 = (*i)->GetSuperNode2();
		s << n1->sSuperNodeId;
		s << n2->sSuperNodeId;
		(*i)->Save(s);
	}
	if (bIsLeaf) {
		assert(!pNodeNetSubGraph.Empty());
		// unresolved edges
		count = sUnresolvedEdges.size();
		s << count;
		for (std::multimap<uint,TEdgeTemp*>::iterator it = sUnresolvedEdges.begin(); it != sUnresolvedEdges.end(); ++it) {
			uint id = it->first;
			TEdgeTemp* e = it->second;
			s << id << e->GetSource() << e->GetDestination() << e->GetWeight();
		}
		// subgraph nodes
		pNodeNetSubGraph->Save(s);
	}
	s.EndPage();
}
void TGTreeSuperNodeEntry::Load(bool recursive)
{
	assert(storage != NULL);
	if (bLoaded) {
		return;
	}
	CpuTimeLog log("SN::Load");
	InputStorage & s = *storage;
	s.NewPage(this->page);
	// list of child nodes
	uint count;
	s >> count;
	while (count--) {
		gtnSubTree.push_back((TGTreeSuperNodeEntry(s,this)));
		if (recursive) {
			gtnSubTree.back().Load(recursive);
		}
	}
	// list of SuperEdges
	s >> count;
	while (count--) {
		std::string sId1, sId2;
		s >> sId1;
		s >> sId2;
		TGTreeSuperNodeEntry *n1 = gtnSubTree.ScanEntries(sId1);
		TGTreeSuperNodeEntry *n2 = gtnSubTree.ScanEntries(sId2);
		assert(n1 != NULL);
		assert(n2 != NULL);
		TPt<TGTreeSuperEdge> e( new TGTreeSuperEdge(n1, n2, s) );
		lListSuperEdges.push_back(e);
	}
	if (bIsLeaf) {
		// unresolved edges
		s >> count;
		while (count--) {
			uint id;
			int src, dest, weight;
			s >> id >> src >> dest >> weight;
			TEdgeTemp *e = new TEdgeTemp(src, dest, weight);
			sUnresolvedEdges.insert( std::pair<uint,TEdgeTemp*>(id,e) );
		}
		// subgraph nodes
#ifndef PERFORMANCE
		pNodeNetSubGraph = TPt< TNodeNetTests<TNodePlot,TEdgePlot> >(new TNodeNetTests<TNodePlot,TEdgePlot>(s));
#else
		pNodeNetSubGraph = TPt< TNodeNetTests<TNodePerformance,TEdgePlot> >(new TNodeNetTests<TNodePerformance,TEdgePlot>(s));
#endif
	}
	bLoaded = true;
	s.EndPage();
}
/*
void TGTreeSuperNodeEntry::Save(GraphTree::Node& node) {
	node.label		  = sLabel.c_str();
	GTreeDisk::Node::Data* data = node.GetDat();
	data->nodeId		 = TConverter<uint32>::FromString(sSuperNodeId);
	data->x				= dX;
	data->y				= dY;
	data->width		  = dHorizontalSize;
	data->height		 = dVerticalSize;
	data->nodesBeneath = iTotalNumberOfGraphNodesBeneath;
	data->level		  = iLevel;
	data->isLeaf		 = bIsLeaf;
}
void TGTreeSuperNodeEntry::Load(GraphTree::Node& node) {
	sLabel = node.label;
	GTreeDisk::Node::Data* data = node.GetDat();
	sSuperNodeId	 = TConverter<wxUint32>::ToString(data->nodeId);
	dX				  = data->x;
	dY				  = data->y;
	dHorizontalSize = data->width;
	dVerticalSize	= data->height;
	iTotalNumberOfGraphNodesBeneath = data->nodesBeneath;
	iLevel			 = data->level;
	bIsLeaf			= data->isLeaf;
}
*/
/*--------------------------------------------------------------------------------*/

#endif
