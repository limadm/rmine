#include "LayoutBipartite.h"

bool ComparePairBySecond(const std::pair<int,int> &a, const std::pair<int,int> &b) {
	return a.second > b.second;
}