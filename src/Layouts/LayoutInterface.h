#pragma once
#ifndef _LayoutInterface_
#define _LayoutInterface_

#include <Commons/stdafx.h>

template <class _TNodeNet>
DeclareInterface(TLayoutInterface){
public:
   virtual void ApplyLayout(TPt<_TNodeNet> pANodeNetData) = 0;
EndInterface

#endif
