#pragma once
#ifndef _LayoutBiPartite_
#define _LayoutBiPartite_

#include <list>
#include <algorithm>

#include <Commons/Auxiliar.h>
#include <Layouts/LayoutInterface.h>

template <class _TNodeNet>
class TLayoutBiPartite : implements TLayoutInterface<_TNodeNet>
{
private:
	std::list<int>* lListOfIsolatedNodes;
public:
	virtual void ApplyLayout(TPt<_TNodeNet> pANodeNetData);
};

bool ComparePairBySecond(const std::pair<int,int> &a, const std::pair<int,int> &b);

template <class _TNodeNet>
void TLayoutBiPartite<_TNodeNet>::ApplyLayout(TPt<_TNodeNet> pANodeNetData)
{
	/*Layout already applied*/
	if(pANodeNetData->bLayoutFlag) {
		return;
	}
	uint size = pANodeNetData->GetNodes();

	/*One single node case*/
	if(size == 1) {
		/*A single node is a special case*/
		(pANodeNetData->BegNI())().SetX(0.5);
		(pANodeNetData->BegNI())().SetY(0.5);
		pANodeNetData->bLayoutFlag = true;
		return;
	}
	
	typename _TNodeNet::TNodeI NI;
	std::pair<int,int> *sortedNodes = new std::pair<int,int>[size];

	int iColor1Counter = 0,
	    iColor2Counter = 0;

	/*Initialize color1 with first item color*/
	std::string sColor1 = (pANodeNetData->BegNI())().GetColor();

	/*Count elements of each color and set their X position and size*/
	uint i = 0;
	for (NI = pANodeNetData->BegNI(); NI != pANodeNetData->EndNI(); NI++) {
		sortedNodes[i].first  = NI.GetId();
		sortedNodes[i].second = NI.GetDeg();
		++i;
		if(NI().GetColor().compare(sColor1) == 0) {
			NI().SetX(0.45);
			iColor1Counter++;
		} else {
			NI().SetX(0.65);
			iColor2Counter++;
		}
		NI().SetSize(0.01);
	}
	std::sort(sortedNodes, sortedNodes+size, ComparePairBySecond);

	/*Calculate increment for each color*/
	double dYPosColor1 = 0.0,
	       dYPosColor2 = 0.0,
	       dYPosColor1Increment = 1.0/(double)(iColor1Counter+2),
	       dYPosColor2Increment = 1.0/(double)(iColor2Counter+2);

	dYPosColor1Increment = std::max(0.02, dYPosColor1Increment),
	dYPosColor2Increment = std::max(0.02, dYPosColor2Increment);

	/*Incrementaly set the X positions of each element*/
	for (uint i = 0; i < size; ++i) {
		NI = pANodeNetData->GetNI(sortedNodes[i].first);
		if(NI().GetColor().compare(sColor1) == 0) {
			NI().SetY(dYPosColor1);
			dYPosColor1 += dYPosColor1Increment;
		} else {
			NI().SetY(dYPosColor2);
			dYPosColor2 += dYPosColor2Increment;
		}
	}
	pANodeNetData->bLayoutFlag = true;
}


#endif