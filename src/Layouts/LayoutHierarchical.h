#pragma once
#ifndef _LayoutHierarchical_
#define _LayoutHierarchical_

#include <set>
#include <vector>

#include <Commons/stdafx.h>
#include <Commons/Auxiliar.h>
#include <Layouts/LayoutInterface.h>

template <class _TNodeNet>
class TLayoutHierarchical : implements TLayoutInterface<_TNodeNet>{
public:
   virtual void ApplyLayout(TPt<_TNodeNet> pANodeNetData); 
private:
   std::vector<double> vPreCalculatedRadius;
   void PreCalculateRadius(TPt<_TNodeNet> pANodeNetData);
   double GetRadiusForLevel(int iALevel){ 
      std::vector<double>::iterator vLevelCounter = vPreCalculatedRadius.begin();
      return (*(vLevelCounter + iALevel));
   };
   void ApplyLayoutRecursive(typename _TNodeNet::TNodeI niParentNode, double dParentsShare, double dParentPlotingAngle, TPt<_TNodeNet> pANodeNetData);
   void SetCartesianPointFromPolarCoordinates(typename _TNodeNet::TNodeI& niANode,
                                              float fARadius,
                                              float fARadianusAngleAlpha);
};

template <class _TNodeNet>
void TLayoutHierarchical<_TNodeNet>::PreCalculateRadius(TPt<_TNodeNet> pANodeNetData){
   vPreCalculatedRadius.clear();
   double dLastRadius = 0;
   double dRadius = 0;
   vPreCalculatedRadius.push_back(dRadius);
   for(int iCounter = 1; iCounter <= pANodeNetData->GetMaxInfluenceLevel(); iCounter++){
      dRadius = dLastRadius + 0.5*((double)pANodeNetData->GetNumberOfNodesInLevel(iCounter)/(double)pANodeNetData->GetNodes());
      dLastRadius = dRadius;
      vPreCalculatedRadius.push_back(dRadius);
   }
}

template <class _TNodeNet>
void TLayoutHierarchical<_TNodeNet>::ApplyLayout(TPt<_TNodeNet> pANodeNetData){
   PreCalculateRadius(pANodeNetData);
   std::set<int>& sFirstLevelNodes = pANodeNetData->GetFirstLevelNodes();   
   std::set<int>::iterator sIterator;

   /*Create a node that is to be used as the root of the influence hierarchy*/
   const int iMaxInt = (std::numeric_limits<int>::max)();
   pANodeNetData->AddNode(iMaxInt);
   typename _TNodeNet::TNodeI niRoot = pANodeNetData->GetNI(iMaxInt);
   niRoot().SetInfluenceLevel(0);
   /*Set the first level nodes as sons of the root*/
   niRoot().ClearNodeSons();
   for (sIterator = sFirstLevelNodes.begin(); sIterator != sFirstLevelNodes.end(); ++sIterator){
      niRoot().AddSonToInfluenceSet(*sIterator);
   }

   ApplyLayoutRecursive(niRoot, 0, 1, pANodeNetData);

   /*Remove root from the graph structure*/
   pANodeNetData->DelNode(iMaxInt);
   pANodeNetData->bLayoutFlag = true;
}

template <class _TNodeNet>
void TLayoutHierarchical<_TNodeNet>::ApplyLayoutRecursive(typename _TNodeNet::TNodeI niParentNode,
                                                          double dParentPlotingAngle,
                                                          double dParentsShare,
                                                          TPt<_TNodeNet> pANodeNetData){
   typename _TNodeNet::TNodeI niNextSon = niParentNode;
   std::set<int>::const_iterator sIterator;

   double dThisNodeCircleShareRatio;   double dThisNodeAngleShare;
   double dThisNodePlotingAngle;       int iSonIndex = 0;
   double dAcumulatedAngle = 0;
   int iParentsGrandSons = pANodeNetData->GetNumberOfGrandSons(niParentNode);
   for (sIterator = niParentNode().GetSonNodes().begin(); sIterator != niParentNode().GetSonNodes().end(); sIterator++){
      niNextSon = pANodeNetData->GetNI(*sIterator);
      int i = niNextSon.GetId();
      if(niNextSon().GetNumberOfSons() == 0)
         dThisNodeCircleShareRatio = 1/(double)niParentNode().GetNumberOfSons();
      else
         dThisNodeCircleShareRatio = niNextSon().GetNumberOfSons()/(double)iParentsGrandSons;

      dThisNodeAngleShare = dThisNodeCircleShareRatio*(dParentsShare - 0.04);
      if((dThisNodeAngleShare > 0.5) && (niNextSon().GetInfluenceLevel() > 1))
         dThisNodeAngleShare = 0.5;

      dThisNodePlotingAngle = dParentPlotingAngle - dParentsShare/(double)2 + dAcumulatedAngle + dThisNodeAngleShare/(double)2;
      this->SetCartesianPointFromPolarCoordinates(niNextSon,
                                                  GetRadiusForLevel(niNextSon().GetInfluenceLevel()),
                                                  dThisNodePlotingAngle*_2PI);

      ApplyLayoutRecursive(niNextSon, dThisNodePlotingAngle, dThisNodeAngleShare, pANodeNetData);
      dAcumulatedAngle += dThisNodeAngleShare;
      iSonIndex++;
   }
}

template <class _TNodeNet>
void TLayoutHierarchical<_TNodeNet>::SetCartesianPointFromPolarCoordinates(typename _TNodeNet::TNodeI& niANode,
                                                                           float fARadius,
                                                                           float fARadianusAngleAlpha){
   if(niANode().GetVisible()){
      /*Gives Cartesian coordinates having the origin (0,0) as reference*/
      niANode().SetX(0.5+fARadius*cos(fARadianusAngleAlpha));
      niANode().SetY(0.5+fARadius*sin(fARadianusAngleAlpha));
   }
}

#endif