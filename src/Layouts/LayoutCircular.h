#pragma once
#ifndef _LayoutCircular_
#define _LayoutCircular_

#include "LayoutInterface.h"
#include <cmath>

template <class TNodeNet>
class TLayoutCircular : public TLayoutInterface<TNodeNet>{
public:
	void ApplyLayout(TPt<TNodeNet> graph) {
		if (graph->bLayoutFlag)
			return;
		double angle = 0;
		double step = 2 * M_PI / graph->GetNodes();
		for (typename TNodeNet::TNodeI ni = graph->BegNI(); ni != graph->EndNI(); ni++) {
			double x = 0.5 * (1 + std::cos(angle));
			double y = 0.5 * (1 + std::sin(angle));
			ni().SetXY(x, y);
			angle += step;
		}
		//graph->bLayoutFlag = true;
	}
};

#endif