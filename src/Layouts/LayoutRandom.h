#pragma once
#ifndef _LayoutRandom_
#define _LayoutRandom_

#include "LayoutInterface.h"
#include <cmath>
#include <ctime>
#include <cstdlib>

template <class TNodeNet>
class TLayoutRandom : public TLayoutInterface<TNodeNet>{
private:
	double random() {
		return std::rand() / (double) RAND_MAX;
	}
public:
	void ApplyLayout(TPt<TNodeNet> graph) {
		if (graph->bLayoutFlag)
			return;
#ifdef DEBUG
		std::srand(108237);
#else
		std::srand(std::time(NULL));
#endif
		for (typename TNodeNet::TNodeI ni = graph->BegNI(); ni != graph->EndNI(); ni++) {
			// random polar coordinates
			double angle  = random() * 2.0 * M_PI;
			double radius = random();
			// convert to cartesian
			double x = 0.5 * (radius * std::cos(angle) + 1);
			double y = 0.5 * (radius * std::sin(angle) + 1);
			ni().SetXY(x, y);
		}
		//graph->bLayoutFlag = true;
	}
};

#endif