#pragma once
#ifndef _LayoutForceDirected_
#define _LayoutForceDirected_

#include <list>
#include <limits>

#include <Commons/Auxiliar.h>
#include <Layouts/LayoutInterface.h>

/*Auxiliar class*/
class T2DVector{
public:
   double dX;  double dY;
   T2DVector(double dAX, double dAY):dX(dAX),dY(dAY){};
   T2DVector():dX(0), dY(0){};
   T2DVector& operator = (const T2DVector& t2dvA2DVector) { 
      dX = t2dvA2DVector.dX; dY = t2dvA2DVector.dY; return *this; }
   double GetNorm(){
      double dValue = sqrt(pow(this->dX,2) + pow(this->dY,2));
      return dValue;
   }
};

template <class _TNodeNet>
class TLayoutForceDirected : implements TLayoutInterface<_TNodeNet>{
private:
   std::list<int> lListOfIsolatedNodes;
public:
   virtual void ApplyLayout(TPt<_TNodeNet> pANodeNetData);
   virtual void ConnectIsolatedNodes(TPt<_TNodeNet> pANodeNetData);
   virtual void DisconnectIsolatedNodes(TPt<_TNodeNet> pANodeNetData);
   double GetRepulsiveForce(double k, double d);
   double GetAttractiveForce(double k,double d);
};

template <class _TNodeNet>
double TLayoutForceDirected<_TNodeNet>::GetRepulsiveForce(double k, double d){
   if(d <= 0) return 1.0;
   return k * k / d;
}

template <class _TNodeNet>
double TLayoutForceDirected<_TNodeNet>::GetAttractiveForce(double k, double d){
   if(d <= 0) return 0.0;
   return d * d / k;
}

/*Nodes disconnected to everything cannot be positioned by the algorithm.
  For this we add temporary edges to guarantee a better positioning.
  Later we remove these edges.*/
template <class _TNodeNet>
void TLayoutForceDirected<_TNodeNet>::ConnectIsolatedNodes(TPt<_TNodeNet> pANodeNetData){
   /*Find isolated nodes*/
   for (typename _TNodeNet::TNodeI NI = pANodeNetData->BegNI(); NI < pANodeNetData->EndNI(); NI++) {
      if(NI.GetDeg() == 0)
         lListOfIsolatedNodes.push_back(NI.GetId());
   }
   std::list<int>::iterator iListIterator;  int iTempNode = (std::numeric_limits<int>::max)();
   if(lListOfIsolatedNodes.size() == 0)
      return;
   /*Add a temporary node for connectivity*/
   pANodeNetData->AddNode(iTempNode);
   for(iListIterator = lListOfIsolatedNodes.begin();iListIterator != lListOfIsolatedNodes.end();iListIterator++){
      pANodeNetData->AddEdge(iTempNode, (*iListIterator));
   }
}
template <class _TNodeNet>
void TLayoutForceDirected<_TNodeNet>::DisconnectIsolatedNodes(TPt<_TNodeNet> pANodeNetData){
   if(lListOfIsolatedNodes.size() == 0)
      return;
   std::list<int>::iterator iListIterator;   int iTempNode = (std::numeric_limits<int>::max)();
   /*Delete temporary edges for connectivity*/
   for(iListIterator = lListOfIsolatedNodes.begin();iListIterator != lListOfIsolatedNodes.end();iListIterator++){
      pANodeNetData->DelEdge(iTempNode, (*iListIterator));
   }
   pANodeNetData->DelNode(iTempNode);
}
template <class _TNodeNet>
void TLayoutForceDirected<_TNodeNet>::ApplyLayout(TPt<_TNodeNet> pANodeNetData){
   if(pANodeNetData->bLayoutFlag)
      return;

   if(pANodeNetData->GetNodes() == 1){
      /*A single node is a special case*/
      (pANodeNetData->BegNI())().SetX(0.5);
      (pANodeNetData->BegNI())().SetY(0.5);
      return;
   }

   THash<TInt,T2DVector> hashDisplacementData;
   THash<TInt,T2DVector> hashNewPositions;
   T2DVector t2dvDistance;
   T2DVector t2dvDumbInitializer;
   double dVectorNorm;
   T2DVector& t2dvDisplacementData = t2dvDumbInitializer;
   T2DVector t2dvNewPositionN1;
   T2DVector t2dvNewPositionN2;
   double fRepulsiveForce;   
   double fAttractiveForce;

   /*Solve isolated nodes problem*/
   this->ConnectIsolatedNodes(pANodeNetData);

   /*Initialize vector of new positions with initial positions*/
   for (typename _TNodeNet::TNodeI NI = pANodeNetData->BegNI(); NI < pANodeNetData->EndNI(); NI++) {
      hashNewPositions.AddDat(NI.GetId(),T2DVector(NI().GetX(),NI().GetX()));
   }

   /*We assume normalized space: Width*Height = 1.0*/
   double k = 0.30*sqrt(1.0/(double)pANodeNetData->GetNodes());
   double t = 0.20;

   while(t > 0){
      for (typename _TNodeNet::TNodeI NI1 = pANodeNetData->BegNI(); NI1 < pANodeNetData->EndNI(); NI1++) {
         if(hashDisplacementData.IsKey(NI1.GetId())){
            hashDisplacementData.DelKey(NI1.GetId());            
         }
         hashDisplacementData.AddDat(NI1.GetId(),T2DVector());
         for (typename _TNodeNet::TNodeI NI2 = pANodeNetData->BegNI(); NI2 < pANodeNetData->EndNI(); NI2++) {
            if(NI1 != NI2){
               /*Recover positions*/
               t2dvNewPositionN1 = hashNewPositions.GetDat(NI1.GetId());
               t2dvNewPositionN2 = hashNewPositions.GetDat(NI2.GetId());
               /*Distance (difference) vector*/
               t2dvDistance.dX = t2dvNewPositionN1.dX - t2dvNewPositionN2.dX;
               t2dvDistance.dY = t2dvNewPositionN1.dY - t2dvNewPositionN2.dY;

               if((t2dvDistance.dX == 0) && (t2dvDistance.dY == 0))
                  t2dvDistance.dX += 0.1;

               dVectorNorm = t2dvDistance.GetNorm();
               /*Force*/
               fRepulsiveForce = this->GetRepulsiveForce(k,dVectorNorm);
               /*Displacement*/
               t2dvDisplacementData = hashDisplacementData.GetDat(NI1.GetId());
               t2dvDisplacementData.dX += ( t2dvDistance.dX / dVectorNorm ) * fRepulsiveForce;
               t2dvDisplacementData.dY += ( t2dvDistance.dY / dVectorNorm ) * fRepulsiveForce;

               hashDisplacementData.DelKey(NI1.GetId());
               hashDisplacementData.AddDat(NI1.GetId(),t2dvDisplacementData);                              
            }       
         }
      }

      for (typename _TNodeNet::TEdgeI eAEdge = pANodeNetData->BegEI(); eAEdge != pANodeNetData->EndEI(); eAEdge++) {
         /*Recover positions*/
         t2dvNewPositionN1 = hashNewPositions.GetDat(eAEdge.GetSrcNId());
         t2dvNewPositionN2 = hashNewPositions.GetDat(eAEdge.GetDstNId());
         /*Distance (diference) vector*/
         t2dvDistance.dX = t2dvNewPositionN1.dX - t2dvNewPositionN2.dX;
         t2dvDistance.dY = t2dvNewPositionN1.dY - t2dvNewPositionN2.dY;
         if((t2dvDistance.dX == 0) && (t2dvDistance.dY == 0))
            t2dvDistance.dY += 0.1;
         dVectorNorm = t2dvDistance.GetNorm();
         /*Force*/
         fAttractiveForce = this->GetAttractiveForce(k,t2dvDistance.GetNorm());

         /*Displacements*/

         t2dvDisplacementData = hashDisplacementData.GetDat(eAEdge.GetSrcNId());
         t2dvDisplacementData.dX -= ( t2dvDistance.dX / dVectorNorm ) * fAttractiveForce;
         t2dvDisplacementData.dY -= ( t2dvDistance.dY / dVectorNorm ) * fAttractiveForce;
         hashDisplacementData.DelKey(eAEdge.GetSrcNId());
         hashDisplacementData.AddDat(eAEdge.GetSrcNId(),t2dvDisplacementData);

         t2dvDisplacementData = hashDisplacementData.GetDat(eAEdge.GetDstNId());
         t2dvDisplacementData.dX += ( t2dvDistance.dX / dVectorNorm) * fAttractiveForce;
         t2dvDisplacementData.dY += ( t2dvDistance.dY / dVectorNorm) * fAttractiveForce;
         hashDisplacementData.DelKey(eAEdge.GetDstNId());
         hashDisplacementData.AddDat(eAEdge.GetDstNId(),t2dvDisplacementData);
      }

      for (typename _TNodeNet::TNodeI NI1 = pANodeNetData->BegNI(); NI1 < pANodeNetData->EndNI(); NI1++) {
         /*Recover position and displacement*/
         t2dvNewPositionN1 = hashNewPositions.GetDat(NI1.GetId());
         t2dvDisplacementData = hashDisplacementData.GetDat(NI1.GetId());
         dVectorNorm = t2dvDisplacementData.GetNorm();
         /*v.pos := v.pos + ( v.disp/ |v.disp|) * min ( |v.disp|, t );*/
         double x = NI1().GetX() + ( t2dvDisplacementData.dX / dVectorNorm ) * TUtil<double>::GetMin(dVectorNorm,t);
         double y = NI1().GetY() + ( t2dvDisplacementData.dY / dVectorNorm ) * TUtil<double>::GetMin(dVectorNorm,t);
         t2dvNewPositionN1.dX = x;
         t2dvNewPositionN1.dY = y;
         hashNewPositions.DelKey(NI1.GetId());
         hashNewPositions.AddDat(NI1.GetId(),t2dvNewPositionN1);
         NI1().SetX(x);
         NI1().SetY(y);
      }
      t -= 0.01;
   }
   pANodeNetData->NormalizeAndSetPositions();
   pANodeNetData->bLayoutFlag = true;
   DisconnectIsolatedNodes(pANodeNetData);
}
#endif
