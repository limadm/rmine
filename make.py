#!/usr/bin/env python2
# coding=utf-8

# RMine custom build system!
#  - recursive compilation of 'sources' folder
#  - #include dependency discovery
#  - no obj name collision
# author: danielm@usp.br

import re
import sys
import subprocess
import os
import os.path

DEFINES = '_CLOCKING _ONRESEARCH _SUPERGRAPHCOMPILATION DEBUG'
INCLUDE = 'src libs/arboretum/include libs/snap'

WX_CONFIG = os.getenv('WX_CONFIG', '/usr/local/bin/wx-config')

SOURCES = 'src'
OUT_DIR = 'build'

CONFIGS = {
	'gcc':{
		'cc':'g++ -c -w -Wno-deprecated -fpermissive -fno-access-control -std=c++0x `%s --cxxflags` -c {} -o {}' % (WX_CONFIG,),
		'ld':'g++ {} -o {} `%s --libs` -Llibs -larboretum -lsnap -lrt' % (WX_CONFIG,),
		'debug':{
			'cc':'-g -O0',
			'ld':'-g'
		},
		'release':{
			'cc':'-O2 -fomit-frame-pointer',
			'ld':'-s -os.static'
		}
	}
}

class Tool:
	def __init__(self, *cfgs):
		def prepend(s, v):
			return ''.join((' '+s+x for x in v.split(' ')))
		self.cc = ' '.join((x['cc'] for x in cfgs)) + prepend('-D',DEFINES) + prepend('-I',INCLUDE)
		self.ld = ' '.join((x['ld'] for x in cfgs))
	def compile(self, s, o):
		print('COMPILING '+s)
		sh(self.cc.format(s, o))
	def link(self, s, o):
		print('LINKING '+o)
		sh(self.ld.format(s, o))

def sh(cmd):
	subprocess.call(cmd, shell=True)

def old(outfile, deps):
	if not os.path.exists(outfile):
		return True
	mtime = os.stat(outfile).st_mtime
	for filename in deps:
		if os.path.exists(filename):
			if mtime < os.stat(filename).st_mtime:
				return True
	return False
	
def includes(srcdir, srcpath):
	files = []
	with open(srcpath) as srcfile:
		for line in srcfile.readlines():
			m = re.match('#include\s*(<|")(.+)(>|")', line)
			if m:
				quote = m.group(1)
				filename = m.group(2)
				abspath = os.path.join(srcdir, filename)
				relpath = os.path.join(srcpath.rsplit(os.sep,1)[0], filename)
				h1,h2 = abspath,relpath
				if quote == '"':
					h2,h1 = h1,h2
				if os.path.exists(h1):
					files.append(h1)
				elif os.path.exists(h2):
					files.append(h2)
	return files

deps_cache = {}
def deps(srcdir, filename):
	if filename not in deps_cache:
		files = [filename]
		headers = [f for f in includes(srcdir, filename) if f not in files]
		files += headers
		for f in headers:
			files += deps(srcdir, f)
		deps_cache[filename] = files
	return deps_cache[filename]

def compile(tool, srcdir, srcfile, objfile):
	if old(objfile, deps(srcdir, srcfile)):
		tool.compile(srcfile, objfile)

def link(tool, outdir, outfile, objs):
	o = os.path.join(outdir, '*.o')
	if old(outfile, objs):
		tool.link(o, outfile)

def build(srcdir, outdir, tool):
	def ext(filename):
		return filename.rsplit('.', 1)[-1].lower()

	def out(filepath):
		return os.path.join(outdir, filepath.replace(srcdir+os.sep, '', 1).replace(os.sep, '_')) + '.o'

	if not os.path.exists(outdir):
		os.makedirs(outdir)

	objs = []
	for root,dirs,files in os.walk(srcdir):
		for filename in files:
			if ext(filename) in ('cc','cpp','cxx'):
				srcpath = os.path.join(root, filename)
				objfile = out(srcpath)
				objs.append(objfile)
				compile(tool, srcdir, srcpath, objfile)

	trash = []
	for root,dirs,files in os.walk(outdir):
		for filename in files:
			if ext(filename) == 'o':
				objfile = os.path.join(root, filename)
				if not objfile in objs:
					trash.append(objfile)
	for filename in trash:
		os.remove(filename)

	link(tool, outdir, 'rmine', objs)

def usage():
	print('RMine build tool!')
	print('Usage: ' + sys.argv[0] + ' clean') 
	print('Usage: ' + sys.argv[0] + ' <chain> <config>') 
	print('       chain: ' + ' '.join(CONFIGS.keys()))
	print('       config: debug release')
	
if __name__ == '__main__':
	if '--help' in sys.argv or '-h' in sys.argv:
		usage()
	elif 'clean' in sys.argv:
		sh('rm -rf ' + OUT_DIR)
	else:
		a = sys.argv + [None,None]
		chain = a[1] or 'gcc'
		config = a[2] or 'debug'
		if not chain in CONFIGS:
			usage()
		elif not config in CONFIGS[chain]:
			usage()
		else:
			tool = Tool(CONFIGS[chain], CONFIGS[chain][config])
			outdir = OUT_DIR + os.sep + chain + '-' + config
			build(SOURCES, outdir, tool)
	print('Build complete.')
