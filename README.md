RMine relational graph mining tool
==================================

RMine is an improvement over the GMine graph mining
tool, developed between 2011 and 2013 in my MSc course (Daniel).
It is based on the [wxWidgets toolkit](https://www.wxwidgets.org/),
the [Stanford Network Analysis Platform](https://snap.stanford.edu/snap/index.html),
the [METIS partitioning tool](http://glaros.dtc.umn.edu/gkhome/metis/metis/overview)
and the [GNUplot graphing utility](http://www.gnuplot.info/).

This repository is the legacy version (which works but lacks more
documentation and testing). A new one is under development, I plan
to expose a Lua API and clean up the UI code (moving to wxLua).

Releases
--------

Binaries are available for 32 and 64-bit Windows:

- [RMine-32.7z (1.2MB)](https://bitbucket.org/limadm/rmine/downloads/RMine-32.7z).
- [RMine-64.7z (1.4MB)](https://bitbucket.org/limadm/rmine/downloads/RMine-64.7z).

You may need to install the appropriate [Visual C++ 2013 Runtime](https://www.microsoft.com/en-us/download/details.aspx?id=40784).

Dataset
-------

The USP Tycho dataset used in the paper is available separately at
[TychoUSP-en.7z (77.0MB)](https://bitbucket.org/limadm/rmine/downloads/TychoUSP-en.7z).

It contains academic data from the University of São Paulo community up to 2011,
translated to English. The file format is a binary `.gtree` (Graph-Tree), which is generated
by RMine when loading a graph partitioned in METIS (`_nf.txt` and `_ef.txt` files), and
supernode labels (`snf.txt` file). See the `SyntheticSemantic` samples for further details
on the file formats.

How to Build
------------

**Linux/BSD/OSX:**

You will need to download and build [wxWidgets 2.8.12](https://www.wxwidgets.org/downloads/).
Then `cd ~/rmine && make`.  
NOTE: If you install wxWidgets in a location different than `/usr/local`, export `WX_CONFIG` with its location and make: e.g. `cd ~/rmine && env WX_CONFIG=~/wx/gtk/wx-config make`.

**Windows:**

You will need Visual Studio (tested on 2013) and the wxWidgets 2.8.12.
The [download section](https://bitbucket.org/limadm/rmine/downloads)
contains wxWidgets binaries, which you can unzip to `libs\wx\lib\`.  
Then open `rmine.sln`, choose desired configuration (Debug or Release, Win32 or x64) and click BUILD > Build Solution.

-----

Authors: {danielm, junio, agma}@icmc.usp.br

Forked from the original GMine software,
by {junio, htong, christos, agma, jure}@cs.cmu.edu

This is free software under the GBDI-ICMC-USP Software License v1.0
(see LICENSE.txt)
